FROM ghcr.io/graalvm/graalvm-ce:ol7-java11-22.3.0
VOLUME /tmp

ARG JAR_FILE
ARG KEY_FILE
ARG KEYCONF_FILE

COPY ${JAR_FILE} /app.jar
RUN mkdir -m 770 -p /tmp/ctt

COPY ${KEY_FILE} /tmp/ctt/keystore.jks
COPY ${KEYCONF_FILE} /tmp/ctt/override.properties

ENTRYPOINT ["java","-Dconf.dir=/tmp/ctt","-Djdk.xml.xpathExprGrpLimit=0","-Djdk.xml.xpathExprOpLimit=0","-Djdk.xml.xpathTotalOpLimit=0","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar","--spring.config.location=classpath:application.properties,file:/tmp/ctt/override.properties"]
