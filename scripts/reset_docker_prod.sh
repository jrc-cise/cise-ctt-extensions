#!/bin/bash
docker stop gitb-ui cise-ctt-extensions gitb-redis gitb-mysql gitb-srv
docker rm -f gitb-ui cise-ctt-extensions gitb-redis gitb-mysql gitb-srv
docker-compose -f docker-compose-prod.yml down --volumes --rmi all
docker rmi cise-ctt-extensions:3.2
docker rmi isaitb/gitb-ui:latest
docker rmi isaitb/gitb-srv:latest
docker rmi isaitb/gitb-mysql:latest
docker rmi redis:6.2.7
docker volume rm ctt_gitb-repo ctt_gitb-dbdata ctt_gitb-extensions
# Prunes all unused volumes on the system
docker volume prune -f
echo "Docker volumes:"
docker volume ls
echo "Docker networks:"
docker network ls