#!/bin/bash

echo "Stopping ctt if it is running"
docker-compose -f docker-compose-prod.yml down

echo "Building newest ctt image"
./build_image.sh

echo "Start all containers"

docker-compose -f docker-compose-prod.yml -p ctt up -d