#!/bin/bash

# Run another command (compact format)
artifact="$(find ./** -type f -name 'cise-ctt-extensions.jar')"

echo "jar file in : ${artifact}"
docker rmi cise-ctt-extensions:3.2 -f
docker image build -t cise-ctt-extensions:3.2 . --build-arg KEY_FILE="data/config/keystore.jks" \
--build-arg KEYCONF_FILE="data/config/signature.properties" \
--build-arg JAR_FILE="cise-ctt-extensions.jar"