package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #78 FORMAT Vessel MMSI Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `MMSI` is defined
 * `*THEN*` attribute `MMSI` value must be composed of up to 9 numerical characters\n\
 * `*AND*` attribute `MMSI` should start with a known 3 characters MID code (Maritime Identification Digits)  .
 */
public class TestStep78FormatVesselMmsi extends CiseTestStep {

    public TestStep78FormatVesselMmsi(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_78_FORMAT_VESSEL_MMSI;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }


    private boolean errorValid(Entity entity) {
        if (entity instanceof Vessel) {
            Long mmsi = ((Vessel) entity).getMMSI();
            if (mmsi == null)
                return true;
            else {
                if (mmsi.toString().length() == 9) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean warningValid(Entity entity) {
        if (entity instanceof Vessel) {
            Long mmsi = ((Vessel) entity).getMMSI();
            if (mmsi == null)
                return true;
            else {
                if (mmsi.toString().length() == 9) {
                    CharSequence mid;
                    MidCodeEnum midCode = MidCodeEnum.getLitteralForMidCode(mmsi.toString().substring(0, 3));
                    return (midCode != null);
                }
            }
        }
        return false;
    }


    /**
     * Captured from
     * https://www.itu.int/en/ITU-R/terrestrial/fmd/Pages/mid.aspx
     * on 06/05/2020
     **/
    private enum MidCodeEnum {
        R201("201", "Albania (Republic of)"),
        R202("202", "Andorra (Principality of)"),
        R203("203", "Austria"),
        R204("204", "Portugal - Azores"),
        R205("205", "Belgium"),
        R206("206", "Belarus (Republic of)"),
        R207("207", "Bulgaria (Republic of)"),
        R208("208", "Vatican City State"),
        R209("209", "Cyprus (Republic of)"),
        R210("210", "Cyprus (Republic of)"),
        R211("211", "Germany (Federal Republic of)"),
        R212("212", "Cyprus (Republic of)"),
        R213("213", "Georgia"),
        R214("214", "Moldova (Republic of)"),
        R215("215", "Malta"),
        R216("216", "Armenia (Republic of)"),
        R218("218", "Germany (Federal Republic of)"),
        R219("219", "Denmark"),
        R220("220", "Denmark"),
        R224("224", "Spain"),
        R225("225", "Spain"),
        R226("226", "France"),
        R227("227", "France"),
        R228("228", "France"),
        R229("229", "Malta"),
        R230("230", "Finland"),
        R231("231", "Denmark - Faroe Islands"),
        R232("232", "United Kingdom of Great Britain and Northern Ireland"),
        R233("233", "United Kingdom of Great Britain and Northern Ireland"),
        R234("234", "United Kingdom of Great Britain and Northern Ireland"),
        R235("235", "United Kingdom of Great Britain and Northern Ireland"),
        R236("236", "United Kingdom of Great Britain and Northern Ireland - Gibraltar"),
        R237("237", "Greece"),
        R238("238", "Croatia (Republic of)"),
        R239("239", "Greece"),
        R240("240", "Greece"),
        R241("241", "Greece"),
        R242("242", "Morocco (Kingdom of)"),
        R243("243", "Hungary"),
        R244("244", "Netherlands (Kingdom of the)"),
        R245("245", "Netherlands (Kingdom of the)"),
        R246("246", "Netherlands (Kingdom of the)"),
        R247("247", "Italy"),
        R248("248", "Malta"),
        R249("249", "Malta"),
        R250("250", "Ireland"),
        R251("251", "Iceland"),
        R252("252", "Liechtenstein (Principality of)"),
        R253("253", "Luxembourg"),
        R254("254", "Monaco (Principality of)"),
        R255("255", "Portugal - Madeira"),
        R256("256", "Malta"),
        R257("257", "Norway"),
        R258("258", "Norway"),
        R259("259", "Norway"),
        R261("261", "Poland (Republic of)"),
        R262("262", "Montenegro"),
        R263("263", "Portugal"),
        R264("264", "Romania"),
        R265("265", "Sweden"),
        R266("266", "Sweden"),
        R267("267", "Slovak Republic"),
        R268("268", "San Marino (Republic of)"),
        R269("269", "Switzerland (Confederation of)"),
        R270("270", "Czech Republic"),
        R271("271", "Turkey"),
        R272("272", "Ukraine"),
        R273("273", "Russian Federation"),
        R274("274", "North Macedonia (Republic of)"),
        R275("275", "Latvia (Republic of)"),
        R276("276", "Estonia (Republic of)"),
        R277("277", "Lithuania (Republic of)"),
        R278("278", "Slovenia (Republic of)"),
        R279("279", "Serbia (Republic of)"),
        R301("301", "United Kingdom of Great Britain and Northern Ireland - Anguilla"),
        R303("303", "United States of America - Alaska (State of)"),
        R304("304", "Antigua and Barbuda"),
        R305("305", "Antigua and Barbuda"),
        R306("306", "Netherlands (Kingdom of the) - Bonaire, Sint Eustatius and Saba"),
        R307("307", "Netherlands (Kingdom of the) - Aruba"),
        R308("308", "Bahamas (Commonwealth of the)"),
        R309("309", "Bahamas (Commonwealth of the)"),
        R310("310", "United Kingdom of Great Britain and Northern Ireland - Bermuda"),
        R311("311", "Bahamas (Commonwealth of the)"),
        R312("312", "Belize"),
        R314("314", "Barbados"),
        R316("316", "Canada"),
        R319("319", "United Kingdom of Great Britain and Northern Ireland - Cayman Islands"),
        R321("321", "Costa Rica"),
        R323("323", "Cuba"),
        R325("325", "Dominica (Commonwealth of)"),
        R327("327", "Dominican Republic"),
        R329("329", "France - Guadeloupe (French Department of)"),
        R330("330", "Grenada"),
        R331("331", "Denmark - Greenland"),
        R332("332", "Guatemala (Republic of)"),
        R334("334", "Honduras (Republic of)"),
        R336("336", "Haiti (Republic of)"),
        R338("338", "United States of America"),
        R339("339", "Jamaica"),
        R341("341", "Saint Kitts and Nevis (Federation of)"),
        R343("343", "Saint Lucia"),
        R345("345", "Mexico"),
        R347("347", "France - Martinique (French Department of)"),
        R348("348", "United Kingdom of Great Britain and Northern Ireland - Montserrat"),
        R350("350", "Nicaragua"),
        R351("351", "Panama (Republic of)"),
        R352("352", "Panama (Republic of)"),
        R353("353", "Panama (Republic of)"),
        R354("354", "Panama (Republic of)"),
        R355("355", "Panama (Republic of)"),
        R356("356", "Panama (Republic of)"),
        R357("357", "Panama (Republic of)"),
        R358("358", "United States of America - Puerto Rico"),
        R359("359", "El Salvador (Republic of)"),
        R361("361", "France - Saint Pierre and Miquelon (Territorial Collectivity of)"),
        R362("362", "Trinidad and Tobago"),
        R364("364", "United Kingdom of Great Britain and Northern Ireland - Turks and Caicos Islands"),
        R366("366", "United States of America"),
        R367("367", "United States of America"),
        R368("368", "United States of America"),
        R369("369", "United States of America"),
        R370("370", "Panama (Republic of)"),
        R371("371", "Panama (Republic of)"),
        R372("372", "Panama (Republic of)"),
        R373("373", "Panama (Republic of)"),
        R374("374", "Panama (Republic of)"),
        R375("375", "Saint Vincent and the Grenadines"),
        R376("376", "Saint Vincent and the Grenadines"),
        R377("377", "Saint Vincent and the Grenadines"),
        R378("378", "United Kingdom of Great Britain and Northern Ireland - British Virgin Islands"),
        R379("379", "United States of America - United States Virgin Islands"),
        R401("401", "Afghanistan"),
        R403("403", "Saudi Arabia (Kingdom of)"),
        R405("405", "Bangladesh (People's Republic of)"),
        R408("408", "Bahrain (Kingdom of)"),
        R410("410", "Bhutan (Kingdom of)"),
        R412("412", "China (People's Republic of)"),
        R413("413", "China (People's Republic of)"),
        R414("414", "China (People's Republic of)"),
        R416("416", "China (People's Republic of) - Taiwan (Province of China)"),
        R417("417", "Sri Lanka (Democratic Socialist Republic of)"),
        R419("419", "India (Republic of)"),
        R422("422", "Iran (Islamic Republic of)"),
        R423("423", "Azerbaijan (Republic of)"),
        R425("425", "Iraq (Republic of)"),
        R428("428", "Israel (State of)"),
        R431("431", "Japan"),
        R432("432", "Japan"),
        R434("434", "Turkmenistan"),
        R436("436", "Kazakhstan (Republic of)"),
        R437("437", "Uzbekistan (Republic of)"),
        R438("438", "Jordan (Hashemite Kingdom of)"),
        R440("440", "Korea (Republic of)"),
        R441("441", "Korea (Republic of)"),
        R443("443", "State of Palestine (In accordance with Resolution 99 Rev. Dubai, 2018)"),
        R445("445", "Democratic People's Republic of Korea"),
        R447("447", "Kuwait (State of)"),
        R450("450", "Lebanon"),
        R451("451", "Kyrgyz Republic"),
        R453("453", "China (People's Republic of) - Macao (Special Administrative Region of China)"),
        R455("455", "Maldives (Republic of)"),
        R457("457", "Mongolia"),
        R459("459", "Nepal (Federal Democratic Republic of)"),
        R461("461", "Oman (Sultanate of)"),
        R463("463", "Pakistan (Islamic Republic of)"),
        R466("466", "Qatar (State of)"),
        R468("468", "Syrian Arab Republic"),
        R470("470", "United Arab Emirates"),
        R471("471", "United Arab Emirates"),
        R472("472", "Tajikistan (Republic of)"),
        R473("473", "Yemen (Republic of)"),
        R475("475", "Yemen (Republic of)"),
        R477("477", "China (People's Republic of) - Hong Kong (Special Administrative Region of China)"),
        R478("478", "Bosnia and Herzegovina"),
        R501("501", "France - Adelie Land"),
        R503("503", "Australia"),
        R506("506", "Myanmar (Union of)"),
        R508("508", "Brunei Darussalam"),
        R510("510", "Micronesia (Federated States of)"),
        R511("511", "Palau (Republic of)"),
        R512("512", "New Zealand"),
        R514("514", "Cambodia (Kingdom of)"),
        R515("515", "Cambodia (Kingdom of)"),
        R516("516", "Australia - Christmas Island (Indian Ocean)"),
        R518("518", "New Zealand - Cook Islands"),
        R520("520", "Fiji (Republic of)"),
        R523("523", "Australia - Cocos (Keeling) Islands"),
        R525("525", "Indonesia (Republic of)"),
        R529("529", "Kiribati (Republic of)"),
        R531("531", "Lao People's Democratic Republic"),
        R533("533", "Malaysia"),
        R536("536", "United States of America - Northern Mariana Islands (Commonwealth of the)"),
        R538("538", "Marshall Islands (Republic of the)"),
        R540("540", "France - New Caledonia"),
        R542("542", "New Zealand - Niue"),
        R544("544", "Nauru (Republic of)"),
        R546("546", "France - French Polynesia"),
        R548("548", "Philippines (Republic of the)"),
        R550("550", "Timor-Leste (Democratic Republic of)"),
        R553("553", "Papua New Guinea"),
        R555("555", "United Kingdom of Great Britain and Northern Ireland - Pitcairn Island"),
        R557("557", "Solomon Islands"),
        R559("559", "United States of America - American Samoa"),
        R561("561", "Samoa (Independent State of)"),
        R563("563", "Singapore (Republic of)"),
        R564("564", "Singapore (Republic of)"),
        R565("565", "Singapore (Republic of)"),
        R566("566", "Singapore (Republic of)"),
        R567("567", "Thailand"),
        R570("570", "Tonga (Kingdom of)"),
        R572("572", "Tuvalu"),
        R574("574", "Viet Nam (Socialist Republic of)"),
        R576("576", "Vanuatu (Republic of)"),
        R577("577", "Vanuatu (Republic of)"),
        R578("578", "France - Wallis and Futuna Islands"),
        R601("601", "South Africa (Republic of)"),
        R603("603", "Angola (Republic of)"),
        R605("605", "Algeria (People's Democratic Republic of)"),
        R607("607", "France - Saint Paul and Amsterdam Islands"),
        R608("608", "United Kingdom of Great Britain and Northern Ireland - Ascension Island"),
        R609("609", "Burundi (Republic of)"),
        R610("610", "Benin (Republic of)"),
        R611("611", "Botswana (Republic of)"),
        R612("612", "Central African Republic"),
        R613("613", "Cameroon (Republic of)"),
        R615("615", "Congo (Republic of the)"),
        R616("616", "Comoros (Union of the)"),
        R617("617", "Cabo Verde (Republic of)"),
        R618("618", "France - Crozet Archipelago"),
        R619("619", "Côte d'Ivoire (Republic of)"),
        R620("620", "Comoros (Union of the)"),
        R621("621", "Djibouti (Republic of)"),
        R622("622", "Egypt (Arab Republic of)"),
        R624("624", "Ethiopia (Federal Democratic Republic of)"),
        R625("625", "Eritrea"),
        R626("626", "Gabonese Republic"),
        R627("627", "Ghana"),
        R629("629", "Gambia (Republic of the)"),
        R630("630", "Guinea-Bissau (Republic of)"),
        R631("631", "Equatorial Guinea (Republic of)"),
        R632("632", "Guinea (Republic of)"),
        R633("633", "Burkina Faso"),
        R634("634", "Kenya (Republic of)"),
        R635("635", "France - Kerguelen Islands"),
        R636("636", "Liberia (Republic of)"),
        R637("637", "Liberia (Republic of)"),
        R638("638", "South Sudan (Republic of)"),
        R642("642", "Libya (State of)"),
        R644("644", "Lesotho (Kingdom of)"),
        R645("645", "Mauritius (Republic of)"),
        R647("647", "Madagascar (Republic of)"),
        R649("649", "Mali (Republic of)"),
        R650("650", "Mozambique (Republic of)"),
        R654("654", "Mauritania (Islamic Republic of)"),
        R655("655", "Malawi"),
        R656("656", "Niger (Republic of the)"),
        R657("657", "Nigeria (Federal Republic of)"),
        R659("659", "Namibia (Republic of)"),
        R660("660", "France - Reunion (French Department of)"),
        R661("661", "Rwanda (Republic of)"),
        R662("662", "Sudan (Republic of the)"),
        R663("663", "Senegal (Republic of)"),
        R664("664", "Seychelles (Republic of)"),
        R665("665", "United Kingdom of Great Britain and Northern Ireland - Saint Helena"),
        R666("666", "Somalia (Federal Republic of)"),
        R667("667", "Sierra Leone"),
        R668("668", "Sao Tome and Principe (Democratic Republic of)"),
        R669("669", "Eswatini (Kingdom of)"),
        R670("670", "Chad (Republic of)"),
        R671("671", "Togolese Republic"),
        R672("672", "Tunisia"),
        R674("674", "Tanzania (United Republic of)"),
        R675("675", "Uganda (Republic of)"),
        R676("676", "Democratic Republic of the Congo"),
        R677("677", "Tanzania (United Republic of)"),
        R678("678", "Zambia (Republic of)"),
        R679("679", "Zimbabwe (Republic of)"),
        R701("701", "Argentine Republic"),
        R710("710", "Brazil (Federative Republic of)"),
        R720("720", "Bolivia (Plurinational State of)"),
        R725("725", "Chile"),
        R730("730", "Colombia (Republic of)"),
        R735("735", "Ecuador"),
        R740("740", "United Kingdom of Great Britain and Northern Ireland - Falkland Islands (Malvinas)"),
        R745("745", "France - Guiana (French Department of)"),
        R750("750", "Guyana"),
        R755("755", "Paraguay (Republic of)"),
        R760("760", "Peru"),
        R765("765", "Suriname (Republic of)"),
        R770("770", "Uruguay (Eastern Republic of)"),
        R775("775", "Venezuela (Bolivarian Republic of)");

        private String midCode;
        private String litteral;

        MidCodeEnum(String midCode, String litteral) {
            this.midCode = midCode;
            this.litteral = litteral;
        }

        public static MidCodeEnum getLitteralForMidCode(String midCode) {
            for (MidCodeEnum value : values()) {
                if (value.getMidCode().equals(midCode)) {
                    return value;
                }
            }
            return null;
        }

        public String getMidCode() {
            return midCode;
        }
    }
}
