package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.ArrayList;
import java.util.List;

public abstract class CiseTestStepAttachedDocument extends CiseTestStep {

    public CiseTestStepAttachedDocument(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    /**
     * In this function we assume that the validAttachedDocumentTypes is a list subclasses of Documents.
     *
     * @param messageXml                 input message
     * @param classesToFind              the Entities to find: from these elements we search backward and forward to verify relations
     * @param validAttachedDocumentTypes classes of Documents to search for
     * @param testStepRef                code to reference the specific test step
     * @return                           returns the report
     */
    protected TAR createReport(String messageXml, Class[] classesToFind, Class[] validAttachedDocumentTypes, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);

        try {
            Pair<String, Boolean> testResult = performTest(messageXml, classesToFind, validAttachedDocumentTypes, testStepRef);
            boolean conform = testResult.getB();
            String failureExplanation = testResult.getA();
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }


    public Pair<String, Boolean> performTest(String messageXml, Class[] classesToFind, Class[] validAttachedDocumentTypes, CiseTestStepsEnum testStepRef) {
        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        boolean conform = true;
        for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
            Entity entity = entityPair.getA();
            String breadcrumb = entityPair.getB();
            if (!hasAttachedDocumentOfValidType(entity, validAttachedDocumentTypes)
            ) {
                conform = false;
                failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), entity);
            }
        }
        return new Pair<>(failureExplanation, conform);
    }

    protected abstract boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes);


}
