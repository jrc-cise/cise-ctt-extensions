package eu.europa.ec.itb.cise.ws.search;

import eu.europa.ec.itb.cise.ws.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Tree
 */
public class Tree {

    private final Logger logger = LoggerFactory.getLogger(TemplateMatcher.class);

    private final Node root;
    private final int maxDepth;

    public Tree(Node root) {
        this.root = root;
        this.maxDepth = Integer.MAX_VALUE;
    }

    public Tree(Node root, int maxDepth) {
        this.root = root;
        this.maxDepth = maxDepth;
    }

    public static Tree build(Object o) {
        return new Tree(new TreeBuilder().build(o));
    }

    public static Tree build(Object o, int maxDepth) {
        return new Tree(new TreeBuilder().build(o), maxDepth);
    }

    public List<String> buildNodePaths() {
        return traverseTree(root, new ArrayList<>(), 1);
    }

    public List<Pair<String, Object>> buildNodePathsWithObject() {
        return traverseTreeWithObjects(root, new ArrayList<>(), 1);
    }

    public boolean exactMatch(Tree destination) {
        logger.debug("\n- checking the tree input paths     {}" +
                        "\n- are contained in the output paths {}",
                buildNodePaths(), destination.buildNodePaths());

        return destination.buildNodePaths().equals(buildNodePaths());
    }

    public boolean bestMatch(Tree destination) {
        logger.debug("\n- checking the tree input paths     {}" +
                        "\n- are contained in the output paths {}",
                buildNodePaths(), destination.buildNodePaths());

        return destination.buildNodePaths().containsAll(buildNodePaths());
    }

    private List<String> traverseTree(Node node, List<String> items, int depth) {
        items.add(node.getComputedName());

        if (!(node.getChildren().isEmpty() || depth >= maxDepth)) {
            node.getChildren().forEach(n -> traverseTree(n, items, depth + 1));
        }

        return items;
    }

    public List<Pair<String, Object>> traverseTreeWithObjects(Node node, List<Pair<String, Object>> items, int depth) {
        items.add(new Pair(node.getComputedName(), node));

        if (!(node.getChildren().isEmpty() || depth >= maxDepth)) {
            node.getChildren().forEach(n -> traverseTreeWithObjects(n, items, depth + 1));
        }

        return items;
    }

    public List<Node> searchOccurrences(Tree needleTree) {
        List<Node> result = new ArrayList<>();
        List<String> needlePaths = needleTree.buildNodePaths();

        String lastNeedlePath = needlePaths.get(needlePaths.size() - 1);

        for (Pair<String, Object> nodePath : buildNodePathsWithObject()) {
            if (nodePath.getA().endsWith(lastNeedlePath)) {
                result.add((Node) nodePath.getB());
            }
        }

        return result;
    }

    public Node searchBackwardOccurrenceOfCiseObject(Class ciseObjectClass, Integer startingNodeId) {
        Node startingNode = traverseTreeTofindNodeWithId(startingNodeId, root);

        Node node = startingNode;
        int depth = 0;
        while (node != null && node.parent != null && depth < 3) {
            node = node.parent;
            if (ciseObjectClass.isInstance(node.getCiseObject())) {
                // debug mode : //System.out.println (node.getComputedName()  +"at Depth "+depth+" match "+ciseObjectClass.getCanonicalName());
                return node;
            }
            depth++;
        }

        return null;
    }

    public Node searchDirectBackwardOccurrenceOfCiseObject(Class ciseObjectClass, Integer startingNodeId) {
        Node startingNode = traverseTreeTofindNodeWithId(startingNodeId, root);

        Node node = startingNode;
        int depth = 0;
        while (node != null && node.parent != null && depth <= 3) {
            node = node.parent;
            if (ciseObjectClass.isInstance(node.getCiseObject())) {
                // debug mode : //System.out.println (node.getComputedName() +"at Depth "+depth+" match "+ ciseObjectClass.getCanonicalName());
                return node;
            }
            depth++;
        }
        return null;
    }

    private Node traverseTreeTofindNodeWithId(Integer nodeIdToFind, Node node) {
        if (node.getId() == nodeIdToFind) {
            return node;
        } else {
            for (Node child : node.getChildren()) {
                Node returnNode = traverseTreeTofindNodeWithId(nodeIdToFind, child);
                if (returnNode != null) {
                    return returnNode;
                }
            }
        }
        return null;
    }

}