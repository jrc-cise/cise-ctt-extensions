package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEntityAll;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #58 Movement Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Movement` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` must define at least one relation to entity(ies) `Location` entity (+ its children entities) or `Object` (+ its children entities)
 */
public class TestStep58MovementMinimumDefinition extends CiseTestStepLinkedEntityAll {

    public TestStep58MovementMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Movement.class;
        Class[] linkedClass = new Class[]{Objet.class, Location.class};

        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_58_MOVEMENT_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }

}