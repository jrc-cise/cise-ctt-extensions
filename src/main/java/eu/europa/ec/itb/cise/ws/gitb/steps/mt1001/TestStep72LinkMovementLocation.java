package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #72 LinkMovementLocation
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Movement` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Location` entity(ies)
 * `*THEN*` its *attribute* `EventArea` _must not be defined_
 * <p>
 * examples:
 */
public class TestStep72LinkMovementLocation extends CiseTestStepRelationship {

    public TestStep72LinkMovementLocation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Movement.class};
        Class[] objectClasses = new Class[]{Location.class, NamedLocation.class, PortLocation.class, PortFacilityLocation.class};
        Class[] relationClasses = new Class[]{Event.LocationRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_72_LINK_MOVEMENT_LOCATION);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        return (relationship instanceof Event.LocationRel && ((Event.LocationRel) relationship).getEventArea() == null);
    }


}
