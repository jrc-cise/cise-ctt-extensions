package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.util.MessageWrapper;

import java.util.List;

/**
 * #55 IMONumber corresponds to the request
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*Background*`: a `VESSEL` *entity* _is defined by_ *_defined and valued attribute_* `IMONumber` in the SUT Information System
 * `*And*`: it _is linked to_ an `Risk`.
 * `*Given*`: `VESSEL` *entity*  _is present_ under a `Risk` *main entity* _in_ `Pull.Request`  message payload.
 * `*When*`: it _is represented by_  *_defined and valued attribute_* `IMONumber`.
 * `*Then*`: must define at least one "VESSEL" in corresponding `Pull.Response` payload
 * `*And*`: it must be defined with  *_defined and valued attribute_* `IMONumber`.
 */
public class TestStep55CheckRiskVesselIMONumber extends CiseTestStep {

    public TestStep55CheckRiskVesselIMONumber(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml, String expectedIMONumber) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_55_CHECK_RISK_VESSEL_IMO_NUMBER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Object> payloadMainEntities = payloadHelper.getPayloadObjects(messageXml);
        boolean conform = true;
        for (Object entityObject : payloadMainEntities) {
            String entityMessageUniqueMainEntityXml = reportBuilder.getXmlMapper().toXML(MessageWrapper.createFakePushMsg(entityObject));
            List<Pair<Entity, String>> payloadSubEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(entityMessageUniqueMainEntityXml, Vessel.class);
            for (Pair<Entity, String> entityPair2 : payloadSubEntitiesOfType) {
                if (!isIMONumberCorrect(expectedIMONumber, payloadSubEntitiesOfType, (Vessel) entityPair2.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair2.getB()), entityPair2.getA());
                }
            }
        }
        report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
        report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));

        return report;
    }

    private boolean isIMONumberCorrect(String expectedIMONumber, List<Pair<Entity, String>> payloadEntitiesOfType, Vessel vessel) {
        return payloadEntitiesOfType.size() == 1 &&
                vessel.getIMONumber() != null &&
                Long.parseLong(expectedIMONumber) == (vessel).getIMONumber();
    }


}
