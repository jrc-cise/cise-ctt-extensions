package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseMetaTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum.*;

public class MetaTestStep1000EntityMinimumDefinition extends CiseMetaTestStep {

    public MetaTestStep1000EntityMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        List<CiseTestStepsEnum> steps = new ArrayList(Arrays.asList(
                REPORT_ELEMENT_MODEL_TS_10_VESSEL_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_11_PERSON_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_12_ORGANISATION_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_13_ORGANISATIONUNIT_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_14_PORTORGANIZATION_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_15_FORMAL_ORGANIZATION_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_16_AIRCRAFT_AND_LANDVEHICLE_MIN_IDENT,
                REPORT_ELEMENT_MODEL_TS_17_ANOMALY_TYPE,
                REPORT_ELEMENT_MODEL_TS_24_ATTACHED_DOCUMENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_25_IDENTIFICATION_OF_DOCUMENT_TYPE,
                REPORT_ELEMENT_MODEL_TS_28_POLLUTION_INCIDENT_TYPE,
                REPORT_ELEMENT_MODEL_TS_39_LOCATION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_40_GEOMETRY_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_41_WKT_VALIDITY,
                REPORT_ELEMENT_MODEL_TS_42_XMLGEOMETRY_VALIDITY,
                REPORT_ELEMENT_MODEL_TS_43_NAMED_LOCATION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_44_PORT_LOCATION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_45_PORT_FACILITY_LOCATION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_46_OBJECTLOCATION_PERIODOFTIME
        ));
        return createReport(messageXml, steps, CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1000_ENTITY_MIN_DEF);
    }
}
