package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.RiskDocument;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #37 AttachedDocument Coherent With Risk
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Risk` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `RiskDocument`
 */
public class TestStep37AttachedDocumentCoherentWithRisk extends CiseTestStepAttachedDocument {

    public TestStep37AttachedDocumentCoherentWithRisk(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Risk.class};
        Class[] validAttachedDocumentTypes = new Class[]{RiskDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_37_ATTACHED_DOCUMENT_COHERENT_WITH_RISK);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Risk risk = (Risk) entity;
        List<Risk.DocumentRel> documentRels = risk.getDocumentRels();
        if (documentRels == null || documentRels.size() == 0) {
            return true;
        }
        for (Risk.DocumentRel documentRel : documentRels) {
            if (!(isInstanceOfOneOf(documentRel.getDocument(), validAttachedDocumentTypes))) {
                return false;
            }
        }

        return true;
    }
}
