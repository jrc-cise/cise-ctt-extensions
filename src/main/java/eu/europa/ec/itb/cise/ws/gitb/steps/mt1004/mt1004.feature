#1004 Entities Minimal Definition Linked Entity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of type _Entity-type_ is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH`, `PULL_RESPONSE/REQUEST`, `PUSH/PULL_SUBSCRIBE`
[]
`*THEN*` it _must define at least on relation to_ Linked Entity...

.specific rule concern
[cols="5%,23%,20%,26%,26%"]
|=======================
|_N_|_Title_|_Entity_|_Attribute_|_Gherkin_
|18|UnexpectedMovement Minimum Context|`PollutionIncident`|
`*AND*` its *attribute* `AnomalyType` have value : `UNEXPECTED_MOVEMENT`|
`*THEN*` it _must define at least one_ *relation to entity(ies)* `Object` (+ subtypes) or  `Person` (+ subtypes).

|=======================

#18 UnexpectedMovement Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`*GIVEN*`  *entity(ies)* of type  `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` its *attribute* `AnomalyType` have value : `UNEXPECTED_MOVEMENT`
`*THEN*` it _must define at least one_ *relation to entity(ies)* `Object` (+ subtypes) or  `Person` (+ subtypes).

#19 CargoLeaking Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` its *attribute* `AnomalyType` have value :`CARGO_LEAKING`
`*THEN*` it must define at least one relation to an entity of type `Vessel` or  `Cargo` or  `ContainmentUnit`.
 
#20 ShiftingOfCargo Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` its *attribute* `AnomalyType` have value :`SHIFTING_OF_CARGO`
`*THEN*` it _must define at least one_ *relation to entity(ies)* `Object` (+ its children entities).
 
#21 VesselAnomaly Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` its *attribute* `AnomalyType` have value :  `VESSEL_OUT_OF_TRAFFIC_LANES` or `VESSEL_WITH_ERRATIC_MOVEMENTS` or `DETECION_OF_CHANGES_IN_AIS_PARAMETERS` or `PERFORMING_AIS_SPOOFING` or `WITHOUT_AIS_TRANSMISSION` or `DO_NOT_ANSWER_ON_VHF_CH_16`
`*THEN*` _must define at least one_ *relation to entity(ies)* `Vessel`.
 
#22 StainOfOilSighted Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` its *attribute* `AnomalyType` have value :  `STAIN_OF_OIL_SIGHTED`
`*THEN*` it _must define at least one_ *relation to entity(ies)* `Location`.

#27 PollutionIncident Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `PollutionIncident` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*` _must define at least one_ *relation to entity(ies)* `EventDocument`,`Risk`,`Object`(+ its children entities),`Agent`(+ its children entities),`Location`(+ its children entities),`Organization`(+ its children entities).
 
#29 MaritimeSafetyIncident* Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `Pollution` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` it define attribute `MaritimeSafetyIncidentType` with  predefined value in :`POLLUTION` or `WASTE` or `LOST_FOUND_CONTAINERS` or `FIRE`
`*THEN*` it _must define at least one_ *relation to entity(ies)* of type `Vessel`, `Cargo`, `Location`.
 
#30 VesselIncidents Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `incidents` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*AND*` it _define_ *attribute* `MaritimeSafetyIncidentType` 
`*AND*` it _define with predefined value in_ *attribute* `MaritimeSafetyIncidentType` :
`VTS_RULES_INFRINGEMENT` or `BANNED_SHIP` or `INSURANCE_FAILURE` or `PILOT_OR_PORT_REPORT`
or `COLLISION` or `CAPSIZING` or `ENGINE_FAILURE` or `STRUCTURAL_FAILURE` or `STEERING_GEAR_FAILURE` or `ELECTRICAL_GENERATING_SYSTEM_FAILURE` or `NAVIGATION_EQUIPMENT_FAILURE`
or `NAVIGATION_EQUIPMENT_FAILURE` or `COMMUNICATION_EQUIPMENT_FAILURE` or `INCIDENT_NATURE_ABANDON_SHIP` or `INCIDENT_NATURE_SINKING` or `DETAINED_SHIP`
`*THEN*` it _must define at least one_ *relation to entity(ies)* of type `Vehicle` (+ its children entities).
 
#31 Incident Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `incident` or  `MaritimeSafetyIncident` or  `IrregularMigrationIncident` or  `LawInfringementIncident` or  `CrisisIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*` it _must define at least one_ *relation to entity(ies)* of type `EventDocument` or `Risk` or `Vehicle`(+ its children entities) or `Location`(+ its children entities) or `Organization`(+ its children entities).

#57 Action Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `Action` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*`  _must define at least one_ *relation to entity(ies)* `Document` (+ its children entities) or  `Event` entity (+ its children entities) or `Object` (+ its children entities).

#58 Movement Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `Movement` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*` _must define at least one_ *relation to entity(ies)* `Location` entity (+ its children entities) or `Object` (+ its children entities).

#60 MeteoOceanographicCondition Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *entity(ies)* of type `MeteoOceanographicCondition` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*` _must define at least one_ *relation to entity(ies)* `Location` (+ its children entities).

#61 Risk Minimum Context
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of type `Risk` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` payload
`*THEN*` it _must define at least one_ a *relation to Entity(ies)* `Document` (+ its children entities) or
`Event` (+ its children entities) or
`Object` (+ its children entities) or
`Agent` (+ its children entities) or
`Location` (+ its children entities).