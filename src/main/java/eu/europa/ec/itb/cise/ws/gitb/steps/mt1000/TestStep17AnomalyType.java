package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 *#17 AnomalyType
 * ^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Anomaly` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define the *attribute*:`AnomalyType`
 */
public class TestStep17AnomalyType extends CiseTestStep {

    public TestStep17AnomalyType(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_17_ANOMALY_TYPE;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadHelper.getPayloadEntitiesOfType(messageXml, Anomaly.class)) {
                if (!isEntityOfCorrectType(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }


    private boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof Anomaly) {
            AnomalyType anomalyType = ((Anomaly) entity).getAnomalyType();
            if (anomalyType != null) {
                return true;
            }
        }
        return false;
    }

}
