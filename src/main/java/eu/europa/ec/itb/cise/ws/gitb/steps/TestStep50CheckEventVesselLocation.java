package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.ServiceTypeMainClassMap;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

/**
 * #50 Location corresponds to the request
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*Background*` : a `VESSEL` entity is defined by attribute `Location` in the SUT Information System
 * `*And*` : it is is linked to an `Event`.
 * `*Given*`: `Event` entity is present in Pull.Request message payload.
 * `*When*` : it is presented by defined  attribute `Location` in WKT format polygon representation.
 * `*Then*` : corresponding Pull.Response message must present at least one `VESSEL`
 * `*And*` : it must be presented with attribute `Location` represented by a latitude / longitude that correspond to the defined WKT format polygon representation.
 */
public class TestStep50CheckEventVesselLocation extends CiseTestStepValidVesselLocation {


    public TestStep50CheckEventVesselLocation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String receivedMessageXml, String sentMessageXml) {
        Class serviceClass = mainEntitySuperClass(sentMessageXml);
        TAR report = super.createReport(receivedMessageXml, sentMessageXml, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_50_CHECK_EVENT_VESSEL_LOCATION, serviceClass);
        return report;
    }

    private Class mainEntitySuperClass(String sentMessageXml) {
        ServiceTypeMainClassMap serviceTypeMainClassMap = new ServiceTypeMainClassMap();
        ServiceType senderServiceType = ((Message) reportBuilder.getXmlMapper().fromXML(sentMessageXml)).getSender().getServiceType();
        Class<? extends Entity> mainEntitySuperClass = serviceTypeMainClassMap.toEntityClass(senderServiceType);
        return (Class) mainEntitySuperClass;
    }

}
