package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #98 Boundary Vessel attributes
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in *payload*
 * []
 * -- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * []
 * -- `*AND*` *attribute* `beam` or `breadth` or `containerCapacity` or `deadweight` or `depth` or `draught` or `grossTonnage` or `length` or `lengthenedYear` or `loa` or `netTonnage`
 * or `segregatedBallastVolume` or `yearBuilt` _is  defined_
 * []
 * `*THEN*` *attribute* values  _must be in accepted range _
 * []
 * -- `beam` range min:0 ,max:100 meter(s)
 * -- `breadth` range min:1 ,max:100 meter(s)
 * -- `containerCapacity` range min:1 ,max:30000 TEU (twenty-foot equivalent unit)
 * -- `deadweight` range min: 1 ,max:300000 tonne(s)
 * -- `depth` range min: 0.99 ,max:50.0 meter(s)
 * -- `draught` range min: 0.0 ,max:50.0 meter(s)
 * -- `grossTonnage` range min: 1.0 ,max:300000.0 tonne(s)
 * -- `length` range min: 0.99 ,max:500.0 meter(s)
 * -- `lengthenedYear` range min: 0 ,max:250 year(s)
 * -- `loa` range min: 1.0 ,max:500.0 meter(s)
 * -- `netTonnage` range min: 0.0 ,max:1.0 tonne(s)
 * -- `segregatedBallastVolume` range min: 1.0 ,max:50000.0 m3
 * -- `yearBuilt` range min: 1800 ,max:2060 (date)
 */
public class TestStep98BoundaryVessel extends CiseTestStepOneOfChildValueBoundary {

    public TestStep98BoundaryVessel(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Vessel.class};
        List<String> fields = Arrays.asList(
                "beam",
                "breadth",
                "containerCapacity",
                "deadweight",
                "depth",
                "draught",
                "grossTonnage",
                "length",
                "lengthenedYear",
                "loa",
                "netTonnage",
                "segregatedBallastVolume",
                "yearBuilt");

        return super.createReport(messageXml,
                fields,
                "Objet",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_98_BOUNDARY_VESSEL
        );

    }


}
