package eu.europa.ec.itb.cise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point to bootstrap the application.
 */
@SpringBootApplication
public class Application {

    /**
     * Main bootstrap method.
     *
     * @param args Runtime args.
     */
    public static void main(String[] args) {
        String confdir = System.getProperty("conf.dir");
        if (confdir != null) System.out.println("conf dir present in " + System.getProperty("conf.dir"));
        SpringApplication.run(Application.class, args);
    }

}
