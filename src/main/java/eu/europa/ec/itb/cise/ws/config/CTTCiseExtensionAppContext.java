package eu.europa.ec.itb.cise.ws.config;

import com.gitb.ms.MessagingService;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.dispatcher.DispatcherFactory;
import eu.cise.dispatcher.DispatcherType;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.CTTMessageProcessor;
import eu.europa.ec.itb.cise.ws.domain.CTTSimEngine;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.domain.SimEngine;
import eu.europa.ec.itb.cise.ws.gitb.*;
import eu.europa.ec.itb.cise.ws.transport.IncomingMessagesServiceImpl;
import eu.europa.ec.itb.cise.ws.transport.OutgoingMessagesServiceImpl;
import eu.europa.ec.itb.cise.ws.util.ProxyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

import static eu.cise.signature.SignatureServiceBuilder.newSignatureService;

@Component
public class CTTCiseExtensionAppContext {
    private XmlMapper xmlMapper;
    private XmlMapper prettyNotValidatingXmlMapper;
    private IncomingMessagesServiceImpl incomingMessagingService;
    private MessagingService outgoingMessagingService;
    private MessageIdStore messageIdStore;
    private SessionManager sessionManager;
    private Dispatcher dispatcher;
    private SignatureService signatureService;
    private ProxyInfo proxyInfo;
    private MessageProcessor messageProcessor;
    private SimEngine cttSimEngine;
    private ReportBuilder reportBuilder;
    private ResourceBundle labels;
    private HashMap<String, Properties> boundaries;
    private CisePayloadHelper cisePayloadHelper;
    private GitbErrorHelper gitbErrorHelper;


    @Autowired
    private CTTCiseExtensionConfig cttConfig;

    @PostConstruct
    public void initialize() {
        this.xmlMapper = new DefaultXmlMapper.PrettyNotValidating();
        this.prettyNotValidatingXmlMapper = new DefaultXmlMapper.PrettyNotValidating();
        this.proxyInfo = makeProxyInfo();
        this.labels = makeLabelsResourceBundle();
        this.boundaries = makeboundariesResourceBundles();
        //GITB
        this.messageIdStore = makeMessageIdStore();
        this.reportBuilder = makeReportBuilder();
        this.sessionManager = makeSessionManager();
        this.cisePayloadHelper = makeCisePayloadHelper();
        this.gitbErrorHelper = makeGitbErrorHelper();
        // business logic objects
        this.dispatcher = makeDispatcher();
        this.signatureService = makeSignatureService();
        this.cttSimEngine = makeEmulatorEngine();
        this.messageProcessor = makeMessageProcessor();
        // services
        this.outgoingMessagingService = makeOutgoingMessagingService();
        this.incomingMessagingService = makeIncomingMessagingService();
    }

    private GitbErrorHelper makeGitbErrorHelper() {
        return new GitbErrorHelper(labels, boundaries, xmlMapper);
    }

    private CisePayloadHelper makeCisePayloadHelper() {
        return new CisePayloadHelper(xmlMapper);
    }

    private ResourceBundle makeLabelsResourceBundle() {
        return ResourceBundle.getBundle("LabelsBundle", cttConfig.getCurrentLocale());
    }

    public static HashMap<String, Properties> getboundariesResourceBundles(ClassLoader aclassloader) {
        HashMap<String, Properties> aggregatedResult = new HashMap<>();
        PathMatchingResourcePatternResolver ressourceSetResolver = new PathMatchingResourcePatternResolver(aclassloader);
        try {
            final InputStream[] inputStream = {null};
            List<Resource> resources = Arrays.asList(ressourceSetResolver.getResources("classpath*:*.properties"));
            resources.stream().forEach(s -> {
                URL url = null;
                try {
                    inputStream[0] = ((Resource) s).getInputStream();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                Properties prop = new Properties();
                if (s.getFilename().contains("Bnd_")) {
                    try {
                        prop.load(inputStream[0]);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    aggregatedResult.put(s.getFilename().substring(4), prop);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return aggregatedResult;
    }

    public HashMap<String, Properties> makeboundariesResourceBundles() {
        return getboundariesResourceBundles(this.getClass().getClassLoader());
    }

    public IncomingMessagesServiceImpl getIcomingMessagingService() {
        return incomingMessagingService;
    }

    public MessagingService getOutgoingMessagingService() {
        return outgoingMessagingService;
    }

    public MessageIdStore getMessageIdStore() {
        return messageIdStore;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

    public SignatureService getSignatureService() {
        return signatureService;
    }

    public DispatcherType getDispatcherType() {
        return cttConfig.getDispatcherType();
    }

    public XmlMapper getXmlMapper() {
        return xmlMapper;
    }

    public XmlMapper getPrettyNotValidatingXmlMapper() {
        return prettyNotValidatingXmlMapper;
    }

    public MessageIdStore makeMessageIdStore() {
        return new MessageIdStore(cttConfig.getPersistentMessageIdFilePath());
    }

    private ProxyInfo makeProxyInfo() {
        return new ProxyInfo(
                cttConfig.isProxyEnabled(),
                cttConfig.getProxyServer(),
                cttConfig.getProxyPort(),
                cttConfig.getProxyType(),
                cttConfig.isProxyAuthEnabled(),
                cttConfig.getProxyUsername(),
                cttConfig.getProxyPassword(),
                cttConfig.getProxyNonProxyHosts());
    }

    public MessagingService makeOutgoingMessagingService() {
        return new OutgoingMessagesServiceImpl(sessionManager, messageIdStore, xmlMapper, messageProcessor, reportBuilder);
    }

    private IncomingMessagesServiceImpl makeIncomingMessagingService() {
        return new IncomingMessagesServiceImpl(sessionManager, messageIdStore, messageProcessor, xmlMapper, reportBuilder);
    }

    private SessionManager makeSessionManager() {
        return new SessionManager(this.proxyInfo, reportBuilder);
    }

    private Dispatcher makeDispatcher() {
        DispatcherFactory dispatcherFactory = new DispatcherFactory();
        return dispatcherFactory.getDispatcher(cttConfig.getDispatcherType(), this.xmlMapper);
    }

    private SignatureService makeSignatureService() {
        return newSignatureService(this.xmlMapper)
                .withKeyStoreName(cttConfig.getKeyStoreFileName())
                .withKeyStorePassword(cttConfig.getKeyStorePassword())
                .withPrivateKeyAlias(cttConfig.getPrivateKeyAlias())
                .withPrivateKeyPassword(cttConfig.getPrivateKeyPassword())
                .build();
    }

    private CTTSimEngine makeEmulatorEngine() {
        return new CTTSimEngine(signatureService, dispatcher);
    }

    private MessageProcessor makeMessageProcessor() {
        return new CTTMessageProcessor(cttSimEngine);
    }


    private ReportBuilder makeReportBuilder() {
        return new ReportBuilder(xmlMapper, signatureService, messageIdStore);
    }


    public CiseValidationService getCiseValidationService() {
        return new CiseValidationService(reportBuilder, cisePayloadHelper, gitbErrorHelper);
    }
}
