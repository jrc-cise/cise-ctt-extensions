package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInLocationType;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.organization.OrganizationalCollaboration;
import eu.cise.datamodel.v1.entity.organization.OrganizationalUnit;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #73 Link Movement Location
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Location` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in
 * `COUNTRY_OF_BIRTH`,
 * `PLACE_OF_BIRTH`,
 * `COUNTRY_OF_DEATH`,
 * `PLACE_OF_DEATH`,
 * `EMBARKATION_PORT`,
 * `DISEMBARKATION_PORT`,
 * `COUNTRY_OF_RESIDENCE`
 */
public class TestStep73LinkOrganizationLocation extends CiseTestStepRelationship {

    public TestStep73LinkOrganizationLocation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Organization.class, OrganizationalCollaboration.class, OrganizationalUnit.class, PortOrganization.class};
        Class[] objectClasses = new Class[]{Location.class, NamedLocation.class, PortLocation.class, PortFacilityLocation.class};
        Class[] relationClasses = new Class[]{Agent.LocationRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_73_LINK_ORGANIZATION_LOCATION);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        locationAgentRole = ((Agent.LocationRel) relationship).getAgentRole();
        return (relationship instanceof Agent.LocationRel &&
                (locationAgentRole != AgentRoleInLocationType.COUNTRY_OF_BIRTH) &&
                (locationAgentRole != AgentRoleInLocationType.PLACE_OF_BIRTH) &&
                (locationAgentRole != AgentRoleInLocationType.COUNTRY_OF_DEATH) &&
                (locationAgentRole != AgentRoleInLocationType.PLACE_OF_DEATH) &&
                (locationAgentRole != AgentRoleInLocationType.EMBARKATION_PORT) &&
                (locationAgentRole != AgentRoleInLocationType.DISEMBARKATION_PORT) &&
                (locationAgentRole != AgentRoleInLocationType.COUNTRY_OF_RESIDENCE)
        );
    }


}
