package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.operationalasset.OperationalAsset;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #102 Boundary Vessel Breadth
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in payload AND `MessageType` is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` attribute `Breadth` is defined
 * `*THEN*` attribute `Breadth` must be inferior to 100 (meters) AND attribute `Breadth` must be superior to 1 (meter).
 */
public class TestStep102BoundaryOperationalAsset extends CiseTestStepOneOfChildValueBoundary {

    public TestStep102BoundaryOperationalAsset(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{OperationalAsset.class};
        List<String> fields = Arrays.asList(
                "maxPassengers",
                "range",
                "maxSpeed"
        );

        return super.createReport(messageXml,
                fields,
                "Objet",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_102_BOUNDARY_OPERATIONALASSET
        );

    }
}
