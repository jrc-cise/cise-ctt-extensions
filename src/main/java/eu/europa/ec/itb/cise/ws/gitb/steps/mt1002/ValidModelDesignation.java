package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import eu.cise.datamodel.v1.entity.action.Action;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.cargo.CargoUnit;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.document.*;
import eu.cise.datamodel.v1.entity.incident.*;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.cise.datamodel.v1.entity.metadata.Metadata;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.organization.*;
import eu.cise.datamodel.v1.entity.period.Period;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.person.PersonIdentifier;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;

import java.util.Arrays;

/**
 * Captured from
 * list-entityJH (sent mail 20200527)
 * http://emsa.europa.eu/cise-documentation/cise-data-model-1.5.3
 * on 06/05/2020
 **/
public enum ValidModelDesignation {
    ACTION("Action", Action.class),
    ANOMALY("Anomaly", Anomaly.class),
    CARGOUNIT("CargoUnit", CargoUnit.class),
    CATCH("Catch", Catch.class),
    PERSONDOCUMENT("PersonDocument", PersonDocument.class),
    EVENTDOCUMENT("EventDocument", EventDocument.class),
    DOCUMENT("Document", Document.class),
    LOCATIONDOCUMENT("LocationDocument", LocationDocument.class),
    RISKDOCUMENT("RiskDocument", RiskDocument.class),
    CERTIFICATEDOCUMENT("CertificateDocument", CertificateDocument.class),
    VESSELDOCUMENT("VesselDocument", VesselDocument.class),
    ORGANIZATIONDOCUMENT("OrganizationDocument", OrganizationDocument.class),
    CARGODOCUMENT("CargoDocument", CargoDocument.class),
    CRISISINCIDENT("CrisisIncident", CrisisIncident.class),
    INCIDENT("Incident", Incident.class),
    LAWINFRINGEMENTINCIDENT("LawInfringementIncident", LawInfringementIncident.class),
    MARITIMESAFETYINCIDENT("MaritimeSafetyIncident", MaritimeSafetyIncident.class),
    METEOOCEANOGRAPHICCONDITION("MeteoOceanographicCondition", MeteoOceanographicCondition.class),
    GEOMETRY("Geometry", Geometry.class),
    LOCATION("Location", Location.class),
    METADATA("Metadata", Metadata.class),
    IRREGULARMIGRATIONINCIDENT("IrregularMigrationIncident", IrregularMigrationIncident.class),
    MOVEMENT("Movement", Movement.class),
    VEHICLE("Vehicle", Vehicle.class),
    FORMALORGANIZATION("FormalOrganization", FormalOrganization.class),
    PORTORGANIZATION("PortOrganization", PortOrganization.class),
    ORGANIZATION("Organization", Organization.class),
    ORGANIZATIONALUNIT("OrganizationalUnit", OrganizationalUnit.class),
    RISK("Risk", Risk.class),
    ORGANIZATIONALCOLLABORATION("OrganizationalCollaboration", OrganizationalCollaboration.class),
    PERIOD("Period", Period.class),
    PERSON("Person", Person.class),
    PERSONIDENTIFIER("PersonIdentifier", PersonIdentifier.class),
    VESSEL("Vessel", Vessel.class);

    private String designation;
    private Class classRef;

    ValidModelDesignation(String designation, Class classRef) {
        this.designation = designation;
        this.classRef = classRef;
    }

    public static boolean isValidDesignation(String designation) {
        return Arrays.stream(values()).anyMatch(s -> s.designation.equals(designation));
    }
}
// TODO: use suppressed relation to the list ... create a relation specific class
//    AGENTRISK("AgentRisk",AgentRisk.class),
//    AGENTLOCATION("AgentLocation",AgentLocation.class),
//    AGENTOBJECT("AgentObject",AgentObject.class),
//    AGENTAGENT("AgentAgent",AgentAgent.class),
//    CORRELATEDWITH("CorrelatedWith",CorrelatedWith.class),
//    AGENTEVENT("AgentEvent",AgentEvent.class),
//    EVENTLOCATION("EventLocation",EventLocation.class),
//    OBJECTEVENT("ObjectEvent",ObjectEvent.class)
//    OBJECTLOCATION("ObjectLocation",ObjectLocation.class),