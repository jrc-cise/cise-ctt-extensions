package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.metadata.Metadata;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

/**
 *#25 Identification Of DocumentType
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `AttachedDocument` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *attribute* `Metadata` must have value key `FileMediaType` with non empty value.
 */
public class TestStep25IdentificationOfDocumentType extends CiseTestStep {

    public TestStep25IdentificationOfDocumentType(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);

        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_25_IDENTIFICATION_OF_DOCUMENT_TYPE;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Document.class);

        if (payloadEntitiesOfType.size() == 0) report.setResult(TestResultType.SUCCESS);

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (entityPair.getA() instanceof Document) {
                    Document document = (Document) entityPair.getA();
                    if (!checkDocumentForMinimumIdentification(document)) {
                        conform = false;
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred", "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }


    private boolean checkDocumentForMinimumIdentification(Document document) {

        if (((document.getMetadatas().size() > 0))
        ) {
            for (Metadata metadata : document.getMetadatas()) {
                if (Strings.isNotEmpty(metadata.getFileMediaType().value())) return true;
            }
        }

        return false;
    }
}



