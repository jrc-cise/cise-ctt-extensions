package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import eu.europa.ec.itb.cise.ws.util.geometry.KMLReader;
import org.apache.logging.log4j.util.Strings;

import java.util.List;
/**
#42 XMLGeometry Validity
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*`  *Element* of type `XMLGeometry` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
`*THEN*` it should be evaluable as `XMLGeometry` syntax.
*/
public class TestStep42XMLGeometryValidity extends CiseTestStep {

    public TestStep42XMLGeometryValidity(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Location.class);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_42_XMLGEOMETRY_VALIDITY;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Location location = (Location) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (isXMLGeometryDefined(location) && !isXMLGeometryCorrectlyDefined(location)) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), location);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred: ", "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }

    private boolean isXMLGeometryDefined(Location location) {
        if (location.getGeometries() == null ||
                (location.getGeometries() != null && location.getGeometries().isEmpty())) {
            return false;
        }

        Geometry geometry = location.getGeometries().get(0);
        if (Strings.isNotBlank(geometry.getXMLGeometry())) {
            return true;
        }
        return false;
    }

    private boolean isXMLGeometryCorrectlyDefined(Location location) {
        Geometry geometry = location.getGeometries().get(0);
        String xmlGeometry = geometry.getXMLGeometry();

        // KML reader is able to read both KML and GML
        KMLReader kmlReader = new KMLReader(xmlGeometry);
        try {
            kmlReader.read();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
