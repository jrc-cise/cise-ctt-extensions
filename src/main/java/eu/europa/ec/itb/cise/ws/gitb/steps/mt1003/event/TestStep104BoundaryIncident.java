package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.event;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.incident.CrisisIncident;
import eu.cise.datamodel.v1.entity.incident.IrregularMigrationIncident;
import eu.cise.datamodel.v1.entity.incident.LawInfringementIncident;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #104 Boundary Vessel DesignSpeed
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in payload AND `MessageType` is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` attribute `Lenght` is defined
 * `*THEN*` attribute `Lenght` must be inferior to 70(knot) AND attribute `Lenght` must be superior to 1 (.).
 */
public class TestStep104BoundaryIncident extends CiseTestStepOneOfChildValueBoundary {

    public TestStep104BoundaryIncident(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{MaritimeSafetyIncident.class, IrregularMigrationIncident.class, LawInfringementIncident.class, CrisisIncident.class};
        List<String> fields = Arrays.asList(
                "deathsOnBoard",
                "numberOfIIllPersons"
        );

        return super.createReport(messageXml,
                fields,
                "Event",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_104_BOUNDARY_INCIDENT
        );

    }
}