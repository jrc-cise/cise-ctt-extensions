package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.List;

public abstract class CiseTestStepLinkedEntityAll extends CiseTestStep {

    public CiseTestStepLinkedEntityAll(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    /**
     * In this function we assume that the entityClassToFind is an Event because is the only one that have relations to other kind of objects.
     *
     * @param messageXml                   input message
     * @param eventClassToFind             the Event to find: from this element we search backward and forward to verify relations
     * @param linkedEntityClassesToLookFor classes of objects that have to be present backward or forward
     * @param testStepRef                  code to reference the specific test step
     * @return                             returns the report
     */
    protected TAR createReport(String messageXml, Class eventClassToFind, Class[] linkedEntityClassesToLookFor, CiseTestStepsEnum testStepRef) {
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadHelper.getPayloadEntitiesOfType(messageXml, eventClassToFind)) {
                Event event = (Event) entityPair.getA();
                String breadcrumb = entityPair.getB();
                for (Class alinkedEntityClass : linkedEntityClassesToLookFor) {
                    Class[] linkedEntityClassesToLookForAdd = new Class[]{alinkedEntityClass};
                    if (isEntityOfCorrectType(event) &&
                            !hasBackwardLinkedObjectOfValidType(messageXml, breadcrumb, linkedEntityClassesToLookForAdd) &&
                            !hasALinkedObjectOfValidType(event, linkedEntityClassesToLookForAdd)
                    ) {
                        conform = false;
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), event);
                    }
                    if (!conform) continue;
                }
            }

            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation + describeCTXwarning), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    protected abstract boolean isEntityOfCorrectType(Entity entity);

    protected boolean hasALinkedObjectOfValidType(Event event, Class[] linkedEntityClasses) {

        // Look for objects
        List<Event.InvolvedObjectRel> involvedObjectRels = event.getInvolvedObjectRels();
        if (involvedObjectRels != null) {
            for (Event.InvolvedObjectRel involvedObjectRel : involvedObjectRels) {
                if (isInstanceOfOneOf(involvedObjectRel.getObject(), linkedEntityClasses)) {
                    return true;
                }
            }
        }

        //Look for Risks
        List<Event.ImpliedRiskRel> impliedRiskRels = event.getImpliedRiskRels();
        if (impliedRiskRels != null) {
            for (Event.ImpliedRiskRel impliedRiskRel : impliedRiskRels) {
                if (isInstanceOfOneOf(impliedRiskRel.getRisk(), linkedEntityClasses)) {
                    return true;
                }
            }
        }

        // Look for documents
        List<Event.DocumentRel> documentRels = event.getDocumentRels();
        if (documentRels != null) {
            for (Event.DocumentRel documentRel : documentRels) {
                if (isInstanceOfOneOf(documentRel.getDocument(), linkedEntityClasses)) {
                    return true;
                }
            }
        }

        // Look for Agents
        List<Event.InvolvedAgentRel> involvedAgentRels = event.getInvolvedAgentRels();
        for (Event.InvolvedAgentRel agentRel : involvedAgentRels) {
            if (isInstanceOfOneOf(agentRel.getAgent(), linkedEntityClasses)) {
                return true;
            }
        }

        // Look for Location
        List<Event.LocationRel> locationRels = event.getLocationRels();
        for (Event.LocationRel locationRel : locationRels) {
            if (isInstanceOfOneOf(locationRel.getLocation(), linkedEntityClasses)) {
                return true;
            }
        }

        return false;
    }

    protected boolean hasBackwardLinkedObjectOfValidType(String messageXml, String breadcrumb, Class[] linkedEntityClasses) {
        for (Class linkedEntityClass : linkedEntityClasses) {
            Entity parentlinkedEntity = payloadHelper.getParentOfType(messageXml, breadcrumb, linkedEntityClass);
            if (parentlinkedEntity != null) return true;
        }
        return false;
    }


}
