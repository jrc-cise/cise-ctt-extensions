package eu.europa.ec.itb.cise.ws.util;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.relationship.Relationship;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DomainTypeValidator {
    public static boolean hasDefinedSpecificSimpleTypeListField(String[] listFields, Entity entity) {
        ArrayList<Pair<String, Boolean>> definedFields = generateListOfNonEmptySimpleTypeListFields(entity);
        for (String expectedFieldname : listFields) {
            for (Pair<String, Boolean> definedField : definedFields)
                if (definedField.getA().equals(expectedFieldname))
                    return true;
        }
        return false;
    }

    public static ArrayList<Pair<String, Boolean>> generateListOfNonEmptySimpleTypeListFields(Entity entity) {
        Class entityClass = entity.getClass();
        ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
        Class entityParentClass = entityClass.getSuperclass();
        while (!(entityParentClass.equals(Entity.class))) {
            Field[] entityParentFields = entityParentClass.getDeclaredFields();
            entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityParentFields)));
            entityParentClass = entityParentClass.getSuperclass();
        }
        ArrayList<Pair<String, Boolean>> areDefinedAndNonEmptyRelations = new ArrayList();
        for (Field field : entityFields) {
            field.setAccessible(true);
            Object content = null;
            if (("serialVersionUID").equals(field.getName()))
                continue;
            try {
                content = field.get(entity);
                if ((content != null) &&
                        ("interface java.util.List".equals(field.getType().toString())) &&
                        (!(((List) content).isEmpty()))) {
                    areDefinedAndNonEmptyRelations.add(new Pair((field.getName()), ("" + ((List<Relationship>) content).size())));
                }
            } catch (IllegalAccessException eIAE) {
                return areDefinedAndNonEmptyRelations;
            }
        }
        return areDefinedAndNonEmptyRelations;
    }
}