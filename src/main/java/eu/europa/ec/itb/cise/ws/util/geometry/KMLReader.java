package eu.europa.ec.itb.cise.ws.util.geometry;

import org.locationtech.jts.geom.Geometry;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.BufferedReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

public class KMLReader {
    private String kmlString;

    public KMLReader(String kmlString) {
        this.kmlString = kmlString;
    }

    public List<Geometry> read()
            throws Exception {
        XMLReader xr;
        xr = XMLReaderFactory.createXMLReader();
        KMLHandler kmlHandler = new KMLHandler();
        xr.setContentHandler(kmlHandler);
        xr.setErrorHandler(kmlHandler);

        Reader inputString = new StringReader(kmlString);
        BufferedReader reader = new BufferedReader(inputString);
        LineNumberReader myReader = new LineNumberReader(reader);
        xr.parse(new InputSource(myReader));

        return kmlHandler.getGeometries();
    }
}
