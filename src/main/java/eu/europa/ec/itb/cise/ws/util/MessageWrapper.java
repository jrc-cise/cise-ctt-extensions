package eu.europa.ec.itb.cise.ws.util;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.servicemodel.v1.message.*;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;

public class MessageWrapper {
    public static Message createFakePushMsg(Object entityObject) {
        List<Entity> entities = new ArrayList<Entity>();
        entities.add((Entity) entityObject);
        Push messageUniqueEntity = newPush().id("messageId")
                .creationDateTime(new Date())
                .priority(PriorityType.HIGH)
                .informationSecurityLevel(InformationSecurityLevelType.NON_CLASSIFIED)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .setEncryptedPayload("false")
                .isPersonalData(false)
                .purpose(PurposeType.NON_SPECIFIED)
                .sender(newService().id("fake.push.provider")
                        .operation(ServiceOperationType.PUSH).type(ServiceType.RISK_SERVICE).build())
                .recipient(newService().id("fake.push.consumer")
                        .operation(ServiceOperationType.PUSH).type(ServiceType.RISK_SERVICE).build())
                .addEntities(entities)
                .build();
        return messageUniqueEntity;
    }
}
