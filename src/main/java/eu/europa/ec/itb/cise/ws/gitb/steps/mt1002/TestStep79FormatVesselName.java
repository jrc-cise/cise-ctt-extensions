package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #79 FORMAT Vessel NAME Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `NAME` is defined
 * `*THEN*` attribute `NAME` must be composed of up to 35 alphanumerical characters including space / dots “.” / dashes “-“ / single apostrophe “ ’ ”.
 */
public class TestStep79FormatVesselName extends CiseTestStep {

    public TestStep79FormatVesselName(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_79_FORMAT_VESSEL_NAME;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!isEntityOfCorrectType(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * Up to 35 characters long. Structure: [a..z][A..Z][0..9]Additional
     * characters allowed are dots “.”, dashes “-“ and single apostrophe “ ’ ”
     */
    private boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof Vessel) {
            List<String> names = ((Vessel) entity).getNames();
            if (names == null)
                return true;
            else {
                boolean valid = true;
                for (String name : names) {
                    if (name.length() < 36) {
                        if (!(name.trim().matches("^[a-zA-Z0-9.\\-'' ]+$")))
                            return false;
                    } else {
                        return false;
                    }
                }
            }
            return true;
        }
        return true;
    }

}
