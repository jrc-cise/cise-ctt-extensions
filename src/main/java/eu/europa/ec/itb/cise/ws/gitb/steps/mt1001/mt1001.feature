#1001 Entities Attributes coherency
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies) of type  in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` payload*

.specific rule concern
[cols="5%,15%,15%,15%,45%"]
|=======================
|_N_|_Title_|_Entity_|_Link_|_Gherkin_
|32|AttachedDocument Coherent With Vessel| `Vessel` |
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `VesselDocument`, `CertificateDocument`
|33|AttachedDocument Coherent With Location|`Location` | 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `LocationDocument`
|34|AttachedDocument Coherent With Event|`Event`| 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `EventDocument`
|35|AttachedDocument Coherent With Cargo| `Cargo`| 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `CargoDocument`
|36|AttachedDocument Coherent With Organization| `Organization`| 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `OrganizationDocument`
|37|AttachedDocument Coherent With Risk| `Risk`| 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `RiskDocument`
|38|AttachedDocument Coherent With Person|`Person`| 
`*AND*` it have a relation to `Document`|
`*THEN*` it must have end with entity(ies) of type `PersonDocument`
|62|LinkObjectLocation CargoUnit|`Cargo` or `ContainmentUnit` or `Catch` |`*AND*` it have a relation to `Location`|
`*THEN*` it _must not define_ attribute `COGHeading`, `PlannedOperation`, `PlannedWork`, `SensorType`, `SOGSpeed`
`*AND*` it _must not have predetermined value for_ attribute `LocationRole` in `PORT_OF_DISCHARGE`, `LENGTHENED_PLACE`, `PORT_OF_EMBARKATION`, `PORT_OF_DISEMBARKATION`
|63|LinkObjectLocation Vehicule|`Cargo` or `ContainmentUnit` or `Catch`|`*AND*` it have a relation to `Location`| 
`*THEN*` it _must not have predetermined value for_ attribute `LocationRole` in `PORT_OF_DISCHARGE`, `PORT_OF_LOADING`
|64|LinkObjectObject Agent Organization|`Organization` or `OrganizationalUnit` or `PortOrganization` or `FormalOrganuzation` or `OrganizationalCollaboration`|  
`*AND*` have relation(s) to `Object` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `PASSENGER`, `CREW_MEMBER`, `CAPTAIN_MASTER`, `COMPANY_SECURITY_OFFICER`, `EMPLOYEE`, `CREW_MEMBER`.
`*AND*` relation _must not define_ attribute :`TransitPassengerDuty`
|65|LinkObjectObject Agent Organization|`Vessel` or `Aircraft` or `LandVehicle`| 
`*AND*` have relation(s) to `Person` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `SHIP_OPERATING_COMPANY`, `VESSEL_COMPANY`, `SHIPPING_LINE`.
|66|LinkObjectObject Agent CargoUnit|`ContainmentUnit` or `Catch` or `PortOrganization` or `Cargo`|  
`*AND*` have relation(s) to `Person` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `PASSENGER`, `CREW_MEMBER`, `CAPTAIN_MASTER`, `SHIP_OPERATING_COMPANY`, `VESSEL_COMPANY`, `EMPLOYEE`
`VESSEL_BUILDER` , `VESSEL_CHARTERER`, `VESSEL_REGISTERED_OWNER`, `SHIPPING_LINE`
`*AND*` relation _must not define_ attribute :`TransitPassengerDuty`.
|67|LinkObjectEvent Event CargoUnit| `ContainmentUnit` or `Catch` or `Cargo`|  
`*AND*` have relation(s) to `Object` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `PARTICIPANT`, `OBSERVER`, `REPORTER`
|68|Link Object Movement| `Movement` | 
`*AND*` have relation(s) to `Object` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `CAUSE`, `VICTIM`
|69|Link Object Anomaly| `Anomaly`| 
`*AND*` have relation(s) to `Object` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `MEAN`.
|70|Link Agent Movement| `Anomaly`| 
`*AND*` have relation(s) to `Movement` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `CAUSE`, `VICTIM`, `PERPETRATOR`.
|71|Link Anomaly Agent| `Anomaly`| 
`*AND*` have relation(s) to `Agent` entity(ies)|
`*THEN*` relation _must not have
|72|Link Movement Location|`Movement`|  
`*AND*` have relation(s) to `Location`  |
`*THEN*` itsattribute* `EventArea` _must not be defined_
|73|Link Movement Location|`Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration`|  
`*AND*` have relation(s) to `Location` entity(ies)|
`*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in
`COUNTRY_OF_BIRTH`,
`PLACE_OF_BIRTH`,
`COUNTRY_OF_DEATH`,
`PLACE_OF_DEATH`,
`EMBARKATION_PORT`,
`DISEMBARKATION_PORT`,
`COUNTRY_OF_RESIDENCE`
|74|Link Person Person|`Person`|  
`*THEN*` its relation(s) cannot have for destination entity(ies) of type `Person`|
|75|Link Movement Location| `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` | 
`*AND*` have relation(s) to `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` entity(ies)|
`*THEN*` its attribute* `Duty` _must not be defined_
|=======================

