#1002 Fields Value Format
^^^^^^^^^^^^^^^^^^^^^^^^^

`*GIVEN*` *entity(ies)* of type _Entity_ is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH`, `PULL_RESPONSE/REQUEST`, `PUSH/PULL_SUBSCRIBE`
[]

.specific rule concern
[cols="5%,23%,20%,26%,26%"]
|=======================
|_N_|_Title_|_Entity_|_Attribute_|_Gherkin_
|76|Format of Vessel CallSign|`Vessel`|`CallSign`|
`*THEN*` *attribute* value _must be composed of_ up to 7 alphanumerical characters.
|77|Format of Vessel IMONumber|`Vessel`|`IMONumber`|
`*THEN*` *attribute* value _must be composed of_ exactly 7 numerical characters.
pass:[</br>] --
`*AND*` should support validation algorithm .
|78|Format of Vessel MMSI|`Vessel`|`MMSI`|
`*THEN*` *attribute* value _must be composed of_ of up to 9 numerical characters\n\
pass:[</br>] --
`*AND*` should start with a known 3 characters MID code (Maritime Identification Digits) .
|79|Format of Vessel Name|`Vessel`|`Name`|
`*THEN*` *attribute* value _must be composed of_ up to 35 alphanumerical characters including space " " / dots “.” / dashes “-“ / single apostrophe “ ’ ”.
|80|Format of Vessel InMarsat|`Vessel`|`InMarsat`|
`*THEN*` *attribute* value _must be composed of_  15 to 18 numerical characters starting with "00879" or "+879".
|82|Format of Vessel Nationality|`Vessel`| `Nationality`|
`*THEN*` *attribute* value _must be composed of_ 2 alphabetic characters in accordance with iso3166-1.
|83|Format of ContainmentUnit UNDG|`ContainmentUnit`| `UNDG`|
`*THEN*` *attribute* value _must be composed of_  4 Digit characters in accordance with UNDG code format.
|84|Format of ContainmentUnit LocationOnBoardContainer| `ContainmentUnit`| `LocationOnBoardContainer`|
`*THEN*` *attribute* value _must be composed of_ 1 upper case char followed by : semicolon ":" + Alphanumerical characters
pass:[</br>] --
`*AND*` ref should be in accordance to ISO 28005 (not evaluated).
|85|Format of ContainmentUnit ContainerMarksAndNumber| `Vessel`| `ContainerMarksAndNumber`|
`*THEN*` *attribute* value _must be composed of_ 4 upper case letter characters followed by 7 digits in accordance to ISO 6346.
|86|Format of catch species| `catch`| `species`|
`*THEN*` *attribute* value _must be composed of_  3 Letter characterspass:[</br>] --
`*AND*` ref should be compliant to EC rules for the implementation of Council Regulation No 1224/2009.
|87|Format of PortLocation LocationCode| `Vessel`| `LocationOnBoardContainer`|
`*THEN*` *attribute* value _must be composed of_ 1 upper case char followed by first 2 Letters in accordance with iso-3160-1 and then following 3 Character identifying location in this country.
|88|Format of (LOCATION - GEOMETRY implicit) Latitude Longitude| `Location`| `Latitude` and `Longitude` in `Geometry` type|
`*THEN*` *attribute* value _must be composed of_ 1-3 digits followed by "." and 1 to 4 digits
pass:[</br>] --
`*AND*` be convertible to a decimal number between accepted range (Latitud -+90 Longitud -+180)
pass:[</br>] --
`*AND*` should present sufficient precision >=2 digits (2500 meter at the equator)
pass:[</br>] --
`*AND*` should represent effective precision < =9 digits (millimeter at the equator)
|89|Format of NamedLocation GeographicIdentifier| `NamedLocation`| `GeographicIdentifier`|
`*THEN*` *attribute* value _must be composed of_  an url  starting with "http://sws.geonames.org/" followed by digits identifying the location
pass:[</br>] --
`*AND*` ref should be in accordance to ISO 28005 (not evaluated).
|90|Format of Portfacility Portfacilitynumber| `Portfacility`| `Portfacilitynumber`|
`*THEN*` *attribute* value _must be composed of_ 5 chars LOCODE followed by "-" + digits
|93|Format of Metadata Language| `Document`| `Language` in `metadata`|
`*THEN*` ref value must be composed of three-letter codes based on ISO 639-3 standard.
|94|Format of Metadata Designation| `Document`| `Designation` in `metadata`|
`*THEN*` ref value must be a  valid name of CISE Data Model entity.
|95|Format of Metadata Language| `Document`| `UniqueIdentifier` in `UUID`|
`*THEN*`  _must be composed of_ standard 8-4-4-4-12 digits format value
|96|Format of Agent ContactInformation| `Agent`| `ContactInformation`|
`*THEN*` ref value _must be composed of_ a VCARD standard format structure.
pass:[</br>] --
`*AND*` ref value _should define_ minimum informations for organization / individual (VERSION + N)
|97|Format of Agent Nationality| `Agent`| `Nationality`|
`*THEN*` ref value _must contains_ with 2 upper characters format
pass:[</br>] --
`*AND*` ref value _should define_ a listed ISO-3166 .
|=======================

