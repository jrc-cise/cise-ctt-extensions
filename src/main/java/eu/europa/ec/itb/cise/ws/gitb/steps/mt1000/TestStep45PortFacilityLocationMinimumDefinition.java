package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #45 PollutionIncident Type
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `PortFacilityLocation` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define attribute `PortFacilityName´, ´PortFacilityNumber`
 */
public class TestStep45PortFacilityLocationMinimumDefinition extends CiseTestStepOneOfChildValue {

    public TestStep45PortFacilityLocationMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClass = new Class[]{PortFacilityLocation.class};
        String[] fieldNames = new String[]{"portFacilityName", "portFacilityNumber"};
        return super.createReport(messageXml, fieldNames, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_45_PORT_FACILITY_LOCATION_MIN_DEF);
    }

    /*
     AlternativeName
    IdentificationNumber
    LegalName
    IMOCompanyIdentificationNumber
     */

}