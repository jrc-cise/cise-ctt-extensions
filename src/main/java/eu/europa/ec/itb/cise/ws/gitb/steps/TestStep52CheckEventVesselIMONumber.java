package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.util.MessageWrapper;

import java.util.List;

/**
 * #52 IMONumber corresponds to the request
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*Background*` : a `VESSEL` entity is defined by attribute `IMONumber` in the SUT Information System
 * `*And*` : it is is linked to an `Event`.
 * `*Given*` : `VESSEL` entity is present in  Pull.Request message payload.
 * `*When*` : it is presented with defined attribute `IMONumber`.
 * `*Then*` : corresponding Pull.Response message must present at least one `VESSEL`
 * `*And*` : it must be presented with defined attribute `IMONumber`.
 */
public class TestStep52CheckEventVesselIMONumber extends CiseTestStep {

    public TestStep52CheckEventVesselIMONumber(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml, String expectedIMONumber) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_52_CHECK_EVENT_VESSEL_IMO_NUMBER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        //GIVEN vessel entity present linked to mainEntity in response
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);

        // WHEN 1.* response
        List<Object> payloadMainEntities = payloadHelper.getPayloadObjects(messageXml);

        boolean conform = true;
        //THEN 1 Vessel per response with given IMOnumber
        for (Object entityObject : payloadMainEntities) {
            String entityMessageUniqueMainEntityXml = reportBuilder.getXmlMapper().toXML(MessageWrapper.createFakePushMsg(entityObject));
            List<Pair<Entity, String>> payloadSubEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(entityMessageUniqueMainEntityXml, Vessel.class);
            for (Pair<Entity, String> entityPair2 : payloadSubEntitiesOfType) {
                if (!isIMONumberCorrect(expectedIMONumber, payloadSubEntitiesOfType, (Vessel) entityPair2.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair2.getB()), entityPair2.getA());
                }
            }
        }
        report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
        report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));

        return report;
    }

    private boolean isIMONumberCorrect(String expectedIMONumber, List<Pair<Entity, String>> payloadEntitiesOfType, Vessel vessel) {
        return payloadEntitiesOfType.size() == 1 &&
                vessel.getIMONumber() != null &&
                Long.parseLong(expectedIMONumber) == (vessel).getIMONumber();
    }
}
