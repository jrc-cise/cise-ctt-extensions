package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #80 FORMAT Vessel InMarsat Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `InMarsat` is defined
 * `*THEN*` attribute `InMarsat` must be composed by 15 to 18 numerical characters starting with "00879" or "+879".
 */
public class TestStep80FormatVesselInmarsat extends CiseTestStep {

    public TestStep80FormatVesselInmarsat(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_80_FORMAT_VESSEL_INMARSAT;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!isEntityOfCorrectType(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * from 13 and up to 18 characters long. Structure: [a..z][A..Z][0..9]Additional
     * characters allowed are dots “.”, dashes “-“ and single apostrophe “ ’ ”
     */
    private boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof Vessel) {
            String inmarsatNumber = ((Vessel) entity).getINMARSATNumber();
            if (inmarsatNumber == null) {
                return true;
            } else {
                if ((inmarsatNumber.length() >= 13) && (inmarsatNumber.length() <= 18))
                    return (inmarsatNumber.trim().matches("^[+0-9 ]+$") && (inmarsatNumber.startsWith("00879") || (inmarsatNumber.startsWith("+879"))));
                else
                    return false;
            }
        }
        return true;
    }

    /**
     * Captured from
     * https://www.itu.int/en/ITU-R/terrestrial/fmd/Pages/mid.aspx
     * on 06/05/2020
     **/
    private enum NationalityCodeEnum {
        RFR("FR", "France"),
        RIT("IT", "Italy");

        private String nationality;
        private String literal;

        NationalityCodeEnum(String nationality, String literal) {
            this.nationality = nationality;
        }

        public static NationalityCodeEnum getnationality(String nationality) {
            for (NationalityCodeEnum value : values()) {
                if (value.getNationality().equals(nationality)) {
                    return value;
                }
            }
            return null;
        }

        public String getNationality() {
            return nationality;
        }
    }
}
