package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.action.Action;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.document.AttachedDocument;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.EventDocument;
import eu.cise.datamodel.v1.entity.document.Stream;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.*;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #57 Action Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Action` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` must define at least one relation to entity(ies) `Document` (+ its children entities) or  `Event` entity (+ its children entities) or `Object` (+ its children entities)
 */
public class TestStep57ActionMinimumDefinition extends CiseTestStepLinkedEvent {
    public TestStep57ActionMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    protected boolean isEntityOfCorrectType(Event event) {
        return (event instanceof Action);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Action.class;
        Class[] linkedClass = new Class[]{
                /*Objet.class->*/
                /*Vehicle.class,->*/
                Vessel.class,
                Aircraft.class,
                LandVehicle.class,
                /*CargoUnit.class->*/
                Catch.class,
                ContainmentUnit.class,
                Cargo.class,
                /*Event.class->*/
                Movement.class,
                Incident.class, /*->*/
                IrregularMigrationIncident.class,
                LawInfringementIncident.class,
                MaritimeSafetyIncident.class,
                CrisisIncident.class,
                Action.class,
                Anomaly.class,
                Document.class, /*->*/
                AttachedDocument.class, /*->*/
                /*VesselDocument.class,
                PersonDocument.class,
                CargoDocument.class,
                OrganizationDocument.class,
                CertificateDocument.class,
                RiskDocument.class,
                LocationDocument.class,*/
                EventDocument.class,
                Stream.class
        };
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_57_ACTION_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }

}
