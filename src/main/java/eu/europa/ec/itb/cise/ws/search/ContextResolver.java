package eu.europa.ec.itb.cise.ws.search;

import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class ContextResolver {

    public ContextResolver() {
    }

    /**
     * It searches for all the occurrences of the class "entityOrRelClass" in forward direction stating from "rootPayloadEntity".
     * the output is a list of pairs containing a reference to the CISE object found and the breadcrumb
     * of the entity (or relationship) within the "rootPayloadEntity".
     *
     * @param entityOrRelClass  Class to serach for
     * @param rootPayloadEntity payload tree to search in
     * @param <T>               indicates whether to search fo an Entity or a Relationship
     * @return                  retrun the list
     */
    @SuppressWarnings("unchecked")
    public static <T> List<Pair<T, String>> findCiseObjectsWithReferences(Class entityOrRelClass, Entity rootPayloadEntity) {
        List<Pair<T, String>> ciseObjects = new ArrayList<>();

        Tree haystackTree = Tree.build(rootPayloadEntity);
        try {
            Tree needleTree = getTreeFromClass(entityOrRelClass);

            List<Node> matchingNodes = haystackTree.searchOccurrences(needleTree);

            for (Node matchingNode : matchingNodes) {
                ciseObjects.add(new Pair<>((T) matchingNode.getCiseObject(), matchingNode.getComputedName() + "." + matchingNode.getId()));
            }

        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return ciseObjects;
    }

    /**
     * It searches in "rootPayloadEntity" for the first entity of type "classToLookFor" starting from "breadcrumb" in backward direction.
     *
     * @param classToLookFor    Class to look for
     * @param breadcrumb        node ID in the tree to start the search from
     * @param rootPayloadEntity reference of the element in CISE payload where to search
     * @return                  reference in Tree if found
     */
    public static <T> T findParentOfType(Class classToLookFor, String breadcrumb, Entity rootPayloadEntity) {
        T parent = null;

        Tree haystackTree = Tree.build(rootPayloadEntity);
        Integer startingNodeId = getNodeIdFromBreadcrumb(breadcrumb);

        Node matchingParentNode = haystackTree.searchBackwardOccurrenceOfCiseObject(classToLookFor, startingNodeId);

        if (matchingParentNode != null) {
            parent = (T) matchingParentNode.getCiseObject();
        }

        return parent;
    }

    public static <T> T findDirectParentOfType(Class classToLookFor, String breadcrumb, Entity rootPayloadEntity) {
        T parent = null;

        Tree haystackTree = Tree.build(rootPayloadEntity);
        Integer startingNodeId = getNodeIdFromBreadcrumb(breadcrumb);

        Node matchingParentNode = haystackTree.searchDirectBackwardOccurrenceOfCiseObject(classToLookFor, startingNodeId);

        if (matchingParentNode != null) {
            parent = (T) matchingParentNode.getCiseObject();
        }
        return parent;
    }

    private static Integer getNodeIdFromBreadcrumb(String breadcrumb) {
        String[] breadcrumbSplit = breadcrumb.split("\\.");
        return Integer.parseInt(breadcrumbSplit[breadcrumbSplit.length - 1]);
    }


    private static Tree getTreeFromClass(Class entityOrRelClass) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Constructor<?> ctor = entityOrRelClass.getConstructor();
        Object entityToFind = ctor.newInstance();
        return Tree.build(entityToFind);
    }


}
