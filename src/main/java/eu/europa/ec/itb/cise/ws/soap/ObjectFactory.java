package eu.europa.ec.itb.cise.ws.soap;

import eu.cise.servicemodel.v1.message.Message;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the eu.cise.accesspoint.service.v1 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private static final  QName MESSAGE_QNAME = new QName("http://www.cise.eu/accesspoint/service/v1/", "message");
    private static final  QName SEND_QNAME = new QName("http://www.cise.eu/accesspoint/service/v1/", "send");
    private static final  QName SEND_RESPONSE_QNAME = new QName("http://www.cise.eu/accesspoint/service/v1/", "sendResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.cise.accesspoint.service.v1
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Send }
     */
    public Send createSend() {
        return new Send();
    }

    /**
     * Create an instance of {@link SendResponse }
     */
    public SendResponse createSendResponse() {
        return new SendResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Message }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Message }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.cise.eu/accesspoint/service/v1/", name = "message")
    public JAXBElement<Message> createMessage(Message value) {
        return new JAXBElement<Message>(MESSAGE_QNAME, Message.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Send }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Send }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.cise.eu/accesspoint/service/v1/", name = "send")
    public JAXBElement<Send> createSend(Send value) {
        return new JAXBElement<Send>(SEND_QNAME, Send.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link SendResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.cise.eu/accesspoint/service/v1/", name = "sendResponse")
    public JAXBElement<SendResponse> createSendResponse(SendResponse value) {
        return new JAXBElement<SendResponse>(SEND_RESPONSE_QNAME, SendResponse.class, null, value);
    }

}
