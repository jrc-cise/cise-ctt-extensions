package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import java.util.Arrays;
import java.util.List;

/**
 * #101 Boundary Vessel Draught
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in payload AND `MessageType` is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` attribute `Draught` is defined
 * `*THEN*` attribute `Draught` must be inferior to 50.0 (meters) AND attribute `Draught` must be superior to 1.0 (meter).
 */
public class TestStep101BoundaryContainedCatch extends CiseTestStepOneOfChildValueBoundary {

    public TestStep101BoundaryContainedCatch(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Catch.class};
        List<String> fields = Arrays.asList(
                "catchWeight",
                "fishNumber",
                "netHeld",
                "quantityHeld",
                "sizeDeclaration",
                "totalNumber",
                "totalWeight"
        );

        return super.createReport(messageXml,
                fields,
                "Objet",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_101_BOUNDARY_CONTAINEDCATCH
        );

    }
}
