package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncidentType;
import eu.cise.datamodel.v1.entity.incident.PollutionIncident;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.List;

/**
 * #28 PollutionIncident Type
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `PollutionIncident` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define attribute `AnomalyType` with predefined value in : `POLLUTION`,`WASTE`,`LOST_FOUND_CONTAINERS`
 */
public class TestStep28PollutionIncidentType extends CiseTestStep {

    public TestStep28PollutionIncidentType(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_28_POLLUTION_INCIDENT_TYPE;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, PollutionIncident.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!isEntityOfCorrectType(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }


    private boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof PollutionIncident) {
            MaritimeSafetyIncidentType maritimeSafetyIncidentType = ((PollutionIncident) entity).getMaritimeSafetyIncidentType();
            if (maritimeSafetyIncidentType != null && (
                    (maritimeSafetyIncidentType == MaritimeSafetyIncidentType.POLLUTION) ||
                            (maritimeSafetyIncidentType == MaritimeSafetyIncidentType.WASTE) ||
                            (maritimeSafetyIncidentType == MaritimeSafetyIncidentType.LOST_FOUND_CONTAINERS)
            )
            ) {
                return true;
            }
        }
        return false;
    }

}
