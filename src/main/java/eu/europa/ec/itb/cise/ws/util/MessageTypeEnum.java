package eu.europa.ec.itb.cise.ws.util;

import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.Push;

/**
 * The possible message types to send to a remote CISE service.
 */
public enum MessageTypeEnum {

    PUSH("Push", Push.class),
    PULL_REQUEST("PullRequest", PullRequest.class),
    PULL_RESPONSE("PullResponse", PullResponse.class),
    ACKNOWLEDGEMENT("Acknowledgement", Acknowledgement.class);

    private String typeString;
    private Class typeClass;

    /**
     * Constructor.
     *
     * @param typeString The type as a string.
     * @param typeClass  The class of the type.
     */
    MessageTypeEnum(String typeString, Class typeClass) {
        this.typeString = typeString;
        this.typeClass = typeClass;
    }

    /**
     * Resolve the message type for the given string type.
     *
     * @param type The type.
     * @return The message type enum.
     */
    public static MessageTypeEnum fromType(String type) {
        if (type.equals(PUSH.getType())) {
            return PUSH;
        } else if (type.equals(PULL_REQUEST.getType())) {
            return PULL_REQUEST;
        } else if (type.equals(PULL_RESPONSE.getType())) {
            return PULL_RESPONSE;
        } else if (type.equals(ACKNOWLEDGEMENT.getType())) {
            return ACKNOWLEDGEMENT;
        } else {
            throw new IllegalArgumentException("Unknown type [" + type + "]");
        }
    }

    /**
     * Get the string type.
     *
     * @return The type.
     */
    public String getType() {
        return typeString;
    }

    /**
     * Get the message class.
     *
     * @return The class.
     */
    public Class<?> getClazz() {
        return typeClass;
    }
}
