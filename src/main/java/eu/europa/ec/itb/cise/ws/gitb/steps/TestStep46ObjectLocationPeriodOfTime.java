package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.period.Period;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.List;

/**
 * #46 Object Location PeriodOfTime
 *
 * `*GIVEN*` relation(s) link together `Object` entity(ies) and `Location` in `PUSH` or ` `PULL_RESPONSE` ` or ` `PUSH_SUBSCRIBE` ` payload
 * `*THEN*` it must define *attribute* `PeriodOfTime`
 * `*AND*` _it must be expressed_ with *simple type attribute combination* `Duration`, `StartDate`, `StartDate+StartTime`, `StartDate+EndDate`, `StartDate+StartTime+EndDate+EndTime`
 */
public class TestStep46ObjectLocationPeriodOfTime extends CiseTestStep {

    public TestStep46ObjectLocationPeriodOfTime(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        boolean conform = true;
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_46_OBJECTLOCATION_PERIODOFTIME;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        try {
            List<Pair<Relationship, String>> payloadRelationOfType = payloadHelper.getPayloadRelationOfType(messageXml, Objet.LocationRel.class);

            for (Pair<Relationship, String> relationPair : payloadRelationOfType) {
                if (relationPair.getA() instanceof Objet.LocationRel) {
                    String breadcrumb = relationPair.getB();
                    Period periodOfTime = ((Objet.LocationRel) relationPair.getA()).getPeriodOfTime();
                    if ((periodOfTime != null)) {
                        /* must be one of Duration|StartDate|StartDate+StarTime|StartDate+StartTime+EndDate+EndTime*/
                        InnerPeriodOfTimeValid innerPeriodOfTimeValid = new InnerPeriodOfTimeValid(periodOfTime);
                        if (!innerPeriodOfTimeValid.isValild()) {
                            conform = false;
                            Entity parentOfType = payloadHelper.getParentOfType(messageXml, breadcrumb, Objet.class);
                            failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), parentOfType);
                        }
                    }
                }
            }

            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    public static class InnerPeriodOfTimeValid {
        private final boolean hasDuration, hasStartDate, hasEndDate, hasEndTime, hasStartTime;

        public InnerPeriodOfTimeValid(Period periodOfTime) {
            hasDuration = periodOfTime.getDuration() != null;
            hasStartDate = periodOfTime.getStartDate() != null;
            hasEndDate = periodOfTime.getEndDate() != null;
            hasEndTime = periodOfTime.getEndTime() != null;
            hasStartTime = periodOfTime.getStartTime() != null;
        }

        public boolean isValild() {
            return (this.hasDuration && !this.hasStartDate && !this.hasEndDate && !this.hasEndTime && !this.hasStartTime)
                    || (!this.hasDuration && this.hasStartDate && !this.hasEndDate && !this.hasEndTime && !this.hasStartTime)
                    || (!this.hasDuration && this.hasStartDate && !this.hasEndDate && !this.hasEndTime && this.hasStartTime)
                    || (!this.hasDuration && this.hasStartDate && this.hasEndDate && this.hasEndTime && this.hasStartTime);
        }

    }
}
