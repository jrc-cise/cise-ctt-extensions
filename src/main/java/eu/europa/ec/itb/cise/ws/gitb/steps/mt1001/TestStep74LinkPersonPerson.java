package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #74 LinkPersonPerson
 * ^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Person` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *relation* cannot have for destination *entity(ies)* of type `Person`
 * include::./src/main/javadoc/static/usage.adoc[]
 *

 * @author mrhaki
 * @version 1.0
 */
public class TestStep74LinkPersonPerson extends CiseTestStepRelationship {

    public TestStep74LinkPersonPerson(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Person.class};
        Class[] objectClasses = new Class[]{Person.class};
        Class[] relationClasses = new Class[]{Agent.InvolvedWithRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_74_LINK_PERSON_PERSON);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        return false;
    }


}
