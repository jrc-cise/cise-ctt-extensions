package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

/**
 * #10 VesselMinimumIdentification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Vessel`  is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define at least one of the attribute:`CallSign`,`IMONumber`,`IRNumber`,`MMSI`,`RegistryNumber`,`UVI`,`Name`,`ExternalMarkings`,`Location`
 */
public class TestStep10VesselMinimumIdentification extends CiseTestStep {

    public TestStep10VesselMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_10_VESSEL_MIN_IDENT;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (entityPair.getA() instanceof Vessel) {
                    Vessel vessel = (Vessel) entityPair.getA();
                    if (!checkVesselEntityForMinimumIdentification(vessel)) {
                        conform = false;
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred", "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }

    private boolean checkVesselEntityForMinimumIdentification(Vessel vessel) {
        if (
                Strings.isNotBlank(vessel.getCallSign()) ||
                        vessel.getIMONumber() != null ||
                        Strings.isNotBlank(vessel.getIRNumber()) ||
                        vessel.getMMSI() != null ||
                        Strings.isNotBlank(vessel.getRegistryNumber()) ||
                        Strings.isNotBlank(vessel.getUVI()) ||
                        (vessel.getNames() != null && vessel.getNames().size() > 0 && Strings.isNotBlank(vessel.getNames().get(0))) ||
                        (vessel.getExternalMarkings() != null && vessel.getExternalMarkings().size() > 0 && Strings.isNotBlank(vessel.getExternalMarkings().get(0))) ||
                        vessel.getLocationRels() != null && vessel.getLocationRels().size() > 0 && verifyLocationHasCoordinatesDefined(vessel.getLocationRels().get(0))
        ) {
            //Success case
            return true;
        }
        return false;
    }


    private boolean verifyLocationHasCoordinatesDefined(Objet.LocationRel locationRel) {
        Location location = locationRel.getLocation();
        if (location != null) {
            List<Geometry> geometries = location.getGeometries();
            if (geometries != null && geometries.size() > 0) {
                for (Geometry geometry : geometries) {
                    if (Strings.isNotBlank(geometry.getLongitude()) && Strings.isNotBlank(geometry.getLatitude())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
