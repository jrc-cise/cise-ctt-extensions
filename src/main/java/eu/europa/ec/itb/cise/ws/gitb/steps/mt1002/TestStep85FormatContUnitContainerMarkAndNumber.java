package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #85 FORMAT ContainmentUnit ContainerMarksAndNumber
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `ContainmentUnit` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `ContainerMarksAndNumber` is defined
 * `*THEN*` attribute `ContainerMarksAndNumber` must be composed by first 4 letters upper case characters then 7 digits in accordance with ISO 6346 standard.
 */
public class TestStep85FormatContUnitContainerMarkAndNumber extends CiseTestStep {

    public TestStep85FormatContUnitContainerMarkAndNumber(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_85_FORMAT_CONT_UNIT_CONTAINER_MARK_AND_NUMBER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, ContainmentUnit.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * TODO: create the logic (return false)
     *
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof ContainmentUnit) {
            String containerMarksAndNumber = ((ContainmentUnit) entity).getContainerMarksAndNumber();
            if (containerMarksAndNumber != null) {
                boolean ownerCode, categoryIdentifier, serialNumber;
                ownerCode = containerMarksAndNumber.trim().substring(0, 3).matches("^[A-Z]+$");
                categoryIdentifier = containerMarksAndNumber.trim().substring(3, 4).matches("^[UJZ]+$");
                serialNumber = containerMarksAndNumber.trim().substring(4, 10).matches("^[0-9:]+$");
                return (ownerCode && categoryIdentifier && serialNumber);
            }
        }
        return true;
    }

    private boolean warningValid(Entity entity) {
        if (entity instanceof ContainmentUnit) {
            String containerMarksAndNumber = ((ContainmentUnit) entity).getContainerMarksAndNumber();
            if (containerMarksAndNumber != null) {
                boolean ownerCode, categoryIdentifier, serialNumber;
                ownerCode = containerMarksAndNumber.trim().substring(0, 3).matches("^[A-Z]+$");
                categoryIdentifier = containerMarksAndNumber.trim().substring(3, 4).matches("^[UJZ]+$");
                serialNumber = containerMarksAndNumber.trim().substring(4, 10).matches("^[0-9:]+$");
                return (ownerCode && categoryIdentifier && serialNumber &&
                        hashCorrect(containerMarksAndNumber.trim().substring(0, 10), containerMarksAndNumber.trim().substring(10, 11)));
            }
        }
        return true;
    }

    private boolean hashCorrect(String substring, String substring1) {
        int posit = 0;
        int sum = 0;
        while (posit < 10) {
            sum = (int) (sum + (ValueStep1.getValue(substring.charAt(posit)) * (Math.pow(2, posit))));
            posit++;
        }
        String result = (Double.toString(sum - (Math.floor(sum / 11.0) * 11)));
        // could be replaced by mod(11)
        return (substring1.equals(result.substring(0, 1)));
    }

    enum ValueStep1 {
        A("A", 10),
        B("B", 12),
        C("C", 13),
        D("D", 14),
        E("E", 15),
        F("F", 16),
        G("G", 17),
        H("H", 18),
        I("I", 19),
        J("J", 20),
        K("K", 21),
        L("L", 23),
        M("M", 24),
        N("N", 25),
        O("O", 26),
        P("P", 27),
        Q("Q", 28),
        R("R", 29),
        S("S", 30),
        T("T", 31),
        U("U", 32),
        V("V", 34),
        W("W", 35),
        X("X", 36),
        Y("Y", 37),
        Z("Z", 38);

        private Character letter;
        private int value;

        ValueStep1(String letter, int value) {
            this.letter = letter.charAt(0);
            this.value = value;
        }

        public static Integer getValue(Character letter) {
            for (ValueStep1 item : values()) {
                if (item.letter.equals(letter)) {
                    return item.value;
                }
            }
            return Integer.parseInt("" + letter);
        }

    }
}
