package eu.europa.ec.itb.cise.ws.gitb;

/**
 * Constants used to identify data maintained as part of a session's state.
 */
public class SessionData {
    /**
     * The URL on which the test bed is to be called back.
     */
    public static final String CALLBACK_URL = "callbackURL";

    /**
     * The URL of the remote system's CISE messaging service.
     */
    public static final String CISE_MESSAGING_SERVICE_ADDRESS = "CISEMessagingServiceAddress";

    /**
     * The URL of the remote system's CISE messaging service.
     */
    public static final String CISE_MESSAGING_MESSAGE_ID = "ciseMessageId";

    /**
     * The service ID for the test session.
     */
    public static final String SERVICE_ID = "serviceID";
    /**
     * The time creation   for the test session.
     */
    public static final String TIME_CREATED = "timeCreated";
}
