#1003 Fields Value Boundary
^^^^^^^^^^^^^^^^^^^^^^^^^

`*GIVEN*` *entity(ies)* of type _Entity_ is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH`, `PULL_RESPONSE/REQUEST`, `PUSH/PULL_SUBSCRIBE`
[]

.specific rule concern
[cols="5%,23%,20%,26%,26%"]
|=======================
|_N_|_Title_|_Entity_|_Attribute_|_Gherkin_
|98|Boundary Vessel attributes|`Vessel`|`beam`, `breadth`, `containerCapacity`, `deadweight`, `depth`, `draught`, `grossTonnage`, `length`, `lengthenedYear`, `loa`, `netTonnage`, `segregatedBallastVolume`, `yearBuilt`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|99|Boundary Vehicule attributes|`Vehicule` subtype `Aircraft`, `LandVehicle`, `Vessel` |`totalPersonsOnBoard`, `maximumSpeed`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|100|Boundary ContainmentUnit attributes|`ContainmentUnit`|`flashPoint`, `grossQuantity`, `netQuantity`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|101|Boundary Catch attributes|`Catch`|`catchWeight`, `fishNumber`, `netHeld`, `quantityHeld`, `sizeDeclaration`, `totalNumber`, `totalWeight`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|102|Boundary OperationalAsset attributes|`OperationalAsset`| `maxPassengers`, `range`, `maxSpeed`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|103|Boundary MeteoOceanographicCondition attributes|`MeteoOceanographicCondition`| `airTemperature`, `cloudCeiling`, `precipitation`, `salinity`, `seaLevelPressure`, `tidalCurrentDirection`, `tidalCurrentSpeed`, `visibility`, `waterTemperature`, `waveDirection`, `waveHeight`, `windCurrentDirection`, `windCurrentSpeed`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|104|Boundary MeteoOceanographicCondition attributes| `Incident` subtype `MaritimeSafetyIncident`, `IrregularMigrationIncident`, `LawInfringementIncident`, `CrisisIncident`| `deathsOnBoard`, `numberOfIIllPersons`|...   _is  defined_
pass:[</br>]
`*THEN*` *attribute* value _must be in accepted range_
|=======================

#98 Boundary Vessel attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of type `Vessel` is defined in *payload*
[] 
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[] 
-- `*AND*` *attribute* `beam` or `breadth` or `containerCapacity` or `deadweight` or `depth` or `draught` or `grossTonnage` or `length` or `lengthenedYear` or `loa` or `netTonnage` or `segregatedBallastVolume` or `yearBuilt`  _is  defined_
pass:[</br>]
`*THEN*` *attribute* _must be in accepted range_ 

|=======================
| *attribute*|_min_      |_max_
|beam |0|70 meter(s)
|breadth |1|70 meter(s)
|containerCapacity |1|30000 TEU (twenty-foot equivalent unit)
|deadweight |1|300000 tonne(s)
|depth |0.99|50.0 meter(s)
|draught |0.0|50.0 meter(s)
|designSpeed|0.0|300.0 knot(s)
|grossTonnage|0.0|400000.0 units(s)
|length |0.99|500.0 meter(s)
|lengthenedYear|1800|2060 (date)
|loa|1.0|500.0 meter(s) 
|netTonnage|0.0|400000.0 units(s)
|segregatedBallastVolume |0.0|50000.0 m3
|yearBuilt|1800|2060 (date)
|=======================

#99 Boundary Vehicule attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of  `Vehicule` subtype `Aircraft` or `LandVehicle` or `Vessel` is defined in *payload*
[] 
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[] 
-- `*AND*` *attribute* `totalPersonsOnBoard` or `maximumSpeed` _is  defined_
pass:[</br>]
`*THEN*`  *attribute* _must be in accepted range_ 

|=======================
| *attribute*|_min_      |_max_
|totalPersonsOnBoard |0|4500 person(s)
|maximumSpeed |0.0|300.0 knot(s)
|=======================

#100 Boundary ContainmentUnit attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of type `ContainmentUnit` is defined in *payload*
[] 
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[] 
-- `*AND*` *attribute* `flashPoint` or `grossQuantity` or `netQuantity` _is  defined_
[]
`*THEN*` *attribute* _must be in accepted range_ 
|=======================
| *attribute*|_min_      |_max_
|flashPoint |-20.0|200.0 Celsius(s)
|grossQuantity |0.0|60000.0 kg(s)
|netQuantity |0.0|60000.0 kg(s)
|=======================

#101 Boundary Catch attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`*GIVEN*` *entity(ies)* of type `Catch` is defined in *payload*
[] 
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[] 
-- `*AND*` *attribute* `catchWeight` or `fishNumber` or `netHeld` or `quantityHeld` or `sizeDeclaration` or `totalNumber` or `totalWeight` _is  defined_
[]
`*THEN*` *attribute* _must be in accepted range_ 
|=======================
| *attribute*|_min_      |_max_
|catchWeight|0.0|20000000.0 kg(s)
|fishNumber|0|15000 unit(s)
|netHeld|0.0|15000.0 unit(s)
|quantityHeld|0.0|15000.0 unit(s)
|sizeDeclaration|0.0|1500.0 meter(s)
|totalNumber|0|15000 unit(s)
|totalWeight|0.0|20000000.0 kg(s)
|=======================

#102 Boundary OperationalAsset attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
`*GIVEN*` *entity(ies)* of type `OperationalAsset` is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[]
-- `*AND*` *attribute* `maxPassengers` or `range` or `maxSpeed` _is  defined_
[]
`*THEN*` *attribute* _must be in accepted range_ 
|=======================
| *attribute*|_min_      |_max_
|maxPassengers|0|4500 person(s)
|range|0.0|10000.0|km(s)
|maxSpeed|0.0|300.0 knot(s)
|=======================



#103 Boundary MeteoOceanographicCondition attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
`*GIVEN*` *entity(ies)* of type `MeteoOceanographicCondition` is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[]
-- `*AND*` *attribute* `airTemperature` or `cloudCeiling` or `precipitation` or `salinity` or `seaLevelPressure` or `tidalCurrentDirection` or `tidalCurrentSpeed` or `visibility` or `waterTemperature` or `waveDirection` or `waveHeight` or `windCurrentDirection` or `windCurrentSpeed` _is  defined_
[]
`*THEN*` *attribute* _must be in accepted range_ 
|===============================================
| *attribute*|_min_      |_max_ 
|airTemperature|-50.0|50.0 Celsius degree(s)

|cloudCeiling|0|30000 meter(s)

|precipitation|0|1000 millimeters(s)

|salinity|0.0|200.0 g/kg

|seaLevelPressure|400.0|1200.0 hPa

|tidalCurrentDirection|0.0|360.0 degree(s)

|tidalCurrentSpeed|0.0|100.0 knot(s)

|visibility|0.0|12.0 nautical mile(s)

|waterTemperature|0.0|40.0 Celsius degree(s)

|waveDirection|0.0|360.0 degree(s)

|waveHeight|0.0|30.0 meter(s)

|windCurrentDirection|0.0 | 360.0 degree(s)

|windCurrentSpeed|0.0|70.0 meter(s)/second
|===============================================

#104 Boundary OperationalAsset attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
`*GIVEN*` *entity(ies)* of `Incident` subtype  `MaritimeSafetyIncident` or `IrregularMigrationIncident` or `LawInfringementIncident` or `CrisisIncident` is defined in *payload*
[]
-- `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
[]
-- `*AND*` *attribute* `deathsOnBoard` or `numberOfIIllPersons` _is  defined_
[]
`*THEN*` *attribute* _must be in accepted range_ 
|=======================
| *attribute*|_min_      |_max_
|deathsOnBoard|0|7500 person(s)
|numberOfIIllPersons|0|7500 person(s)
|=======================



