package eu.europa.ec.itb.cise.ws.search;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.relationship.Relationship;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * This builder will create a tree out of a Entity with its relationships
 */
public class TreeBuilder {

    private int nodeIdentifierLabeler = 0;

    private static List<Field> getAllFields(Class<?> klass) {
        List<Field> fields = new ArrayList<>();

        fields.addAll(Arrays.asList(klass.getDeclaredFields()));

        if (klass.getSuperclass() != null) {
            fields.addAll(getAllFields(klass.getSuperclass()));
        }
        return fields;
    }

    public Node build(Object ciseObject) {
        return build(ciseObject, null);
    }

    private Node build(Object ciseObject, Node parent) {
        Node currentNode = new Node(ciseObject.getClass().getSimpleName(), parent, ciseObject, nodeIdentifierLabeler);
        nodeIdentifierLabeler++;

        extractListRelFieldObjects(ciseObject)
                .forEach(o -> currentNode.addChild(build(o, currentNode)));

        extractSingleRelObjects(ciseObject)
                .forEach(o -> currentNode.addChild(build(o, currentNode)));

        extractEntityFieldObjects(ciseObject)
                .forEach(o -> currentNode.addChild(build(o, currentNode)));

        return currentNode;
    }

    public List<Object> extractListRelFieldObjects(Object ciseObject) {
        List<Field> fields = getAllFields(ciseObject.getClass());

        return fields.stream().
                peek(f -> f.setAccessible(true)).
                filter(fieldsOfTypeList()).
                map(f -> toObjectList(f, ciseObject)).
                filter(Objects::nonNull).
                filter(listsWithAtLeastOneElement()).
                flatMap(Collection::stream).
                filter(o -> o instanceof Relationship).
                collect(Collectors.toList());
    }

    public List<Object> extractSingleRelObjects(Object ciseObject) {

        List<Field> fields = getAllFields(ciseObject.getClass());

        return fields.stream().
                peek(f -> f.setAccessible(true)).
                map(f -> toObject(f, ciseObject)).
                filter(Objects::nonNull).
                filter(o -> o instanceof Relationship).
                collect(Collectors.toList());
    }

    public List<Object> extractEntityFieldObjects(Object ciseObject) {

        List<Field> fields = getAllFields(ciseObject.getClass());

        return fields.stream().
                peek(f -> f.setAccessible(true)).
                map(f -> toObject(f, ciseObject)).
                filter(Objects::nonNull).
                filter(o -> o instanceof Entity).
                collect(Collectors.toList());

    }

    private Function<List<Object>, Object> toTheFirstElement() {
        return l -> l.get(0);
    }

    private Predicate<List<Object>> listsWithAtLeastOneElement() {
        return l -> !l.isEmpty();
    }

    private Predicate<Field> fieldsOfTypeList() {
        return f -> f.getType().isAssignableFrom(List.class);
    }

    @SuppressWarnings("unchecked")
    private List<Object> toObjectList(Field field, Object entity) {
        try {
            return (List<Object>) field.get(entity);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object toObject(Field field, Object entity) {
        try {
            return field.get(entity);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

}