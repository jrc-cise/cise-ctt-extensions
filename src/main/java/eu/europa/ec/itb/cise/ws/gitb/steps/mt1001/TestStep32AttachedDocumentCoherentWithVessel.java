package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.CertificateDocument;
import eu.cise.datamodel.v1.entity.document.VesselDocument;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;


/**
 * #32 AttachedDocument Coherent With Vessel
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Vessel` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `VesselDocument`, `CertificateDocument`
 */
public class TestStep32AttachedDocumentCoherentWithVessel extends CiseTestStepAttachedDocument {

    public TestStep32AttachedDocumentCoherentWithVessel(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Vessel.class};
        Class[] validAttachedDocumentTypes = new Class[]{VesselDocument.class, CertificateDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_32_ATTACHED_DOCUMENT_COHERENT_WITH_VESSEL);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Vessel vessel = (Vessel) entity;
        List<Objet.DocumentRel> documentRels = vessel.getDocumentRels();
        if (documentRels == null || documentRels.size() == 0) {
            return true;
        }
        for (Objet.DocumentRel documentRel : documentRels) {
            if (!(isInstanceOfOneOf(documentRel.getDocument(), validAttachedDocumentTypes))) {
                return false;
            }
        }

        return true;
    }

}
