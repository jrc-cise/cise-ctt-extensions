package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.ServiceTypeMainClassMap;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.List;

public class TestStep08ServiceTypeToMainEntity extends CiseTestStep {

    public TestStep08ServiceTypeToMainEntity(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_08_SERVICE_TYPE_TO_MAIN_ENTITY;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        Class<? extends Entity> mainEntitySuperClass = getMainEntitySuperClass(messageXml);
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, mainEntitySuperClass);
        if (payloadEntitiesOfType.isEmpty()) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), failureExplanation, "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        } else {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), reportBuilder.REPORT_STEP_SUCCESS, "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;
    }

    private Class<? extends Entity> getMainEntitySuperClass(String messageXml) {
        Message message = reportBuilder.getXmlMapper().fromXML(messageXml);
        ServiceTypeMainClassMap serviceTypeMainClassMap = new ServiceTypeMainClassMap();
        ServiceType senderServiceType = message.getSender().getServiceType();
        return serviceTypeMainClassMap.toEntityClass(senderServiceType);
    }
}
