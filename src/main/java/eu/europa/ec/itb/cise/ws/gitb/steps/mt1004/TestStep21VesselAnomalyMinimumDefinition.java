package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #21 VesselAnomaly Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` its *attribute* `AnomalyType` have value :  `VESSEL_OUT_OF_TRAFFIC_LANES` or `VESSEL_WITH_ERRATIC_MOVEMENTS` or `DETECION_OF_CHANGES_IN_AIS_PARAMETERS` or `PERFORMING_AIS_SPOOFING` or `WITHOUT_AIS_TRANSMISSION` or `DO_NOT_ANSWER_ON_VHF_CH_16`
 * `*THEN*` it must define at least one relation to an entity of type `Vessel`
 */
public class TestStep21VesselAnomalyMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep21VesselAnomalyMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Anomaly.class;
        Class[] linkedClass = new Class[]{Vessel.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_21_VESSEL_ANOMALY_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        Anomaly anomaly = (Anomaly) event;
        return anomaly.getAnomalyType() == AnomalyType.VESSEL_OUT_OF_TRAFFIC_LANES ||
                anomaly.getAnomalyType() == AnomalyType.VESSEL_WITH_ERRATIC_MOVEMENTS ||
                anomaly.getAnomalyType() == AnomalyType.DETECION_OF_CHANGES_IN_AIS_PARAMETERS ||
                anomaly.getAnomalyType() == AnomalyType.PERFORMING_AIS_SPOOFING ||
                anomaly.getAnomalyType() == AnomalyType.WITHOUT_AIS_TRANSMISSION ||
                anomaly.getAnomalyType() == AnomalyType.DO_NOT_ANSWER_ON_VHF_CH_16;
    }

}
