package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.core.Configuration;

import java.util.List;

public class GitbConfigHelper {
    public GitbConfigHelper() {
    }

    /**
     * Get a configuration value.
     *
     * @param configs The set of configuration values.
     * @param name    The name to look for.
     * @return The configured value (null if missing).
     */
    public static String getConfigValue(List<Configuration> configs, String name) {
//        Configuration config = Stream.of(configs)
//                .filter(e -> e.getName().equal(name))
//                .findFirst()
//                .orElse(null);
//        return config.getValue();
        if (configs != null) {
            for (Configuration config : configs) {
                if (name.equals(config.getName())) {
                    return config.getValue();
                }
            }
        }
        return null;
    }
}