package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;


public abstract class CiseTestStep {

    protected final String describeCTXwarning = "\n * XML content returned via parser as error context may differ slightly from the original.";
    protected ReportBuilder reportBuilder;
    protected CisePayloadHelper payloadHelper;
    protected TAR report;
    protected GitbErrorHelper errorHelper;

    public CiseTestStep(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        this.reportBuilder = reportBuilder;
        this.payloadHelper = payloadHelper;
        this.errorHelper = errorHelper;
    }


    public TAR createReport(String xmlMessage) {
        return null;
    }

    public TAR createReport(String xmlMessage, String sentParams) {
        return null;
    }


    protected boolean isInstanceOfOneOf(Entity entity, Class[] linkedEntityClasses) {
        if (entity == null) {
            return false;
        }
        for (Class linkedEntityClass : linkedEntityClasses) {
            if (linkedEntityClass.isAssignableFrom(entity.getClass())) {
                return true;
            }
        }
        return false;
    }

}
