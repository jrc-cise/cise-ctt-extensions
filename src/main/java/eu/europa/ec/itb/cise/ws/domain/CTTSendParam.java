package eu.europa.ec.itb.cise.ws.domain;

import java.util.Objects;

/**
 * This class is a value object that contains the xml elements
 * to be overridden in the message.
 * <p>
 * The requireAck is a mandatory field
 * The messageId is a mandatory field
 * The correlationId is optional
 */
public class CTTSendParam extends SendParam {

    private String destinationEndpoint;

    public Boolean getRequiresAck() {
        return requiresAck;
    }

    public void setRequiresAck(Boolean requiresAck) {
        this.requiresAck = requiresAck;
    }

    private Boolean requiresAck;

    public CTTSendParam(Boolean requiresAck, String messageId, String correlationId, String destinationEndpoint) {
        super((requiresAck == null ? false : requiresAck), messageId, correlationId);
        this.requiresAck = requiresAck;
        this.destinationEndpoint = destinationEndpoint;
    }

    public String getDestinationEndpoint() {
        return destinationEndpoint;
    }

    public void setDestinationEndpoint(String destinationEndpoint) {
        this.destinationEndpoint = destinationEndpoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CTTSendParam that = (CTTSendParam) o;
        return Objects.equals(destinationEndpoint, that.destinationEndpoint) &&
                Objects.equals(requiresAck, that.requiresAck);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), destinationEndpoint, requiresAck);
    }
}
