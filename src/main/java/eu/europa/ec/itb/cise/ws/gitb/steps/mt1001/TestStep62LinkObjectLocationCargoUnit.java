package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.CargoUnit;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.LocationRoleType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedObjet;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #62 LinkObjectLocation CargoUnit
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Cargo` or `ContainmentUnit` or `Catch` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it _must not define_ attribute `COGHeading`,`PlannedOperation`,`PlannedWork`,`SensorType`,`SOGSpeed`
 * `*AND*` it _must not have predetermined value for_ attribute `LocationRole` in `PORT_OF_DISCHARGE`, `LENGTHENED_PLACE`, `PORT_OF_EMBARKATION`, `PORT_OF_DISEMBARKATION`
 */

public class TestStep62LinkObjectLocationCargoUnit extends CiseTestStepLinkedObjet {

    public TestStep62LinkObjectLocationCargoUnit(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Cargo.class, ContainmentUnit.class, Catch.class};
        Class[] linkedClass = new Class[]{Location.class};
        return createReport(messageXml, entityClasses, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_62_LINK_OBJECT_LOCATION_CARGOUNIT);
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        CargoUnit cargoUnit = (CargoUnit) entity;

        for (Objet.LocationRel locationRel : cargoUnit.getLocationRels()) {
            if (locationRel.getCOG() != null ||
                    locationRel.getHeading() != null ||
                    (locationRel.getPlannedOperations() != null && locationRel.getPlannedOperations().size() > 0) ||
                    locationRel.getPlannedWorks() != null ||
                    locationRel.getSensorType() != null ||
                    locationRel.getSOG() != null ||
                    locationRel.getSpeed() != null ||
                    (locationRel.getLocationRole() != null &&
                            (locationRel.getLocationRole() == LocationRoleType.PORT_OF_DISCHARGE ||
                                    locationRel.getLocationRole() == LocationRoleType.LENGTHENED_PLACE ||
                                    locationRel.getLocationRole() == LocationRoleType.PORT_OF_EMBARKATION ||
                                    locationRel.getLocationRole() == LocationRoleType.PORT_OF_DISEMBARKATION
                            )
                    )
            ) {
                return false;
            }
        }

        return true;
    }
}
