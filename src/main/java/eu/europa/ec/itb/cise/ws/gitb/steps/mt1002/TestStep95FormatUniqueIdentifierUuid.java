package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.uniqueidentifier.UniqueIdentifier;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;
import java.util.UUID;

/**
 * #95 Format Document UniqueIdentifier UUID
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Document` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `UniqueIdentifier` _is  defined_
 * `*THEN*` its *attribute* `UUID` _must be filled_ in standard 8-4-4-4-12 digits format
 */
public class TestStep95FormatUniqueIdentifierUuid extends CiseTestStep {

    public TestStep95FormatUniqueIdentifierUuid(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_95_FORMAT_UNIQUE_IDENTIFIER_UUID;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        try {
            boolean conform = true;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Document.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof Document) {
            UniqueIdentifier uniqueIdentifier = ((Document) entity).getIdentifier();
            if (uniqueIdentifier != null) {
                if (uniqueIdentifier.getUUID() == null)
                    return false; // required for model integrity validation > inv: UniqueIdentifier->allInstances()->forAll(n1, n2| n1.UUID<>n2.UUID)
                UUID uuid = new UUID(uniqueIdentifier.getUUID());
                return (uuid.isValid());
            } else return true;
        }
        return true;
    }

    // class to resolve UUID serialized value
    private class UUID {
        String valueUUID;

        UUID(String valueUUID) {
            this.valueUUID = valueUUID;
        }

        public boolean isNotNull() {
            return valueUUID != null;
        }

        public boolean isValid() {
            try {
                java.util.UUID uuidReal = java.util.UUID.fromString(valueUUID);
            } catch (Exception e) {
                return false;
            }
            return true;
        }
    }
}
