package eu.europa.ec.itb.cise.ws.gitb.steps;

import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1002.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.MetaTestStep1003DataFieldsBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.event.TestStep104BoundaryIncident;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.meteo.TestStep103BoundaryMeteoOceanCondition;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.*;

public enum CiseTestStepsEnum {
    REPORT_ELEMENT_SERVICE_TS_02_XML_VALIDATION("ts02XMLValidation", TestStep02XMLValidation.class),
    REPORT_ELEMENT_SERVICE_TS_03_MESSAGEID_UNIQUENESS("ts03MessageIdUniqueness", TestStep03MessageIdUniqueness.class),
    REPORT_ELEMENT_SERVICE_TS_04_CREATION_DATE("ts04CreationDate", TestStep04CreationDate.class),
    REPORT_ELEMENT_SERVICE_TS_08_SERVICE_TYPE_TO_MAIN_ENTITY("ts08ServiceTypeToMainEntity", TestStep08ServiceTypeToMainEntity.class),
    REPORT_ELEMENT_MODEL_TS_10_VESSEL_MIN_IDENT("ts10VesselMinimumIdentification", TestStep10VesselMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_11_PERSON_MIN_IDENT("ts11PersonMinimumIdentification", TestStep11PersonMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_12_ORGANISATION_MIN_IDENT("ts12OrganisationMinimumIdentification", TestStep12OrganisationMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_13_ORGANISATIONUNIT_MIN_IDENT("ts13OrganisationunitMinimumIdentification", TestStep13OrganisationunitMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_14_PORTORGANIZATION_MIN_IDENT("ts14PortOrganizationMinimumIdentification", TestStep14PortOrganizationMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_15_FORMAL_ORGANIZATION_MIN_IDENT("ts15FormalOrganizationMinimumIdentification", TestStep15FormalOrganizationMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_16_AIRCRAFT_AND_LANDVEHICLE_MIN_IDENT("ts16AircraftAndLandvehicleMinimumIdentification", TestStep16AircraftAndLandvehicleMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_17_ANOMALY_TYPE("ts17AnomalyType", TestStep17AnomalyType.class),
    REPORT_ELEMENT_MODEL_TS_18_UNEXPECTED_MOVEMENT_MIN_DEF("ts18UnexpectedMovementMinimumDefinition", TestStep18UnexpectedMovementMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF("ts19CargoLeakingMinimumDefinition", TestStep19CargoLeakingMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_20_SHIFTING_OF_CARGO_MIN_DEF("ts20ShiftingOfCargoMinimumDefinition", TestStep20ShiftingOfCargoMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_21_VESSEL_ANOMALY_MIN_DEF("ts21VesselAnomalyMinimumDefinition", TestStep21VesselAnomalyMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_22_STAIN_OF_OIL_SIGHTED_MIN_DEF("ts22StainOfOilSightedMinimumDefinition", TestStep22StainOfOilSightedMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_24_ATTACHED_DOCUMENT_MIN_DEF("ts24AttachedDocumentMinimumDefinition", TestStep24AttachedDocumentMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_25_IDENTIFICATION_OF_DOCUMENT_TYPE("ts25IdentificationOfDocumentType", TestStep25IdentificationOfDocumentType.class),
    REPORT_ELEMENT_MODEL_TS_26_STREAM_MIN_DEFINITION("ts26StreamMinimumDefinition", TestStep26StreamMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_27_POLLUTION_INCIDENT_MIN_DEF("ts27PollutionIncidentMinimumDefinition", TestStep27PollutionIncidentMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_28_POLLUTION_INCIDENT_TYPE("ts28PollutionIncidentType", TestStep28PollutionIncidentType.class),
    REPORT_ELEMENT_MODEL_TS_29_POLLUTION_INCIDENT_MIN_DEF("ts29PollutionIncidentMinimumDefinition", TestStep29PollutionIncidentMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_30_VESSEL_INCIDENTS_MIN_DEF("ts30VesselIncidentsMinimumDefinition", TestStep30VesselIncidentsMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_31_INCIDENT_MIN_DEF("ts31IncidentMinimumDefinition", TestStep31IncidentMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_32_ATTACHED_DOCUMENT_COHERENT_WITH_VESSEL("ts32AttachedDocumentCoherentWithVessel", TestStep32AttachedDocumentCoherentWithVessel.class),
    REPORT_ELEMENT_MODEL_TS_33_ATTACHED_DOCUMENT_COHERENT_WITH_LOCATION("ts33AttachedDocumentCoherentWithLocation", TestStep33AttachedDocumentCoherentWithLocation.class),
    REPORT_ELEMENT_MODEL_TS_34_ATTACHED_DOCUMENT_COHERENT_WITH_EVENT("ts34AttachedDocumentCoherentWithEvent", TestStep34AttachedDocumentCoherentWithEvent.class),
    REPORT_ELEMENT_MODEL_TS_35_ATTACHED_DOCUMENT_COHERENT_WITH_CARGO("ts35AttachedDocumentCoherentWithCargo", TestStep35AttachedDocumentCoherentWithCargo.class),
    REPORT_ELEMENT_MODEL_TS_36_ATTACHED_DOCUMENT_COHERENT_WITH_ORGANIZATION("ts36AttachedDocumentCoherentWithOrganization", TestStep36AttachedDocumentCoherentWithOrganization.class),
    REPORT_ELEMENT_MODEL_TS_37_ATTACHED_DOCUMENT_COHERENT_WITH_RISK("ts37AttachedDocumentCoherentWithRisk", TestStep37AttachedDocumentCoherentWithRisk.class),
    REPORT_ELEMENT_MODEL_TS_38_ATTACHED_DOCUMENT_COHERENT_WITH_PERSON("ts38AttachedDocumentCoherentWithPerson", TestStep38AttachedDocumentCoherentWithPerson.class),
    REPORT_ELEMENT_MODEL_TS_39_LOCATION_MIN_DEF("ts39LocationMinimumDefinition", TestStep39LocationMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_40_GEOMETRY_MIN_DEF("ts40GeometryMinimumDefinition", TestStep40GeometryMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_41_WKT_VALIDITY("ts41WKTValidity", TestStep41WKTValidity.class),
    REPORT_ELEMENT_MODEL_TS_42_XMLGEOMETRY_VALIDITY("ts42XMLGeometryValidity", TestStep42XMLGeometryValidity.class),
    REPORT_ELEMENT_MODEL_TS_43_NAMED_LOCATION_MIN_DEF("ts43NamedLocationMinimumDefinition", TestStep43NamedLocationMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_44_PORT_LOCATION_MIN_DEF("ts44PortLocationMinimumDefinition", TestStep44PortLocationMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_45_PORT_FACILITY_LOCATION_MIN_DEF("ts45PortFacilityLocationMinimumDefinition", TestStep45PortFacilityLocationMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_46_OBJECTLOCATION_PERIODOFTIME("ts46ObjectLocationPeriodOfTime", TestStep46ObjectLocationPeriodOfTime.class),
    REPORT_ELEMENT_MODEL_TS_47_CHECK_VESSEL_IMO_NUMBER("ts47CheckVesselIMONumber", TestStep47CheckVesselIMONumber.class),
    REPORT_ELEMENT_MODEL_TS_48_CHECK_VESSEL_MMSI("ts48CheckVesselMMSI", TestStep48CheckVesselMMSI.class),
    REPORT_ELEMENT_MODEL_TS_49_VESSEL_LOCATION_PULL_RESPONSE("ts49VesselLocationPullResponse", TestStep49VesselLocationPullResponse.class),
    REPORT_ELEMENT_MODEL_TS_50_CHECK_EVENT_VESSEL_LOCATION("ts50CheckEventVesselLocation", TestStep50CheckEventVesselLocation.class),
    REPORT_ELEMENT_MODEL_TS_51_CHECK_EVENT_VESSEL_MMSI("ts51CheckEventVesselMMSI", TestStep51CheckEventVesselMMSI.class),
    REPORT_ELEMENT_MODEL_TS_52_CHECK_EVENT_VESSEL_IMO_NUMBER("ts52CheckEventVesselIMONumber", TestStep52CheckEventVesselIMONumber.class),
    REPORT_ELEMENT_MODEL_TS_53_CHECK_RISK_VESSEL_LOCATION("ts53CheckRiskVesselLocation", TestStep53CheckRiskVesselLocation.class),
    REPORT_ELEMENT_MODEL_TS_54_CHECK_RISK_VESSEL_MMSI("ts54CheckRiskVesselMMSI", TestStep54CheckRiskVesselMMSI.class),
    REPORT_ELEMENT_MODEL_TS_55_CHECK_RISK_VESSEL_IMO_NUMBER("ts55CheckRiskVesselIMONumber", TestStep55CheckRiskVesselIMONumber.class),
    REPORT_ELEMENT_MODEL_TS_57_ACTION_MIN_DEF("ts57ActionMinimumDefinition", TestStep57ActionMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_58_MOVEMENT_MIN_DEF("ts58MovementMinimumDefinition", TestStep58MovementMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_59_METEOOCEANOGRAPHICCONDITION_MIN_IDENT("ts59MeteoOceanographicConditionMinimumIdentification", TestStep59MeteoOceanographicConditionMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_60_METEOOCEANOGRAPHICCONDITION_MIN_DEF("ts60MeteoOceanographicConditionMinimumDefinition", TestStep60MeteoOceanographicConditionMinimumDefinition.class),
    REPORT_ELEMENT_MODEL_TS_61_RISK_MINIMUM_IDENTIFICATION("ts61RiskMinimumIdentification", TestStep61RiskMinimumIdentification.class),
    REPORT_ELEMENT_MODEL_TS_62_LINK_OBJECT_LOCATION_CARGOUNIT("ts62LinkObjectLocationCargoUnit", TestStep62LinkObjectLocationCargoUnit.class),
    REPORT_ELEMENT_MODEL_TS_63_LINK_OBJECT_LOCATION_VEHICLE("ts63LinkObjectLocationVehicle", TestStep63LinkObjectLocationVehicle.class),
    REPORT_ELEMENT_MODEL_TS_64_LINK_OBJECT_AGENT_ORGANIZATION("ts64LinkObjectAgentOrganization", TestStep64LinkObjectAgentOrganization.class),
    REPORT_ELEMENT_MODEL_TS_65_LINK_OBJECT_AGENT_VEHICLE("ts65LinkObjectAgentVehicle", TestStep65LinkObjectAgentVehicle.class),
    REPORT_ELEMENT_MODEL_TS_66_LINK_OBJECT_AGENT_CARGOUNIT("ts66LinkObjectAgentCargoUnit", TestStep66LinkObjectAgentCargoUnit.class),
    REPORT_ELEMENT_MODEL_TS_67_LINK_OBJECT_EVENT_CARGOUNIT("ts67LinkObjectEventCargoUnit", TestStep67LinkObjectEventCargoUnit.class),
    REPORT_ELEMENT_MODEL_TS_68_LINK_OBJECT_MOVEMENT("ts68LinkObjectMovement", TestStep68LinkObjectMovement.class),
    REPORT_ELEMENT_MODEL_TS_69_LINK_OBJECT_ANOMALY("ts69LinkObjectAnomaly", TestStep69LinkObjectAnomaly.class),
    REPORT_ELEMENT_MODEL_TS_70_LINK_MOVEMENT_AGENT("ts70LinkMovementAgent", TestStep70LinkMovementAgent.class),
    REPORT_ELEMENT_MODEL_TS_71_LINK_ANOMALY_AGENT("ts71LinkAnomalyAgent", TestStep71LinkAnomalyAgent.class),
    REPORT_ELEMENT_MODEL_TS_72_LINK_MOVEMENT_LOCATION("ts72LinkMovementLocation", TestStep72LinkMovementLocation.class),
    REPORT_ELEMENT_MODEL_TS_73_LINK_ORGANIZATION_LOCATION("ts73LinkOrganizationLocation", TestStep73LinkOrganizationLocation.class),
    REPORT_ELEMENT_MODEL_TS_74_LINK_PERSON_PERSON("ts74LinkPersonPerson", TestStep74LinkPersonPerson.class),
    REPORT_ELEMENT_MODEL_TS_75_LINK_ORGANIZATION_ORGANIZATION("ts75LinkOrganizationOrganization", TestStep75LinkOrganizationOrganization.class),
    REPORT_ELEMENT_MODEL_TS_76_FORMAT_VESSEL_CALLSIGN("ts76FormatVesselCallsign", TestStep76FormatVesselCallsign.class),
    REPORT_ELEMENT_MODEL_TS_77_FORMAT_VESSEL_IMONUMBER("ts77FormatVesselImonumber", TestStep77FormatVesselImonumber.class),
    REPORT_ELEMENT_MODEL_TS_78_FORMAT_VESSEL_MMSI("ts78FormatVesselMmsi", TestStep78FormatVesselMmsi.class),
    REPORT_ELEMENT_MODEL_TS_79_FORMAT_VESSEL_NAME("ts79FormatVesselName", TestStep79FormatVesselName.class),
    REPORT_ELEMENT_MODEL_TS_80_FORMAT_VESSEL_INMARSAT("ts80FormatVesselInMarsat", TestStep80FormatVesselInmarsat.class),
    REPORT_ELEMENT_MODEL_TS_82_FORMAT_VESSEL_NATIONALITY("ts82FormatVesselNationality", TestStep82FormatVesselNationality.class),
    REPORT_ELEMENT_MODEL_TS_83_FORMAT_CONT_UNIT_UNDG("ts83FormatContUnitUNDG", TestStep83FormatContUnitUNDG.class),
    REPORT_ELEMENT_MODEL_TS_84_FORMAT_CONT_UNIT_LOC_ON_BOARD_CONTAINER("ts84FormatContUnitLocOnBoardContainer", TestStep84FormatContUnitLocOnBoardContainer.class),
    REPORT_ELEMENT_MODEL_TS_85_FORMAT_CONT_UNIT_CONTAINER_MARK_AND_NUMBER("ts85FormatContUnitContainerMarkAndNumber", TestStep85FormatContUnitContainerMarkAndNumber.class),
    REPORT_ELEMENT_MODEL_TS_86_FORMAT_CATCH_SPECIES("ts86FormatCatchSpecies", TestStep86FormatCatchSpecies.class),
    REPORT_ELEMENT_MODEL_TS_87_FORMAT__PORTLOCATION_LOCATIONCODE("ts87FormatPortlocationLocationcode", TestStep87FormatPortlocationLocationcode.class),
    REPORT_ELEMENT_MODEL_TS_88_FORMAT_LATITUDE_LONGITUDE("ts88FormatLatitudeLongitude", TestStep88FormatLatitudeLongitude.class),
    REPORT_ELEMENT_MODEL_TS_89_FORMAT_NAMED_LOCATION_GEOGRAPHIC_IDENTIFIER("ts89FormatNamedLocationGeographicIdentifier", TestStep89FormatNamedLocationGeographicIdentifier.class),
    REPORT_ELEMENT_MODEL_TS_90_FORMAT_PORTFACILITY_PORTFACILITYNUMBER("ts90FormatPortfacilityPortfacilitynumber", TestStep90FormatPortfacilityPortfacilitynumber.class),
    REPORT_ELEMENT_MODEL_TS_93_FORMAT_METADATA_LANGUAGE("ts93FormatMetadataLanguage", TestStep93FormatMetadataLanguage.class),
    REPORT_ELEMENT_MODEL_TS_94_FORMAT_METADATA_DESIGNATION("ts94FormatMetadataDesignation", TestStep94FormatMetadataDesignation.class),
    REPORT_ELEMENT_MODEL_TS_95_FORMAT_UNIQUE_IDENTIFIER_UUID("ts95FormatUniqueIdentifierUuid", TestStep95FormatUniqueIdentifierUuid.class),
    REPORT_ELEMENT_MODEL_TS_96_FORMAT_AGENT_CONTACTINFORMATION("ts96FormatAgentContactinformation", TestStep96FormatAgentContactinformation.class),
    REPORT_ELEMENT_MODEL_TS_97_FORMAT_AGENT_NATIONALITY("ts97FormatAgentNationality", TestStep97FormatAgentNationality.class),
    REPORT_ELEMENT_MODEL_TS_98_BOUNDARY_VESSEL("ts98BoundaryVessel", TestStep98BoundaryVessel.class),
    REPORT_ELEMENT_MODEL_TS_99_BOUNDARY_VEHICLE("ts99BoundaryVehicle", TestStep99BoundaryVehicle.class),
    REPORT_ELEMENT_MODEL_TS_100_BOUNDARY_CONTAINMENTUNIT("ts100BoundaryContainmentUnit", TestStep100BoundaryContainmentUnit.class),
    REPORT_ELEMENT_MODEL_TS_101_BOUNDARY_CONTAINEDCATCH("ts101BoundaryContainedCatch", TestStep101BoundaryContainedCatch.class),
    REPORT_ELEMENT_MODEL_TS_102_BOUNDARY_OPERATIONALASSET("ts102BoundaryOperationalAsset", TestStep102BoundaryOperationalAsset.class),
    REPORT_ELEMENT_MODEL_TS_103_BOUNDARY_METEOOCEANCONDITION("ts103BoundaryMeteoOceanCondition", TestStep103BoundaryMeteoOceanCondition.class),
    REPORT_ELEMENT_MODEL_TS_104_BOUNDARY_INCIDENT("ts104BoundaryIncidentTest", TestStep104BoundaryIncident.class),
    REPORT_ELEMENT_META_MTS_1000_ENTITY_MIN_DEF("mts1000EntityMinimumDefinition", MetaTestStep1000EntityMinimumDefinition.class),
    REPORT_ELEMENT_META_MTS_1001_COHERENCE_ENTITIES_ATTRIBUTES("mts1001EntityAttributeCoherence", MetaTestStep1001CoherenceEntitiesAttributes.class),
    REPORT_ELEMENT_META_MTS_1002_DATA_FIELDS_FORMAT("mts1002DataFieldsFormat", MetaTestStep1002DataFieldsFormat.class),
    REPORT_ELEMENT_META_MTS_1003_DATA_FIELDS_BOUNDARY("mts1003DataFieldsBoundary", MetaTestStep1003DataFieldsBoundary.class),
    REPORT_ELEMENT_META_MTS_1004_ENTITY_MIN_DEF_LINKED("mts1004EntityMinimumDefinitionLinked", MetaTestStep1004EntityMinimumDefinitionLinked.class);


    private final String uiCheckName;
    private final Class testTestImpl;

    CiseTestStepsEnum(String uiCheckName, Class testTestImpl) {
        this.uiCheckName = uiCheckName;
        this.testTestImpl = testTestImpl;
    }

    public Class getTestTestImpl() {
        return testTestImpl;
    }

    public String getUiCheckName() {
        return uiCheckName;
    }

    public static CiseTestStepsEnum getValueFromUiCheckName(String uiCheckName) {
        for (CiseTestStepsEnum value : values()) {
            if (value.getUiCheckName().equals(uiCheckName)) {
                return value;
            }
        }
        return null;
    }

    public static Class getClassFromUiCheckName(String uiCheckName) {
        for (CiseTestStepsEnum value : values()) {
            String uiCheckNameNormalized = uiCheckName.replaceAll("[^a-zA-Z0-9_-]", "");
            if (value.getUiCheckName().equals(uiCheckNameNormalized)) {
                return value.getTestTestImpl();
            }
            /*inform non equivalence in string if fail after normalization*/
            if (!(uiCheckName.equals(uiCheckNameNormalized)))
                System.out.println("WARNING ! altered content parameter 'type'  passed to ciseValidationService contained non standard characters ");
        }
        return null;
    }


}
