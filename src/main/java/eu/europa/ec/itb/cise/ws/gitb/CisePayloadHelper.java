package eu.europa.ec.itb.cise.ws.gitb;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.search.ContextResolver;
import eu.europa.ec.itb.cise.ws.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class CisePayloadHelper {
    private XmlMapper xmlMapper;

    public CisePayloadHelper() {
    }

    public CisePayloadHelper(XmlMapper xmlMapper) {
        this.xmlMapper = xmlMapper;
    }

    public List<Object> getPayloadObjects(String receivedMessageXml) {
        Message receivedMessage = xmlMapper.fromXML(receivedMessageXml);
        XmlEntityPayload receivedMessagePayload = (XmlEntityPayload) receivedMessage.getPayload();
        return receivedMessagePayload.getAnies();
    }

    public List<Pair<Entity, String>> getPayloadEntitiesOfType(String messageXml, Class targetClass) {
        List<Pair<Entity, String>> foundEntitiesWithReferences = new ArrayList<>();

        List<Object> payloadObjects = getPayloadObjects(messageXml);
        for (Object payloadObject : payloadObjects) {
            foundEntitiesWithReferences.addAll(ContextResolver.findCiseObjectsWithReferences(targetClass, (Entity) payloadObject));
        }

        return foundEntitiesWithReferences;
    }

    public List<Pair<Entity, String>> getPayloadEntitiesOfTypes(String messageXml, List<Class> targetClasses) {
        List<Pair<Entity, String>> foundEntitiesWithReferences = new ArrayList<>();
        List<Object> payloadObjects = getPayloadObjects(messageXml);
        for (Class targetClass : targetClasses) {
            for (Object payloadObject : payloadObjects) {
                foundEntitiesWithReferences.addAll(ContextResolver.findCiseObjectsWithReferences(targetClass, (Entity) payloadObject));
            }
        }
        return foundEntitiesWithReferences;
    }

    public List<Pair<Relationship, String>> getPayloadRelationOfType(String messageXml, Class targetClass) {
        List<Pair<Relationship, String>> foundRelationsWithReferences = new ArrayList<>();

        List<Object> payloadObjects = getPayloadObjects(messageXml);
        for (Object payloadObject : payloadObjects) {
            foundRelationsWithReferences.addAll(ContextResolver.findCiseObjectsWithReferences(targetClass, (Entity) payloadObject));
        }

        return foundRelationsWithReferences;
    }

    public Entity getParentOfType(String messageXml, String breadcrumb, Class classToLookFor) {
        for (Object payloadObject : getPayloadObjects(messageXml)) {

            Entity parentOfType = ContextResolver.findParentOfType(classToLookFor, breadcrumb, (Entity) payloadObject);

            if (parentOfType != null) {
                return parentOfType;
            }
        }

        return null;
    }
}
