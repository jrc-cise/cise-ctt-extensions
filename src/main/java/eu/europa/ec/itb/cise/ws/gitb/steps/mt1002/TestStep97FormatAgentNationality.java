package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #97 Format Agent Nationality
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Agent` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `Nationality` _is  defined_
 * `*THEN*` *attribute* `Nationality` _must be composed_ with 2 upper characters format
 * `*AND*` *attribute* `Nationality` _should be present_ in ISO-3166 reference list.
 */
public class TestStep97FormatAgentNationality extends CiseTestStep {

    public TestStep97FormatAgentNationality(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_97_FORMAT_AGENT_NATIONALITY;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());


        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfTypes(messageXml, Arrays.asList(Agent.class, Person.class, Organization.class, PortOrganization.class));
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        boolean wellFormated = true;
        if (entity instanceof Agent) {
            for (String nationality : ((List<String>) ((Agent) entity).getNationalities())) {
                if (nationality != null) {
                    wellFormated = (wellFormated && (
                            (nationality.trim().length() == 3) &&
                                    (nationality.trim().matches("^[A-Z]+$"))
                    ));
                }
            }
            return wellFormated;
        }
        return true;
    }

    /**
     * TODO: create the logic (return false)
     * String.format("%.20f", a));
     *
     * @param entity
     * @return
     */
    private boolean warningValid(Entity entity) {
        if (entity instanceof Agent) {
            boolean wellFormated = true;
            for (String nationality : ((List<String>) ((Agent) entity).getNationalities())) {
                wellFormated = (wellFormated && (
                        ValidNationalCode.getLitteralForNatCode3(nationality) != null
                ));
            }
            return wellFormated;
        }
        return true;
    }

}
