package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 *
 #22 StainOfOilSighted Minimum Definition
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 `*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 `*AND*` its *attribute* `AnomalyType` have value :  `STAIN_OF_OIL_SIGHTED`
 `*THEN*` it must define at least one relation to an entity of type `Location`
 */
public class TestStep22StainOfOilSightedMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep22StainOfOilSightedMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }


    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Anomaly.class;
        Class[] linkedClass = new Class[]{Location.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_22_STAIN_OF_OIL_SIGHTED_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        Anomaly anomaly = (Anomaly) event;
        return AnomalyType.STAIN_OF_OIL_SIGHTED == anomaly.getAnomalyType();
    }


}
