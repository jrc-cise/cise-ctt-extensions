package eu.europa.ec.itb.cise.ws.search;

import eu.cise.datamodel.v1.entity.Entity;

/**
 * This class should match the input (the template coming from the service
 * specified in the pull request) and the output (the template stored in the service
 * registry that describes the data that will be returned in the pull response)
 */
public class TemplateMatcher {

    /**
     * The input and the output matches when:
     * <p>
     * - there is an exact match
     * - there is a partial match of the input with the output
     * - there is a exact/partial match with an attribute filtering
     *
     * @param input  the input template coming from the pull request
     * @param output the output template recorded in the registry
     * @return true if the data provided by the service fulfill the input
     * proposed by the pull request
     */
    public boolean matches(Entity input, Entity output) {
        return Tree.build(input, 3).bestMatch(Tree.build(output, 3))
                && Tree.build(input).bestMatch(Tree.build(input, 3));
    }

    /**
     * The input and the output matches when:
     * <p>
     * - there is an exact match
     * - there is a exact match with an attribute filtering
     *
     * @param input  the input template coming from the pull request
     * @param output the output template recorded in the registry
     * @return true if the data provided by the service fulfill the input
     * proposed by the pull request
     */
    public boolean exactMatches(Entity input, Entity output) {
        return Tree.build(input, 3).exactMatch(Tree.build(output, 3));
    }

}