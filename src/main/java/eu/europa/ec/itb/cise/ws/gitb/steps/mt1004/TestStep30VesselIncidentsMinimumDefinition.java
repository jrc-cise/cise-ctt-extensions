package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncidentType;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 *#30 VesselIncidents Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `incidents` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it define attribute `MaritimeSafetyIncidentType` with  predefined value in : `VTS_RULES_INFRINGEMENT` or `BANNED_SHIP` or `INSURANCE_FAILURE` or `PILOT_OR_PORT_REPORT`
 * or `COLLISION` or `CAPSIZING` or `ENGINE_FAILURE` or `STRUCTURAL_FAILURE` or `STEERING_GEAR_FAILURE` or `ELECTRICAL_GENERATING_SYSTEM_FAILURE` or `NAVIGATION_EQUIPMENT_FAILURE`
 * or `NAVIGATION_EQUIPMENT_FAILURE` or `COMMUNICATION_EQUIPMENT_FAILURE` or `INCIDENT_NATURE_ABANDON_SHIP` or `INCIDENT_NATURE_SINKING` or `DETAINED_SHIP`
 * `*THEN*` it must define at least one of relation to *entity(ies)* of type `Vehicle` (+ its children entities)
 */
public class TestStep30VesselIncidentsMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep30VesselIncidentsMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = MaritimeSafetyIncident.class;
        Class[] linkedClass = new Class[]{Vehicle.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_30_VESSEL_INCIDENTS_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        MaritimeSafetyIncident maritimeSafetyIncident = (MaritimeSafetyIncident) event;
        MaritimeSafetyIncidentType maritimeSafetyIncidentType = maritimeSafetyIncident.getMaritimeSafetyIncidentType();
        if (maritimeSafetyIncidentType == null) {
            return false;
        }
        switch (maritimeSafetyIncidentType) {
            case VTS_RULES_INFRINGEMENT:
            case BANNED_SHIP:
            case INSURANCE_FAILURE:
            case PILOT_OR_PORT_REPORT:
            case COLLISION:
            case CAPSIZING:
            case ENGINE_FAILURE:
            case STRUCTURAL_FAILURE:
            case STEERING_GEAR_FAILURE:
            case ELECTRICAL_GENERATING_SYSTEM_FAILURE:
            case NAVIGATION_EQUIPMENT_FAILURE:
            case COMMUNICATION_EQUIPMENT_FAILURE:
            case INCIDENT_NATURE_ABANDON_SHIP:
            case INCIDENT_NATURE_SINKING:
            case DETAINED_SHIP:
                return true;
        }

        return false;
    }

}
