package eu.europa.ec.itb.cise.ws.util;

import org.apache.cxf.configuration.security.ProxyAuthorizationPolicy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.ProxyServerType;

/**
 * Class used to hold and use the proxy configuration.
 */
public class ProxyInfo {

    private boolean enabled;
    private String server;
    private Integer port;
    private String type;
    private boolean authEnabled;
    private String username;
    private String password;
    private String nonProxyHosts;

    public ProxyInfo(boolean enabled, String server, Integer port, String type, boolean authEnabled, String username, String password, String nonProxyHosts) {
        this.enabled = enabled;
        this.server = server;
        this.port = port;
        this.type = type;
        this.authEnabled = authEnabled;
        this.username = username;
        this.password = password;
        this.nonProxyHosts = nonProxyHosts;
    }

    /**
     * Check to see if a proxy should be used.
     *
     * @return The check result.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Apply the proxy configuration to the given CXF HTTPConduit.
     *
     * @param httpConduit The conduit to process.
     */
    public void applyToCxfConduit(HTTPConduit httpConduit) {
        httpConduit.getClient().setProxyServer(server);
        httpConduit.getClient().setProxyServerPort(port);
        httpConduit.getClient().setProxyServerType(ProxyServerType.fromValue(type));
        httpConduit.getClient().setNonProxyHosts(nonProxyHosts);
        if (authEnabled) {
            if (httpConduit.getProxyAuthorization() == null) {
                httpConduit.setProxyAuthorization(new ProxyAuthorizationPolicy());
            }
            httpConduit.getProxyAuthorization().setUserName(username);
            httpConduit.getProxyAuthorization().setPassword(password);
        }
    }
}
