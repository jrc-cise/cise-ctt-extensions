package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildListValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #39 Location Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `NamedLocation` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define the `GeographicName` attribute
 */
public class TestStep39LocationMinimumDefinition extends CiseTestStepOneOfChildListValue {

    public TestStep39LocationMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }


    @Override
    public TAR createReport(String messageXml) {
        String[] fieldsName = new String[]{"geometries"};
        Class[] entityClass = new Class[]{Location.class};
        return super.createReport(messageXml, fieldsName, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_39_LOCATION_MIN_DEF);
    }

    @Override
    protected boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof PortLocation || entity instanceof NamedLocation || entity instanceof PortFacilityLocation)
            return false;
        return true;
    }
}

