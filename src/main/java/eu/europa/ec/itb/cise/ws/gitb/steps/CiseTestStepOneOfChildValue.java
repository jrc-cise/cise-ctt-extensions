package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import org.apache.logging.log4j.util.Strings;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class CiseTestStepOneOfChildValue extends CiseTestStep {

    public CiseTestStepOneOfChildValue(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    protected static boolean hasAChildValueOfValidType(CiseTestStepOneOfChildValue ciseTestStepOneOfChildValue, String[] listFields, Entity entity) {

        return (hasDefinedField(listFields, entity));

    }

    private static boolean hasDefinedField(String[] listFields, Entity entity) {
        ArrayList<Pair<String, Boolean>> definedFields = generateListOfSetStringFields(entity);
        for (String expectedFieldname : listFields) {
            for (Pair<String, Boolean> definedField : definedFields)
                if (definedField.getA().equals(expectedFieldname))
                    return true;
        }
        return false;
    }

    private static ArrayList<Pair<String, Boolean>> generateListOfSetStringFields(Entity entity) {
        Class entityClass = entity.getClass();
        ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
        Class entityParentClass = entityClass.getSuperclass();
        while (!(entityParentClass.equals(Entity.class))) {
            Field[] entityParentFields = entityParentClass.getDeclaredFields();
            entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityParentFields)));
            entityParentClass = entityParentClass.getSuperclass();
        }
        ArrayList<Pair<String, Boolean>> areDefined = new ArrayList();
        for (Field field : entityFields) {
            field.setAccessible(true);
            Object content = null;
            if (("serialVersionUID").equals(field.getName()))
                continue;
            try {
                content = field.get(entity);
            } catch (IllegalAccessException eIAE) {
                content = null;
            }
            if (content != null) {
                switch (field.getType().toString()) {
                    case "class java.lang.String":
                        if (validateString(entity, field, (String) content))
                            areDefined.add(new Pair((field.getName()), ("" + content)));
                        break;
                    case ("double"):
                    case ("class java.lang.Double"):
                    case ("long"):
                    case ("class java.lang.Long"):
                    case ("int"):
                    case ("class java.lang.Integer"):
                        areDefined.add(new Pair((field.getName()), ("" + content)));
                        break;
                    default:
                        if (field.isEnumConstant())
                            areDefined.add(new Pair((field.getName()), ("" + content.toString())));
                }
            }
        }
        return areDefined;
    }

    private static boolean validateString(Entity entity, Field field, Object content) {
        try {
            if (!(((List) content).isEmpty()))
                return true;
        } catch (Exception eE1) {
            // System.out.println(field.getName() + "_not_a_list");
            try {
                if (Strings.isNotEmpty((String) content))
                    return true;
            } catch (Exception eE2) {
                // System.out.println(field.getName() + " nor a String");
            }
        }
        return false;
    }

    /**
     * @param messageXml    input message
     * @param classesToFind the classes to find: from this elements we search forward to verify relations
     * @param testStepRef   code to reference the specific test step
     * @return              returns the report
     */
    protected TAR createReport(String messageXml, String[] fieldNames, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }

        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Entity entity = (Entity) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (!hasAChildValueOfValidType(this, fieldNames, entity) &&
                        isEntityOfCorrectType(entity)
                ) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), entity);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation + describeCTXwarning), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }
}

