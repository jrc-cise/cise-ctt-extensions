package eu.europa.ec.itb.cise.ws.config;

import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interceptor used to capture the complete received XML payload.
 * <p>
 * This is used to ensure that even invalid content is recorded. Using the
 * JAXB serlaised/deserialised objects is not sufficient as it would hide
 * non-fatal errors.
 */
public class PayloadInterceptor extends AbstractPhaseInterceptor<Message> {

    public static final String RAW_PAYLOAD = "eu.europa.ec.itb.RAW_PAYLOAD";

    public PayloadInterceptor() {
        super(Phase.RECEIVE);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        try (InputStream in = message.getContent(InputStream.class)) {
            CachedOutputStream out = new CachedOutputStream();
            IOUtils.copy(in, out);
            out.flush();
            message.setContent(InputStream.class, out.getInputStream());
            byte[] payload = IOUtils.readBytesFromStream(out.getInputStream());
            message.getExchange().put(RAW_PAYLOAD, payload);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to read message content", e);
        }
    }

    public void handleFault(Message messageParam) {
        //Invoked when interceptor fails
    }
}
