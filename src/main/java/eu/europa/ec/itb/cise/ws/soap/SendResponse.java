package eu.europa.ec.itb.cise.ws.soap;

import eu.cise.servicemodel.v1.message.Acknowledgement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="sendResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://www.cise.eu/servicemodel/v1/message/}Acknowledgement" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendResponse", propOrder = {
        "acknowledgement"
})
public class SendResponse {

    @XmlElement(name = "return")
    protected Acknowledgement acknowledgement;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link Acknowledgement }
     */
    public Acknowledgement getReturn() {
        return acknowledgement;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link Acknowledgement }
     */
    public void setReturn(Acknowledgement value) {
        this.acknowledgement = value;
    }

}
