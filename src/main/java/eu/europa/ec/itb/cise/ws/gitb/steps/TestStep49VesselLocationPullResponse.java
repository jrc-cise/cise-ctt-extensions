package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

/**
 * #49 Location corresponds to the request
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*Background*` : a `VESSEL` entity is defined by attribute `Location` in the SUT Information System.
 * `*Given*` : `VESSEL` entity is present in  Pull.Request message payload.
 * `*When*` : it is presented with defined attribute `Location` in WKT format polygon representation.
 * `*Then*` : corresponding Pull.Response message must present at least one `Location`
 * `*And*` : it must be presented with attribute `Location`  represented by a latitude / longitude that correspond to the defined WKT format polygon representation.
 */
public class TestStep49VesselLocationPullResponse extends CiseTestStepValidVesselLocation {


    public TestStep49VesselLocationPullResponse(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String receivedMessageXml, String sentMessageXml) {
        TAR report = super.createReport(receivedMessageXml, sentMessageXml, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_49_VESSEL_LOCATION_PULL_RESPONSE, Vessel.class);
        return report;
    }

}