package eu.europa.ec.itb.cise.ws.config;

import eu.cise.dispatcher.DispatcherType;

import java.util.Locale;

interface CTTCiseExtensionConfig {

    DispatcherType getDispatcherType();

    String getKeyStoreFileName();

    String getKeyStorePassword();

    String getPrivateKeyAlias();

    String getPrivateKeyPassword();

    String getPersistentMessageIdFilePath();

    boolean isProxyEnabled();

    String getProxyServer();

    Integer getProxyPort();

    String getProxyType();

    boolean isProxyAuthEnabled();

    String getProxyUsername();

    String getProxyPassword();

    String getProxyNonProxyHosts();

    Locale getCurrentLocale();
}
