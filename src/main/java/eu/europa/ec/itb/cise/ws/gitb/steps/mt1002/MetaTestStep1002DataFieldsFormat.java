package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MetaTestStep1002DataFieldsFormat extends CiseMetaTestStep {

    public MetaTestStep1002DataFieldsFormat(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        List<CiseTestStepsEnum> steps = new ArrayList(Arrays.asList(
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_76_FORMAT_VESSEL_CALLSIGN,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_77_FORMAT_VESSEL_IMONUMBER,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_78_FORMAT_VESSEL_MMSI,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_79_FORMAT_VESSEL_NAME,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_80_FORMAT_VESSEL_INMARSAT,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_82_FORMAT_VESSEL_NATIONALITY,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_83_FORMAT_CONT_UNIT_UNDG,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_84_FORMAT_CONT_UNIT_LOC_ON_BOARD_CONTAINER,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_85_FORMAT_CONT_UNIT_CONTAINER_MARK_AND_NUMBER,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_86_FORMAT_CATCH_SPECIES,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_87_FORMAT__PORTLOCATION_LOCATIONCODE,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_88_FORMAT_LATITUDE_LONGITUDE,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_89_FORMAT_NAMED_LOCATION_GEOGRAPHIC_IDENTIFIER,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_90_FORMAT_PORTFACILITY_PORTFACILITYNUMBER,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_93_FORMAT_METADATA_LANGUAGE,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_94_FORMAT_METADATA_DESIGNATION,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_95_FORMAT_UNIQUE_IDENTIFIER_UUID,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_96_FORMAT_AGENT_CONTACTINFORMATION,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_97_FORMAT_AGENT_NATIONALITY
        ));
        return createReport(messageXml, steps, CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1002_DATA_FIELDS_FORMAT);
    }
}
