package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.CiseValidationService;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.List;

public abstract class CiseMetaTestStep extends CiseTestStep {

    private CiseValidationService ciseValidationService;

    public CiseMetaTestStep(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
        ciseValidationService = new CiseValidationService(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml, List<CiseTestStepsEnum> steps, CiseTestStepsEnum testStepRef) {
        return createReport(messageXml, null, steps, testStepRef);
    }

    public TAR createReport(String messageXml, String requestContent, List<CiseTestStepsEnum> steps, CiseTestStepsEnum testStepRef) {
        TAR globalReport = reportBuilder.createReport(TestResultType.SUCCESS);

        String failureExplanation = errorHelper.getErrorDescription(testStepRef.getUiCheckName());
        for (CiseTestStepsEnum step : steps) {
            TAR tar = ciseValidationService.generateReport(step.getUiCheckName(), messageXml, requestContent);
            TestResultType result = tar.getResult();

            if (globalReport.getResult() != TestResultType.FAILURE && result == TestResultType.WARNING) {
                globalReport.setResult(TestResultType.WARNING);
                failureExplanation = errorHelper.addFailureFromAnotherReport(failureExplanation, tar);
            } else if (result == TestResultType.FAILURE) {
                globalReport.setResult(TestResultType.FAILURE);
                failureExplanation = errorHelper.addFailureFromAnotherReport(failureExplanation, tar);
            }

        }
        if (globalReport.getResult() == TestResultType.SUCCESS) {
            failureExplanation = reportBuilder.REPORT_STEP_SUCCESS;
        }

        globalReport.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), failureExplanation, "string", ValueEmbeddingEnumeration.STRING));

        return globalReport;
    }
}
