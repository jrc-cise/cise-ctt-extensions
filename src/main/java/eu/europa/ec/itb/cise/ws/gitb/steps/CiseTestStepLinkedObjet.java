package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.util.ArrayList;
import java.util.List;

public abstract class CiseTestStepLinkedObjet extends CiseTestStep {

    public CiseTestStepLinkedObjet(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    /**
     * In this function we assume that the entityClassToFind is an Event because is the only one that have relations to other kind of objects.
     *
     * @param messageXml                   input message
     * @param classesToFind                the classes to find: from this elements we search forward to verify relations
     * @param linkedEntityClassesToLookFor classes of objects that have to be present forward
     * @param testStepRef                  code to reference the specific test step
     * @return                             returns the report
     */
    protected TAR createReport(String messageXml, Class[] classesToFind, Class[] linkedEntityClassesToLookFor, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }

        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Objet objet = (Objet) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (hasALinkedObjectOfValidType(objet, linkedEntityClassesToLookFor) &&
                        !isEntityOfCorrectType(objet)
                ) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), objet);
                }
            }

            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation + describeCTXwarning), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    protected abstract boolean isEntityOfCorrectType(Entity entity);

    protected boolean hasALinkedObjectOfValidType(Objet objet, Class[] linkedEntityClasses) {
        //Look for Risks
        List<Objet.InvolvedRiskRel> involvedRiskRels = objet.getInvolvedRiskRels();
        if (involvedRiskRels != null) {
            for (Objet.InvolvedRiskRel involvedRiskRel : involvedRiskRels) {
                if (isInstanceOfOneOf(involvedRiskRel.getRisk(), linkedEntityClasses)) {
                    return true;
                }
            }
        }

        // Look for Locations
        List<Objet.LocationRel> locationRels = objet.getLocationRels();
        for (Objet.LocationRel locationRel : locationRels) {
            if (isInstanceOfOneOf(locationRel.getLocation(), linkedEntityClasses)) {
                return true;
            }
        }

        // Look for Events
        List<Objet.InvolvedEventRel> involvedEventRels = objet.getInvolvedEventRels();
        for (Objet.InvolvedEventRel involvedEventRel : involvedEventRels) {
            if (isInstanceOfOneOf(involvedEventRel.getEvent(), linkedEntityClasses)) {
                return true;
            }
        }

        // Look for documents
        List<Objet.DocumentRel> documentRels = objet.getDocumentRels();
        if (documentRels != null) {
            for (Objet.DocumentRel documentRel : documentRels) {
                if (isInstanceOfOneOf(documentRel.getDocument(), linkedEntityClasses)) {
                    return true;
                }
            }
        }

        // Look for Agents
        List<Objet.InvolvedAgentRel> involvedAgentRels = objet.getInvolvedAgentRels();
        for (Objet.InvolvedAgentRel agentRel : involvedAgentRels) {
            if (isInstanceOfOneOf(agentRel.getAgent(), linkedEntityClasses)) {
                return true;
            }
        }

        return false;
    }

}
