package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.meteo;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #103 Boundary Vessel DeadWeight
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in payload AND `MessageType` is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` attribute `DeadWeight` is defined
 * `*THEN*` attribute `DeadWeight` must be inferior to 300000 tonnes AND attribute `DeadWeight` must be superior to 1 (.).
 */
public class TestStep103BoundaryMeteoOceanCondition extends CiseTestStepOneOfChildValueBoundary {


    public TestStep103BoundaryMeteoOceanCondition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{MeteoOceanographicCondition.class};
        List<String> fields = Arrays.asList(
                "airTemperature",
                "cloudCeiling",
                "precipitation",
                "salinity",
                "seaLevelPressure",
                "tidalCurrentDirection",
                "tidalCurrentSpeed",
                "visibility",
                "waterTemperature",
                "waveDirection",
                "waveHeight",
                "windCurrentDirection",
                "windCurrentSpeed"
        );

        return super.createReport(messageXml,
                fields,
                "Meteo",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_103_BOUNDARY_METEOOCEANCONDITION
        );

    }
}
