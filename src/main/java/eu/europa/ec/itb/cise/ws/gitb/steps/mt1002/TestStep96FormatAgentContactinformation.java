package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import ezvcard.Ezvcard;
import ezvcard.VCard;

import java.util.Arrays;
import java.util.List;

/**
 * #96 Format Agent ContactInformation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Agent`  (+ allchildren) is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `ContactInformation` is  defined
 * `*THEN*` *attribute* `ContactInformation` must be composed according to VCARD standard format.
 */
public class TestStep96FormatAgentContactinformation extends CiseTestStep {

    public TestStep96FormatAgentContactinformation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum teststepref = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_96_FORMAT_AGENT_CONTACTINFORMATION;
        String failureExplanation = errorHelper.getBaseErrorDescription(teststepref.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(teststepref.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfTypes(messageXml, Arrays.asList(Agent.class, Person.class, Organization.class, PortOrganization.class));
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(teststepref.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(teststepref.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(teststepref.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(teststepref.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof Agent) {
            String contactInformation = (((Agent) entity).getContactInformation());
            if (contactInformation != null) {
                return ((contactInformation.trim().length() > 35) &&
                        contactInformation.trim().startsWith("BEGIN:VCARD") &&
                        contactInformation.trim().endsWith("END:VCARD")
                );
            }

        }
        return true;
    }

    /**
     * TODO: create the logic (return false)
     * String.format("%.20f", a));
     *
     * @param entity
     * @return
     */
    private boolean warningValid(Entity entity) {
        if (entity instanceof Agent) {
            String contactInformation = (((Agent) entity).getContactInformation());
            if (contactInformation != null) {
                try {
                    VCard vcard = Ezvcard.parse(contactInformation).first();
                    boolean versionCorrect = Arrays.asList("2.1", "3.0", "4.0").contains(vcard.getVersion().toString());
                    boolean isMinimalOrganization = ((vcard.getFormattedName() == null) || !(vcard.getFormattedName().toString().isEmpty()));
                    boolean isMinimalIndividual = (vcard.getStructuredName() == null) || (!(vcard.getStructuredName().getFamily().equals("")));
                    return (versionCorrect && (isMinimalIndividual || isMinimalOrganization));
                } catch (Exception e) {
                    return false;
                }
            }
        }
        return true;
    }

}
