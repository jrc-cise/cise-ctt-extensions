package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #66 LinkObjectObject Agent CargoUnit
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `ContainmentUnit` or `Catch` or `PortOrganization` or `Cargo` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Person` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `PASSENGER`, `CREW_MEMBER`, `CAPTAIN_MASTER`, `SHIP_OPERATING_COMPANY`, `VESSEL_COMPANY`, `EMPLOYEE`
 * `VESSEL_BUILDER` , `VESSEL_CHARTERER`, `VESSEL_REGISTERED_OWNER`, `SHIPPING_LINE`
 * `*AND*` relation _must not define_ attribute :`TransitPassengerDuty`.
 */
public class TestStep66LinkObjectAgentCargoUnit extends CiseTestStepRelationship {

    public TestStep66LinkObjectAgentCargoUnit(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Catch.class, ContainmentUnit.class, Cargo.class};
        Class[] organizationClasses = new Class[]{Person.class};
        Class[] relationClasses = new Class[]{Objet.InvolvedAgentRel.class, Agent.InvolvedObjectRel.class};
        return createReport(messageXml, relationClasses, organizationClasses, entityClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_66_LINK_OBJECT_AGENT_CARGOUNIT);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        if (relationship instanceof Objet.InvolvedAgentRel) {
            Objet.InvolvedAgentRel rel = (Objet.InvolvedAgentRel) relationship;
            objectAgentRole = rel.getAgentRole();
            isTransitPassenger = rel.isTransitPassenger();
            dutyType = rel.getDuty();
        } else if (relationship instanceof Agent.InvolvedObjectRel) {
            Agent.InvolvedObjectRel rel = (Agent.InvolvedObjectRel) relationship;
            objectAgentRole = rel.getAgentRole();
            isTransitPassenger = rel.isTransitPassenger();
            dutyType = rel.getDuty();
        } else {
            return false;
        }

        if (objectAgentRole == AgentRoleInObjectType.PASSENGER ||
                objectAgentRole == AgentRoleInObjectType.CREW_MEMBER ||
                objectAgentRole == AgentRoleInObjectType.CAPTAIN_MASTER ||
                objectAgentRole == AgentRoleInObjectType.SHIP_OPERATING_COMPANY ||
                objectAgentRole == AgentRoleInObjectType.VESSEL_COMPANY ||
                objectAgentRole == AgentRoleInObjectType.EMPLOYEE ||
                objectAgentRole == AgentRoleInObjectType.VESSEL_BUILDER ||
                objectAgentRole == AgentRoleInObjectType.VESSEL_CHARTERER ||
                objectAgentRole == AgentRoleInObjectType.VESSEL_REGISTERED_OWNER ||
                objectAgentRole == AgentRoleInObjectType.SHIPPING_LINE ||
                isTransitPassenger != null ||
                dutyType != null
        ) {
            return false;
        }

        return true;
    }
}
