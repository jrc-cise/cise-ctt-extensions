package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.metadata.Metadata;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #94 Format Document Metadata Designation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Document` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` `Metadata` *attribute* _is defined_
 * `*THEN*` key `Designation` of *attribute* `Metadata` must have CISE Data Model entity valid name .
 */
public class TestStep94FormatMetadataDesignation extends CiseTestStep {

    public TestStep94FormatMetadataDesignation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_94_FORMAT_METADATA_DESIGNATION;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Document.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if ((!errorValid(entityPair.getA())) | (!errorValid2(entityPair.getA()))) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else conform = true;
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        boolean wellFormated = true;
        if (entity instanceof Document) {
            for (Metadata metadata : ((List<Metadata>) ((Document) entity).getMetadatas())) {
                String designation = (metadata.getDesignation());
                if (designation != null) {
                    wellFormated = (wellFormated && (
                            designation == null ||
                            (designation.trim().matches("^[A-Z][a-z]+$"))
                    ));
                }
            }
            return wellFormated;
        }
        return true;
    }

    /**
     * TODO: create the logic (return false)
     * String.format("%.20f", a));
     *
     * @param entity
     * @return
     */
    private boolean errorValid2(Entity entity) {
        if (entity instanceof Document) {
            boolean wellFormated = true;
            for (Metadata metadata : ((List<Metadata>) ((Document) entity).getMetadatas())) {
                String designation = (metadata.getDesignation());
                wellFormated = (wellFormated && (
                        designation == null ||
                        ValidModelDesignation.isValidDesignation(designation)
                ));
            }
            return wellFormated;
        }
        return true;
    }

}
