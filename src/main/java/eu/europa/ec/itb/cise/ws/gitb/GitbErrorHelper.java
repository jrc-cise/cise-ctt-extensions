package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.eucise.xml.XmlMapper;

import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;

public class GitbErrorHelper {
    private final ResourceBundle labelsRS;
    private final HashMap<String, Properties> boundariesRS;
    private XmlMapper xmlMapper;

    public GitbErrorHelper(ResourceBundle labelsRS, HashMap<String, Properties> boundariesRSList, XmlMapper xmlMapper) {
        this.labelsRS = labelsRS;
        this.boundariesRS = boundariesRSList;
        this.xmlMapper = xmlMapper;
    }


    public String addPointOfFailure(String failureExplanation, String breadcrumb, Entity entity) {
        failureExplanation += "- " + breadcrumb + "\n";
        failureExplanation += xmlMapper.toXML(entity) + "\n";
        return failureExplanation;
    }



    public String addBoundaryPointOfFailure(String failureExplanation, String breadcrumb, String fieldName, String actualValue, String min, String max, String unit) {
        failureExplanation += "- " + breadcrumb + " : " + fieldName + " \n actual value :" + actualValue + " accepted range:" + min + " ,max:" + max + " " + unit + "\n\n";
        return failureExplanation;
    }

    public String addFailureFromAnotherReport(String failureExplanation, TAR tar) {
        if (failureExplanation == null) {
            failureExplanation = tar.getContext().getItem().get(0).getValue();
        } else {
            failureExplanation += tar.getContext().getItem().get(0).getValue();
        }
        return failureExplanation;
    }

    public String avoidNodeId(String breadcrumb) {
        return breadcrumb.substring(0, breadcrumb.lastIndexOf('.'));
    }

    public String getBaseErrorDescription(String uiCheckName) {
        String failureExplanation = labelsRS.getString(uiCheckName) + "\n\nFailing points:\n\n";
        return failureExplanation;
    }

    public String getBoundaryErrorDescription(String uiCheckName, boolean first) {
        String failureExplanation = "";
        if (first) failureExplanation = failureExplanation + labelsRS.getString(uiCheckName);
        else failureExplanation = failureExplanation + "\n\nFailing boundary:\n\n";
        return failureExplanation;
    }

    public String getWarningDescription(String uiCheckName) {
        return getExtraErrorDescription(uiCheckName, "warning");
    }

    public String getErrorDescription(String uiCheckName) {
        String failureExplanation = labelsRS.getString(uiCheckName) + "\n\n";
        return failureExplanation;
    }

    public String getExtraErrorDescription(String uiCheckName, String extraKeyPart) {
        String failureExplanation = labelsRS.getString(uiCheckName + "." + extraKeyPart);
        return failureExplanation;
    }

    public Properties getErrorBoundaries(String arg) {
        return boundariesRS.get(arg + ".properties");
    }

}
