package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #88 FORMAT (LOCATION - GEOMETRY implicit) Latitude Longitude
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Location` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE`  *payload*
 * `*AND*` attribute `Latitude` and `Longitude` of its `Geometry` is defined
 * `*THEN*` attribute `Latitude` and `Longitude` must contain 1-2 digits followed by "." and 1 to 4 digits
 * `*AND*` attribute `Latitude` and `Longitude` must represent a decimal number between accepted range (Latitud -+90 Longitud -+180)
 * `*AND*` attribute `Latitude` and `Longitude` should represent a decimal number with sufficient precision >=2 (2500 meter at the equator)
 */
public class TestStep88FormatLatitudeLongitude extends CiseTestStep {

    public TestStep88FormatLatitudeLongitude(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_88_FORMAT_LATITUDE_LONGITUDE;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Location.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof Location) {
            boolean wellFormated = true;
            for (Geometry geometry : ((List<Geometry>) ((Location) entity).getGeometries())) {
                String latitude = (geometry.getLatitude());
                String longitude = (geometry.getLongitude());
                if (longitude != null) {
                    wellFormated = (wellFormated && (
                            (latitude.trim().matches("^(-)?[0-9]+(.[0-9]+)?$")) &&
                                    (longitude.trim().matches("^(-)?[0-9]+(.[0-9]+)?$")) &&
                                    Double.parseDouble(latitude) > -90.00 &&
                                    Double.parseDouble(latitude) < 90.00 &&
                                    Double.parseDouble(longitude) > -180.00 &&
                                    Double.parseDouble(longitude) < 180.00
                    ));
                }
            }
            return wellFormated;
        }
        return true;
    }

    /**
     * TODO: create the logic (return false)
     * String.format("%.20f", a));
     *
     * @param entity
     * @return
     */
    private boolean warningValid(Entity entity) {
        if (entity instanceof Location) {
            boolean wellFormated = true;
            for (Geometry geometry : ((List<Geometry>) ((Location) entity).getGeometries())) {
                String latitude = (geometry.getLatitude());
                String longitude = (geometry.getLongitude());
                wellFormated = (wellFormated && (
                        (latitude.split("\\.")[1].length() > 2) &&
                                (longitude.split("\\.")[1].length() > 2) &&
                                (longitude.split("\\.")[1].length() < 9)
                ));
            }
            return wellFormated;
        }
        return true;
    }

}
