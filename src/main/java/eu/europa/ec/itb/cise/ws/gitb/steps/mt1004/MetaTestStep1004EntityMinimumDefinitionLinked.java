package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseMetaTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum.*;

public class MetaTestStep1004EntityMinimumDefinitionLinked extends CiseMetaTestStep {

    public MetaTestStep1004EntityMinimumDefinitionLinked(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        List<CiseTestStepsEnum> steps = new ArrayList(Arrays.asList(
                REPORT_ELEMENT_MODEL_TS_18_UNEXPECTED_MOVEMENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_20_SHIFTING_OF_CARGO_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_21_VESSEL_ANOMALY_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_22_STAIN_OF_OIL_SIGHTED_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_27_POLLUTION_INCIDENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_29_POLLUTION_INCIDENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_30_VESSEL_INCIDENTS_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_31_INCIDENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_57_ACTION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_58_MOVEMENT_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_60_METEOOCEANOGRAPHICCONDITION_MIN_DEF,
                REPORT_ELEMENT_MODEL_TS_61_RISK_MINIMUM_IDENTIFICATION
        ));

        return createReport(messageXml, steps, CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1004_ENTITY_MIN_DEF_LINKED);
    }

}
