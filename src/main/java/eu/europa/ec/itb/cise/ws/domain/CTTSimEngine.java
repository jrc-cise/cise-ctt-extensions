package eu.europa.ec.itb.cise.ws.domain;

import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.dispatcher.DispatcherException;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.signature.SignatureService;
import eu.europa.ec.itb.cise.ws.exceptions.EmptyMessageIdEx;
import eu.europa.ec.itb.cise.ws.exceptions.EndpointNotFoundEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullClockEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullDispatcherEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullMessageEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullSendParamEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullSenderEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullSignatureServiceEx;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Date;
import java.time.Clock;

import static eu.eucise.helpers.DateHelper.toXMLGregorianCalendar;
import static eu.europa.ec.itb.cise.ws.util.Asserts.notBlank;
import static eu.europa.ec.itb.cise.ws.util.Asserts.notNull;


public class CTTSimEngine implements SimEngine {

    private final Clock clock;
    private final SignatureService signature;
    private final org.slf4j.Logger log;
    private Dispatcher dispatcher;
    private String destinationEndpoint;
    private SyncAckFactory acknowledgementFactory;

    /**
     * Default constructor that uses UTC as a reference clock
     * <p>
     * TODO is now clear that this class has way too many
     * responsibilities. It should be split in several classes
     */
    public CTTSimEngine(
            SignatureService signature,
            Dispatcher dispatcher) {

        this(signature, dispatcher, Clock.systemUTC());
        this.dispatcher = dispatcher;
        this.acknowledgementFactory = new SyncAckFactory();
    }

    /**
     * Constructor that expect a clock as a reference to compute date and time.
     *
     * @param signature  the signature service used to sign messages
     * @param dispatcher the object used to dispatch the message
     * @param clock      the reference clock
     *                   <p>
     *                   NOTE: this constructor is used only in tests
     */
    CTTSimEngine(
            SignatureService signature,
            Dispatcher dispatcher,
            Clock clock) {

        this.signature = notNull(signature, NullSignatureServiceEx.class);
        this.dispatcher = notNull(dispatcher, NullDispatcherEx.class);
        this.clock = notNull(clock, NullClockEx.class);
        this.log = LoggerFactory.getLogger(CTTSimEngine.class);
    }

    @Override
    public <T extends Message> T prepare(T message, CTTSendParam param) {
        CTTSendParam cttParam = (CTTSendParam) param;

        notNull(param, NullSendParamEx.class);
        notNull(message.getSender(), NullSenderEx.class);

        if (cttParam.getRequiresAck() != null) {
            message.setRequiresAck(param.isRequiresAck());
        }
        message.setMessageID(param.getMessageId());
        message.setCorrelationID(
                computeCorrelationId(param.getCorrelationId(), param.getMessageId())
        );

        message.setCreationDateTime(now());

        this.destinationEndpoint = cttParam.getDestinationEndpoint();

        return (T) signature.sign(message);
    }

    @Override
    public Acknowledgement send(Message message) {
        try {
            log.info("destinationEndpoint: {}", destinationEndpoint);
            DispatchResult sendResult = dispatcher.send(message, this.destinationEndpoint);

            log.info("sendResult: {}", sendResult);

            return sendResult.getResult();
        } catch (DispatcherException e) {
            throw new EndpointNotFoundEx();
        }
    }

    @Override
    public Acknowledgement sendSigned(String payload) {
        return null;
    }

    @Override
    public Acknowledgement receive(Message message) {
        notNull(message, NullMessageEx.class);
        notBlank(message.getMessageID(), EmptyMessageIdEx.class);

        if (message.getSender() == null) {
            throw new NullSenderEx();
        }

        signature.verify(message);

        return acknowledgementFactory.buildAck(message, SyncAckType.SUCCESS, "");
    }


    private String computeCorrelationId(String correlationId, String messageId) {
        if (StringUtils.isEmpty(correlationId)) {
            return messageId;
        }
        return correlationId;
    }

    private XMLGregorianCalendar now() {
        return toXMLGregorianCalendar(Date.from(clock.instant()));
    }

}
