
package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.Arrays;
import java.util.List;

/**
 * #86 FORMAT catch species
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Catch` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `Species` is defined
 * `*THEN*` attribute `Species` must be composed of a list of element of 3 characters separed by coma "," in accordance with rules for the implementation of Council Regulation (EC) No 1224/2009.
 */
public class TestStep86FormatCatchSpecies extends CiseTestStep {

    public TestStep86FormatCatchSpecies(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_86_FORMAT_CATCH_SPECIES;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Catch.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * TODO: create the logic (return false)
     *
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {

        boolean allValid = true;
        if (entity instanceof Catch) {
            String species = ((Catch) entity).getSpecies();
            if (species != null) {
                for (String oneOfSpecies : species.trim().split(",")) {
                    if ((oneOfSpecies.trim().matches("^[A-Z]+$")))
                        allValid = (allValid ? codeSpecies.stream().anyMatch(s -> s.equals(oneOfSpecies)) : allValid);
                }
            }
        }
        return allValid;
    }

    private boolean warningValid(Entity entity) {

        boolean allValid = true;
        if (entity instanceof Catch) {
            String species = ((Catch) entity).getSpecies();
            if (species != null) {
                for (String oneOfSpecies : species.trim().split(",")) {
                    if ((oneOfSpecies.trim().matches("^[A-Z]+$")))
                        allValid = (allValid ? codeSpecies.stream().anyMatch(s -> s.equals(oneOfSpecies)) : allValid);
                }
            }
        }
        return allValid;
    }

    private final List<String> codeSpecies = Arrays.asList("ALB", "ALF", "ANE", "ANF", "ANI", "ARU", "BET", "BLI", "BLL", "BSF", "BUM",
            "CAP", "COD", "DAB", "DGS", "FLE", "GFB", "GHL", "HAD", "HAL", "HER", "HKE",
            "HKW", "JAX", "KRI", "LEM", "LEZ", "LIC", "LIN", "MAC", "NEP", "NOG", "NOP",
            "NOR", "ORY", "PCR", "PEN", "PLE", "POK", "POL", "PRA", "RED", "RHG", "RNG",
            "SAN", "SBR", "SDH", "SDU", "SGI", "SOL", "SPR", "SQI", "SQS", "SRX", "SWO",
            "TOP", "TUR", "USK", "WHB", "WHG", "WHM", "WIT", "YEL");

}
