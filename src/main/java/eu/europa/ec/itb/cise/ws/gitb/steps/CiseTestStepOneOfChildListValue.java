package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.util.DomainTypeValidator;

import java.util.ArrayList;
import java.util.List;

public abstract class CiseTestStepOneOfChildListValue extends CiseTestStep {

    public CiseTestStepOneOfChildListValue(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    protected static boolean hasAChildValueOfValidType(CiseTestStepOneOfChildListValue ciseTestStepOneOfChildListValue, String[] fieldNames, Entity entity) {
        Class entityClass = entity.getClass();
        return (DomainTypeValidator.hasDefinedSpecificSimpleTypeListField(fieldNames, entity));
    }

    /**
     * @param messageXml        CISE XML message
     * @param classesToFind     the classes to find: from this elements we search forward to verify relations
     * @param testStepRef       code to reference the specific test step
     * @return                  returns the report
     */
    protected TAR createReport(String messageXml, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        String[] fieldsNames = new String[]{};
        return createReport(messageXml, fieldsNames, classesToFind, testStepRef);
    }

    /**
     * @param messageXml    input message
     * @param fieldsName    the fields with list to find: from this elements we search to verify list of simple type elements
     * @param classesToFind the classes to find: from this elements we search forward to verify relations
     * @param testStepRef   code to reference the specific test step
     * @return              returns the report
     */
    protected TAR createReport(String messageXml, String[] fieldsName, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }

        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Entity entity = (Entity) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (!hasAChildValueOfValidType(this, fieldsName, entity) &&
                        isEntityOfCorrectType(entity)
                ) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), entity);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation + describeCTXwarning), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }

}