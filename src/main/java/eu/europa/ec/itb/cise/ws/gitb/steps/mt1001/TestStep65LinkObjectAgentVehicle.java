package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #65 LinkObjectObject Agent Organization
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type  `Vessel` or `Aircraft` or `LandVehicle`  is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Person` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `SHIP_OPERATING_COMPANY`, `VESSEL_COMPANY`, `SHIPPING_LINE`.
 */
public class TestStep65LinkObjectAgentVehicle extends CiseTestStepRelationship {

    public TestStep65LinkObjectAgentVehicle(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Vessel.class, Aircraft.class, LandVehicle.class};
        Class[] organizationClasses = new Class[]{Person.class};
        Class[] relationClasses = new Class[]{Objet.InvolvedAgentRel.class, Agent.InvolvedObjectRel.class};
        return createReport(messageXml, relationClasses, organizationClasses, entityClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_65_LINK_OBJECT_AGENT_VEHICLE);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        if (relationship instanceof Objet.InvolvedAgentRel) {
            Objet.InvolvedAgentRel rel = (Objet.InvolvedAgentRel) relationship;
            objectAgentRole = rel.getAgentRole();
        } else if (relationship instanceof Agent.InvolvedObjectRel) {
            Agent.InvolvedObjectRel rel = (Agent.InvolvedObjectRel) relationship;
            objectAgentRole = rel.getAgentRole();
        } else {
            return false;
        }

        if (objectAgentRole == AgentRoleInObjectType.SHIP_OPERATING_COMPANY ||
                objectAgentRole == AgentRoleInObjectType.VESSEL_COMPANY ||
                objectAgentRole == AgentRoleInObjectType.SHIPPING_LINE
        ) {
            return false;
        }

        return true;
    }

}
