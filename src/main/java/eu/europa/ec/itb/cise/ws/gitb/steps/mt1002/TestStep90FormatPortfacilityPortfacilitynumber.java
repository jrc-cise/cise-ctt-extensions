package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #90 FORMAT Portfacility Portfacilitynumber
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Portfacility` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE`  *payload*
 * `*AND*` attribute `Portfacilitynumber` is defined
 * `*THEN*` attribute `Portfacilitynumber` must be composed by 5 character LOCODE corresponding to port followed by "-" and 4 digits
 */
public class TestStep90FormatPortfacilityPortfacilitynumber extends CiseTestStep {

    public TestStep90FormatPortfacilityPortfacilitynumber(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_90_FORMAT_PORTFACILITY_PORTFACILITYNUMBER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, PortFacilityLocation.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof PortFacilityLocation) {
            String portFacilityNumber = ((PortFacilityLocation) entity).getPortFacilityNumber();
            if (portFacilityNumber != null) {
                if (!(portFacilityNumber.contains("-")))
                    return false;
                String[] splittedValue = portFacilityNumber.split("-");
                return (
                        (splittedValue.length == 2) &&
                                ((splittedValue[0]).length() == 5) &&
                                ((splittedValue[0]).matches("^[A-Z]+$")) &&
                                //   5 character LOCODE corresponding to port
                                ((splittedValue[1]).length() == 4) &&
                                ((splittedValue[1]).matches("^[0-9]+$")) //followed by "-" and 4 digits
                );
            }
        }
        return true;
    }

    /**
     * TODO: create the logic (return false)
     *
     * @param entity
     * @return
     */
    private boolean warningValid(Entity entity) {
        if (entity instanceof PortFacilityLocation) {
            String portFacilityNumber = ((PortFacilityLocation) entity).getPortFacilityNumber();
            if (portFacilityNumber != null) {
                return (ValidNationalCode.getLitteralForNatCode2(portFacilityNumber.substring(0, 2)) != null);
            }

        }
        return true;
    }

}
