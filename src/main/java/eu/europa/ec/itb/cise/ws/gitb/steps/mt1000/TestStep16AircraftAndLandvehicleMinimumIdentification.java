package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildListValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #16 AircraftAndLandvehicle Minimum Identification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Aircraft` or `Landvehicle` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define at least one of the attribute:`ExternalMarkings`,`Name`
 * `*OR*` define at least a relation to `Location`
 */
public class TestStep16AircraftAndLandvehicleMinimumIdentification extends CiseTestStepOneOfChildListValue {

    public TestStep16AircraftAndLandvehicleMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }


    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClass = new Class[]{Aircraft.class, LandVehicle.class};
        String[] fieldNames = {"externalMarkings", "names", "locationRels"};
        return super.createReport(messageXml, fieldNames, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_16_AIRCRAFT_AND_LANDVEHICLE_MIN_IDENT);
    }

    @Override
    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }

}

