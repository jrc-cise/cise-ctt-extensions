package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.core.*;
import com.gitb.tr.ObjectFactory;
import com.gitb.tr.*;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.AcknowledgementType;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.transport.CustomNamespaceContext;
import eu.europa.ec.itb.cise.ws.util.BomStrippingReader;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Class to build reports
 */
public class ReportBuilder {

    public static final String REPORT_ELEMENT_MESSAGE = "message";
    public static final String REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT = "syncAcknowledgement";
    public static final String REPORT_ELEMENT_SENT_MESSAGE = "sentMessage";
    public static final String REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT_FOR_SENT_MESSAGE = "syncAcknowledgementForSentMessage";
    public static final String REPORT_ELEMENT_SIGNATURE = "signature";
    public static final String REPORT_STEP_SUCCESS = "VALID";
    private static final String[] CISE_DATA_MODEL_ELEMENT = new String[]{"Action", "Agent", "Aircraft", "Anomaly", "Cargo", "CargoDocument", "Catch", "CertificateDocument", "ContainmentUnit", "CrisisIncident", "Document", "EventDocument", "FormalOrganization", "Incident", "IrregularMigrationIncident", "LandVehicle", "LawInfringementIncident", "Location", "LocationDocument", "MaritimeSafetyIncident", "MeteoOceanographicCondition", "Movement", "NamedLocation", "OperationalAsset", "Organization", "OrganizationalCollaboration", "OrganizationalUnit", "OrganizationDocument", "Person", "PersonDocument", "PollutionIncident", "PortFacilityLocation", "PortLocation", "PortOrganization", "Risk", "RiskDocument", "Stream", "Vessel", "VesselDocument"};
    private final ObjectFactory gitbTrObjectFactory = new ObjectFactory();
    private final XmlMapper xmlMapper;
    private final MessageIdStore messageIdStore;
    private XPathExpression dataElementXPath;
    private XPathExpression envelopeElementXPath;
    private SignatureService signature;

    /**
     * Private constructor to prevent this class from being instantiated.
     *
     * @param xmlMapper CISE Message marshaller 
     * @param signature CISE signature service
     * @param messageIdStore messageIDs persistence handler 
     */
    public ReportBuilder(XmlMapper xmlMapper, SignatureService signature, MessageIdStore messageIdStore) {
        this.xmlMapper = xmlMapper;
        this.signature = signature;
        this.messageIdStore = messageIdStore;
        initialise();
    }

    public MessageIdStore getMessageIdStore() {
        return messageIdStore;
    }

    public XmlMapper getXmlMapper() {
        return xmlMapper;
    }

    public void initialise() {
        XPath path = XPathFactory.newInstance().newXPath();
        path.setNamespaceContext(new CustomNamespaceContext());
        try {
            dataElementXPath = path.compile(buildDataElementXPathPattern());
            envelopeElementXPath = path.compile("/soapenv:Envelope/soapenv:Body/*/*");
        } catch (XPathExpressionException e) {
            throw new IllegalStateException("Unable to initialise XPath expression for payload detection", e);
        }
    }

    /**
     * Create a report for the given result.
     * <p>
     * This method creates the report, sets its time and constructs an empty context map to return values with.
     *
     * @param result The overall result of the report.
     * @return The report.
     */
    public TAR createReport(TestResultType result) {
        TAR report = new TAR();
        report.setContext(new AnyContent());
        report.getContext().setType("map");
        report.setResult(result);
        report.setDate(getTimestamp());
        return report;
    }

    public XMLGregorianCalendar getTimestamp() {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
        } catch (DatatypeConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Create a parameter definition.
     *
     * @param name        The name of the parameter.
     * @param type        The type of the parameter. This needs to match one of the GITB types.
     * @param use         The use (required or optional).
     * @param kind        The kind og parameter it is (whether it should be provided as the specific value, as BASE64 content or as a URL that needs to be looked up to obtain the value).
     * @param description The description of the parameter.
     * @return The created parameter.
     */
    public TypedParameter createParameter(String name, String type, UsageEnumeration use, ConfigurationType kind, String description) {
        TypedParameter parameter = new TypedParameter();
        parameter.setName(name);
        parameter.setType(type);
        parameter.setUse(use);
        parameter.setKind(kind);
        parameter.setDesc(description);
        return parameter;
    }

    /**
     * Collect the inputs that match the provided name.
     *
     * @param parameterItems The items to look through.
     * @param inputName      The name of the input to look for.
     * @return The collected inputs (not null).
     */
    public List<AnyContent> getInputsForName(List<AnyContent> parameterItems, String inputName) {
        List<AnyContent> inputs = new ArrayList<>();
        if (parameterItems != null) {
            for (AnyContent anInput : parameterItems) {
                if (inputName.equals(anInput.getName())) {
                    inputs.add(anInput);
                }
            }
        }
        return inputs;
    }

    /**
     * Get the single input that match the provided name.
     *
     * @param parameterItems The items to look through.
     * @param inputName      The name of the input to look for.
     * @return The collected inputs (not null).
     */
    public AnyContent getInputForName(List<AnyContent> parameterItems, String inputName) {
        List<AnyContent> inputs = getInputsForName(parameterItems, inputName);
        if (inputs.size() == 0) {
            return null;
        }
        if (inputs.size() != 1) {
            throw new IllegalArgumentException("A single input named [" + inputName + "] is expected.");
        }
        return inputs.get(0);
    }

    /**
     * Extract the String represented by the provided content.
     *
     * @param content The content to process.
     * @return The content's String representation.
     */
    public String extractContent(AnyContent content) {
        String stringContent = null;
        if (content != null && content.getValue() != null) {
            switch (content.getEmbeddingMethod()) {
                case STRING:
                    // Use string as-is.
                    stringContent = content.getValue();
                    break;
                case URI:
                    // Read the string from the provided URI.
                    URI uri = URI.create(content.getValue());
                    Proxy proxy = null;
                    List<Proxy> proxies = ProxySelector.getDefault().select(uri);
                    if (proxies != null && !proxies.isEmpty()) {
                        proxy = proxies.get(0);
                    }
                    BufferedReader in = null;
                    try {
                        URLConnection connection;
                        if (proxy == null) {
                            connection = uri.toURL().openConnection();
                        } else {
                            connection = uri.toURL().openConnection(proxy);
                        }
                        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuilder builder = new StringBuilder();
                        while ((inputLine = in.readLine()) != null) {
                            builder.append(inputLine);
                        }
                        stringContent = builder.toString();
                    } catch (IOException e) {
                        throw new IllegalArgumentException("Unable to read provided URI", e);
                    } finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e) {
                                // Ignore.
                            }
                        }
                    }
                    break;
                default: // BASE_64
                    // Construct the string from its BASE64 encoded bytes.
                    char[] buffer = new char[1024];
                    int numCharsRead;
                    StringBuilder sb = new StringBuilder();
                    try (BomStrippingReader reader = new BomStrippingReader(new ByteArrayInputStream(Base64.decodeBase64(content.getValue())))) {
                        while ((numCharsRead = reader.read(buffer, 0, buffer.length)) != -1) {
                            sb.append(buffer, 0, numCharsRead);
                        }
                    } catch (IOException e) {
                        throw new IllegalArgumentException("Failed to extract the provided content", e);
                    }
                    stringContent = sb.toString();
                    break;
            }
        }
        return stringContent;
    }

    /**
     * Create a AnyContent object value based on the provided parameters.
     *
     * @param name            The name of the value.
     * @param value           The value itself.
     * @param embeddingMethod The way in which this value is to be considered.
     * @return The value.
     */
    public AnyContent createAnyContentSimple(String name, String value, ValueEmbeddingEnumeration embeddingMethod) {
        return createAnyContentSimple(name, value, "string", embeddingMethod);
    }

    /**
     * Create a AnyContent object value based on the provided parameters.
     *
     * @param name            The name of the value.
     * @param value           The value itself.
     * @param type            The GITB type.
     * @param embeddingMethod The way in which this value is to be considered.
     * @return The value.
     */
    public AnyContent createAnyContentSimple(String name, String value, String type, ValueEmbeddingEnumeration embeddingMethod) {
        AnyContent input = new AnyContent();
        input.setName(name);
        input.setValue(value);
        input.setType(type);
        input.setEmbeddingMethod(embeddingMethod);
        return input;
    }

    /**
     * Add an error message to the report.
     *
     * @param message     The message.
     * @param reportItems The report's items.
     */
    private void addReportItemError(String message, List<JAXBElement<TestAssertionReportType>> reportItems) {
        reportItems.add(gitbTrObjectFactory.createTestAssertionGroupReportsTypeError(createReportItemContent(message)));
    }

    /**
     * Create the internal content of a report's item.
     *
     * @param message The message.
     * @return The content to wrap.
     */
    private BAR createReportItemContent(String message) {
        BAR itemContent = new BAR();
        itemContent.setDescription(message);
        return itemContent;
    }

    /**
     * Build the XPath expression to detect CISE payload data.
     *
     * @return The XPath expression.
     */
    private String buildDataElementXPathPattern() {
        StringBuilder bld = new StringBuilder();
        bld.append("/soapenv:Envelope/soapenv:Body/*/*[local-name()='Push' or  local-name()='PullResponse'  or  local-name()='PullRequest' or local-name()='Feedback' or local-name()='message']/*[local-name()='Payload']/*");
        bld.append("[local-name() = '");
        bld.append(CISE_DATA_MODEL_ELEMENT[0]);
        bld.append("'");
        for (int i = 1; i < CISE_DATA_MODEL_ELEMENT.length; ++i) {
            bld.append(" or  local-name() = '");
            bld.append(CISE_DATA_MODEL_ELEMENT[i]);
            bld.append("'");
        }
        bld.append("]");
        return bld.toString();
    }

    public TAR createMessageReportMessageAndAck(Acknowledgement ack, Message message) {
        TAR report = this.createReport(TestResultType.SUCCESS);
        String messageString = xmlMapper.toXML(message);
        report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_MESSAGE, messageString, "string", ValueEmbeddingEnumeration.STRING));
        report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT, xmlMapper.toXML(ack), "object", ValueEmbeddingEnumeration.STRING));
        if (ack.getAckCode() == AcknowledgementType.SUCCESS) {
            report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SIGNATURE, "VALID_SIGNATURE", "string", ValueEmbeddingEnumeration.STRING));
        } else {
            report.setResult(TestResultType.FAILURE);
            report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SIGNATURE, ack.getAckDetail(), "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;
    }

    public TAR createSentMessageReportMessageAndAck(Acknowledgement ack, Message message) {
        TAR report = this.createReport(TestResultType.SUCCESS);
        String messageString = xmlMapper.toXML(message);
        report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SENT_MESSAGE, messageString, "string", ValueEmbeddingEnumeration.STRING));
        report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT_FOR_SENT_MESSAGE, xmlMapper.toXML(ack), "object", ValueEmbeddingEnumeration.STRING));
        if (ack.getAckCode() == AcknowledgementType.SUCCESS) {
            report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SIGNATURE, "VALID_SIGNATURE", "string", ValueEmbeddingEnumeration.STRING));
        } else {
            report.setResult(TestResultType.FAILURE);
            report.getContext().getItem().add(this.createAnyContentSimple(REPORT_ELEMENT_SIGNATURE, ack.getAckDetail(), "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;
    }

}
