package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseMetaTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MetaTestStep1003DataFieldsBoundary extends CiseMetaTestStep {

    public MetaTestStep1003DataFieldsBoundary(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        List<CiseTestStepsEnum> steps = new ArrayList(Arrays.asList(
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_98_BOUNDARY_VESSEL,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_99_BOUNDARY_VEHICLE,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_100_BOUNDARY_CONTAINMENTUNIT,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_101_BOUNDARY_CONTAINEDCATCH,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_102_BOUNDARY_OPERATIONALASSET,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_103_BOUNDARY_METEOOCEANCONDITION,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_104_BOUNDARY_INCIDENT
        ));
        return createReport(messageXml, steps, CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1003_DATA_FIELDS_BOUNDARY);
    }
}
