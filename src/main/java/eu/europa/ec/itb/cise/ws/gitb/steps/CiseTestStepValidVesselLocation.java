package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import org.locationtech.spatial4j.context.jts.JtsSpatialContext;
import org.locationtech.spatial4j.context.jts.JtsSpatialContextFactory;
import org.locationtech.spatial4j.io.WKTReader;
import org.locationtech.spatial4j.shape.Shape;
import org.locationtech.spatial4j.shape.SpatialRelation;
import org.locationtech.spatial4j.shape.impl.PointImpl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class
CiseTestStepValidVesselLocation extends CiseTestStep {

    public CiseTestStepValidVesselLocation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }


    public TAR createReport(String receivedMessageXml, String sentMessageXml, CiseTestStepsEnum testStepRef, Class serviceClass) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        Location sentLocation = (Location) payloadHelper.getPayloadEntitiesOfType(sentMessageXml, Location.class).get(0).getA(); // assuming there is only one location in the request of form [|event.|risk.]vessel.location
        List<Pair<Entity, String>> receivedMainEntities = payloadHelper.getPayloadEntitiesOfType(receivedMessageXml, serviceClass);
        boolean vesselIsMainEntity = (serviceClass == Vessel.class);
        String[] errorText = new String[]{""};
        int countItemL0 = 0, countErrorL0 = 0;
        List resultsError = new ArrayList();
        List resultWarning = new ArrayList<>();
        for (Pair<Entity, String> receivedMainEntityPair : receivedMainEntities) {
            Entity mainEntity = receivedMainEntityPair.getA();
            String breadcrumb = receivedMainEntityPair.getB();
            if (vesselIsMainEntity) {
                LocalResult localResult = evaluateVesselLocation(receivedMainEntityPair, sentLocation, testStepRef);
                if (localResult.getCount() > 0) {
                    resultsError.add((String) (localResult.getError().stream().collect(Collectors.joining("/n/"))));
                } else if (localResult.getWarning().size() > 0) {
                    resultWarning.add((String) (localResult.getWarning()).toString());
                }

                countItemL0 = countItemL0 + 1;
            } else {
                String breadCrumbL1 = errorHelper.avoidNodeId(breadcrumb) + ".involvedObjectRel.vessel.";
                countItemL0 = countItemL0 + 100;
                int countError = 0, totalVessel = 0;
                List resultErrorPerMainEntity = new ArrayList<>();
                LocalResult perObjRelResult = null;
                if (serviceClass == Risk.class) {
                    List<Risk.InvolvedObjectRel> listObjectRels = ((Risk) mainEntity).getInvolvedObjectRels(); //*candidate stream*/
                    totalVessel = listObjectRels.size();
                    for (Risk.InvolvedObjectRel receivedObjectInMainEntityRelation : listObjectRels) {
                        countItemL0 = countItemL0 + 1;
                        perObjRelResult = evaluateVesselLocation(new Pair<Entity, String>(receivedObjectInMainEntityRelation.getObject(), breadCrumbL1 + countItemL0), sentLocation, testStepRef);
                        if (perObjRelResult.getCount() > 0) {
                            countError++;
                            resultErrorPerMainEntity.add((String) (perObjRelResult.getError()).toString());
                        } else if (perObjRelResult.getWarning().size() > 0) {
                            resultWarning.add((String) (perObjRelResult.getWarning()).toString());
                        }
                    }
                } else {
                    List<Event.InvolvedObjectRel> listObjectRels = ((Event) mainEntity).getInvolvedObjectRels(); //*candidate stream*/
                    totalVessel = listObjectRels.size();
                    for (Event.InvolvedObjectRel receivedObjectInMainEntityRelation : listObjectRels) {
                        countItemL0 = countItemL0 + 1;
                        perObjRelResult = evaluateVesselLocation(new Pair<Entity, String>(receivedObjectInMainEntityRelation.getObject(), breadCrumbL1 + countItemL0), sentLocation, testStepRef);
                        if (perObjRelResult.getCount() > 0) {
                            countError++;
                            resultErrorPerMainEntity.add((String) (perObjRelResult.getError()).toString());
                            // resultWarning.add((String) (perObjRelResult.getWarning()).toString()); // error exclude warning - don't add warning?
                        } else if (perObjRelResult.getWarning().size() > 0) {
                            resultWarning.add((String) (perObjRelResult.getWarning()).toString());
                        }
                    }
                }
                if (countError == totalVessel || (countError > 0 && (countError + resultWarning.size() == totalVessel))) {
                    resultsError.addAll(resultErrorPerMainEntity);
                    break;
                }
            }
        }
        if (resultsError.isEmpty()) {
            // TODO treat all warnings and local error as warning
            if (resultWarning.size() > 0) {
                report.setResult(TestResultType.WARNING);
                String warningsDescription = (String) resultsError.stream().collect(Collectors.joining("/n/"));
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), warningsDescription, "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.setResult(TestResultType.SUCCESS);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), ReportBuilder.REPORT_STEP_SUCCESS, "string", ValueEmbeddingEnumeration.STRING));
            }
        } else {
            report.setResult(TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), resultsError.toString(), "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;
    }


    private LocalResult evaluateVesselLocation(Pair<Entity, String> receivedEntityPair, Location sentReferenceLocation, CiseTestStepsEnum testStepRef) {
        LocalResult qualifiedResult = new LocalResult(); //[errortxt-warningtxt]
        int countError = 0;
        Vessel vessel = (Vessel) receivedEntityPair.getA();
        String breadcrumb = receivedEntityPair.getB();
        List<Objet.LocationRel> receivedLocationRels = vessel.getLocationRels();
        if (receivedLocationRels != null && !receivedLocationRels.isEmpty()) {
            // if coordinates are not expressed in Longitude/Latitude then add a WARNING
            if (coordinatesAreExpressedWithLongitudeAndLatitude(receivedLocationRels.get(0).getLocation())) {
                // else if coordinates are expressed correctly then check for coordinates in polygon
                if (!locationsHaveCoordinatesInPolygon(receivedLocationRels, sentReferenceLocation)) {
                    qualifiedResult.addError(
                            errorHelper.getExtraErrorDescription(testStepRef.getUiCheckName(), ".failure")
                                    + errorHelper.addPointOfFailure("", breadcrumb, vessel));
                }
            } else {
                qualifiedResult.addWarning(
                        errorHelper.getExtraErrorDescription(testStepRef.getUiCheckName(), ".warning.wrong.coordinates")
                                + errorHelper.addPointOfFailure("", breadcrumb, vessel));
            }
        } else {
            // if there are no locations defined ad a warning
            qualifiedResult.addWarning(
                    errorHelper.getExtraErrorDescription(testStepRef.getUiCheckName(), ".warning.missing.location")
                            + errorHelper.addPointOfFailure("", breadcrumb, vessel));
        }
        return (qualifiedResult);
    }

    private String addWarningToReport(TAR report, String failureExplanation, Vessel vessel, String breadcrumb) {
        if (report.getResult() != TestResultType.FAILURE) {
            report.setResult(TestResultType.WARNING);
        }
        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), vessel);
        return failureExplanation;
    }

    private boolean coordinatesAreExpressedWithLongitudeAndLatitude(Location receivedLocation) {

        if (receivedLocation != null && receivedLocation.getGeometries() != null && !receivedLocation.getGeometries().isEmpty()) {
            Geometry geometry = receivedLocation.getGeometries().get(0);
            return geometry.getLongitude() != null &&
                    geometry.getLatitude() != null &&
                    geometry.getWKT() == null &&
                    geometry.getXMLGeometry() == null;
        }
        return false;
    }

    private boolean locationsHaveCoordinatesInPolygon(List<Objet.LocationRel> vesselLocationsRels, Location targetAreaPolygon) {
        for (Objet.LocationRel locationRel : vesselLocationsRels) {
            if (!locationHasCoordinatesInPolygon(locationRel, targetAreaPolygon)) {
                return false;
            }
        }
        return true;
    }

    private boolean locationHasCoordinatesInPolygon(Objet.LocationRel vesselLocationRel, Location targetAreaPolygon) {
        // We expect only one geometry for target area and for vessel
        String targetAreaLocationWKT = targetAreaPolygon.getGeometries().get(0).getWKT();
        if (vesselLocationRel.getLocation() != null) {
            List<Geometry> vesselLocationGeometries = vesselLocationRel.getLocation().getGeometries();
            if (vesselLocationGeometries != null && vesselLocationGeometries.size() > 0) {
                Geometry vesselLocationGeometry = vesselLocationGeometries.get(0);
                return vesselIsInsideAreaWKT(vesselLocationGeometry, targetAreaLocationWKT);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean vesselIsInsideAreaWKT(Geometry vesselLocationGeometry, String targetAreaLocationwWKT) {
        JtsSpatialContextFactory jtsSpatialContextFactory = new JtsSpatialContextFactory();
        JtsSpatialContext jtsSpatialContext = jtsSpatialContextFactory.newSpatialContext();
        WKTReader wktReader = new WKTReader(jtsSpatialContext, jtsSpatialContextFactory);
        try {
            Shape targetAreaShape = wktReader.parse(targetAreaLocationwWKT);
            Shape vesselLocationShape = new PointImpl(Double.parseDouble(vesselLocationGeometry.getLongitude()), Double.parseDouble(vesselLocationGeometry.getLatitude()), jtsSpatialContext);
            SpatialRelation relate = targetAreaShape.relate(vesselLocationShape);
            return relate.intersects();
        } catch (ParseException e) {
            return false;
        }
    }


    private class LocalResult {
        List<String> errorList = new ArrayList<String>();
        List<String> warningList = new ArrayList<String>();
        int countError = 0;

        public List<String> getError() {
            return errorList;
        }

        public void addError(String error) {
            countError++;
            this.errorList.add(error);
        }

        public List<String> getWarning() {
            return warningList;
        }

        public void addWarning(String warning) {
            this.warningList.add(warning);
        }

        public int getCount() {
            return countError;
        }

    }
}

