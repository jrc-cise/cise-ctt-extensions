package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.*;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #24 AttachedDocument Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `VesselDocument` or `CargoDocument` or `EventDocument` or `LocationDocument` or `OrganizationDocument` or `PersonDocument` or `CertificateDocument` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define attribute `ContentReferenceURI`
 */
public class TestStep24AttachedDocumentMinimumDefinition extends CiseTestStep {


    public TestStep24AttachedDocumentMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, VesselDocument.class);
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, CargoDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, EventDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, LocationDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, OrganizationDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, RiskDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, PersonDocument.class));
        payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, CertificateDocument.class));

        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_24_ATTACHED_DOCUMENT_MIN_DEF;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (entityPair.getA() instanceof AttachedDocument) {
                    AttachedDocument document = (AttachedDocument) entityPair.getA();
                    if (!checkDocumentEntityForMinimumIdentification(document)) {
                        conform = false;
                        String breadcrumb = entityPair.getB();
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), document);
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred: " + e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }

    private boolean checkDocumentEntityForMinimumIdentification(Document document) {
        byte[] content = ((AttachedDocument) document).getContent();
        String referenceUri = ((AttachedDocument) document).getReferenceURI();

        if ((content != null && content.length > 0) ||
                (referenceUri != null && referenceUri.length() > 0)) {
            return true;
        }
        return false;
    }

}

