package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;

import java.util.List;
 /**
 * #40 Geometry Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *Element* of type `Geometry` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define set of Attribute `Latitude,Longitude'  Or `WKT` or `XMLGeometry` (exclusively one from others)
 */
public class TestStep40GeometryMinimumDefinition extends CiseTestStep {

    public TestStep40GeometryMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_40_GEOMETRY_MIN_DEF;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Location.class);
        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Location location = (Location) entityPair.getA();
                if (!geometryHasMinimumDefinitionInforamtion(location)) {
                    conform = false;
                    String breadcrumb = entityPair.getB();
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), location);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred: " + e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }


    protected boolean geometryHasMinimumDefinitionInforamtion(Location location) {
        List<Geometry> geometries = location.getGeometries();
        if (geometries == null || (geometries != null && geometries.isEmpty())) {
            return true;
        }
        Geometry geometry = geometries.get(0);
        if (
                (Strings.isNotBlank(geometry.getLatitude()) &&
                        Strings.isNotBlank(geometry.getLongitude()) &&
                        Strings.isBlank(geometry.getWKT()) &&
                        Strings.isBlank(geometry.getXMLGeometry())
                ) ||
                        (Strings.isBlank(geometry.getLatitude()) &&
                                Strings.isBlank(geometry.getLongitude()) &&
                                Strings.isNotBlank(geometry.getWKT()) &&
                                Strings.isBlank(geometry.getXMLGeometry())
                        ) ||
                        (Strings.isBlank(geometry.getLatitude()) &&
                                Strings.isBlank(geometry.getLongitude()) &&
                                Strings.isBlank(geometry.getWKT()) &&
                                Strings.isNotBlank(geometry.getXMLGeometry())
                        )
        ) {
            return true;
        }

        return false;
    }
}
