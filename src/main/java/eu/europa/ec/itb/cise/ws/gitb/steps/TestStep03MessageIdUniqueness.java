package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.servicemodel.v1.message.Message;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

public class TestStep03MessageIdUniqueness extends CiseTestStep {

    MessageIdStore messageIdStore;

    public TestStep03MessageIdUniqueness(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
        messageIdStore = reportBuilder.getMessageIdStore();
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_03_MESSAGEID_UNIQUENESS;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        Message message = reportBuilder.getXmlMapper().fromXML(messageXml);

        if (messageIdStore.exists(message.getMessageID())) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), failureExplanation, "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        } else {
            messageIdStore.record(message.getMessageID());
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), reportBuilder.REPORT_STEP_SUCCESS, "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;
    }
}
