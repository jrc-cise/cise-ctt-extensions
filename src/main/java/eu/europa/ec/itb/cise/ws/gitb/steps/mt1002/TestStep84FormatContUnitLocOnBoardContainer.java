package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #84 FORMAT ContainmentUnit LocationOnBoardContainer
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `ContainmentUnit` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `LocationOnBoardContainer` is defined
 * `*THEN*` attribute `LocationOnBoardContainer` must be composed by 1 upper case letter followed by a semicolon ":" and Alphanumerical characters in accordance to ISO 28005.
 */
public class TestStep84FormatContUnitLocOnBoardContainer extends CiseTestStep {

    public TestStep84FormatContUnitLocOnBoardContainer(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_84_FORMAT_CONT_UNIT_LOC_ON_BOARD_CONTAINER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, ContainmentUnit.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!isValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * TODO: create the logic (return false)
     *
     * @param entity
     * @return
     */
    private boolean isValid(Entity entity) {
        if (entity instanceof ContainmentUnit) {
            String locationOnBoardContainer = ((ContainmentUnit) entity).getLocationOnBoardContainer();
            if (locationOnBoardContainer != null) {
                boolean char1, char2, followingChars;
                char1 = locationOnBoardContainer.trim().substring(0, 1).matches("^[a-zA-Z]+$");
                char2 = locationOnBoardContainer.trim().substring(1, 2).equals(":");
                followingChars = locationOnBoardContainer.trim().substring(2).matches("^[a-zA-Z0-9:]+$");
                return (char1 && char2 && followingChars);
            }
        }
        return true;
    }

}
