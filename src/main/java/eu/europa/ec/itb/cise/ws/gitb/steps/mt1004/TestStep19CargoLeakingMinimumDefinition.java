package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 *#19 CargoLeaking Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `PollutionIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` its *attribute* `AnomalyType` have value :`CARGO_LEAKING`
 * `*THEN*` it must define at least one relation to an entity of type `Vessel` or  `Cargo` or  `ContainmentUnit`
 */
public class TestStep19CargoLeakingMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep19CargoLeakingMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }


    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Anomaly.class;
        Class[] linkedClass = new Class[]{Vessel.class, Cargo.class, ContainmentUnit.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        Anomaly anomaly = (Anomaly) event;
        return AnomalyType.CARGO_LEAKING == anomaly.getAnomalyType();
    }


}
