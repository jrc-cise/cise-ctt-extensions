package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #59 MeteoOceanographicCondition Minimum Identification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * *GIVEN* entity(ies) of type "`MeteoOceanographicCondition`" is defined
 * in PUSH or PUSH_RESPONSE or PUSH_SUBSCRIBE payload
 * *THEN* it must define attribute
 * AirTemperature
 * or CloudCeiling
 * or CloudCover or
 * or Precipitation
 * or ReferencePeriod
 * or Salinity
 * or SeaCondition
 * or SeaLevelPressure
 * or SourceType
 * or TidalCurrentDirection
 * or TidalCurrentSpeed
 * or Tides
 * or Visibility
 * or WaterTemperature
 * or WaveDirection
 * or WaveHeight
 * or WeatherCondition
 * or WindCurrentDirection
 * or WindCurrentSpeed.
 */

public class TestStep59MeteoOceanographicConditionMinimumIdentification extends CiseTestStepOneOfChildValue {

    public TestStep59MeteoOceanographicConditionMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClass = new Class[]{MeteoOceanographicCondition.class};
        String[] fieldNames = new String[]{
                "airTemperature",
                "cloudCeiling",
                "cloudCover",
                "precipitation",
                "referencePeriod",
                "salinity",
                "seaCondition",
                "seaLevelPressure",
                "sourceType",
                "tidalCurrentDirection",
                "tidalCurrentSpeed",
                "tides",
                "visibility",
                "waterTemperature",
                "waveDirection",
                "waveHeight",
                "weatherCondition",
                "windCurrentDirection",
                "windCurrentSpeed"};
        return super.createReport(messageXml, fieldNames, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_59_METEOOCEANOGRAPHICCONDITION_MIN_IDENT);
    }
}