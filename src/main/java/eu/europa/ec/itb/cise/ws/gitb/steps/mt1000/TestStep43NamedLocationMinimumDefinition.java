package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildListValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #43 NamedLocation Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `NamedLocation` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define attribute `geographicNames`
 */
public class TestStep43NamedLocationMinimumDefinition extends CiseTestStepOneOfChildListValue {

    public TestStep43NamedLocationMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        String[] fieldsName = new String[]{"geographicNames"};
        Class[] entityClass = new Class[]{NamedLocation.class};
        return super.createReport(messageXml, fieldsName, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_43_NAMED_LOCATION_MIN_DEF);
    }

    @Override
    protected boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof NamedLocation)
            return true;
        return false;
    }
}
