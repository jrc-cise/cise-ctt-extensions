package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.document.CargoDocument;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #35 AttachedDocument Coherent With Cargo
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Cargo` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `CargoDocument`
 */
public class TestStep35AttachedDocumentCoherentWithCargo extends CiseTestStepAttachedDocument {

    public TestStep35AttachedDocumentCoherentWithCargo(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Cargo.class};
        Class[] validAttachedDocumentTypes = new Class[]{CargoDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_35_ATTACHED_DOCUMENT_COHERENT_WITH_CARGO);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Cargo cargo = (Cargo) entity;
        List<Objet.DocumentRel> documentRels = cargo.getDocumentRels();
        if (documentRels == null || documentRels.size() == 0) {
            return true;
        }
        for (Objet.DocumentRel documentRel : documentRels) {
            if (!(isInstanceOfOneOf(documentRel.getDocument(), validAttachedDocumentTypes))) {
                return false;
            }
        }

        return true;
    }

}
