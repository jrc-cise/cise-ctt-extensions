package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #44 PortLocation Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `PortLocation` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define attribute `portName´, ´locationCode`
 */
public class TestStep44PortLocationMinimumDefinition extends CiseTestStepOneOfChildValue {

    public TestStep44PortLocationMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClass = new Class[]{PortLocation.class};
        String[] fieldNames = new String[]{"portName", "locationCode"};
        return super.createReport(messageXml, fieldNames, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_44_PORT_LOCATION_MIN_DEF);
    }

    @Override
    protected boolean isEntityOfCorrectType(Entity entity) {
        if (entity instanceof PortLocation)
            return true;
        return false;
    }

}
