package eu.europa.ec.itb.cise.ws.search;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Node {

    private final String name;
    private final List<Node> children;
    private final String computedName;
    private final Object ciseObject;
    Node parent;
    private Integer id;

    public Node(String name, Object ciseObject) {
        this.name = name;
        this.computedName = name;
        this.ciseObject = ciseObject;
        this.parent = null;
        this.children = new LinkedList<>();
    }

    public Node(String name, Node parent, Object ciseObject, Integer id) {
        this.name = name;
        this.ciseObject = ciseObject;
        this.id = id;
        this.computedName = parent == null ? name : parent.getComputedName() + "." + name;
        this.parent = parent;
        this.children = new LinkedList<>();
    }

    public Integer getId() {
        return id;
    }

    public Object getCiseObject() {
        return ciseObject;
    }

    public String getComputedName() {
        return computedName;
    }

    public Node addChild(Node node) {
        this.children.add(node);
        return this;
    }

    public String getName() {
        return name;
    }

    public List<Node> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(name, node.name) &&
                Objects.equals(ciseObject, node.ciseObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ciseObject);
    }
}