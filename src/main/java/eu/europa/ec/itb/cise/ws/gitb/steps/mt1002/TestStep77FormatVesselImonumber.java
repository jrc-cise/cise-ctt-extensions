package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #77 FORMAT Vessel IMONumber Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `IMONumber` is defined
 * `*THEN*` attribute `IMONumber` value must be composed of exactly 7 numerical characters
 * `*AND*` attribute `IMONumber` value should support validation algorithm .
 */
public class TestStep77FormatVesselImonumber extends CiseTestStep {

    private TAR report;

    public TestStep77FormatVesselImonumber(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_77_FORMAT_VESSEL_IMONUMBER;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    private boolean warningValid(Entity entity) {
        if (entity instanceof Vessel) {
            Long imoNumber = ((Vessel) entity).getIMONumber();
            if (imoNumber == null)
                return true;
            else {
                if (imoNumber.toString().length() == 7)
                    return imoNumberCheckSumTest(imoNumber.toString());
            }
        }
        return false;
    }

    private boolean errorValid(Entity entity) {
        if (entity instanceof Vessel) {
            Long imoNumber = ((Vessel) entity).getIMONumber();
            if (imoNumber == null)
                return true;
            else if (imoNumber.toString().length() != 7)
                return false;
        }
        return true;
    }

    /**
     * Checksum:  last digit of (digit1*7+ digit2*6+ digit3*5+ digit4*4+ digit5*3+ digit6*2) is digit7 (! Be careful, do not share this checksum)
     *
     * @param imonumber
     * @return boolean
     */
    private boolean imoNumberCheckSumTest(CharSequence imonumber) {
        boolean valid = false;
        Integer checksumPadding = (Integer.parseInt("" + imonumber.charAt(0)) * 7) +
                (Integer.parseInt("" + imonumber.charAt(1)) * 6) +
                (Integer.parseInt("" + imonumber.charAt(2)) * 5) +
                (Integer.parseInt("" + imonumber.charAt(3)) * 4) +
                (Integer.parseInt("" + imonumber.charAt(4)) * 3) +
                (Integer.parseInt("" + imonumber.charAt(5)) * 2);
        String checksumPaddingString = checksumPadding.toString();
        return (checksumPaddingString.substring(checksumPaddingString.length() - 1)).equals("" + imonumber.charAt(6));
    }

    /**/

}
