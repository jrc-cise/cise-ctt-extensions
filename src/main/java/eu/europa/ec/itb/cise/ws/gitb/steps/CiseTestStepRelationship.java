package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.agent.*;
import eu.cise.datamodel.v1.entity.event.AgentRoleInEventType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.event.ObjectRoleInEventType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.gitb.core.ValueEmbeddingEnumeration.STRING;
import static com.gitb.tr.TestResultType.FAILURE;
import static com.gitb.tr.TestResultType.SUCCESS;

public abstract class CiseTestStepRelationship extends CiseTestStep {

    protected ObjectRoleInEventType objectRole;
    protected AgentRoleInObjectType objectAgentRole;
    protected AgentRoleInLocationType locationAgentRole;
    protected AgentRoleInAgentType agentAgentRole;
    protected AgentRoleInEventType eventAgentRole;
    protected Boolean isTransitPassenger = null;
    protected DutyType dutyType = null;

    public CiseTestStepRelationship(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    /**
     * Creates the report for linked Entities and handle criteria on relationship properties
     *
     * @param messageXml                input message
     * @param relationshipClassesToFind relationship to find
     * @param classesA                  first set of classes to be related
     * @param classesB                  second set of classes to be related
     * @param testStepRef               code to reference the specific test step
     * @return                          returns the report
     */
    protected TAR createReport(String messageXml, Class<Relationship>[] relationshipClassesToFind, Class<Entity>[] classesA, Class<Entity>[] classesB, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Relationship, String>> relationships = getRelationshipsOfType(messageXml, relationshipClassesToFind);

        try {
            RelationshipTest relationshipTest = new RelationshipTest(messageXml, classesA, classesB, failureExplanation, relationships, true).performTest();
            report.setResult(relationshipTest.computeResultType());
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), relationshipTest.computeFailureDescription(), "string", STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", STRING));
            report.setResult(FAILURE);
        }
        return report;
    }


    @NotNull
    private List<Pair<Relationship, String>> getRelationshipsOfType(String messageXml, Class<Relationship>[] relationshipClassesToFind) {
        List<Pair<Relationship, String>> relationships = new ArrayList<>();
        for (Class aClass : relationshipClassesToFind) {
            relationships.addAll(payloadHelper.getPayloadRelationOfType(messageXml, aClass));
        }
        return relationships;
    }

    protected abstract boolean isRelationshipOfCorrectType(Relationship relationship);

    /**
     * This function should return true if one class of classesA is in relation with one class of classesB starting from th node indicated by breadcrumb.
     *
     * @param relationship
     * @param classesA
     * @param classesB
     * @param messageXml
     * @param breadcrumb
     * @return
     */
    private boolean relationshipIsLinkingOneMemberOfTheTwoSets(Relationship relationship, Class<Entity>[] classesA, Class<Entity>[] classesB, String messageXml, String breadcrumb) {
        if (relationshipIsLinkingOneMemberOfSetAtoOneMemberOfSetB(relationship, classesA, classesB, messageXml, breadcrumb)) {
            return true;
        }

        return relationshipIsLinkingOneMemberOfSetAtoOneMemberOfSetB(relationship, classesB, classesA, messageXml, breadcrumb);
    }

    private boolean relationshipIsLinkingOneMemberOfSetAtoOneMemberOfSetB(Relationship relationship, Class<Entity>[] classesA, Class<Entity>[] classesB, String messageXml, String breadcrumb) {
        Entity parentOfTypeA = null;
        for (Class<Entity> entityClassA : classesA) {
            parentOfTypeA = payloadHelper.getParentOfType(messageXml, breadcrumb, entityClassA);
            if (parentOfTypeA != null) {
                if (relationship instanceof Objet.InvolvedEventRel) {
                    if (isInstanceOfOneOf(((Objet.InvolvedEventRel) relationship).getEvent(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Objet.InvolvedAgentRel) {
                    if (isInstanceOfOneOf(((Objet.InvolvedAgentRel) relationship).getAgent(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Agent.LocationRel) {
                    if (isInstanceOfOneOf(((Agent.LocationRel) relationship).getLocation(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Agent.InvolvedWithRel) {
                    if (isInstanceOfOneOf(((Agent.InvolvedWithRel) relationship).getAgent(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Event.InvolvedObjectRel) {
                    if (isInstanceOfOneOf(((Event.InvolvedObjectRel) relationship).getObject(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Event.LocationRel) {
                    if (isInstanceOfOneOf(((Event.LocationRel) relationship).getLocation(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Agent.InvolvedObjectRel) {
                    if (isInstanceOfOneOf(((Agent.InvolvedObjectRel) relationship).getObject(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Agent.InvolvedEventRel) {
                    if (isInstanceOfOneOf(((Agent.InvolvedEventRel) relationship).getEvent(), classesB)) {
                        return true;
                    }
                }
                if (relationship instanceof Event.InvolvedAgentRel) {
                    if (isInstanceOfOneOf(((Event.InvolvedAgentRel) relationship).getAgent(), classesB)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected String addPointOfFailureForRelationship(String failureExplanation, String breadcrumb, Relationship relationship) {
        failureExplanation += "- " + breadcrumb + "\n";
        if (locationAgentRole != null) {
            failureExplanation += "  AgentRole: " + locationAgentRole.value() + "\n";
        }
        if (objectRole != null) {
            failureExplanation += "  ObjectRole: " + objectRole.value() + "\n";
        }
        if (objectAgentRole != null) {
            failureExplanation += "  Agent Role: " + objectAgentRole.value() + "\n";
        }
        if (eventAgentRole != null) {
            failureExplanation += "  Agent Role: " + eventAgentRole.value() + "\n";
        }
        if (agentAgentRole != null) {
            failureExplanation += "  Agent Role: " + eventAgentRole.value() + "\n";
        }
        if (isTransitPassenger != null) {
            failureExplanation += "  Transit Passenger: " + isTransitPassenger + "\n";
        }
        if (dutyType != null) {
            failureExplanation += "  Duty Type: " + dutyType.value() + "\n";
        }
        return failureExplanation;
    }

    private class RelationshipTest {
        private final String messageXml;
        private final Class<Entity>[] classesA;
        private final Class<Entity>[] classesB;
        private final List<Pair<Relationship, String>> payloadRelationshipOfType;
        private String failureExplanation;
        private boolean conform;

        RelationshipTest(String messageXml, Class<Entity>[] classesA, Class<Entity>[] classesB, String failureExplanation, List<Pair<Relationship, String>> payloadRelationshipOfType, boolean conform) {
            this.messageXml = messageXml;
            this.classesA = classesA;
            this.classesB = classesB;
            this.failureExplanation = failureExplanation;
            this.payloadRelationshipOfType = payloadRelationshipOfType;
            this.conform = conform;
        }

        public String getFailureExplanation() {
            return failureExplanation;
        }

        public boolean isConform() {
            return conform;
        }

        public RelationshipTest performTest() {
            for (Pair<Relationship, String> relationshipStringPair : payloadRelationshipOfType) {
                Relationship relationship = relationshipStringPair.getA();
                String breadcrumb = relationshipStringPair.getB();

                if (relationshipIsLinkingOneMemberOfTheTwoSets(relationship, classesA, classesB, messageXml, breadcrumb) &&
                        !isRelationshipOfCorrectType(relationship)
                ) {
                    conform = false;
                    failureExplanation = addPointOfFailureForRelationship(failureExplanation, errorHelper.avoidNodeId(breadcrumb), relationship);
                }
            }
            return this;
        }

        public String computeFailureDescription() {
            return conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation;
        }

        @NotNull
        private TestResultType computeResultType() {
            return conform ? SUCCESS : FAILURE;
        }


    }
}
