package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.document.PersonDocument;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #37 AttachedDocument Coherent With Person
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Person` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `PersonDocument`
 */
public class TestStep38AttachedDocumentCoherentWithPerson extends CiseTestStepAttachedDocument {

    public TestStep38AttachedDocumentCoherentWithPerson(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Person.class};
        Class[] validAttachedDocumentTypes = new Class[]{PersonDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_38_ATTACHED_DOCUMENT_COHERENT_WITH_PERSON);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Person person = (Person) entity;
        List<Agent.DocumentRel> documentRels = person.getDocumentRels();
        if (documentRels == null || documentRels.size() == 0) {
            return true;
        }
        for (Agent.DocumentRel documentRel : documentRels) {
            if (!(isInstanceOfOneOf(documentRel.getDocument(), validAttachedDocumentTypes))) {
                return false;
            }
        }

        return true;
    }
}
