package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.core.*;
import com.gitb.tr.TAR;
import com.gitb.vs.Void;
import com.gitb.vs.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Custom validation service used to validate message creation timestamps and message ID uniqueness.
 * <p>
 * In general the input of this service can be used to validate text tokens.
 */
public class CiseValidationService implements ValidationService {

    private static final String INPUT_VALIDATION_TYPE = "type";
    private static final String INPUT_CONTENT = "content";
    private static final String INPUT_REQUEST_CONTENT = "requestContent";
    private ReportBuilder reportBuilder;
    private CisePayloadHelper payloadHelper;
    private GitbErrorHelper errorHelper;
    private static final Logger LOG = LoggerFactory.getLogger(CiseValidationService.class);
    ;

    public CiseValidationService(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        this.reportBuilder = reportBuilder;
        this.payloadHelper = payloadHelper;
        this.errorHelper = errorHelper;
    }

    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        GetModuleDefinitionResponse response = new GetModuleDefinitionResponse();
        response.setModule(new ValidationModule());
        response.getModule().setId("CISE_Validator");
        response.getModule().setMetadata(new Metadata());
        response.getModule().getMetadata().setName(response.getModule().getId());
        response.getModule().getMetadata().setVersion("1.0");
        response.getModule().setInputs(new TypedParameters());
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_VALIDATION_TYPE, "string", UsageEnumeration.R, ConfigurationType.SIMPLE, "The type of validation to perform."));
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_CONTENT, "string", UsageEnumeration.R, ConfigurationType.SIMPLE, "The content to validate."));
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_REQUEST_CONTENT, "string", UsageEnumeration.O, ConfigurationType.SIMPLE, "The content sent to the SUT."));
        return response;
    }

    @Override
    public ValidationResponse validate(ValidateRequest validateRequest) {
        List<AnyContent> validateRequestInput = validateRequest.getInput();
        AnyContent validationTypeInput = reportBuilder.getInputForName(validateRequestInput, INPUT_VALIDATION_TYPE);
        AnyContent validationContentInput = reportBuilder.getInputForName(validateRequestInput, INPUT_CONTENT);
        AnyContent validationRequestContentInput = reportBuilder.getInputForName(validateRequestInput, INPUT_REQUEST_CONTENT);

        String validationRequestContentInputValue = null;
        if (validationRequestContentInput != null) {
            validationRequestContentInputValue = validationRequestContentInput.getValue();
        }

        TAR report = generateReport(validationTypeInput.getValue(), validationContentInput.getValue(), validationRequestContentInputValue);

        ValidationResponse response = new ValidationResponse();
        response.setReport(report);
        return response;
    }

    public TAR generateReport(String validationTypeInputValue, String validationContentInputValue, String validationRequestContentInputValue) {
        TAR report = null;
        try {
            Class classFromUiCheckName = CiseTestStepsEnum.getClassFromUiCheckName(validationTypeInputValue);
            Constructor constructor = classFromUiCheckName.getConstructor(ReportBuilder.class, CisePayloadHelper.class, GitbErrorHelper.class);
            CiseTestStep testStep = (CiseTestStep) constructor.newInstance(reportBuilder, payloadHelper, errorHelper);

            if (validationRequestContentInputValue != null) {
                report = testStep.createReport(validationContentInputValue, validationRequestContentInputValue);
            } else {
                report = testStep.createReport(validationContentInputValue);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            LOG.error("error with validation v TypeInputValue: [" + validationTypeInputValue + "]" +
                    " / ContentInputValue: " + validationContentInputValue + "]" +
                    " / RequestContentInputValue : " + validationRequestContentInputValue + "]");
            e.printStackTrace();
        }
        return report;
    }

}
