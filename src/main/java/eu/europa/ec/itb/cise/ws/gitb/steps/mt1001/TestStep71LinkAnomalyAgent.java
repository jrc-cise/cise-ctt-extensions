package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.event.AgentRoleInEventType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #71 Link Anomaly Agent
 * ^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Anomaly` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Agent` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `COORDINATOR`,`PERPETRATOR`.
 */
public class TestStep71LinkAnomalyAgent extends CiseTestStepRelationship {

    public TestStep71LinkAnomalyAgent(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Anomaly.class};
        Class[] agentClasses = new Class[]{Agent.class};
        Class[] relationClasses = new Class[]{Event.InvolvedAgentRel.class, Agent.InvolvedEventRel.class};
        return createReport(messageXml, relationClasses, eventClasses, agentClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_71_LINK_ANOMALY_AGENT);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {

        if (relationship instanceof Agent.InvolvedEventRel) {
            eventAgentRole = ((Agent.InvolvedEventRel) relationship).getAgentRole();
        } else if (relationship instanceof Event.InvolvedAgentRel) {
            eventAgentRole = ((Event.InvolvedAgentRel) relationship).getAgentRole();
        } else {
            return false;
        }

        return !isAgentRoleOfWrongType(eventAgentRole);
    }

    private boolean isAgentRoleOfWrongType(AgentRoleInEventType eventAgentRole) {
        return eventAgentRole == AgentRoleInEventType.COORDINATOR ||
                eventAgentRole == AgentRoleInEventType.PERPETRATOR;
    }
}
