package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.servicemodel.v1.message.Message;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class TestStep04CreationDate extends CiseTestStep {

    public TestStep04CreationDate(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_04_CREATION_DATE;

        Message message = reportBuilder.getXmlMapper().fromXML(messageXml);
        Instant messageCreationTime = message.getCreationDateTime().toGregorianCalendar().toInstant();
        Instant now = Instant.now();
        if (messageCreationTime.isBefore(now.minus(3, ChronoUnit.HOURS))) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName() + ".before"), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        } else if (messageCreationTime.isAfter(now)) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName() + ".after"), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        } else {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), reportBuilder.REPORT_STEP_SUCCESS, "string", ValueEmbeddingEnumeration.STRING));
        }
        return report;

    }
}
