package eu.europa.ec.itb.cise.ws;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Used to generate, record and check message IDs (for uniqueness).
 * <p>
 * For simplicity, all previously received message IDs are persisted in a file (one message ID per line).
 */
public class MessageIdStore {

    private String persistentMessageIdFilePath;

    private Path messageIdFilePath;
    private Set<String> previouslyPersistedMessageIds = Collections.synchronizedSet(new HashSet<>());
    private Map<String, Integer> currentlyReceivedMessageIds = new ConcurrentHashMap<>();

    public MessageIdStore(String persistentMessageIdFilePath) {
        this.persistentMessageIdFilePath = persistentMessageIdFilePath;
        initialise();
    }

    /**
     * Load the previously recorded message IDs.
     */
    public void initialise() {
        // Load the already recorded message IDs. Each message ID is stored on a different line.
        if (System.getProperty("os.name").contains("Windows"))
            persistentMessageIdFilePath = System.getProperty("user.home") + persistentMessageIdFilePath.replace("/", "\\");
        try {
            messageIdFilePath = Paths.get(persistentMessageIdFilePath);
            if (Files.exists(messageIdFilePath)) {
                // File exists - load contents
                if (Files.isDirectory(messageIdFilePath)
                        || !Files.isWritable(messageIdFilePath)
                        || !Files.isReadable(messageIdFilePath)) {
                    throw new IllegalStateException("The message ID file is not a writable text file.");
                }
                previouslyPersistedMessageIds = Collections.synchronizedSet(new HashSet<>(Files.readAllLines(messageIdFilePath)));
            } else {
                // File missing - initialise
                Files.createFile(messageIdFilePath);
            }
        } catch (IOException e) {
            throw new IllegalStateException("An error occurred while reading the already recorded message ID file [" + persistentMessageIdFilePath + "].");
        }
    }

    /**
     * Check if a given message ID has already been received.
     *
     * @param messageId The ID to check.
     * @return The check result.
     */
    public boolean exists(String messageId) {
        /*
        A message ID is considered as existing if it was previously persisted or if it has been received
        more than once. When this is checked for a valid message ID it will already be recorded with a
        count of "1".
         */
        return previouslyPersistedMessageIds.contains(messageId)
                || (currentlyReceivedMessageIds.getOrDefault(messageId, 0) > 1);
    }

    /**
     * Generate an ID and record it.
     *
     * @return The ID.
     */
    public String getAndRecord() {
        String uuid = UUID.randomUUID().toString();
        record(uuid);
        return uuid;
    }

    /**
     * Record a message ID.
     *
     * @param messageId The ID To record.
     */
    public void record(String messageId) {
        if (messageId != null && !previouslyPersistedMessageIds.contains(messageId)) {
            try {
                if (currentlyReceivedMessageIds.containsKey(messageId)) {
                    currentlyReceivedMessageIds.put(messageId, currentlyReceivedMessageIds.getOrDefault(messageId, 0) + 1);
                } else {
                    currentlyReceivedMessageIds.put(messageId, 1);
                    Files.write(messageIdFilePath, (System.lineSeparator() + messageId).getBytes(), StandardOpenOption.APPEND);
                }
            } catch (IOException e) {
                throw new IllegalStateException("A message ID could not be recorded.", e);
            }
        }
    }

}
