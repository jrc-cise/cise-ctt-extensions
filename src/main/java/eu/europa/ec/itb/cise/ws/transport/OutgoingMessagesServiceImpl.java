package eu.europa.ec.itb.cise.ws.transport;

import com.gitb.core.*;
import com.gitb.ms.Void;
import com.gitb.ms.*;
import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Message;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.CTTSendParam;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.gitb.GitbConfigHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.SessionData;
import eu.europa.ec.itb.cise.ws.gitb.SessionManager;
import eu.europa.ec.itb.cise.ws.util.MessageTypeEnum;
import eu.europa.ec.itb.cise.ws.util.Pair;
import org.apache.cxf.headers.Header;
import org.apache.cxf.ws.addressing.AddressingConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.annotation.Resource;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import java.util.List;

/**
 * Spring component that realises the messaging service.
 */
public class OutgoingMessagesServiceImpl implements MessagingService {

    /**
     * Service inputs.
     */
    public static final String INPUT_MESSAGE_TO_SEND = "messageToSend";
    public static final String INPUT_MESSAGE_TYPE_TO_SEND = "messageTypeToSend";
    public static final String INPUT_REPLACE_UUID = "replaceUuid";
    public static final String INPUT_REPLACE_CREATION_TIMESTAMP = "replaceCreationTimestamp";
    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(OutgoingMessagesServiceImpl.class);
    /**
     * The name of the WS-Addressing ReplyTo field.
     */
    private static QName replyToQName = new AddressingConstants().getReplyToQName();
    private final GitbConfigHelper gitbConfigHelper = new GitbConfigHelper();
    private SessionManager sessionManager;
    @Resource
    private WebServiceContext wsContext = null;
    private MessageIdStore messageIdStore;
    private XmlMapper xmlMapper;
    private MessageProcessor messageProcessor;

    private ReportBuilder reportBuilder;

    public OutgoingMessagesServiceImpl(SessionManager sessionManager, MessageIdStore messageIdStore, XmlMapper xmlMapper, MessageProcessor messageProcessor, ReportBuilder reportBuilder) {
        this.sessionManager = sessionManager;
        this.messageIdStore = messageIdStore;
        this.xmlMapper = xmlMapper;
        this.messageProcessor = messageProcessor;
        this.reportBuilder = reportBuilder;
    }

    /**
     * The purpose of the getModuleDefinition call is to inform its caller on how the service is supposed to be called.
     * <p>
     * In this case its main purpose is to define the expected input and output parameters:
     * <ul>
     *     <li>The message to send (string).</li>
     *     <li>The message received (string).</li>
     * </ul>
     * Note that defining output messages is optional as any and all output will be send back to the test bed regardless.
     * It is important however to define all expected inputs here as these are checked by the test bed before making the
     * actual call. Typically all such inputs need to be defined as optional considering that they apply both to the send
     * and receive operations which would likely not require the same inputs. The reason for this was to support inputs
     * for the receive operation but maintain backwards compatibility for existing services. As such, it is best to
     * define all input as optional and simply check them as part of the send and/or receive logic.
     *
     * @param parameters No parameters are expected.
     * @return The response.
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void parameters) {
        GetModuleDefinitionResponse response = new GetModuleDefinitionResponse();
        response.setModule(new MessagingModule());
        response.getModule().setId("CISE_GITBMessagingService");
        response.getModule().setMetadata(new Metadata());
        response.getModule().getMetadata().setName(response.getModule().getId());
        response.getModule().getMetadata().setVersion("1.0");
        response.getModule().setInputs(new TypedParameters());
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_MESSAGE_TO_SEND, "object", UsageEnumeration.O, ConfigurationType.SIMPLE, "The message to send."));
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_MESSAGE_TYPE_TO_SEND, "string", UsageEnumeration.O, ConfigurationType.SIMPLE, "The message type to send [" + MessageTypeEnum.PUSH.getType() + ", " + MessageTypeEnum.PULL_REQUEST.getType() + ", " + MessageTypeEnum.PULL_RESPONSE.getType() + ", " + MessageTypeEnum.ACKNOWLEDGEMENT.getType() + "]."));
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_REPLACE_UUID, "boolean", UsageEnumeration.O, ConfigurationType.SIMPLE, "Whether or not to replace the UUID."));
        response.getModule().getInputs().getParam().add(reportBuilder.createParameter(INPUT_REPLACE_CREATION_TIMESTAMP, "boolean", UsageEnumeration.O, ConfigurationType.SIMPLE, "Whether or not to replace the creation timestamp."));
        return response;
    }

    /**
     * The initiate operation is called by the test bed when a new test session is being prepared.
     * <p>
     * This call expects from the service to do the following:
     * <ul>
     *     <li>Generate and store a session identifier to keep track of messages linked to the test session.</li>
     *     <li>Process, if needed, the configuration provided by the SUT.</li>
     *     <li>Return, if needed, configuration to be displayed to the user for the SUT actor.</li>
     * </ul>
     *
     * @param parameters The actor configuration provided by the SUT.
     * @return The session ID and any generated configuration to display for the SUT.
     */
    @Override
    public InitiateResponse initiate(InitiateRequest parameters) {
        InitiateResponse response = new InitiateResponse();
        // Get the ReplyTo address for the test bed callbacks based on WS-Addressing.
        String replyToAddress = getReplyToAddress();
        String sessionId = sessionManager.createSession(replyToAddress);
        response.setSessionId(sessionId);
        LOG.info("Initiated a new session [{}] with callback address [{}] with [{}]", sessionId, replyToAddress, parameters.toString());
        return response;
    }

    /**
     * The receive operation is called when the test bed is expecting to receive a message.
     * <p>
     * Typically this implementation is empty but it could be customised based on received input parameters.
     *
     * @param parameters The input parameters to consider (if any).
     * @return A void result.
     */
    @Override
    public Void receive(ReceiveRequest parameters) {
        sessionManager.assertUniqueServiceId(parameters);
        return new Void();
    }

    /**
     * The send operation is called when the test bed wants to send a message through this service.
     * <p>
     * This is the point where input is received for the call that this service needs to translate into an actual
     * communication. This communication would be specific to a communication protocol (e.g. send an email) or
     * a separate system's API.
     * <p>
     * The result of the operation is typically an empty success or failure report depending on whether or not the
     * communication was successful. This report could however include additional information that would be reported
     * back to the test bed.
     *
     * @param parameters The input parameters and configuration to consider for the send operation.
     * @return A status report for the call that will be returned to the test bed.
     */
    @Override
    public SendResponse send(SendRequest parameters) {
        sessionManager.assertUniqueServiceId(parameters);
        String contentToSend = reportBuilder.extractContent(reportBuilder.getInputForName(parameters.getInput(), INPUT_MESSAGE_TO_SEND));
        String ciseMessagingServiceAddress = (String) sessionManager.getSessionInfo(parameters.getSessionId(), SessionData.CISE_MESSAGING_SERVICE_ADDRESS);

        LOG.info("CISE_MESSAGING_SERVICE_ADDRESS: {}", ciseMessagingServiceAddress);

        // send message to CISE Network
        CTTSendParam sendParam = new CTTSendParam(null, messageIdStore.getAndRecord(), null, ciseMessagingServiceAddress);
        Pair<Acknowledgement, Message> messageProcessorSendResponse = messageProcessor.send(xmlMapper.fromXML(contentToSend), sendParam);
        Acknowledgement syncAcknowledgement = messageProcessorSendResponse.getA();
        Message sentMessage = messageProcessorSendResponse.getB();

        // Report back to the test bed.
        TAR report = reportBuilder.createSentMessageReportMessageAndAck(syncAcknowledgement, sentMessage);
        SendResponse response = new SendResponse();
        response.setReport(report);
        return response;
    }

    /**
     * The `beginTransaction` *operation* is called by the test bed with a *transaction starts*.
     * <p>
     * Often there is no need to take any action here but it could be interesting to do so if you need specific
     * actions per transaction.
     * - here is created  for further use in the sessionManager the session reference identified by SERVICE_ID.
     *
     * @param parameters : BeginTransactionRequest type the transaction configuration.
     * @return Void : A void result.
     */
    @Override
    public Void beginTransaction(BeginTransactionRequest parameters) {
        LOG.info("Transaction starting for session [{}]", parameters.getSessionId());
        String ciseMessagingServiceAddress = gitbConfigHelper.getConfigValue(parameters.getConfig(), SessionData.CISE_MESSAGING_SERVICE_ADDRESS);
        sessionManager.associateSessionIdToIdentifier(parameters, ciseMessagingServiceAddress);
        return new Void();
    }

    /**
     * The endTransaction operation is the counterpart of the beginTransaction and is called when the transaction terminates.
     *
     * @param parameters The session ID this transaction related to.
     * @return A void result.
     */
    @Override
    public Void endTransaction(BasicRequest parameters) {
        LOG.info("Transaction ending for session [{}]", parameters.getSessionId());
        return new Void();
    }

    /**
     * The finalize operation is called by the test bed when a test session completes.
     * <p>
     * A typical action that needs to take place here is the cleanup of any resources that were specific to the session
     * in question. This would typically involve the state recorded for the session.
     *
     * @param parameters The session ID that completed.
     * @return A void result.
     */
    @Override
    public Void finalize(FinalizeRequest parameters) {
        LOG.info("Finalising session [{}]", parameters.getSessionId());
        // Cleanup in-memory state for the completed session.
        sessionManager.destroySession(parameters.getSessionId());
        return new Void();
    }

    /**
     * Get the test bed address to reply to.
     *
     * @return The address.
     */
    private String getReplyToAddress() {
        List<Header> headers = (List<Header>) wsContext.getMessageContext().get(Header.HEADER_LIST);
        for (Header header : headers) {
            if (header.getName().equals(replyToQName)) {
                String replyToAddress = ((Element) header.getObject()).getTextContent().trim();
                if (!replyToAddress.toLowerCase().endsWith("?wsdl")) {
                    replyToAddress += "?wsdl";
                }
                return replyToAddress;
            }
        }
        return null;
    }

}
