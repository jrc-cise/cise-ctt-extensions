package eu.europa.ec.itb.cise.ws.util;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.relationship.Relationship;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DomainValidator {
    public static boolean hasDefinedSpecificRelationField(String[] listFields, Entity entity) {
        ArrayList<Pair<String, Boolean>> definedFields = generateListOfNonEmptyRelationFields(entity);
        for (String expectedFieldname : listFields) {
            for (Pair<String, Boolean> definedField : definedFields)
                if (definedField.getA().equals(expectedFieldname))
                    return true;
        }
        return false;
    }

    public static ArrayList<Pair<String, Boolean>> generateListOfNonEmptyRelationFields(Entity entity) {
        Class entityClass = entity.getClass();
        ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
        Class entityParentClass = entityClass.getSuperclass();
        while (!(entityParentClass.equals(Entity.class))) {
            Field[] entityParentFields = entityParentClass.getDeclaredFields();
            entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityParentFields)));
            entityParentClass = entityParentClass.getSuperclass();
        }
        ArrayList<Pair<String, Boolean>> areDefinedAndNonEmptyRelations = new ArrayList();
        for (Field field : entityFields) {
            field.setAccessible(true);
            Object content = null;
            if (("serialVersionUID").equals(field.getName()))
                continue;

            try {
                content = field.get(entity);
            } catch (IllegalAccessException eIAE) {
                return areDefinedAndNonEmptyRelations;
            }

            if ((content != null) && ("interface java.util.List".equals(field.getType().toString())) && validateRelationWithNonEmptyContent(content)) {
                //could put a condition on type content = List<Relationship> as ListValue
                areDefinedAndNonEmptyRelations.add(new Pair((field.getName()), ("" + ((List<Relationship>) content).size())));
            }
        }
        return areDefinedAndNonEmptyRelations;
    }

    public static boolean validateRelationWithNonEmptyContent(Object content) {
        try {
            if (!(((List) content).isEmpty())) {
                Relationship relationship = ((Relationship) (((List) content).get(0)));
                String className = relationship.getClass().getName();
                switch (className) {
                    case "eu.cise.datamodel.v1.entity.risk.Risk$DocumentRel":
                        return (null != ((Risk.DocumentRel) relationship).getDocument());
                    case "eu.cise.datamodel.v1.entity.risk.Risk$InvolvedAgentRels":
                        return (null != ((Risk.InvolvedAgentRel) relationship).getAgent());
                    case "eu.cise.datamodel.v1.entity.risk.Risk$LocationRel":
                        return (null != ((Risk.LocationRel) relationship).getLocation());
                    case "eu.cise.datamodel.v1.entity.risk.Risk$ImpliedEventRels":
                        return (null != ((Risk.ImpliedEventRel) relationship).getEvent());
                    case "eu.cise.datamodel.v1.entity.risk.Risk$InvolvedObjectRels":
                        return (null != ((Risk.InvolvedObjectRel) relationship).getObject());
                    // event.Event relations
                    case "eu.cise.datamodel.v1.entity.event.Event$DocumentRel":
                        return (null != ((Event.DocumentRel) relationship).getDocument());
                    case "eu.cise.datamodel.v1.entity.event.Event$InvolvedAgentRels":
                        return (null != ((Event.InvolvedAgentRel) relationship).getAgent());
                    case "eu.cise.datamodel.v1.entity.event.Event$LocationRel":
                        return (null != ((Event.LocationRel) relationship).getLocation());
                    case "eu.cise.datamodel.v1.entity.event.Event$InvolvedWithRel":
                        return (null != ((Event.InvolvedWithRel) relationship).getEvent());
                    case "eu.cise.datamodel.v1.entity.event.Event$InvolvedObjectRels":
                        return (null != ((Event.InvolvedObjectRel) relationship).getObject());
                    // object.objet relations
                    case "eu.cise.datamodel.v1.entity.object.Objet$DocumentRel":
                        return (null != ((Objet.DocumentRel) relationship).getDocument());
                    case "eu.cise.datamodel.v1.entity.object.Objet$InvolvedAgentRels":
                        return (null != ((Objet.InvolvedAgentRel) relationship).getAgent());
                    case "eu.cise.datamodel.v1.entity.object.Objet$LocationRel":
                        return (null != ((Objet.LocationRel) relationship).getLocation());
                    case "eu.cise.datamodel.v1.entity.object.Objet$InvolvedWithRel":
                        return (null != ((Objet.InvolvedEventRel) relationship).getEvent());
                }
                return false;
            }
        } catch (Exception eE1) {
            return false;
        }
        return false;
    }
}