package eu.europa.ec.itb.cise.ws.config;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.servicemodel.v1.message.Message;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import java.util.Collections;

/**
 * Configuration class responsible for creating the Spring beans required by the service.
 */
@Configuration
public class WebServiceConfig {

    public static final Class[] CISE_CLASSES = new Class[]{Message.class, Entity.class};

    @Autowired
    CTTCiseExtensionAppContext cttCiseExtensionAppContext;

    @Bean
    public PayloadInterceptor payloadInterceptor() {
        return new PayloadInterceptor();
    }

    /**
     * The CXF endpoint that will serve service calls for the GITB messaging service API.
     *
     * @return The endpoint.
     */
    @Bean
    public Endpoint gitbMessagingServiceEndpoint(Bus cxfBus) {
        EndpointImpl endpoint = new EndpointImpl(cxfBus, cttCiseExtensionAppContext.getOutgoingMessagingService());
        endpoint.setServiceName(new QName("http://www.gitb.com/ms/v1/", "MessagingServiceService"));
        endpoint.setEndpointName(new QName("http://www.gitb.com/ms/v1/", "MessagingServicePort"));
        endpoint.publish("/gitb-messaging");
        return endpoint;
    }

    /**
     * The CXF endpoint that will serve service calls for the GITB messaging service API.
     *
     * @return The endpoint.
     */
    @Bean
    public Endpoint ciseMessagingServiceEndpoint(Bus cxfBus, PayloadInterceptor payloadInterceptor) {
        EndpointImpl endpoint = new EndpointImpl(cxfBus, cttCiseExtensionAppContext.getIcomingMessagingService());
        endpoint.setServiceName(new QName("http://www.cise.eu/accesspoint/service/v1/", "CISEMessageService"));
        endpoint.setEndpointName(new QName("http://www.cise.eu/accesspoint/service/v1/", "CISEMessageServiceSoapPort"));
        endpoint.setProperties(Collections.singletonMap("jaxb.additionalContextClasses", CISE_CLASSES));
        endpoint.getInInterceptors().add(payloadInterceptor);
        endpoint.publish("/cise-messaging");
        return endpoint;
    }

    /**
     * The CXF endpoint that will serve service calls for the GITB validation service to validate tokens.
     *
     * @return The endpoint.
     */
    @Bean
    public Endpoint gitbCiseValidationServiceEndpoint(Bus cxfBus) {
        EndpointImpl endpoint = new EndpointImpl(cxfBus, cttCiseExtensionAppContext.getCiseValidationService());
        endpoint.setServiceName(new QName("http://www.gitb.com/vs/v1/", "ValidationService"));
        endpoint.setEndpointName(new QName("http://www.gitb.com/vs/v1/", "ValidationServicePort"));
        endpoint.publish("/cise-validation");
        return endpoint;
    }


}
