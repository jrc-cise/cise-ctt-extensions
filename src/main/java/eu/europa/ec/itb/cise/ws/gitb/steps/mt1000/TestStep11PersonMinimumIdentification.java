package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.person.PersonIdentifier;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

/**
 * #11 Person Minimum Identification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Person`  is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define at least one of the attribute:`AlternativeName`,`BirthName`,`ContactInformation`,`FamilyName`,`FullName`,`GIVENName`,`PatronymicName`,`PersonIdentifier`
 */
public class TestStep11PersonMinimumIdentification extends CiseTestStep {

    public TestStep11PersonMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_11_PERSON_MIN_IDENT;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Person.class);
        if (payloadEntitiesOfType.size() == 0) report.setResult(TestResultType.SUCCESS);

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (entityPair.getA() instanceof Person) {
                    Person person = (Person) entityPair.getA();
                    if (!checkPersonForMinimumIdentification(person)) {
                        conform = false;
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred", "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }

    private boolean checkPersonForMinimumIdentification(Person person) {
        if (Strings.isNotBlank(((String) person.getAlternativeName())) ||
                Strings.isNotBlank(((String) person.getBirthName())) ||
                Strings.isNotBlank(((String) person.getContactInformation())) ||
                Strings.isNotBlank(((String) person.getFamilyName())) ||
                Strings.isNotBlank(((String) person.getFullName())) ||
                Strings.isNotBlank(((String) person.getGivenName())) ||
                Strings.isNotBlank(((String) person.getPatronymicName()))
        ) {
            return true;
        }

        if (person != null && person.getPersonIdentifiers().size() > 0) {
            List<PersonIdentifier> personIdentifiers = person.getPersonIdentifiers();
            for (PersonIdentifier personIdentifier : personIdentifiers) {
                if (((personIdentifier.getIdentifierType()) != null) &&
                        (Strings.isNotBlank(((String) (personIdentifier.getIdentifierValue()))))) {
                    return true;
                }
            }
        }

        return false;
    }
}