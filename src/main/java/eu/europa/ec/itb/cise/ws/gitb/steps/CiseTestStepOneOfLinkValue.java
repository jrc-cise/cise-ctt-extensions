package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.util.DomainValidator;

import java.util.ArrayList;
import java.util.List;

public abstract class CiseTestStepOneOfLinkValue extends CiseTestStep {

    public CiseTestStepOneOfLinkValue(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    protected static boolean hasAChildValueOfValidType(CiseTestStepOneOfLinkValue ciseTestStepOneOfChildValue, String[] fieldNames, Entity entity) {
        Class entityClass = entity.getClass();
        return (DomainValidator.hasDefinedSpecificRelationField(fieldNames, entity));
    }

    protected TAR createReport(String messageXml, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        ArrayList<String> fieldNames = new ArrayList<>();
        return createReport(messageXml, (String[]) fieldNames.toArray(), classesToFind, testStepRef);
    }

    /**
     * In this function we assume that the entityClassToFind is an Event because is the only one that have relations to other kind of objects.
     *
     * @param messageXml    input message
     * @param classesToFind the classes to find: from this elements we search forward to verify relations
     * @param testStepRef   code to reference the specific test step
     * @return              returns the report
     */
    protected TAR createReport(String messageXml, String[] fieldNames, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }

        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Entity entity = (Entity) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (!hasAChildValueOfValidType(this, fieldNames, entity) &&
                        isEntityOfCorrectType(entity)
                ) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), entity);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation + describeCTXwarning), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    // allow inherited to redefine default comportement.
    protected boolean isEntityOfCorrectType(Entity entity) {
        return true;
    }

}
