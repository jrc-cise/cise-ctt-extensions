package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

/**
 * #12 Organization Minimum Identification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `Organization`  is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define at least one of the attribute:`AlternativeName`,`IdentificationNumber`,`LegalName`
 */
public class TestStep12OrganisationMinimumIdentification extends CiseTestStep {

    public TestStep12OrganisationMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_12_ORGANISATION_MIN_IDENT;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Organization.class);
        if (payloadEntitiesOfType.size() == 0) report.setResult(TestResultType.SUCCESS);

        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (entityPair.getA() instanceof Organization) {
                    Organization organisation = (Organization) entityPair.getA();
                    if (!checkOrganisationForMinimumIdentification(organisation)) {
                        conform = false;
                        failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred", "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }


    private boolean checkOrganisationForMinimumIdentification(Organization organization) {
        if (Strings.isNotBlank(((String) organization.getAlternativeName())) ||
                Strings.isNotBlank(((String) organization.getIdentificationNumber())) ||
                Strings.isNotBlank(((String) organization.getLegalName()))
        ) {
            return true;
        }
        return false;
    }
}
