package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * #60 MeteoOceanographicCondition Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 *
 * `*GIVEN*`  *entity(ies)* of type `MeteoOceanographicCondition` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` must define at least one relation to entity(ies) `Location` (+ its children entities).
 */
public class TestStep60MeteoOceanographicConditionMinimumDefinition extends CiseTestStep {

    public TestStep60MeteoOceanographicConditionMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{MeteoOceanographicCondition.class};
        Class[] linkedClass = new Class[]{Location.class};
        return createReport(messageXml, entityClasses, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_60_METEOOCEANOGRAPHICCONDITION_MIN_DEF);
    }


    /**
     * @param messageXml    input message
     * @param classesToFind the Entities to find: from these elements we search backward and forward to verify relations
     * @param linkedClasses classes of Documents to search for
     * @param testStepRef   code to reference the specific test step
     * @return              returns the report
     */
    protected TAR createReport(String messageXml, Class<Entity>[] classesToFind, Class<Entity>[] linkedClasses, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);

        try {
            Pair<String, Boolean> testResult = performTest(messageXml, classesToFind, linkedClasses, testStepRef);
            boolean conform = testResult.getB();
            String failureExplanation = testResult.getA();
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }


    public Pair<String, Boolean> performTest(String messageXml, Class<Entity>[] classesToFind, Class<Entity>[] linkedClasses, CiseTestStepsEnum testStepRef) {
        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        boolean conform = true;
        for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
            Entity entity = entityPair.getA();
            String breadcrumb = entityPair.getB();
            if (!isEntityLinkedTo(entity, linkedClasses)
            ) {
                conform = false;
                failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), entity);
            }
        }
        return new Pair<>(failureExplanation, conform);
    }


    protected boolean isEntityLinkedTo(Entity entity, Class<Entity>[] linkedClasses) {
        MeteoOceanographicCondition meteoOceanographicCondition = (MeteoOceanographicCondition) entity;
        MeteoOceanographicCondition.LocationRel locationRel = meteoOceanographicCondition.getLocationRel();

        if (locationRel == null) return false;

        if (locationRel.getLocation() == null) return false;

        return true;
    }

}
