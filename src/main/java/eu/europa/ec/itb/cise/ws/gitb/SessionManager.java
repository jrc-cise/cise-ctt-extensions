package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.ms.BasicRequest;
import com.gitb.ms.BeginTransactionRequest;
import com.gitb.ms.MessagingClient;
import com.gitb.ms.NotifyForMessageRequest;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.servicemodel.v1.message.Message;
import eu.europa.ec.itb.cise.ws.util.ProxyInfo;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Component used to store sessions and their state.
 * <p>
 * This class is key in maintaining an overall context across a request and one or more
 * responses. It allows mapping of received data to a given test session running in the
 * test bed.
 * <p>
 * This implementation stores session information in memory. An alternative solution
 * that would be fault-tolerant could store test session data in a DB.
 * <p>
 * This class also holds the implementation responsible for notifying the test bed for
 * received messages. This is done via the test bed's call-back service interface. Apache
 * CXF is used for this for maximum flexibility. As an example, the configuration of
 * a proxy to be used for this call is provided that can be optionally set on the call-back
 * service proxy via configuration properties (set in application.properties).
 */
public class SessionManager {

    /**
     * CONFIG_SERVICE_ID element of the unique identification
     */
    public static final String CONFIG_SERVICE_ID = "serviceID";

    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SessionManager.class);

    /**
     * The map of in-memory active sessions.
     */
    private Map<String, Map<String, Object>> sessions = new ConcurrentHashMap<>();

    private ProxyInfo proxy;

    private ReportBuilder reportBuilder;


    public SessionManager(ProxyInfo proxy, ReportBuilder reportBuilder) {
        this.proxy = proxy;
        this.reportBuilder = reportBuilder;
    }


    /**
     * Create a new session.
     *
     * @param callbackURL The callback URL to set for this session.
     * @return The session ID that was generated.
     */
    public String createSession(String callbackURL) {
        if (callbackURL == null) {
            throw new IllegalArgumentException("A callback URL must be provided");
        }
        String sessionId = UUID.randomUUID().toString();
        Map<String, Object> sessionInfo = new HashMap<>();
        long instant = Instant.now().toEpochMilli();
        sessionInfo.put(SessionData.CALLBACK_URL, callbackURL);
        sessionInfo.put(SessionData.TIME_CREATED, instant);
        sessions.put(sessionId, sessionInfo);
        cleanSession(instant, 60000, 180000);
        return sessionId;
    }

    private void cleanSession(long instant, int timerShort, int timerLong) {
        for (Map.Entry<String, Map<String, Object>> entry : getAllSessions().entrySet()) {
            if ((entry.getValue()).get((SessionData.SERVICE_ID)) != null) {
                if ((Long) entry.getValue().get(SessionData.TIME_CREATED) < (instant - timerLong))
                    sessions.remove(entry.getKey());
            } else if ((Long) entry.getValue().get(SessionData.TIME_CREATED) < (instant - timerLong))
                sessions.remove(entry.getKey());
        }
    }


    /**
     * Create a new session.
     *
     * @param message message
     * @return The session ID that was generated.
     */
    public String createSessionForIncomingMessage(Message message) {
        if (message.getMessageID() == null) {
            throw new IllegalArgumentException("A messageId must be provided");
        }
        if (message.getSender().getServiceID() == null) {
            throw new IllegalArgumentException("A serviceId must be provided");
        }
        String sessionId = UUID.randomUUID().toString();
        Map<String, Object> sessionInfo = new HashMap<>();
        sessionInfo.put(SessionData.CISE_MESSAGING_MESSAGE_ID, message.getMessageID());
        sessionInfo.put(SessionData.SERVICE_ID, message.getSender().getServiceID());
        sessionInfo.put(SessionData.TIME_CREATED, Instant.now().toEpochMilli());
        sessions.put(sessionId, sessionInfo);
        return sessionId;
    }

    /**
     * Raise error if other test sessions are actively using the same service ID.
     *
     * @param parameters The current test session ID.
     */
    public void assertUniqueServiceId(BasicRequest parameters) {
        String testSessionId = parameters.getSessionId();
        if (currentServiceIdUsedInOtherTransactions(testSessionId)) {
            throw new IllegalStateException("ERROR ! : The tool expect one message at a time for a given ServiceID from the SUT (System Under Test)  [" +
                    getSessionInfo(testSessionId, SessionData.SERVICE_ID) + "] \n" +
                    "Please, retry after a couple of minutes");
        }
    }

    /**
     * Raise error if other test sessions are actively using the same service ID.
     *
     * @param serviceid The current test service ID.
     */
    public boolean asCurrentSessionWithSameServiceId(String serviceid) {
        // Check against other sessions.
        for (Map.Entry<String, Map<String, Object>> entry : getAllSessions().entrySet()) {
            if ((entry.getValue().get(SessionData.SERVICE_ID)).equals(serviceid))
                return true;
        }
        return false;
    }

    /**
     * Check whether a given service ID is used in other transactions.
     *
     * @param currentTestUUID The current test session ID (to skip)
     * @return The check result.
     */
    private boolean currentServiceIdUsedInOtherTransactions(String currentTestUUID) {
        // Get the service ID used in the current session.
        String currentServiceIdentifier = (String) getSessionInfo(currentTestUUID, SessionData.SERVICE_ID);
        // Check against other sessions.
        for (Map.Entry<String, Map<String, Object>> entry : getAllSessions().entrySet()) {
            if (!currentTestUUID.equals(entry.getKey())
                    && currentServiceIdentifier.equals(entry.getValue().get(SessionData.SERVICE_ID))) {
                // We found the same service ID used by another test session.
                return true;
            }
        }
        return false;
    }

    // specific identifier isolation strategy
    public String obtainIdentifierFromMessage(Message message) {
        return ((message.getSender() != null) ? message.getSender().getServiceID() : null);
    }

    // specific identifier validation strategy
    public boolean isIdentifiedBy(Object identifier, Map.Entry<String, Map<String, Object>> entry) {
        boolean test = identifier.equals(entry.getValue().get(SessionData.SERVICE_ID));
        return test;
    }

    /**
     * Remove the provided session from the list of tracked sessions.
     *
     * @param sessionId The session ID to remove.
     */
    public void destroySession(String sessionId) {
        sessions.remove(sessionId);
    }

    /**
     * Get a given item of information linked to a specific session.
     *
     * @param sessionId The session ID we want to lookup.
     * @param infoKey   The key of the value that we want to retrieve.
     * @return The retrieved value.
     */
    public Object getSessionInfo(String sessionId, String infoKey) {
        Object value = null;
        if (sessions.containsKey(sessionId)) {
            value = sessions.get(sessionId).get(infoKey);
        }
        return value;
    }

    /**
     * Set the given information item for a session.
     *
     * @param sessionId The session ID to set the information for.
     * @param infoKey   The information key.
     * @param infoValue The information value.
     */
    public void setSessionInfo(String sessionId, String infoKey, Object infoValue) {
        sessions.get(sessionId).put(infoKey, infoValue);
    }

    /**
     * Get all the active sessions.
     *
     * @return An unmodifiable map of the sessions.
     */
    public Map<String, Map<String, Object>> getAllSessions() {
        return Collections.unmodifiableMap(sessions);
    }

    /**
     * Notify the test bed for a given session.
     *
     * @param sessionId The session ID to notify the test bed for.
     * @param report    The report to notify the test bed with.
     */
    public void notifyTestBed(String sessionId, TAR report) {
        String callback = (String) getSessionInfo(sessionId, SessionData.CALLBACK_URL);
        String serviceId = (String) getSessionInfo(sessionId, SessionData.SERVICE_ID);
        if (callback == null) {
            LOG.warn("Could not find callback URL for session [{}]", sessionId);
        } else {
            try {
                LOG.info("Notifying test bed for session [" + sessionId + "] with serviceId {} ", " " +
                        (String) getSessionInfo(sessionId, SessionData.SERVICE_ID) +
                        " created at " + ((Long) getSessionInfo(sessionId, SessionData.TIME_CREATED)).toString());
                callTestBed(sessionId, report, callback);
            } catch (Exception e) {
                LOG.warn("Error while notifying test bed for session [{}]", sessionId, e);
                callTestBed(sessionId, reportBuilder.createReport(TestResultType.FAILURE), callback);
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Call the test bed to notify it of received communication.
     *
     * @param sessionId       The session ID that this notification relates to.
     * @param report          The TAR report to send back.
     * @param callbackAddress The address on which the call is to be made.
     */
    private void callTestBed(String sessionId, TAR report, String callbackAddress) {
        /*
         * First setup the service client. This is not created once and reused since the address to call
         * is determined dynamically from the WS-Addressing information (passed here as the callback address).
         */
        JaxWsProxyFactoryBean proxyFactoryBean = new JaxWsProxyFactoryBean();
        proxyFactoryBean.setServiceClass(MessagingClient.class);
        proxyFactoryBean.setAddress(callbackAddress);
        MessagingClient serviceProxy = (MessagingClient) proxyFactoryBean.create();
        Client client = ClientProxy.getClient(serviceProxy);
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.getClient().setAutoRedirect(true);
        // Apply proxy settings (if applicable).
        if (proxy.isEnabled()) {
            proxy.applyToCxfConduit(httpConduit);
        }
        // Make the call.
        NotifyForMessageRequest request = new NotifyForMessageRequest();
        request.setSessionId(sessionId);
        request.setReport(report);
        serviceProxy.notifyForMessage(request);
    }

    public void associateSessionIdToIdentifier(BeginTransactionRequest parameters, String ciseMessagingServiceAddress) {
        String serviceId = GitbConfigHelper.getConfigValue(parameters.getConfig(), CONFIG_SERVICE_ID);
        long instant = Instant.now().toEpochMilli();
        if (ciseMessagingServiceAddress != null)
            setSessionInfo(parameters.getSessionId(), SessionData.CISE_MESSAGING_SERVICE_ADDRESS, ciseMessagingServiceAddress);
        if (serviceId != null) {
            setSessionInfo(parameters.getSessionId(), SessionData.SERVICE_ID, serviceId);
            setSessionInfo(parameters.getSessionId(), SessionData.TIME_CREATED, instant);
        } else
            LOG.warn("Couldn't find serviceId for session [{}]", parameters.getSessionId());

        cleanSession(instant, 60000, 180000);
    }

}
