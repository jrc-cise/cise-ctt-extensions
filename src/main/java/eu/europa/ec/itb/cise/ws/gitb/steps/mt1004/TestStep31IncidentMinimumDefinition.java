package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.EventDocument;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.*;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #31 Incident Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `incident` or  `MaritimeSafetyIncident` or  `IrregularMigrationIncident` or  `LawInfringementIncident` or  `CrisisIncident` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define a relation to *entity(ies)* of type `EventDocument` or `Risk` or `Vehicle`(+ its children entities) or `Location`(+ its children entities) or `Organization`(+ its children entities)
 */
public class TestStep31IncidentMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep31IncidentMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = Incident.class;
        Class[] linkedClass = new Class[]{Vehicle.class, Location.class, Risk.class, EventDocument.class, Person.class, Organization.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_31_INCIDENT_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        Incident incident = (Incident) event;

        return incident instanceof MaritimeSafetyIncident ||
                incident instanceof IrregularMigrationIncident ||
                incident instanceof LawInfringementIncident ||
                incident instanceof CrisisIncident;
    }
}
