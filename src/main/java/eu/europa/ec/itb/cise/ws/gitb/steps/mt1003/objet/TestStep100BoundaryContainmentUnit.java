package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #100 Boundary Vessel Lenght
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * <p>
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in payload
 * `*AND*` `MessageType` is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` attribute `Lenght` is defined
 * `*THEN*` attribute `Lenght` must be inferior to 500 (meters) AND attribute `Lenght` must be superior to 1 (meter).
 */
public class TestStep100BoundaryContainmentUnit extends CiseTestStepOneOfChildValueBoundary {

    public TestStep100BoundaryContainmentUnit(ReportBuilder reportBuilder,
                                              CisePayloadHelper payloadHelper,
                                              GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{ContainmentUnit.class};
        List<String> fields = Arrays.asList(
                "flashPoint",
                "grossQuantity",
                "netQuantity");

        return super.createReport(messageXml,
                fields,
                "Objet",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_100_BOUNDARY_CONTAINMENTUNIT
        );

    }
}
