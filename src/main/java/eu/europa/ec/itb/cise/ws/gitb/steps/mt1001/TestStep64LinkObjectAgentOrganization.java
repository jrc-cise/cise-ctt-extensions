package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #64 LinkObjectObject Agent Organization
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Organization` or `OrganizationalUnit` or `PortOrganization` or `FormalOrganuzation` or `OrganizationalCollaboration` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to Object entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `AgentRole` in `PASSENGER`, `CREW_MEMBER`, `CAPTAIN_MASTER`, `COMPANY_SECURITY_OFFICER`, `EMPLOYEE`, `CREW_MEMBER`.
 * `*AND*` relation _must not define_ attribute :`TransitPassengerDuty`
 */
public class TestStep64LinkObjectAgentOrganization extends CiseTestStepRelationship {

    public TestStep64LinkObjectAgentOrganization(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Vessel.class, Aircraft.class, LandVehicle.class, Catch.class, ContainmentUnit.class, Cargo.class};
        Class[] organizationClasses = new Class[]{Organization.class};
        Class[] relationClasses = new Class[]{Objet.InvolvedAgentRel.class, Agent.InvolvedObjectRel.class};
        return createReport(messageXml, relationClasses, organizationClasses, entityClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_64_LINK_OBJECT_AGENT_ORGANIZATION);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        if (relationship instanceof Objet.InvolvedAgentRel) {
            Objet.InvolvedAgentRel rel = (Objet.InvolvedAgentRel) relationship;
            objectAgentRole = rel.getAgentRole();
            isTransitPassenger = rel.isTransitPassenger();
            dutyType = rel.getDuty();
        } else if (relationship instanceof Agent.InvolvedObjectRel) {
            Agent.InvolvedObjectRel rel = (Agent.InvolvedObjectRel) relationship;
            objectAgentRole = rel.getAgentRole();
            isTransitPassenger = rel.isTransitPassenger();
            dutyType = rel.getDuty();
        } else {
            return false;
        }

        if (objectAgentRole == AgentRoleInObjectType.PASSENGER ||
                objectAgentRole == AgentRoleInObjectType.CREW_MEMBER ||
                objectAgentRole == AgentRoleInObjectType.CAPTAIN_MASTER ||
                objectAgentRole == AgentRoleInObjectType.COMPANY_SECURITY_OFFICER ||
                objectAgentRole == AgentRoleInObjectType.EMPLOYEE ||
                isTransitPassenger != null ||
                dutyType != null
        ) {
            return false;
        }

        return true;
    }


}
