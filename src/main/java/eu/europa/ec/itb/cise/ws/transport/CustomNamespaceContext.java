package eu.europa.ec.itb.cise.ws.transport;

import java.util.Iterator;

/**
 * CISE custom namespace context.
 */
public class CustomNamespaceContext implements javax.xml.namespace.NamespaceContext {

    @Override
    public String getNamespaceURI(String prefix) {
        if (prefix == null) {
            throw new NullPointerException("Null prefix");
        } else if ("vessel".equals(prefix)) {
            return "http://www.cise.eu/datamodel/v1/entity/vessel/";
        } else if ("message".equals(prefix)) {
            return "http://www.cise.eu/servicemodel/v1/message/";
        } else if ("object".equals(prefix)) {
            return "http://www.cise.eu/datamodel/v1/entity/object/";
        } else if ("soapenv".equals(prefix)) {
            return "http://schemas.xmlsoap.org/soap/envelope/";
        } else if ("service".equals(prefix)) {
            return "http://www.cise.eu/accesspoint/service/v1/";
        } else {
            return "xml".equals(prefix) ? "http://www.w3.org/XML/1998/namespace" : "";
        }
    }

    @Override
    public String getPrefix(String namespaceURI) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        throw new UnsupportedOperationException();
    }
}
