package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.*;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedObjet;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #63 LinkObjectLocation Vehicule
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Cargo` or `ContainmentUnit` or `Catch` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it _must not have predetermined value for_ attribute `LocationRole` in `PORT_OF_DISCHARGE`, `PORT_OF_LOADING`
 */
public class TestStep63LinkObjectLocationVehicle extends CiseTestStepLinkedObjet {

    public TestStep63LinkObjectLocationVehicle(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Vessel.class, Aircraft.class, LandVehicle.class};
        Class[] linkedClass = new Class[]{Location.class};
        return createReport(messageXml, entityClasses, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_63_LINK_OBJECT_LOCATION_VEHICLE);
    }

    protected boolean isEntityOfCorrectType(Entity entity) {
        Vehicle vehicle = (Vehicle) entity;

        for (Objet.LocationRel locationRel : vehicle.getLocationRels()) {
            if (locationRel.getLocationRole() != null &&
                    (locationRel.getLocationRole() == LocationRoleType.PORT_OF_LOADING ||
                            locationRel.getLocationRole() == LocationRoleType.PORT_OF_DISCHARGE
                    )
            ) {
                return false;
            }
        }
        return true;
    }

}

