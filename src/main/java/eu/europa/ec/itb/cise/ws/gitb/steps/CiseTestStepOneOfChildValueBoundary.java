package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;

import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.util.Pair;
import org.apache.logging.log4j.util.Strings;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public abstract class CiseTestStepOneOfChildValueBoundary extends CiseTestStep {
    private static final String OBJET_BOUND_PROP = "./src/main/java/eu/europa/ec/itb/cise/ws/gitb/steps/mt1003/objet/Objet.properties";
    //="eu/europa/ec/itb/cise/ws/itb/steps/mt1003/objet/Object.properties";

    public <T> CiseTestStepOneOfChildValueBoundary(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    protected static String nullOrValueNotInValidRange(CiseTestStepOneOfChildValueBoundary ciseTestStepOneOfChildValue, String field, Entity entity, String min, String max, Class type) {
        ArrayList<Pair<String, String>> definedFields = generateListOfSetFields(entity);
        for (Pair<String, String> definedField : definedFields)
            if (definedField.getA().equals(field)) {
                if (Double.class.equals(type)) {
                    double valueDouble = Double.parseDouble(definedField.getB().toString()),
                            maxDouble = Double.parseDouble(max), minDouble = Double.parseDouble(min);
                    return ((minDouble < valueDouble) && (valueDouble < maxDouble)) ? null : "" + valueDouble;
                } else if (Integer.class.equals(type)) {
                    int valueInteger = Integer.parseInt(definedField.getB().toString()),
                            maxInteger = Integer.parseInt(max), minInteger = Integer.parseInt(min);
                    return ((minInteger < valueInteger) && (valueInteger < maxInteger)) ? null : "" + valueInteger;
                } else {
                    throw new IllegalStateException("Unexpected value: " + type.getCanonicalName());
                }
            }
        return null;
    }

    private static ArrayList<Pair<String, String>> generateListOfSetFields(Entity entity) {
        Class entityClass = entity.getClass();
        ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
        Class entityParentClass = entityClass.getSuperclass();
        while (!(entityParentClass.equals(Entity.class))) {
            Field[] entityParentFields = entityParentClass.getDeclaredFields();
            entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityParentFields)));
            entityParentClass = entityParentClass.getSuperclass();
        }
        ArrayList<Pair<String, String>> areDefined = new ArrayList();
        for (Field field : entityFields) {
            field.setAccessible(true);
            Object content = null;
            if (("serialVersionUID").equals(field.getName()))
                continue;
            try {
                content = field.get(entity);
            } catch (IllegalAccessException eIAE) {
                content = null;
            }
            if (content != null) {
                switch (field.getType().toString()) {
                    case ("double"):
                    case ("class java.lang.Double"):
                    case ("long"):
                    case ("class java.lang.Long"):
                    case ("int"):
                    case ("class java.lang.Integer"):
                        areDefined.add(new Pair((field.getName()), ("" + content)));
                        break;
                    default:
                        break;
                }
            }
        }
        return areDefined;
    }

    private static boolean validateString(Entity entity, Field field, Object content) {
        try {
            if (!(((List) content).isEmpty()))
                return true;
        } catch (Exception eE1) {
            // System.out.println(field.getName() + "_not_a_list");
            try {
                if (Strings.isNotEmpty((String) content))
                    return true;
            } catch (Exception eE2) {
                // System.out.println(field.getName() + " nor a String");
            }
        }
        return false;
    }

    /**
     * @param messageXml    input message
     * @param classesToFind the classes to find: from this elements we search forward to verify relations
     * @param testStepRef   code to reference the specific test step
     * @return              returns the report
     */
    protected TAR createReport(String messageXml, String fieldName, Class[] classesToFind, CiseTestStepsEnum testStepRef, String min, String max, Class type) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }

        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Entity entity = (Entity) entityPair.getA();
                String breadcrumb = entityPair.getB();
                String nullOrBadValue = nullOrValueNotInValidRange(this,
                        fieldName,
                        entity,
                        min,
                        max,
                        type);
                if (nullOrBadValue != null) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, breadcrumb, entity);
                    failureExplanation = errorHelper.addBoundaryPointOfFailure(failureExplanation,
                            errorHelper.avoidNodeId(breadcrumb),
                            fieldName,
                            nullOrBadValue,
                            min,
                            max,
                            "");
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    protected TAR createReport(String messageXml, List<String> fields, String typeProperties, Class[] classesToFind, CiseTestStepsEnum testStepRef) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        List<Pair<Entity, String>> payloadEntitiesOfType = new ArrayList<>();
        for (Class aClass : classesToFind) {
            payloadEntitiesOfType.addAll(payloadHelper.getPayloadEntitiesOfType(messageXml, aClass));
        }
        Properties prop = errorHelper.getErrorBoundaries(typeProperties);
        try {
            boolean conform = true;
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Entity entity = (Entity) entityPair.getA();
                String breadcrumb = entityPair.getB();
                for (String fieldName : fields) {
                    Class typeField = Class.forName(prop.getProperty(fieldName + ".type"));
                    String unit = prop.getProperty(fieldName + ".unit");
                    String nullOrBadValue = nullOrValueNotInValidRange(this,
                            fieldName,
                            entity,
                            prop.getProperty(fieldName + ".min"),
                            prop.getProperty(fieldName + ".max"),
                            typeField);
                    if (nullOrBadValue != null) {
                        conform = false;
                        failureExplanation = errorHelper.addBoundaryPointOfFailure(failureExplanation,
                                errorHelper.avoidNodeId(breadcrumb),
                                fieldName,
                                nullOrBadValue,
                                prop.getProperty(fieldName + ".min"),
                                prop.getProperty(fieldName + ".max"),
                                unit);
                    }
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? ReportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException | ClassNotFoundException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }

        return report;
    }
}

