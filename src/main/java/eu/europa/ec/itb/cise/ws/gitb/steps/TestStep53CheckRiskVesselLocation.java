package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

/**
 * #53  Location corresponds to the request
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * <p>
 * `*Background*`: a `VESSEL` *entity* _is defined by_ *_defined and valued attribute_* `Location`
 * with *simple type attribute combination* `latitude+longitude` in the SUT Information System
 * `*And*`: it _is linked to_ a `Risk`.
 * `*Given*`: `VESSEL` *entity* _is present_ under a `Risk` *main entity* _in_ `Pull.Request` message payload.
 * `*When*`: it _is represented by_ `Location` expressed by _defined query_ of `WKT` format with `polygon` complex type
 * `*Then*`: must define at least one "VESSEL" in corresponding `Pull.Response` payload
 * `*And*`: it must be defined with *_defined and valued attribute_* `Location` _represented
 * by_ a *simple type attribute combination* `latitude+longitude` correspond to _defined query_ of `WKT` format
 * with `polygon` complex type .
 */
public class TestStep53CheckRiskVesselLocation extends CiseTestStepValidVesselLocation {

    public TestStep53CheckRiskVesselLocation(ReportBuilder reportBuilder,
                                             CisePayloadHelper payloadHelper,
                                             GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String receivedMessageXml, String sentMessageXml) {
        TAR report = super.createReport(receivedMessageXml,
                sentMessageXml,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_53_CHECK_RISK_VESSEL_LOCATION, Risk.class);
        return report;
    }

}
