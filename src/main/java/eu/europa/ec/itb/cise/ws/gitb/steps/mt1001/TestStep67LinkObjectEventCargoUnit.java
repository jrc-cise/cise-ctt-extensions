package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.action.Action;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.event.ObjectRoleInEventType;
import eu.cise.datamodel.v1.entity.incident.Incident;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #67 LinkObjectEvent Event CargoUnit
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `ContainmentUnit` or `Catch` or `Cargo` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Object` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `PARTICIPANT`, `OBSERVER`, `REPORTER`
 */
public class TestStep67LinkObjectEventCargoUnit extends CiseTestStepRelationship {

    public TestStep67LinkObjectEventCargoUnit(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Movement.class, Incident.class, Action.class, Anomaly.class};
        Class[] objectClasses = new Class[]{Catch.class, ContainmentUnit.class, Cargo.class};
        Class[] relationClasses = new Class[]{Objet.InvolvedEventRel.class, Event.InvolvedObjectRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_67_LINK_OBJECT_EVENT_CARGOUNIT);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        if (relationship instanceof Event.InvolvedObjectRel) {
            objectRole = ((Event.InvolvedObjectRel) relationship).getObjectRole();
        } else if (relationship instanceof Objet.InvolvedEventRel) {
            objectRole = ((Objet.InvolvedEventRel) relationship).getObjectRole();
        } else {
            return false;
        }

        if (isObjectRoleOfWrongType(objectRole)
        ) {
            return false;
        }
        return true;
    }

    private boolean isObjectRoleOfWrongType(ObjectRoleInEventType objectRole) {
        return objectRole == ObjectRoleInEventType.COORDINATOR ||
                objectRole == ObjectRoleInEventType.PARTICIPANT ||
                objectRole == ObjectRoleInEventType.OBSERVER ||
                objectRole == ObjectRoleInEventType.REPORTER;
    }

}
