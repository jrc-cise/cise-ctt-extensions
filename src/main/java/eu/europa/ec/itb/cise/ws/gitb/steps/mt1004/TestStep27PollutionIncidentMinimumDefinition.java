package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.EventDocument;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.PollutionIncident;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #27 PollutionIncident Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `PollutionIncident` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` it must define at least one of *relation to entity(ies)* `EventDocument`,`Risk`,`Object`(+ its children entities),`Agent`(+ its children entities),`Location`(+ its children entities),`Organization`(+ its children entities)
 */
public class TestStep27PollutionIncidentMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep27PollutionIncidentMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = PollutionIncident.class;
        Class[] linkedClass = new Class[]{Objet.class, Location.class, EventDocument.class, Person.class, Organization.class}; //Vessel.class, Cargo.class, ContainmentUnit.class
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_27_POLLUTION_INCIDENT_MIN_DEF);
    }

    protected boolean isEntityOfCorrectType(Event event) {
        return true;
    }


}
