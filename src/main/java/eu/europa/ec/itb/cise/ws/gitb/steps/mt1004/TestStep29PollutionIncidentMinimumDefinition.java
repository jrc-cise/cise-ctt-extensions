package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncidentType;
import eu.cise.datamodel.v1.entity.incident.PollutionIncident;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepLinkedEvent;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 *#29PollutionIncident Minimum Definition
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Pollution` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it define attribute `MaritimeSafetyIncidentType` with  predefined value in :`POLLUTION` or `WASTE` or `LOST_FOUND_CONTAINERS` or `FIRE`
 * `*THEN*` it must define at least one of relation to *entity(ies)* of type `Vessel`, `Cargo`, `Location`
 */
public class TestStep29PollutionIncidentMinimumDefinition extends CiseTestStepLinkedEvent {

    public TestStep29PollutionIncidentMinimumDefinition(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class entityClass = PollutionIncident.class;
        Class[] linkedClass = new Class[]{Vessel.class, Cargo.class, Location.class};
        return super.createReport(messageXml, entityClass, linkedClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_29_POLLUTION_INCIDENT_MIN_DEF);
    }

    @Override
    protected boolean isEntityOfCorrectType(Event event) {
        MaritimeSafetyIncident maritimeSafetyIncident = (MaritimeSafetyIncident) event;
        MaritimeSafetyIncidentType maritimeSafetyIncidentType = maritimeSafetyIncident.getMaritimeSafetyIncidentType();
        return (maritimeSafetyIncidentType != null && (maritimeSafetyIncidentType == MaritimeSafetyIncidentType.POLLUTION ||
                maritimeSafetyIncidentType == MaritimeSafetyIncidentType.WASTE ||
                maritimeSafetyIncidentType == MaritimeSafetyIncidentType.LOST_FOUND_CONTAINERS ||
                maritimeSafetyIncidentType == MaritimeSafetyIncidentType.FIRE));
    }

}
