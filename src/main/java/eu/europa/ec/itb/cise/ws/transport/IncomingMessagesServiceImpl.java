package eu.europa.ec.itb.cise.ws.transport;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.signature.exceptions.SignatureEx;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlMapper;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.domain.SyncAckFactory;
import eu.europa.ec.itb.cise.ws.domain.SyncAckType;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.SessionData;
import eu.europa.ec.itb.cise.ws.gitb.SessionManager;
import eu.europa.ec.itb.cise.ws.soap.CISEMessageServiceSoapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Web service implementation of the CISE messaging service API.
 * <p>
 * This service is used as the entry point for all communication received from external
 * systems towards the test bed.
 */
public class IncomingMessagesServiceImpl implements CISEMessageServiceSoapImpl {

    private static final Logger LOG = LoggerFactory.getLogger(IncomingMessagesServiceImpl.class);
    private final SyncAckFactory synchronousAcknowledgementFactory = new SyncAckFactory();
    private SessionManager sessionManager;
    private MessageIdStore messageIdStore;
    private MessageProcessor messageProcessor;
    private XmlMapper xmlMapper;

    private ReportBuilder reportBuilder;

    public IncomingMessagesServiceImpl(SessionManager sessionManager, MessageIdStore messageIdStore, MessageProcessor messageProcessor, XmlMapper xmlMapper, ReportBuilder reportBuilder) {
        this.sessionManager = sessionManager;
        this.messageIdStore = messageIdStore;
        this.messageProcessor = messageProcessor;
        this.xmlMapper = xmlMapper;
        this.reportBuilder = reportBuilder;
    }

    /**
     * Initialise XPaths used to detect elements in CISE payloads.
     */


    @Override
    public Acknowledgement send(Message message) {
        String testSessionId = determineTestSessionFromMessage(message);
        if (testSessionId != null) {
            messageIdStore.record(message.getMessageID());
            Acknowledgement ack = processMessage(message);
            TAR report = reportBuilder.createMessageReportMessageAndAck(ack, message);
            sessionManager.notifyTestBed(testSessionId, report);
            return ack;
        } else {
            LOG.warn("No session could be located for received message : " + message.getMessageID() + " : " + message.getSender());
            return null;
        }
    }

    private Acknowledgement processMessage(Message message) {
        try {
            return messageProcessor.receive(message);
        } catch (SignatureEx signatureEx) {
            return synchronousAcknowledgementFactory.buildAck(message, SyncAckType.INVALID_SIGNATURE, "" + signatureEx.getMessage());
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException eXmlMalformed) {
            return synchronousAcknowledgementFactory.buildAck(message, SyncAckType.XML_MALFORMED, "" + eXmlMalformed.getMessage());
        }
    }


    /**
     * Get the message for the synchronous ACK.
     *
     * @param receivedMessage The message received.
     * @return The ACK detail message.
     */
    private String getSynchronousAckMessage(Message receivedMessage) {
        return receivedMessage.getClass().getSimpleName() + " accepted";
    }

    /**
     * Determine which of the current test sessions should be notified for the received
     * message.
     * <p>
     * look in the message for a specific value known in the test session to match it against the SUT's configuration in the Test Bed.
     * - sender's  caracteristic .
     * - business-level properties (as common constant of the message)
     * that don't need to be validated.
     *
     * @param message The CISE message.
     * @return The corresponding test bed session ID.
     */
    private String determineTestSessionFromMessage(Message message) {
        // Get the service ID linked to the received message.
        String identifierFromMessage = sessionManager.obtainIdentifierFromMessage(message);
        try {
            if ((!sessionManager.getAllSessions().isEmpty()) && (identifierFromMessage != null)) {
                // Find the test session that is linked to this specific ID.
                String sessionFromIdentifier = findSessionFromIdentifier(identifierFromMessage);
                if (sessionFromIdentifier == null) return (resolveAssociatingLastEmpty(identifierFromMessage));
                return sessionFromIdentifier;
            }
        } catch (Exception e) {
            LOG.info("no session found !");
        }
        return null;
    }

    private String resolveAssociatingLastEmpty(String serviceId) {
        String result = null;
        Long maxFound = 0L;
        for (Map.Entry<String, Map<String, Object>> entry : sessionManager.getAllSessions().entrySet()) {
            Long valueref = (Long) sessionManager.getSessionInfo(entry.getKey().toString(), SessionData.TIME_CREATED);
            if (valueref > maxFound) {
                result = entry.getKey();
                maxFound = valueref;
            }
        }
        sessionManager.setSessionInfo(result, SessionData.SERVICE_ID, serviceId);
        return result;
    }

    private String findSessionFromIdentifier(String identifier) {
        for (Map.Entry<String, Map<String, Object>> entry : sessionManager.getAllSessions().entrySet()) {
            if (sessionManager.isIdentifiedBy(identifier, entry)) {
                return entry.getKey();
            }
        }
        return null;
    }

}
