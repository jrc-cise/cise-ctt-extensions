package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.CISEMalformedXmlException;
import eu.eucise.xml.CISEXmlValidationException;
import eu.eucise.xml.XmlNotParsableException;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;

import java.util.List;

/**
 * #82 FORMAT Vessel Nationality
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `Nationality` is defined
 * `*THEN*` attribute `Nationality` must be composed by 2 alphabetic characters in accordance with iso3166-1.
 **/
public class TestStep82FormatVesselNationality extends CiseTestStep {

    public TestStep82FormatVesselNationality(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_82_FORMAT_VESSEL_NATIONALITY;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());
        String warningExplanation = errorHelper.getWarningDescription(testStepRef.getUiCheckName());
        try {
            boolean conform = true, warning = false;
            List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Vessel.class);
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                if (!errorValid(entityPair.getA())) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                } else if (!warningValid(entityPair.getA())) {
                    warning = true;
                    warningExplanation = errorHelper.addPointOfFailure(warningExplanation, errorHelper.avoidNodeId(entityPair.getB()), entityPair.getA());
                }
            }
            if (!conform) {
                report.setResult(TestResultType.FAILURE);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else if (warning) {
                report.setResult(TestResultType.WARNING);
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (warningExplanation), "string", ValueEmbeddingEnumeration.STRING));
            } else {
                report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (ReportBuilder.REPORT_STEP_SUCCESS), "string", ValueEmbeddingEnumeration.STRING));
            }
        } catch (XmlNotParsableException | CISEXmlValidationException | CISEMalformedXmlException e) {
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
            report.setResult(TestResultType.FAILURE);
        }
        return report;
    }

    /**
     * @param entity
     * @return
     */
    private boolean errorValid(Entity entity) {
        if (entity instanceof Vessel) {
            List<String> nationalities = ((Vessel) entity).getNationalities();
            if (nationalities == null) {
                return true;
            } else {
                for (String nationality : nationalities) {
                    return !(nationality.length() != 2);
                }
            }
        }
        return true;
    }

    /**
     * @param entity
     * @return
     */
    private boolean warningValid(Entity entity) {
        if (entity instanceof Vessel) {
            List<String> nationalities = ((Vessel) entity).getNationalities();
            if (nationalities == null) {
                return true;
            } else {
                for (String nationality : nationalities) {
                    if (nationality.length() != 2)
                        return false; //  (undg.trim().matches("^[+0-9 ]+$") && (undg.startsWith("00879") || (undg.startsWith("+879"))));
                    else
                        return (ValidNationalCode.getLitteralForNatCode2(nationality) != null);
                }
            }
        }
        return true;
    }


}
