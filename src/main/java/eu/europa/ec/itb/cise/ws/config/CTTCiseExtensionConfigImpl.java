package eu.europa.ec.itb.cise.ws.config;

import eu.cise.dispatcher.DispatcherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

@Component
public class CTTCiseExtensionConfigImpl implements CTTCiseExtensionConfig {

    @Autowired
    private Environment env;

    // emulator
    private DispatcherType dispatcherType;
    private String keyStoreFileName;
    private String keyStorePassword;
    private String privateKeyAlias;
    private String privateKeyPassword;

    // ctt
    private String persistentMessageIdFilePath;
    private Locale currentLocale;

    // proxy
    private boolean proxyEnabled;
    private String proxyServer;
    private Integer proxyPort;
    private String proxyType;
    private boolean proxyAuthEnabled;
    private String proxyUsername;
    private String proxyPassword;
    private String proxyNonProxyHosts;


    @PostConstruct
    public void initialize() {
        this.dispatcherType = DispatcherType.valueOf(env.getProperty("transport.mode"));
        this.keyStoreFileName = env.getProperty("signature.keystore.filename");
        this.keyStorePassword = env.getProperty("signature.keystore.password");
        this.privateKeyAlias = env.getProperty("signature.privatekey.alias");
        this.privateKeyPassword = env.getProperty("signature.privatekey.password");

        this.persistentMessageIdFilePath = env.getProperty("validation.persistentMessageIdFilePath");

        this.proxyEnabled = Boolean.valueOf(env.getProperty("proxy.enabled"));
        this.proxyServer = env.getProperty("proxy.server");
        this.proxyPort = Integer.valueOf(env.getProperty("proxy.port"));
        this.proxyType = env.getProperty("proxy.type");
        this.proxyAuthEnabled = Boolean.valueOf(env.getProperty("proxy.auth.enabled"));
        this.proxyUsername = env.getProperty("proxy.auth.username");
        this.proxyPassword = env.getProperty("proxy.auth.password");
        this.proxyNonProxyHosts = env.getProperty("proxy.nonProxyHosts");
        this.currentLocale = new Locale(env.getProperty("current.locale"));
    }

    public DispatcherType getDispatcherType() {
        return dispatcherType;
    }

    public String getKeyStoreFileName() {
        return keyStoreFileName;
    }

    public String getKeyStorePassword() {
        return keyStorePassword;
    }

    public String getPrivateKeyAlias() {
        return privateKeyAlias;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public String getPersistentMessageIdFilePath() {
        return persistentMessageIdFilePath;
    }

    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    public String getProxyServer() {
        return proxyServer;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public String getProxyType() {
        return proxyType;
    }

    public boolean isProxyAuthEnabled() {
        return proxyAuthEnabled;
    }

    public String getProxyUsername() {
        return proxyUsername;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public String getProxyNonProxyHosts() {
        return proxyNonProxyHosts;
    }

    @Override
    public Locale getCurrentLocale() {
        return currentLocale;
    }


}
