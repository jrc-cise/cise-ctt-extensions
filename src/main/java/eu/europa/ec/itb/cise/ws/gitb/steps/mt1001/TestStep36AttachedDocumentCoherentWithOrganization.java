package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.document.OrganizationDocument;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #36 AttachedDocument Coherent With Organization
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Organization` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `OrganizationDocument`
 */
public class TestStep36AttachedDocumentCoherentWithOrganization extends CiseTestStepAttachedDocument {

    public TestStep36AttachedDocumentCoherentWithOrganization(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Organization.class};
        Class[] validAttachedDocumentTypes = new Class[]{OrganizationDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_36_ATTACHED_DOCUMENT_COHERENT_WITH_ORGANIZATION);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Organization cargo = (Organization) entity;
        List<Agent.DocumentRel> documentRels = cargo.getDocumentRels();
        if (documentRels == null || documentRels.size() == 0) {
            return true;
        }
        for (Agent.DocumentRel documentRel : documentRels) {
            if (!(isInstanceOfOneOf(documentRel.getDocument(), validAttachedDocumentTypes))) {
                return false;
            }
        }

        return true;
    }

}
