package eu.europa.ec.itb.cise.ws.gitb.steps.mt1004;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfLinkValue;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #61 Risk Minimum Identification
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Risk` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` payload
 * `*THEN*` it _must define_ a *relation to Entity(ies)* `Document` (+ its children entities) or
 * `Event` (+ its children entities) or
 * `Object` (+ its children entities) or
 * `Agent` (+ its children entities) or
 * `Location` (+ its children entities).
 */
public class TestStep61RiskMinimumIdentification extends CiseTestStepOneOfLinkValue {

    public TestStep61RiskMinimumIdentification(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClass = new Class[]{Risk.class};
        String[] fieldNames = new String[]{"documentRels", "impliedEventRels", "involvedObjectRels", "involvedAgentRels", "locationRels"};
        fieldNames = resolveChild(fieldNames);
        return super.createReport(messageXml, fieldNames, entityClass, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_61_RISK_MINIMUM_IDENTIFICATION);
    }

    private String[] resolveChild(String[] fieldNames) {
        /*augment the fieldnames ArrayList with search of childs*/
        return fieldNames;
    }


}