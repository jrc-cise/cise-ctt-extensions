package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepOneOfChildValueBoundary;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.Arrays;
import java.util.List;

/**
 * #99 Boundary Vehicle attributes
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vehicle` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `xxxx` _is  defined_
 * `*THEN*` *attribute* `xxx` is defined in acceptable limits
 */
public class TestStep99BoundaryVehicle extends CiseTestStepOneOfChildValueBoundary {

    public TestStep99BoundaryVehicle(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] entityClasses = new Class[]{Aircraft.class, LandVehicle.class, Vessel.class};
        List<String> fields = Arrays.asList(
                "totalPersonsOnBoard",
                "maximumSpeed");

        return super.createReport(messageXml,
                fields,
                "Objet",
                entityClasses,
                CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_99_BOUNDARY_VEHICLE
        );

    }
}

