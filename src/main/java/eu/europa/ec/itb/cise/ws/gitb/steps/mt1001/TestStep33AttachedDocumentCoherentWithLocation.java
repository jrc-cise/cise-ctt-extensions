package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.LocationDocument;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepAttachedDocument;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.List;

/**
 * #33 AttachedDocument Coherent With Location
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Location` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` it have a relation to `Document`
 * `*THEN*` it must have end with entity(ies) of type `LocationDocument`
 */
public class TestStep33AttachedDocumentCoherentWithLocation extends CiseTestStepAttachedDocument {

    public TestStep33AttachedDocumentCoherentWithLocation(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        Class[] classesToFind = new Class[]{Document.class};
        Class[] validAttachedDocumentTypes = new Class[]{LocationDocument.class};
        return createReport(messageXml, classesToFind, validAttachedDocumentTypes, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_33_ATTACHED_DOCUMENT_COHERENT_WITH_LOCATION);
    }

    protected boolean hasAttachedDocumentOfValidType(Entity entity, Class[] validAttachedDocumentTypes) {
        Document document = (Document) entity;
        List<Document.LocationRel> locationRels = document.getLocationRels();
        if (locationRels == null || locationRels.size() == 0) {
            return true;
        }

        if (document.getLocationRels().get(0) != null && !(isInstanceOfOneOf(document, validAttachedDocumentTypes))) {
            return false;
        }

        return true;
    }

}
