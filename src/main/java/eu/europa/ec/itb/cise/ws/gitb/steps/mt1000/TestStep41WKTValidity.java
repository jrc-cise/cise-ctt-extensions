package eu.europa.ec.itb.cise.ws.gitb.steps.mt1000;

import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.apache.logging.log4j.util.Strings;
import org.locationtech.spatial4j.context.jts.JtsSpatialContext;
import org.locationtech.spatial4j.context.jts.JtsSpatialContextFactory;
import org.locationtech.spatial4j.io.WKTReader;
import org.locationtech.spatial4j.shape.Shape;
import org.locationtech.spatial4j.shape.jts.JtsGeometry;

import java.util.List;

/**
* #41 Geometry WKT Validity
* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* `*GIVEN*`  *Element* of type `WKT` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
* `*THEN*` it should be evaluable as `WKT` syntax.
*/
public class TestStep41WKTValidity extends CiseTestStep {

    public TestStep41WKTValidity(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        TAR report = reportBuilder.createReport(TestResultType.SUCCESS);
        CiseTestStepsEnum testStepRef = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_41_WKT_VALIDITY;
        String failureExplanation = errorHelper.getBaseErrorDescription(testStepRef.getUiCheckName());

        boolean conform = true;
        List<Pair<Entity, String>> payloadEntitiesOfType = payloadHelper.getPayloadEntitiesOfType(messageXml, Location.class);
        try {
            for (Pair<Entity, String> entityPair : payloadEntitiesOfType) {
                Location location = (Location) entityPair.getA();
                String breadcrumb = entityPair.getB();
                if (isWKTDefined(location) && !isWKTCorrectlyDefined(location)) {
                    conform = false;
                    failureExplanation = errorHelper.addPointOfFailure(failureExplanation, errorHelper.avoidNodeId(breadcrumb), location);
                }
            }
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), (conform ? reportBuilder.REPORT_STEP_SUCCESS : failureExplanation), "string", ValueEmbeddingEnumeration.STRING));
        } catch (Exception e) {
            report.setResult(conform ? TestResultType.SUCCESS : TestResultType.FAILURE);
            report.getContext().getItem().add(reportBuilder.createAnyContentSimple(testStepRef.getUiCheckName(), "Unknown exception occurred: " + e.getMessage(), "string", ValueEmbeddingEnumeration.STRING));
        }

        return report;
    }

    private boolean isWKTDefined(Location location) {
        if (location.getGeometries() == null ||
                (location.getGeometries() != null && location.getGeometries().isEmpty())) {
            return false;
        }

        Geometry geometry = location.getGeometries().get(0);
        if (Strings.isNotBlank(geometry.getWKT())) {
            return true;
        }
        return false;
    }

    private boolean isWKTCorrectlyDefined(Location location) {
        Geometry geometry = location.getGeometries().get(0);

        String wkt = geometry.getWKT();
        JtsSpatialContextFactory jtsSpatialContextFactory = new JtsSpatialContextFactory();
        JtsSpatialContext jtsSpatialContext = jtsSpatialContextFactory.newSpatialContext();
        WKTReader wktReader = new WKTReader(jtsSpatialContext, jtsSpatialContextFactory);
        try {
            Shape targetAreaShape = wktReader.parse(wkt);
            String geometryType = ((JtsGeometry) targetAreaShape).getGeom().getGeometryType();
            if (geometryType.equals("Point") ||
                    geometryType.equals("LineString") ||
                    geometryType.equals("Polygon") ||
                    geometryType.equals("MultiPoint") ||
                    geometryType.equals("MultiLineString") ||
                    geometryType.equals("MultiPolygon") ||
                    geometryType.equals("GeometryCollection")
            ) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

}
