#1000 Entities Minimal Definition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`*GIVEN*` *entity(ies)* of type ...
[]
`*THEN*` it _must define   ...
[]

.specific rule concern
[cols="5%,15%,15%,65%"]
|=======================
|_N_|_Title_|_Entity_|_Gherkin_
|10|Vessel Minimum Identification|`Vessel`|...   _is  defined_
pass:[</br>]
`*THEN*` it  _must define one of_ *attribute*: `CallSign`, `IMONumber`, `IRNumber`, `MMSI`, `RegistryNumber`, `UVI`, `Name`, `ExternalMarkings`, `Location`.
|11|Person Minimum Identification|`Person`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `AlternativeName`, `BirthName`, `ContactInformation`, `FamilyName`, `FullName`, `GIVENName`, `PatronymicName`, `PersonIdentifier`.
|12|Organization Minimum Identification|`Organization`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `AlternativeName`, `IdentificationNumber`, `LegalName`.
|13|OrganizationUnit Minimum Identification|`OrganizationUnit`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `AlternativeName`, `IdentificationNumber`, `LegalName`, `UnitIdentifier`.
|14|PortOrganization Minimum Identification|`PortOrganization`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `AlternativeName`, `IdentificationNumber`, `LegalName`, `IMOCompany`, `IdentificationNumber`.
|15|FormalOrganization Minimum Identification|`FormalOrganization`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `AlternativeName`, `IdentificationNumber`, `LegalName`, `FormalOrganizationName`.
|16|AircraftAndLandvehicle Minimum Identification|`Aircraft` or `Landvehicle`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define one of_ *attribute*: `ExternalMarkings`, `Name`
`*OR*` define at least a *relation to*: `Location`.
|17|Anomaly Minimum Identification|`Anomaly`|...   _is  defined_
pass:[</br>]
`*THEN*`  it _must define_ *attribute* : `AnomalyType`.
|24|AttachedDocument Minimum Identification|`XxxxxDocument where Xxxxx can be Vessel, Cargo, Event, Location, Organization, Person, Certificate`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define_ *attribute* `ContentReferenceURI`.
|25|Identification Of DocumentType|`XxxxxDocument where Xxxxx can be Vessel, Cargo, Event, Location, Organization, Person, Certificate`|...   _is  defined_
pass:[</br>]
`*THEN*` it  _must define_ *attribute* `Metadata`  pass:[</br>]
`*AND*` _must define at least one_  `Metadata` element with  key `FileMediaType` and non empty value.
 |39|Location Minimum Definition|`Location`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define_ *attribute* `GeographicName`.
 |40|Geometry Minimum Validity|`Geometry`|...   _is  defined_
pass:[</br>]
`*THEN*` it  _must define_ *attribute* `Latitude` and `Longitude`, or `WKT`, or `XMLGeometry` (exclusively).
 |41|Geometry WKT Validity|`Geometry` sub-type `WKT`|...   _is  defined_
pass:[</br>]
`*THEN*` _must define a valid value of_ *Element* `WKT` syntax.
 |42|Geometry XMLGeometry Validity|`Geometry` sub-type `XMLGeometry`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define a valid value of_  *Element* `XMLGeometry` syntax.
 |43|NamedLocation Minimum Definition|`NamedLocation`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define at least one_ *attribute* `geographicNames`.
 |44|PortLocation Minimum Definition|`PortLocation`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define at least one_ *attribute* `portName´, ´locationCode`.
 |45|PollutionIncident Type Minimum Identification|`PortFacilityLocation`|...   _is  defined_
pass:[</br>]
`*THEN*`  _must define at least one_ *attribute* `PortFacilityName´, ´PortFacilityNumber`.
 |59|MeteoOceanographicCondition Minimum Identification|`MeteoOceanographicCondition`|...   _is  defined_
pass:[</br>]
`*THEN*` it _must define at least one_ *attribute* `AirTemperature`, `CloudCeiling`, `CloudCover or`, `Precipitation`, `ReferencePeriod`, `Salinity`, `SeaCondition`, `SeaLevelPressure`, `SourceType`, `TidalCurrentDirection`, `TidalCurrentSpeed`, `Tides`, `Visibility`, `WaterTemperature`, `WaveDirection`, `WaveHeight`, `WeatherCondition`, `WindCurrentDirection`, `WindCurrentSpeed`.
|=======================
