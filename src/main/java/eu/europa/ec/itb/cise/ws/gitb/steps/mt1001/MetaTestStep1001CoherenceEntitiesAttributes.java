package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseMetaTestStep;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum.*;

public class MetaTestStep1001CoherenceEntitiesAttributes extends CiseMetaTestStep {

    public MetaTestStep1001CoherenceEntitiesAttributes(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    @Override
    public TAR createReport(String messageXml) {
        List<CiseTestStepsEnum> steps = new ArrayList(Arrays.asList(
                REPORT_ELEMENT_MODEL_TS_28_POLLUTION_INCIDENT_TYPE,
                REPORT_ELEMENT_MODEL_TS_32_ATTACHED_DOCUMENT_COHERENT_WITH_VESSEL,
                REPORT_ELEMENT_MODEL_TS_33_ATTACHED_DOCUMENT_COHERENT_WITH_LOCATION,
                REPORT_ELEMENT_MODEL_TS_34_ATTACHED_DOCUMENT_COHERENT_WITH_EVENT,
                REPORT_ELEMENT_MODEL_TS_35_ATTACHED_DOCUMENT_COHERENT_WITH_CARGO,
                REPORT_ELEMENT_MODEL_TS_36_ATTACHED_DOCUMENT_COHERENT_WITH_ORGANIZATION,
                REPORT_ELEMENT_MODEL_TS_37_ATTACHED_DOCUMENT_COHERENT_WITH_RISK,
                REPORT_ELEMENT_MODEL_TS_38_ATTACHED_DOCUMENT_COHERENT_WITH_PERSON,
                REPORT_ELEMENT_MODEL_TS_63_LINK_OBJECT_LOCATION_VEHICLE,
                REPORT_ELEMENT_MODEL_TS_64_LINK_OBJECT_AGENT_ORGANIZATION,
                REPORT_ELEMENT_MODEL_TS_65_LINK_OBJECT_AGENT_VEHICLE,
                REPORT_ELEMENT_MODEL_TS_66_LINK_OBJECT_AGENT_CARGOUNIT,
                REPORT_ELEMENT_MODEL_TS_67_LINK_OBJECT_EVENT_CARGOUNIT,
                REPORT_ELEMENT_MODEL_TS_68_LINK_OBJECT_MOVEMENT,
                REPORT_ELEMENT_MODEL_TS_69_LINK_OBJECT_ANOMALY
        ));

        return createReport(messageXml, steps, REPORT_ELEMENT_META_MTS_1001_COHERENCE_ENTITIES_ATTRIBUTES);
    }

}
