package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.organization.OrganizationalCollaboration;
import eu.cise.datamodel.v1.entity.organization.OrganizationalUnit;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #73 Link Movement Location
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` entity(ies)
 * `*THEN*` its *attribute* `Duty` _must not be defined_
 */
public class TestStep75LinkOrganizationOrganization extends CiseTestStepRelationship {

    public TestStep75LinkOrganizationOrganization(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Organization.class, OrganizationalCollaboration.class, OrganizationalUnit.class, PortOrganization.class};
        Class[] objectClasses = new Class[]{Organization.class, OrganizationalCollaboration.class, OrganizationalUnit.class, PortOrganization.class};
        Class[] relationClasses = new Class[]{Agent.InvolvedWithRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_75_LINK_ORGANIZATION_ORGANIZATION);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {
        return (relationship instanceof Agent.InvolvedWithRel &&
                ((Agent.InvolvedWithRel) relationship).getDuty() == null);
    }


}
