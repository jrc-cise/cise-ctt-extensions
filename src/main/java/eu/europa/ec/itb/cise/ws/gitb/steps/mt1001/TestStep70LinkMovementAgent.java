package eu.europa.ec.itb.cise.ws.gitb.steps.mt1001;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.event.AgentRoleInEventType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepRelationship;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;

/**
 * #70 Link Agent Movement
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Anomaly` is defined in `PUSH` or `PUSH_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` have relation(s) to `Movement` entity(ies)
 * `*THEN*` relation _must not have predetermined value for_ attribute `ObjectRole` in `COORDINATOR`, `CAUSE`, `VICTIM`, `PERPETRATOR`.
 */
public class TestStep70LinkMovementAgent extends CiseTestStepRelationship {

    public TestStep70LinkMovementAgent(ReportBuilder reportBuilder, CisePayloadHelper payloadHelper, GitbErrorHelper errorHelper) {
        super(reportBuilder, payloadHelper, errorHelper);
    }

    public TAR createReport(String messageXml) {
        Class[] eventClasses = new Class[]{Movement.class};
        Class[] objectClasses = new Class[]{Agent.class};
        Class[] relationClasses = new Class[]{Agent.InvolvedEventRel.class, Event.InvolvedAgentRel.class};
        return createReport(messageXml, relationClasses, objectClasses, eventClasses, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_70_LINK_MOVEMENT_AGENT);
    }

    @Override
    protected boolean isRelationshipOfCorrectType(Relationship relationship) {

        if (relationship instanceof Event.InvolvedAgentRel) {
            eventAgentRole = ((Event.InvolvedAgentRel) relationship).getAgentRole();
        } else if (relationship instanceof Agent.InvolvedEventRel) {
            eventAgentRole = ((Agent.InvolvedEventRel) relationship).getAgentRole();
        } else {
            return false;
        }

        return !isAgentRoleOfWrongType(eventAgentRole);
    }

    private boolean isAgentRoleOfWrongType(AgentRoleInEventType eventAgentRole) {
        return eventAgentRole == AgentRoleInEventType.COORDINATOR ||
                eventAgentRole == AgentRoleInEventType.CAUSE ||
                eventAgentRole == AgentRoleInEventType.VICTIM ||
                eventAgentRole == AgentRoleInEventType.PERPETRATOR;
    }
}
