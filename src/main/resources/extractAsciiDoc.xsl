<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="xhtml">
    <xsl:template match="text()"/>
    <xsl:template match="xhtml:div[@class='block']">
        <html lang="en">
            <head></head>
            <body>
                <xsl:call-template name="identity"/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="node()|@*" mode="identity" name="identity">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="identity"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>