package eu.europa.ec.itb.cise.ws.transport;

import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.AcknowledgementType;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.PriorityType;
import eu.cise.servicemodel.v1.service.Service;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.AckBuilder;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

import static eu.cise.servicemodel.v1.service.ServiceType.VESSEL_SERVICE;
import static eu.eucise.helpers.AckBuilder.newAck;
import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;

public class MessageBuilderUtil {
    public static Message createMessage() {
        Service service = newService().type(VESSEL_SERVICE).build();
        service.setServiceID("serviceID");
        service.setServiceType(ServiceType.VESSEL_SERVICE);
        service.setServiceOperation(ServiceOperationType.PUSH);


        return newPush()
                .id("sampleMessageId")
                .sender(service)
                .priority(PriorityType.HIGH)
                .creationDateTime(Date.from(Instant.now()))
                .build();
    }
    public static Acknowledgement createAcknowledgeMessage() {
        Message message = createMessage();
        AcknowledgementType acknowledgementType;
        String acknowledgementDetail;

        // define the acknowledgementType
        if (!message.getSender().getServiceType().equals(VESSEL_SERVICE)) {
            acknowledgementType = AcknowledgementType.SERVICE_TYPE_NOT_SUPPORTED;
            acknowledgementDetail = "Supported service type is " + message.getSender().getServiceType().value();
        } else {
            acknowledgementType = AcknowledgementType.SUCCESS;
            acknowledgementDetail = "Message delivered";
        }

        // build the acknowledgement
        AckBuilder ackBuilder = newAck()
                .id(UUID.randomUUID().toString())
                .sender(newService()
                        .id(message.getSender().getServiceID())
                        .operation(ServiceOperationType.ACKNOWLEDGEMENT))
                .creationDateTime(Date.from(java.time.ZonedDateTime.now(ZoneId.of("UTC")).toInstant()))
                .informationSecurityLevel(message.getPayload().getInformationSecurityLevel())
                .informationSensitivity(message.getPayload().getInformationSensitivity())
                .purpose(message.getPayload().getPurpose())
                .priority(message.getPriority())
                .ackCode(acknowledgementType)
                .ackDetail(acknowledgementDetail)
                .isRequiresAck(false);

        Acknowledgement acknowledgement = ackBuilder.build();
        return acknowledgement;
    }

}