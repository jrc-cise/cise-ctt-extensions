package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #90 FORMAT Portfacility Portfacilitynumber
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Portfacility` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE`  *payload*
 * `*AND*` attribute `Portfacilitynumber` is defined
 * `*THEN*` attribute `Portfacilitynumber` must be composed by 5 character LOCODE corresponding to port followed by "-" and 4 digits
 */
public class TestStep90FormatPortfacilityPortfacilitynumberTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step90_FormatPortfacilityPortfacilitynumber";
    private TestStep90FormatPortfacilityPortfacilitynumber testStep90FormatPortfacilityPortfacilitynumber;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep90FormatPortfacilityPortfacilitynumber = new TestStep90FormatPortfacilityPortfacilitynumber(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_90_FORMAT_PORTFACILITY_PORTFACILITYNUMBER.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_success();
        payloadCreator_FormatCatchSpeciesTest_failure();
        payloadCreator_FormatCatchSpeciesTest_warning();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep90FormatPortfacilityPortfacilitynumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep90FormatPortfacilityPortfacilitynumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_warning_report_with_correct_validation() {
        TAR report = testStep90FormatPortfacilityPortfacilitynumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        portFacilityLocation.setPortFacilityNumber("FRPFR-2354"); //   5 character LOCODE corresponding to port followed by "-" and 4 digits
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        organization.getLocationRels().add(locationRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        portFacilityLocation.setPortFacilityNumber("FROFR-354"); //   5 character LOCODE corresponding to port followed by "-" and 4 digits
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_warning() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        portFacilityLocation.setPortFacilityNumber("FXPFR-2354"); //   5 character LOCODE corresponding to port followed by "-" and 4 digits
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning.xml", xmlMapper, vessel);
    }
}