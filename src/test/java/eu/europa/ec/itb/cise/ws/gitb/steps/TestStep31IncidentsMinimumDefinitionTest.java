package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep31IncidentMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep31IncidentsMinimumDefinitionTest extends CiseTestStepTest {



    private TestStep31IncidentMinimumDefinition testStep31IncidentMinimumDefinition;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep31IncidentMinimumDefinition = new TestStep31IncidentMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_31_INCIDENT_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident() {
        String messageXml = utils.readFile("messages/push_Step31_IncidentsMinimumDefinition_success.xml");
        TAR report = testStep31IncidentMinimumDefinition.createReport(messageXml);
        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident_failure() {
        String messageXml = utils.readFile("messages/push_Step31_IncidentsMinimumDefinition_failure.xml");
        TAR report = testStep31IncidentMinimumDefinition.createReport(messageXml);
        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident_searching_backward() {
        String messageXml = utils.readFile("messages/push_Step31_IncidentsMinimumDefinition_backward_success.xml");
        TAR report = testStep31IncidentMinimumDefinition.createReport(messageXml);
        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident_searching_backward_failure() {
        String messageXml = utils.readFile("messages/push_Step31_IncidentsMinimumDefinition_backward_failure.xml");
        TAR report = testStep31IncidentMinimumDefinition.createReport(messageXml);
        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(reportResult).isNotEqualTo("VALID");
    }


    //@Test
    public void payloadCreator_backward_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create another maritime incident (to link to vessel)
        MaritimeSafetyIncident maritimeSafetyIncident2 = new MaritimeSafetyIncident();
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident2);
        vehicle.getInvolvedEventRels().add(involvedEventRel);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
    }

    //@Test
    public void payloadCreator_backward_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        vehicle = (Vehicle) new Vessel();

        // add vessel to first maritime incident
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);

        //OCNET-225 : We assume cargo is not child of vehicle
        // so idea is to add cargo REL to first maritime incident
        Cargo vehicle2;
        vehicle2 = /*(Vehicle)*/ new Cargo();
        Event.InvolvedObjectRel involvedObjectRel2 = new Event.InvolvedObjectRel();
        involvedObjectRel2.setObject(vehicle2);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel2);

        // create another maritime incident (to link to vessel)
        MaritimeSafetyIncident maritimeSafetyIncident2 = new MaritimeSafetyIncident();
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident2);
        vehicle2.getInvolvedEventRels().add(involvedEventRel);

        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
    }


}
