package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #89 FORMAT NamedLocation GeographicIdentifier
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `NamedLocation` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `GeographicIdentifier` is defined
 * `*THEN*` attribute `GeographicIdentifier` must be an url  starting with "http://sws.geonames.org/" followed by digits identifying the location
 */
public class TestStep89FormatNamedLocationGeographicIdentifierTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step89_FormatNamedLocationGeographicIdentifier";
    private TestStep89FormatNamedLocationGeographicIdentifier testStep89FormatNamedLocationGeographicIdentifier;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep89FormatNamedLocationGeographicIdentifier = new TestStep89FormatNamedLocationGeographicIdentifier(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_89_FORMAT_NAMED_LOCATION_GEOGRAPHIC_IDENTIFIER.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_success();
        payloadCreator_FormatCatchSpeciesTest_failure();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep89FormatNamedLocationGeographicIdentifier.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep89FormatNamedLocationGeographicIdentifier.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        NamedLocation namedLocation = new NamedLocation();
        namedLocation.setGeographicIdentifier("http://sws.geonames.org/21342345");// success factor digit
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(namedLocation);
        organization.getLocationRels().add(locationRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        // here should have a link to a location
        NamedLocation namedLocation = new NamedLocation();
        namedLocation.setGeographicIdentifier("http://sws.geonames.org/213sd42345");// success factor digit
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(namedLocation);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

}