package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep21VesselAnomalyMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep21VesselAnomalyMinimumDefinitionTest extends CiseTestStepTest {



    private TestStep21VesselAnomalyMinimumDefinition testStep21VesselAnomalyMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep21VesselAnomalyMinimumDefinition = new TestStep21VesselAnomalyMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_21_VESSEL_ANOMALY_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_ModelCargoLeaking_validation() {
        TAR report = testStep21VesselAnomalyMinimumDefinition.createReport(utils.readFile("messages/push_Step21_ModelVesselAnomalyMinimumDefinition_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_backward_ModelCargoLeaking_validation() {
        TAR report = testStep21VesselAnomalyMinimumDefinition.createReport(utils.readFile("messages/push_Step21_ModelVesselAnomalyMinimumDefinition_success_backward.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_ModelCargoLeaking_validation() {
        TAR report = testStep21VesselAnomalyMinimumDefinition.createReport(utils.readFile("messages/push_Step21_ModelVesselAnomalyMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_empty_ModelCargoLeaking_validation() {
        TAR report = testStep21VesselAnomalyMinimumDefinition.createReport(utils.readFile("messages/push_Step21_ModelVesselAnomalyMinimumDefinition_failure_empty.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}
