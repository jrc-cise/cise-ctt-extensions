package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_01_IN_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_01_IN_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_03_OUT_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_03_OUT_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_04_IN_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_04_IN_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_05_OUT_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_05_OUT_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POLYGON_SEARCH_AREA;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.SAMPLE_VESSEL_ONE_IMO_NUMBER;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.SAMPLE_VESSEL_TWO_IMO_NUMBER;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.getSampleVessel;
import static org.assertj.core.api.Assertions.assertThat;

public class TestStep49VesselLocationPullResponseTest extends CiseTestStepTest {
    private static final String TEST_FILE_REQUEST_PREFIX = "messages/pullRequest_Step49_CheckVesselLocation";
    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step49_CheckVesselLocation";

    private TestStep49VesselLocationPullResponse testStep49VesselLocationPullResponse;

    private String pullRequest = "";

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep49VesselLocationPullResponse = new TestStep49VesselLocationPullResponse(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckVesselLocation_request();
        pullRequest = utils.readFile(TEST_FILE_REQUEST_PREFIX + ".xml");
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_49_VESSEL_LOCATION_PULL_RESPONSE.getUiCheckName();
    }

    @Test
    public void it_creates_a_success_report_with_successful_localisation_of_two_vessel() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_successful_localisation_of_two_vessel();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_successful_localisation_of_two_vessel() {
        Vessel vessel1 = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_04_IN_AREA_LON, POINT_04_IN_AREA_LAT);
        Vessel vessel2 = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_01_IN_AREA_LON, POINT_01_IN_AREA_LAT);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_successful_localisation_of_two_vessel.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel1, vessel2)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_failure_report_with_unsuccessful_localisation_of_two_vessel() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_failure_report_with_unsuccessful_localisation_of_two_vessel();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_failure_report_with_unsuccessful_localisation_of_two_vessel() {
        Vessel vessel1 = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_03_OUT_AREA_LON, POINT_03_OUT_AREA_LAT);
        Vessel vessel2 = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LON, POINT_05_OUT_AREA_LAT);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_failure_report_with_unsuccessful_localisation_of_two_vessel.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel1, vessel2)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_for_localisation_containing_malformed_coordinates() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_for_localisation_containing_malformed_coordinates();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_for_localisation_containing_malformed_coordinates() {
        Vessel vessel = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_for_localisation_containing_malformed_coordinates.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_for_localisation_containing_empty_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_for_localisation_containing_empty_polygon();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_for_localisation_containing_empty_polygon() {
        Vessel vessel = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_for_localisation_containing_empty_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_no_vessel() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_no_vessel();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_no_vessel() {
        ArrayList<Entity> entities = new ArrayList<Entity>(); // no vessel to entities arrayList
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_no_vessel.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, entities, Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_failure_report_with_two_vessels_from_which_one_is_outside_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_one_is_outside_polygon();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_one_is_outside_polygon() {
        Vessel vessel1 = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LON, POINT_01_IN_AREA_LAT);
        Vessel vessel2 = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LON, POINT_05_OUT_AREA_LAT);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_failure_report_with_two_vessels_from_which_one_is_outside_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel1, vessel2)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_failure_report_with_two_vessels_from_which_first_is_inside_polygon_and_second_is_an_empty_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_first_is_inside_polygon_and_second_is_an_empty_polygon();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_first_is_inside_polygon_and_second_is_an_empty_polygon() {
        Vessel vessel1 = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LON, POINT_01_IN_AREA_LAT);
        Vessel vessel2 = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_failure_report_with_two_vessels_from_which_first_is_inside_polygon_and_second_is_an_empty_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel1, vessel2)), Vessel.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_failure_report_with_two_vessels_from_which_first_is_outside_polygon_and_second_is_an_empty_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_first_is_outside_polygon_and_second_is_an_empty_polygon();

        //act
        TAR report = testStep49VesselLocationPullResponse.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_failure_report_with_two_vessels_from_which_first_is_outside_polygon_and_second_is_an_empty_polygon() {
        Vessel vessel1 = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_03_OUT_AREA_LON, POINT_03_OUT_AREA_LAT);
        Vessel vessel2 = getSampleVessel(SAMPLE_VESSEL_TWO_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_failure_report_with_two_vessels_from_which_first_is_outside_polygon_and_second_is_an_empty_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(vessel1, vessel2)), Vessel.class);
        return utils.readFile(path);
    }

    private void payloadCreator_CheckVesselLocation_request() {
        Vessel vessel = getSampleVessel(SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        utils.writePullResponseMessageFiles((TEST_FILE_REQUEST_PREFIX + ".xml"), xmlMapper, new ArrayList<>(Arrays.asList(vessel)), Vessel.class);
    }

}
