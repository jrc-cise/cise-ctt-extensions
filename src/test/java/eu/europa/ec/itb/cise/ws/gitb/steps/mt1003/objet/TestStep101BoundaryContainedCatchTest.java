package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep101BoundaryContainedCatchTest extends CiseTestStepTestChildValueBoundaries {

    private final String testFilePrefix = "messages/push_Step101_BoundaryContainedCatchTest";
    private final String propertyFile = "src/test/java/eu/europa/ec/itb/cise/ws/gitb/steps/mt1003/objet/Objet.properties";
    private TestStep101BoundaryContainedCatch testStep101BoundaryContainedCatch;

    private static final String[] FIELDS = new String[]{
            "catchWeight",
            "fishNumber",
            "netHeld",
            "quantityHeld",
            "sizeDeclaration",
            "totalNumber",
            "totalWeight"};

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep101BoundaryContainedCatch = new TestStep101BoundaryContainedCatch(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_101_BOUNDARY_CONTAINEDCATCH.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Objet");
        if (errorBoundaries == null)
            if (reinitCondition)
                payload_PropertyBoundary_maker(Catch.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }
    }


    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep101BoundaryContainedCatch.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep101BoundaryContainedCatch.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts101BoundaryContainedCatch");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }

    @NotNull
    public Cargo getTestedEntity(String[] fields, boolean purposeSuccess) {
        Cargo cargo = new Cargo();
        Catch catch1 = (Catch) PropertyMaker.populateAttributes(errorBoundaries, Catch.class, fields, purposeSuccess);
        Cargo.ContainedCargoUnitRel containedCargoUnitRel = new Cargo.ContainedCargoUnitRel();
        containedCargoUnitRel.setCargoUnit(catch1);
        cargo.getContainedCargoUnitRels().add(containedCargoUnitRel);
        return cargo;
    }

}