package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep28PollutionIncidentTypeTest extends CiseTestStepTest {

    private TestStep28PollutionIncidentType testStep28PollutionIncidentType;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep28PollutionIncidentType = new TestStep28PollutionIncidentType(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_28_POLLUTION_INCIDENT_TYPE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void createReportForModelPollutionIncident_creates_a_report_with_successful_ModelPollutionIncident_validation() {
        TAR report = testStep28PollutionIncidentType.createReport(utils.readFile("messages/push_Step28_PollutionIncidentType_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void createReportForModelPollutionIncident_creates_a_report_with_unsuccessful_ModelPollutionIncident_validation() {
        TAR report = testStep28PollutionIncidentType.createReport(utils.readFile("messages/push_Step28_PollutionIncidentType_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


}
