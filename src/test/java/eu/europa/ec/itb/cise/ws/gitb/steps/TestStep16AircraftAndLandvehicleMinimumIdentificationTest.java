package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep16AircraftAndLandvehicleMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep16AircraftAndLandvehicleMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step16_AircraftAndLandvehicleMinimumIdentification";

    private TestStep16AircraftAndLandvehicleMinimumIdentification testStep16AircraftAndLandvehicleMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_FormalOrganizationMinimumIdentification_failure();
        payloadCreator_FormalOrganizationMinimumIdentification_names_success();
        payloadCreator_FormalOrganizationMinimumIdentification_externalMarkings_success();
        payloadCreator_FormalOrganizationMinimumIdentification_location_success();
        testStep16AircraftAndLandvehicleMinimumIdentification = new TestStep16AircraftAndLandvehicleMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_16_AIRCRAFT_AND_LANDVEHICLE_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_AircraftAndLandvehicleMinimumDefinition_externalMarkings_validation() {
        TAR report = testStep16AircraftAndLandvehicleMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_externalMarkings_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_AircraftAndLandvehicleMinimumDefinition_location_validation() {
        TAR report = testStep16AircraftAndLandvehicleMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_location_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_AircraftAndLandvehicleMinimumDefinition_names_validation() {
        TAR report = testStep16AircraftAndLandvehicleMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_names_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_AircraftAndLandvehicleMinimumDefinition_validation() {
        TAR report = testStep16AircraftAndLandvehicleMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormalOrganizationMinimumIdentification_externalMarkings_success() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        // success factor
        Aircraft aircraft = new Aircraft();
        aircraft.getExternalMarkings().add("blue mark");
        // linkage
        involvedObjectRel.setObject(aircraft);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident);
        cargo.getInvolvedEventRels().add(involvedEventRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_externalMarkings_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_FormalOrganizationMinimumIdentification_names_success() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        // success factor
        Aircraft aircraft = new Aircraft();
//        aircraft.getNames().add("blue squirrel");
        aircraft.getNames().add("blue squirrel");
        // linkage
        involvedObjectRel.setObject(aircraft);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident);
        cargo.getInvolvedEventRels().add(involvedEventRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_names_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_FormalOrganizationMinimumIdentification_location_success() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        // success factor
        Aircraft aircraft = new Aircraft();
        Objet.LocationRel locationrel = new Objet.LocationRel();
        Location location = new Location();
        locationrel.setLocation(location);
        aircraft.getLocationRels().add(locationrel);
        // linkage
        involvedObjectRel.setObject(aircraft);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident);
        cargo.getInvolvedEventRels().add(involvedEventRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_location_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_FormalOrganizationMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        // success factor unset
        Aircraft aircraft = new Aircraft();
        /* unsuccess factor  aircraft.getExternalMarkings().add("blue mark"); */
        // linkage
        involvedObjectRel.setObject(aircraft);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(maritimeSafetyIncident);
        cargo.getInvolvedEventRels().add(involvedEventRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
