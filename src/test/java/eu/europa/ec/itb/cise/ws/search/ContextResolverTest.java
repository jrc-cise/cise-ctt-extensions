package eu.europa.ec.itb.cise.ws.search;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.PollutionIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.cise.datamodel.v1.relationship.Relationship;
import eu.europa.ec.itb.cise.ws.util.Pair;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;


public class ContextResolverTest {
    private final ContextResolver contextResolver = new ContextResolver();
    /* from the definition of CONTEXT RESOLVER Responsability in our first test step
    #10 : find any Entity/vessel at any level
    #19 : find any Entity/Anomaly at any level (exclude posterior filter of anomalyType= CargoLeaking)
    #27 : find any Entity/PollutionIncident (sub E/Incident)
    #46 : find any Relation/LocationRel
    #49 : find Relation/LocationRel from Entity/Vessel
     */

    private Vessel vessel;
    private Anomaly anomaly;
    private Risk risk;
    private XmlMapper mapper;

    @BeforeEach
    public void before() {
        mapper = new DefaultXmlMapper.Pretty();
        String anomalyXML = Utils.readFile("payloads/TreeBuilderTestfile.xml");
        anomaly = mapper.fromXML(anomalyXML);
        // A- Anomaly.RiskRel.Risk
        // C- Anomaly.LocationRel.Location

        // B- risk.InvolvedObjectRel.vessel
        risk = anomaly.getImpliedRiskRels().get(0).getRisk();

        // G- vessel.locationRel.location
        vessel = (Vessel) risk.getInvolvedObjectRels().get(0).getObject();
        // D- Vessel.InvolvedEventRel.Anomaly
        //  E- anomaly.InvolvedObjectRel.vessel
        // F- risk.impliedEventRel.PollutionIncident
    }


    /*
    #10 : find any Entity/vessel at 3 level depth
    will use E- anomaly.InvolvedObjectRel.vessel
    */
    @Test
    public void it_finds_the_list_of_Vessels_with_references_in_the_message() {
        List<Pair<Entity, String>> entityList;
        entityList = contextResolver.findCiseObjectsWithReferences(Vessel.class, anomaly);
        assertThat(entityList, hasSize(2));
    }

    @Test
    public void it_finds_the_list_of_InvolvedObjectRel_with_references_in_the_message() {
        List<Pair<Relationship, String>> entityList;
        entityList = contextResolver.findCiseObjectsWithReferences(Event.InvolvedObjectRel.class, anomaly);
        assertThat(entityList, hasSize(3));
    }

    /*
    #19 : find any Entity/anomaly at 3 level depth
    will use D- Vessel.InvolvedEventRel.Anomaly
    */
    @Test
    public void it_should_find_a_node_anomaly_within_three_levels_of_depth() {
        List<Pair<Entity, String>> entityList;
        entityList = contextResolver.findCiseObjectsWithReferences(Anomaly.class, vessel);
        assertThat(entityList, hasSize(1));
    }

    /*
    #10 : find any Entity/vessel at 5 level depth
    will use A- Anomaly.RiskRel.Risk AND B- risk.InvolvedObjectRel.vessel
    */
    @Test
    public void it_should_find_a_node_vessel_within_five_levels_of_depth() {
        List<Pair<Entity, String>> entityList;
        entityList = contextResolver.findCiseObjectsWithReferences(Vessel.class, risk);
        assertThat(entityList, hasSize(1));
    }

    /*
    #27 : find any Entity/PollutionIncident (sub E/Incident)
    will use // F- risk.impliedEventRel.PollutionIncident
     */
    @Test
    public void it_should_find_a_node_pollutionIncident_within_three_levels_of_depth() {
        List<Pair<Entity, String>> entityList;
        entityList = contextResolver.findCiseObjectsWithReferences(PollutionIncident.class, risk);
        assertThat(entityList, hasSize(1));
    }

    /*
    #46 : find any Relation/LocationRel
    will use A- Anomaly.RiskRel.Risk
             B- risk.InvolvedObjectRel.vessel
             G- vessel.locationRel.location> this one
             C- Anomaly.LocationRel.Location> this one also

    */
    @Test
    public void it_should_find_all_Relation_LocationRel_within_five_levels_of_depth() {
        List<Pair<Relationship, String>> relationshipsWithReferences = contextResolver.findCiseObjectsWithReferences(Objet.LocationRel.class, anomaly);
        assertThat(relationshipsWithReferences, hasSize(2));
    }

    /*
    #49 : find Relation/LocationRel from Entity/Vessel ?
         will use A- Anomaly.RiskRel.Risk
              B- risk.InvolvedObjectRel.vessel
              G- vessel.locationRel.location > this only one
           // C- Anomaly.LocationRel.Location
     */
    @Test
    public void it_should_find_all_Relation_LocationRel_from_Vessel_within_three_levels_of_depth() {
        List<Pair<Relationship, String>> relationshipsWithReferences = contextResolver.findCiseObjectsWithReferences(Objet.LocationRel.class, anomaly);
        String entityClassName = "Vessel.LocationRel";
        List<Pair<Relationship, String>> relationshipsResult = new ArrayList<Pair<Relationship, String>>();
        for (Pair<Relationship, String> relationship : relationshipsWithReferences) {
            if (relationship.getB().contains(entityClassName)) {
                relationshipsResult.add(relationship);
            }
        }

        assertThat(relationshipsResult, hasSize(1));
    }

    @Test
    public void it_finds_parent_objects_of_a_specified_type_with_success() {
        List<Pair<Entity, String>> vesselEntities = contextResolver.findCiseObjectsWithReferences(Vessel.class, anomaly);

        Pair<Entity, String> entityStringPair = vesselEntities.get(0);

        Entity parent = contextResolver.findParentOfType(Risk.class, entityStringPair.getB(), anomaly);

        assertThat(parent, instanceOf(Risk.class));
    }

    @Test
    public void it_finds_parent_objects_of_a_specified_type_with_failure() {
        List<Pair<Entity, String>> vesselEntities = contextResolver.findCiseObjectsWithReferences(Vessel.class, anomaly);

        Pair<Entity, String> entityStringPair = vesselEntities.get(1);

        Entity parent = contextResolver.findParentOfType(Risk.class, entityStringPair.getB(), anomaly);

        assertThat(parent, nullValue());
    }
}
