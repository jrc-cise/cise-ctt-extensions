package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.agent.DutyType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep75LinkOrganizationOrganization;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #75 LinkMovementLocation
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*SCENARIO*` it must not provide *attribute* `EventArea` to *relation(s)* between `Movement` and `Location`
 * `*GIVEN*` relation(s) to `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` *entity(ies)* from *entity* of type `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *attribute* `Duty` _must not be defined_
 * <p>
 * examples:
 */
public class TestStep75LinkOrganizationOrganizationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step75_LinkOrganizationOrganization";
    private TestStep75LinkOrganizationOrganization testStep75LinkOrganizationOrganization;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep75LinkOrganizationOrganization = new TestStep75LinkOrganizationOrganization(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_75_LINK_ORGANIZATION_ORGANIZATION.getUiCheckName();
        payloadCreator_LinkOrganization_failure();
        payloadCreator_LinkOrganization_success();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep75LinkOrganizationOrganization.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep75LinkOrganizationOrganization.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_LinkOrganization_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortOrganization portOrganization = new PortOrganization();
        portOrganization.setLegalName("ANTERP");
        Agent.InvolvedWithRel withRel = new Agent.InvolvedWithRel();
        withRel.setAgent(portOrganization);
        organization.getInvolvedWithRels().add(withRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Vessel) vessel);
    }

    public void payloadCreator_LinkOrganization_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortOrganization portOrganization = new PortOrganization();
        portOrganization.setLegalName("ANTERP");
        Agent.InvolvedWithRel withRel = new Agent.InvolvedWithRel();
        withRel.setAgent(portOrganization);
        organization.getInvolvedWithRels().add(withRel);
        // failure factor
        withRel.setDuty(DutyType.CREW_MEMBER);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Vessel) vessel);
    }
}