package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.event.AgentRoleInEventType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep71LinkAnomalyAgent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep71LinkAnomalyAgentTest extends CiseTestStepTest {

    private TestStep71LinkAnomalyAgent testStep71LinkAnomalyAgent;
    private final String pathPrefix= "messages/push_Step71_LinkAnomalyAgent";

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep71LinkAnomalyAgent = new TestStep71LinkAnomalyAgent(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_71_LINK_ANOMALY_AGENT.getUiCheckName();
        payloadCreator_Agent_to_Anomaly_success();
        payloadCreator_Agent_to_Anomaly_failure();
        payloadCreator_LinkOrganization_success();
        payloadCreator_LinkOrganization_failure();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_Agent_to_Anomaly() {
        TAR report = testStep71LinkAnomalyAgent.createReport(utils.readFile(pathPrefix+"_Agent_to_Anomaly_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Agent_to_Anomaly() {
        TAR report = testStep71LinkAnomalyAgent.createReport(utils.readFile(pathPrefix+"_Agent_to_Anomaly_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_Anomaly_to_Agent() {
        TAR report = testStep71LinkAnomalyAgent.createReport(utils.readFile(pathPrefix+"_Anomaly_to_Agent_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Anomaly_to_Agent() {
        TAR report = testStep71LinkAnomalyAgent.createReport(utils.readFile(pathPrefix+"_Anomaly_to_Agent_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_Agent_to_Anomaly_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have now the proper agent to anomaly
        Anomaly anomaly = new Anomaly();
        Agent.InvolvedEventRel involvedEventRel = new Agent.InvolvedEventRel();
        involvedEventRel.setEvent(anomaly);
        // success as not AgentRoleInEventType.COORDINATOR | AgentRoleInEventType.PERPETRATOR
        involvedEventRel.setAgentRole(AgentRoleInEventType.INFORMED);
        organization.getInvolvedEventRels().add(involvedEventRel);

        utils.writePushMessageFiles(pathPrefix+"_Agent_to_Anomaly_success.xml", xmlMapper, (Vessel) vessel);
    }

    public void payloadCreator_Agent_to_Anomaly_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have now the proper agent to anomaly
        Anomaly anomaly = new Anomaly();
        Agent.InvolvedEventRel involvedEventRel = new Agent.InvolvedEventRel();
        involvedEventRel.setEvent(anomaly);
        // failure as AgentRoleInEventType.COORDINATOR | AgentRoleInEventType.PERPETRATOR
        involvedEventRel.setAgentRole(AgentRoleInEventType.COORDINATOR);
        organization.getInvolvedEventRels().add(involvedEventRel);
        utils.writePushMessageFiles(pathPrefix+"_Agent_to_Anomaly_failure.xml", xmlMapper, (Vessel) vessel);
    }


    public void payloadCreator_LinkOrganization_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        Anomaly anomaly = new Anomaly();
        involvedEventRel.setEvent(anomaly);
        vessel.getInvolvedEventRels().add(involvedEventRel);
        // here should have now the proper agent to anomaly

        Organization organization = new Organization();
        Event.InvolvedAgentRel involvedAgentRel = new Event.InvolvedAgentRel();
        involvedAgentRel.setAgent(organization);
        // success as not AgentRoleInEventType.COORDINATOR | AgentRoleInEventType.PERPETRATOR
        involvedAgentRel.setAgentRole(AgentRoleInEventType.INFORMED);
        anomaly.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(pathPrefix+"_Anomaly_to_Agent_success.xml", xmlMapper, (Vessel) vessel);
    }

    public void payloadCreator_LinkOrganization_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedEventRel involvedeventrel = new Objet.InvolvedEventRel();
        Anomaly anomaly = new Anomaly();
        involvedeventrel.setEvent(anomaly);
        vessel.getInvolvedEventRels().add(involvedeventrel);
        // here should have now the proper agent to anomaly
        Organization organization = new Organization();
        Event.InvolvedAgentRel involvedAgentRel= new Event.InvolvedAgentRel();
        involvedAgentRel.setAgent(organization);
        // failure as AgentRoleInEventType.COORDINATOR | AgentRoleInEventType.PERPETRATOR
        involvedAgentRel.setAgentRole(AgentRoleInEventType.COORDINATOR);
        anomaly.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(pathPrefix+"_Anomaly_to_Agent_failure.xml", xmlMapper, (Vessel) vessel);
    }

}