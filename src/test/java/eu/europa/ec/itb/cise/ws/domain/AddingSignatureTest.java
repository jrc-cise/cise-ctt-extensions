package eu.europa.ec.itb.cise.ws.domain;

import eu.cise.dispatcher.Dispatcher;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.cise.signature.SignatureService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

public class AddingSignatureTest {

    private SimEngine engine;
    private SignatureService signatureService;
    private Push push;
    private Dispatcher dispatcher;

    @BeforeEach
    public void before() {
        signatureService = mock(SignatureService.class);
        dispatcher = mock(Dispatcher.class);
        engine = new CTTSimEngine(signatureService, dispatcher);
        push = newPush().id("aMessageId").sender(newService()).build();
    }

    @AfterEach
    public void after() {
        reset(signatureService);
        reset(dispatcher);
    }

    @Test
    public void it_signs_the_message() {
        engine.prepare(push, params());

        verify(signatureService).sign(push);
    }

    @Test
    public void it_verify_the_signature() {
        push.getSender().setServiceType(ServiceType.VESSEL_SERVICE);

        engine.receive(push);

        verify(signatureService).verify(push);
    }

    private CTTSendParam params() {
        return new CTTSendParam(false, "msgId", "corrId", "destinationEndpoint");
    }

}
