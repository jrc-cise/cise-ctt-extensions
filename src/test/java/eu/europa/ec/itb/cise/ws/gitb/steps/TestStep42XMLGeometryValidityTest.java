package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep42XMLGeometryValidity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep42XMLGeometryValidityTest extends CiseTestStepTest {

    private TestStep42XMLGeometryValidity testStep42XMLGeometryValidity;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep42XMLGeometryValidity = new TestStep42XMLGeometryValidity(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_42_XMLGEOMETRY_VALIDITY.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_for_correctly_defined_Location_with_KML() {
        String messageXml = utils.readFile("messages/push_Step42_XMLGeometryValidity_KML_success.xml");

        TAR report = testStep42XMLGeometryValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_for_correctly_defined_Location_with_GML() {
        String messageXml = utils.readFile("messages/push_Step42_XMLGeometryValidity_GML_success.xml");

        TAR report = testStep42XMLGeometryValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_for_Location_with_no_WKT() {
        String messageXml = utils.readFile("messages/push_Step42_XMLGeometryValidity_no_XMLGeometry_success.xml");

        TAR report = testStep42XMLGeometryValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_for_bad_defined_Location_with_XMLGeometry() {
        String messageXml = utils.readFile("messages/push_Step42_XMLGeometryValidity_failure.xml");

        TAR report = testStep42XMLGeometryValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

}