package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep30VesselIncidentsMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep30VesselIncidentsMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep30VesselIncidentsMinimumDefinition testStep30VesselIncidentsMinimumDefinition;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep30VesselIncidentsMinimumDefinition = new TestStep30VesselIncidentsMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_30_VESSEL_INCIDENTS_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident() {
        String messageXml = utils.readFile("messages/push_Step30_VesselIncidentsMinimumDefinition_success.xml");

        TAR report = testStep30VesselIncidentsMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_Incident_searching_backward() {
        String messageXml = utils.readFile("messages/push_Step30_VesselIncidentsMinimumDefinition_backward_success.xml");

        TAR report = testStep30VesselIncidentsMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_MaritimeSafetyIncident_not_linked_any_object() {
        String messageXml = utils.readFile("messages/push_Step30_VesselIncidentsMinimumDefinition_failure.xml");

        TAR report = testStep30VesselIncidentsMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_MaritimeSafetyIncident_not_linked_to_any_object_of_type_vehicle() {
        String messageXml = utils.readFile("messages/push_Step30_VesselIncidentsMinimumDefinition_noLinkedVehicles_failure.xml");

        TAR report = testStep30VesselIncidentsMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }


}
