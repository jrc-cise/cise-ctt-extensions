package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import com.gitb.vs.ValidateRequest;
import com.gitb.vs.ValidationResponse;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CiseValidationServiceTest extends CiseTestStepTest {

    private CiseValidationService ciseValidationService;


    @BeforeEach
    public void setUp() {
        super.setUp();
        ciseValidationService = new CiseValidationService(reportBuilder, payloadHelper, errorHelper);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_instantiate_the_correct_testStep_implementation_for_check_with_single_input() {
        ValidateRequest validateRequest = getValidateRequestCargoLeakingFailure();

        ValidationResponse validationResponse = ciseValidationService.validate(validateRequest);
        TAR resultReport = validationResponse.getReport();
        assertThat(resultReport.getResult()).isEqualTo(TestResultType.FAILURE);
        AnyContent testStepReport = resultReport.getContext().getItem().get(0);
        assertThat(testStepReport.getName()).isEqualTo(CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF.getUiCheckName());
    }

    @Test
    public void it_instantiate_the_correct_testStep_implementation_for_check_with_two_inputs() {
        ValidateRequest validateRequest = getValidateRequestForVesselIMONumber();

        ValidationResponse validationResponse = ciseValidationService.validate(validateRequest);
        TAR resultReport = validationResponse.getReport();
        assertThat(resultReport.getResult()).isEqualTo(TestResultType.FAILURE);
        AnyContent testStepReport = resultReport.getContext().getItem().get(0);
        assertThat(testStepReport.getName()).isEqualTo(CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_47_CHECK_VESSEL_IMO_NUMBER.getUiCheckName());
    }


    private ValidateRequest getValidateRequestCargoLeakingFailure() {
        ValidateRequest validateRequest = new ValidateRequest();
        validateRequest.setSessionId("session-id");
        List<AnyContent> input = validateRequest.getInput();
        AnyContent type = new AnyContent();
        type.setName("type");
        type.setValue(CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF.getUiCheckName());
        type.setType("string");
        type.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        input.add(type);
        AnyContent content = new AnyContent();
        content.setName("content");
        content.setValue(utils.readFile("messages/push_Step19_ModelCargoLeaking_failure.xml"));
        content.setType("string");
        content.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        input.add(content);
        return validateRequest;
    }

    private ValidateRequest getValidateRequestForVesselIMONumber() {
        ValidateRequest validateRequest = new ValidateRequest();
        validateRequest.setSessionId("session-id");
        List<AnyContent> input = validateRequest.getInput();
        AnyContent type = new AnyContent();
        type.setName("type");
        type.setValue(CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_47_CHECK_VESSEL_IMO_NUMBER.getUiCheckName());
        type.setType("string");
        type.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        input.add(type);
        AnyContent content = new AnyContent();
        content.setName("content");
        content.setValue(utils.readFile("messages/pullResponse_step47_checkVesselIMONumber_failure.xml"));
        content.setType("string");
        content.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        input.add(content);
        AnyContent requestContent = new AnyContent();
        requestContent.setName("requestContent");
        requestContent.setValue("7710525");
        requestContent.setType("string");
        requestContent.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        input.add(requestContent);
        return validateRequest;
    }

}
