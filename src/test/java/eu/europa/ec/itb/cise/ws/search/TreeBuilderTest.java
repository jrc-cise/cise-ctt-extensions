package eu.europa.ec.itb.cise.ws.search;

import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.CargoType;
import eu.cise.datamodel.v1.entity.incident.Incident;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TreeBuilderTest {

    private Vessel vessel;
    private TreeBuilder builder;
    private Objet.InvolvedEventRel involvedEventRel;
    private MaritimeSafetyIncident incidentType;
    private Cargo cargo;
    private Vehicle.CargoRel cargoRel;

    @BeforeEach
    public void before() {
        vessel = new Vessel();
        involvedEventRel = new Objet.InvolvedEventRel();
        incidentType = new MaritimeSafetyIncident();
        involvedEventRel.setEvent(incidentType);
        vessel.getInvolvedEventRels().add(involvedEventRel);

        cargo = new Cargo();
        cargo.setCargoType(CargoType.LARGE_FREIGHT_CONTAINERS);
        cargoRel = new Vehicle.CargoRel();
        cargoRel.setCargo(cargo);
        vessel.setCargoRel(cargoRel);

        builder = new TreeBuilder();
    }

    @Test
    public void it_should_return_a_node_from_a_single_variable_of_type_relation() {
        assertThat(builder.extractSingleRelObjects(vessel), hasItem(cargoRel));
    }

    @Test
    public void it_should_return_a_node_with_name_from_an_entihasItemty() {
        assertThat(builder.build(vessel).getName(), is("Vessel"));
    }

    @Test
    public void it_should_return_a_node_with_name_from_an_relationship() {
        assertThat(builder.build(involvedEventRel).getName(), is("InvolvedEventRel"));
    }

    @Test
    public void it_should_return_a_not_empty_list_of_objects_of_type_relationship() {
        assertThat(builder.extractListRelFieldObjects(vessel), hasSize(1));
        assertThat(builder.extractListRelFieldObjects(vessel), hasItem(involvedEventRel));
    }

    @Test
    public void it_should_return_a_node_with_2_child() {
        assertThat(builder.build(vessel).getChildren(), hasSize(2));
    }

    @Test
    public void it_should_return_a_not_empty_list_of_objects_of_type_entity() {
        assertThat(builder.extractEntityFieldObjects(involvedEventRel), hasSize(1));
        assertThat(builder.extractEntityFieldObjects(involvedEventRel), hasItem(incidentType));
    }

    @Test
    public void it_should_return_a_node_with_one_level_of_children() {
        assertThat(builder.build(involvedEventRel).getChildren(), hasSize(1));
    }

    @Test
    public void it_should_create_a_computed_name_holding_the_tree_path() {
        assertThat(builder.build(vessel).getChildren().get(0).getChildren().get(0).getComputedName(), is("Vessel.InvolvedEventRel.MaritimeSafetyIncident"));
    }

    @Test
    public void it_should_return_a_node_with_two_levels_of_children() {
        assertThat(builder.build(vessel).getChildren(), hasSize(2));
        assertThat(builder.build(vessel).getChildren().get(0).getChildren(), hasSize(1));
    }

    @Test
    public void it_should_create_a_multiple_node_for_multiple_relationship_of_the_same_type() {
        Objet.InvolvedEventRel involvedEventRel2= new Objet.InvolvedEventRel();
        Incident incidentType2= new Incident();
        involvedEventRel2.setEvent(incidentType2);
        vessel.getInvolvedEventRels().add(involvedEventRel2);
        vessel.getInvolvedEventRels().add(new Objet.InvolvedEventRel());

        System.out.println(vessel.getInvolvedEventRels().size());
        assertThat(builder.build(vessel).getChildren(), hasSize(4));
    }
}