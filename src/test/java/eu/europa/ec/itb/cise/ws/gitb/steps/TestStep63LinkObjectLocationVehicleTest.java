package eu.europa.ec.itb.cise.ws.gitb.steps;


import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep63LinkObjectLocationVehicle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep63LinkObjectLocationVehicleTest extends CiseTestStepTest {

    private TestStep63LinkObjectLocationVehicle testStep63LinkObjectLocationVehicle;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep63LinkObjectLocationVehicle = new TestStep63LinkObjectLocationVehicle(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_63_LINK_OBJECT_LOCATION_VEHICLE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_LinkObjectLocationCargoUnit_validation() {
        TAR report = testStep63LinkObjectLocationVehicle.createReport(utils.readFile("messages/push_Step63_LinkObjectLocationVehicle_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_empty_LinkObjectLocationVehicle_validation() {
        TAR report = testStep63LinkObjectLocationVehicle.createReport(utils.readFile("messages/push_Step63_LinkObjectLocationVehicle_VehicleWithNoLocation_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_LinkObjectLocationVehicle_validation() {
        TAR report = testStep63LinkObjectLocationVehicle.createReport(utils.readFile("messages/push_Step63_LinkObjectLocationVehicle_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}