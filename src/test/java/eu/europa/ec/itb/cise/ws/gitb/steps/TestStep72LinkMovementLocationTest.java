package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.event.EventAreaType;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep72LinkMovementLocation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #72 LinkMovementLocation
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*SCENARIO*` it must not provide *attribute* `EventArea` to *relation(s)* between `Movement` and `Location`
 * `*GIVEN*` relation(s) to `Movement` *entity(ies)* from *entity* of type `Location`is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *attribute* `EventArea` _must not be defined_
 * <p>
 * examples:
 */
public class TestStep72LinkMovementLocationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step72_LinkMovementLocation";
    private TestStep72LinkMovementLocation testStep72LinkMovementLocation;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep72LinkMovementLocation = new TestStep72LinkMovementLocation(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_72_LINK_MOVEMENT_LOCATION.getUiCheckName();
        payloadCreator_LinkMovement_failure();
        payloadCreator_LinkMovement_success();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep72LinkMovementLocation.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep72LinkMovementLocation.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_LinkMovement_success() {
        Movement movement = new Movement();
        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setPortName("ANTERP");
        Event.LocationRel locationRel = new Event.LocationRel();
        locationRel.setLocation(portLocation);
        movement.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Movement) movement);
    }

    public void payloadCreator_LinkMovement_failure() {
        Movement movement = new Movement();
        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setPortName("ANTERP");
        Event.LocationRel locationRel = new Event.LocationRel();
        locationRel.setLocation(portLocation);
        movement.getLocationRels().add(locationRel);
        // failure point
        locationRel.setEventArea(EventAreaType.AIR);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Movement) movement);
    }
}