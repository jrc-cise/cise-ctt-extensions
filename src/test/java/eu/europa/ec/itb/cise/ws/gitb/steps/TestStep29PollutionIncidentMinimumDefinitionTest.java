package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep29PollutionIncidentMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep29PollutionIncidentMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep29PollutionIncidentMinimumDefinition testStep29PollutionIncidentMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep29PollutionIncidentMinimumDefinition = new TestStep29PollutionIncidentMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_29_POLLUTION_INCIDENT_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep29PollutionIncidentMinimumDefinition.createReport(utils.readFile("messages/push_Step29_PollutionIncidentMinimumDefinition_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_backward_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep29PollutionIncidentMinimumDefinition.createReport(utils.readFile("messages/push_Step29_PollutionIncidentMinimumDefinition_backward_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep29PollutionIncidentMinimumDefinition.createReport(utils.readFile("messages/push_Step29_PollutionIncidentMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_empty_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep29PollutionIncidentMinimumDefinition.createReport(utils.readFile("messages/push_Step29_PollutionIncidentMinimumDefinition_backward_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}
