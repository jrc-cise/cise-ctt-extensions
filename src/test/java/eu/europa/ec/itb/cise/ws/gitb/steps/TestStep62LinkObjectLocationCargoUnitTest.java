package eu.europa.ec.itb.cise.ws.gitb.steps;


import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep62LinkObjectLocationCargoUnit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep62LinkObjectLocationCargoUnitTest extends CiseTestStepTest {

    private TestStep62LinkObjectLocationCargoUnit testStep62LinkObjectLocationCargoUnit;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep62LinkObjectLocationCargoUnit = new TestStep62LinkObjectLocationCargoUnit(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_62_LINK_OBJECT_LOCATION_CARGOUNIT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_LinkObjectLocationCargoUnit_validation() {
        TAR report = testStep62LinkObjectLocationCargoUnit.createReport(utils.readFile("messages/push_Step62_LinkObjectLocationCargoUnit_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_empty_LinkObjectLocationCargoUnit_validation() {
        TAR report = testStep62LinkObjectLocationCargoUnit.createReport(utils.readFile("messages/push_Step62_LinkObjectLocationCargoUnit_CargoUnitWithNoLocation_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_LinkObjectLocationCargoUnit_validation() {
        TAR report = testStep62LinkObjectLocationCargoUnit.createReport(utils.readFile("messages/push_Step62_LinkObjectLocationCargoUnit_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}