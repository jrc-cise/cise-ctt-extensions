package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #77 FORMAT Vessel ImoNumber Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` attribute `ImoNumber` must follow a predefined format.
 */
public class TestStep77FormatVesselImonumberTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step77_FormatVesselImonumber";
    private TestStep77FormatVesselImonumber testStep77FormatVesselImonumber;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep77FormatVesselImonumber = new TestStep77FormatVesselImonumber(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_77_FORMAT_VESSEL_IMONUMBER.getUiCheckName();
        payloadCreator_FormatVesselImoNumber_failure();
        payloadCreator_FormatVesselImoNumber_success();
        payloadCreator_FormatVesselImoNumber_failureMore7();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep77FormatVesselImonumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep77FormatVesselImonumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure__More7_report_with_correct_validation() {
        TAR report = testStep77FormatVesselImonumber.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure_More7.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_FormatVesselImoNumber_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setIMONumber(Long.parseLong("1111117"));// success factor 7+6+5+4+3+2= 27 -> 7 as last char
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselImoNumber_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setIMONumber(Long.parseLong("1111118"));// failure factor 7+6+5+4+3+2= 27 -> 7 as last char
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselImoNumber_failureMore7() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setIMONumber(Long.parseLong("111111899"));// failure factor !=7
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure_More7.xml", xmlMapper, vessel);
    }
}