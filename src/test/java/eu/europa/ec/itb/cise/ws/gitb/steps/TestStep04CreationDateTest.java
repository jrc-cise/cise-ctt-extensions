package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;

public class TestStep04CreationDateTest extends CiseTestStepTest {



    private TestStep04CreationDate testStep04CreationDate;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep04CreationDate = new TestStep04CreationDate(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_04_CREATION_DATE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_creationDate() {
        String messageXml = xmlMapper.toXML(utils.getPush());
        TAR report = testStep04CreationDate.createReport(messageXml);

        String creationDateReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(creationDateReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_not_valid_creationDate_in_the_past() {

        GregorianCalendar wrongCreationDateGC = new GregorianCalendar();
        wrongCreationDateGC.setTimeInMillis(Instant.now().minus(3, ChronoUnit.HOURS).minus(1, ChronoUnit.SECONDS).toEpochMilli());
        XMLGregorianCalendar wrongCreationDate = null;
        try {
            wrongCreationDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(wrongCreationDateGC);
        } catch (DatatypeConfigurationException e) {
            fail("Creation date setting impossible.");
        }
        Push push = utils.getPush();
        push.setCreationDateTime(wrongCreationDate);
        String messageXml = xmlMapper.toXML(push);
        TAR report = testStep04CreationDate.createReport(messageXml);

        String creationDateReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(creationDateReport).isNotEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_simple_content_for_not_valid_creationDate_in_the_future() {

        GregorianCalendar wrongCreationDateGC = new GregorianCalendar();
        wrongCreationDateGC.setTimeInMillis(Instant.now().plus(1, ChronoUnit.SECONDS).toEpochMilli());
        XMLGregorianCalendar wrongCreationDate = null;
        try {
            wrongCreationDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(wrongCreationDateGC);
        } catch (DatatypeConfigurationException e) {
            fail("Creation date setting impossible.");
        }
        Push push = utils.getPush();
        push.setCreationDateTime(wrongCreationDate);
        String messageXml = xmlMapper.toXML(push);
        TAR report = testStep04CreationDate.createReport(messageXml);

        String creationDateReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(creationDateReport).isNotEqualTo("VALID");
    }
}
