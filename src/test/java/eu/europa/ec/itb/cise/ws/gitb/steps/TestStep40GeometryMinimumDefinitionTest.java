package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep40GeometryMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep40GeometryMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep40GeometryMinimumDefinition testStep40GeometryMinimumDefinition;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep40GeometryMinimumDefinition = new TestStep40GeometryMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_40_GEOMETRY_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_for_correctly_defined_Location_Geometry() {
        String messageXml = utils.readFile("messages/push_Step40_GeometryMinimumDefinition_success.xml");

        TAR report = testStep40GeometryMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_for_no_Geometry_defined_in_Location() {
        String messageXml = utils.readFile("messages/push_Step40_GeometryMinimumDefinition_no_Geometry_success.xml");

        TAR report = testStep40GeometryMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_for_Location_with_no_WKT() {
        String messageXml = utils.readFile("messages/push_Step40_GeometryMinimumDefinition_failure.xml");

        TAR report = testStep40GeometryMinimumDefinition.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }
}