package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;

import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #78 FORMAT Vessel MMSI Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*`  *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` attribute `MMSI` must follow a predefined format.
 */
public class TestStep78FormatVesselMmsiTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step78_FormatVesselMmsi";
    private TestStep78FormatVesselMmsi testStep78FormatVesselMmsi;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep78FormatVesselMmsi = new TestStep78FormatVesselMmsi(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_78_FORMAT_VESSEL_MMSI.getUiCheckName();
        payloadCreator_FormatVesselMmsi_failure();
        payloadCreator_FormatVesselMmsi_success();
        payloadCreator_FormatVesselMmsi_failure_NotInEnum();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep78FormatVesselMmsi.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep78FormatVesselMmsi.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_NotInEnum_report_with_correct_validation() {
        TAR report = testStep78FormatVesselMmsi.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure_NotInEnum.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_FormatVesselMmsi_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setMMSI(Long.parseLong("229111700"));// success factor malte 229 reference as start Mid code
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselMmsi_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setMMSI(Long.parseLong("38011800"));// failure factor lenght should be 9 char
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }


    public void payloadCreator_FormatVesselMmsi_failure_NotInEnum() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setMMSI(Long.parseLong("380111899"));// failure factor 380 not a reference as in Mid code enum
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure_NotInEnum.xml", xmlMapper, vessel);
    }
}