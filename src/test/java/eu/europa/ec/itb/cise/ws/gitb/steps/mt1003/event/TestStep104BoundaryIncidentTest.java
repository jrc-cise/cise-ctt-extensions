package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.event;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.incident.*;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep104BoundaryIncidentTest extends CiseTestStepTestChildValueBoundaries {
    private TestStep104BoundaryIncident testStep104BoundaryIncident;
    private final String testFilePrefix = "messages/push_Step104_BoundaryIncident";
    private final String propertyFile = "src/test/java/eu/europa/ec/itb/cise/ws/gitb/steps/mt1003/event/Event.properties";

    private static final String[] FIELDS = new String[]{
            "deathsOnBoard",
            "numberOfIIllPersons"
    };

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep104BoundaryIncident = new TestStep104BoundaryIncident(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_104_BOUNDARY_INCIDENT.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Event");
        if (reinitCondition)
            payload_PropertyBoundary_maker(Catch.class, propertyFile); //used to create / update properties list
        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }
    }

    @Override
    protected Vessel getTestedEntity(String[] fields, boolean expectedDefault) {
        Vessel vessel = new Vessel();

        Incident incident1 = (MaritimeSafetyIncident) PropertyMaker.populateAttributes(errorBoundaries, MaritimeSafetyIncident.class, fields, expectedDefault);
        Objet.InvolvedEventRel eventrel1 = new Objet.InvolvedEventRel();
        eventrel1.setEvent(incident1);

        Incident incident2 = (IrregularMigrationIncident) PropertyMaker.populateAttributes(errorBoundaries, IrregularMigrationIncident.class, fields, expectedDefault);
        Objet.InvolvedEventRel eventrel2 = new Objet.InvolvedEventRel();
        eventrel2.setEvent(incident2);

        Incident incident3 = (CrisisIncident) PropertyMaker.populateAttributes(errorBoundaries, CrisisIncident.class, fields, expectedDefault);
        Objet.InvolvedEventRel eventrel3 = new Objet.InvolvedEventRel();
        eventrel3.setEvent(incident3);

        Incident incident4 = (LawInfringementIncident) PropertyMaker.populateAttributes(errorBoundaries, LawInfringementIncident.class, fields, expectedDefault);
        Objet.InvolvedEventRel eventrel4 = new Objet.InvolvedEventRel();
        eventrel4.setEvent(incident4);

        vessel.getInvolvedEventRels().add(eventrel1);
        vessel.getInvolvedEventRels().add(eventrel2);
        vessel.getInvolvedEventRels().add(eventrel3);
        vessel.getInvolvedEventRels().add(eventrel4);
        return vessel;
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep104BoundaryIncident.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep104BoundaryIncident.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts104BoundaryIncidentTest");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }


}