package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.action.Action;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep57ActionMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep57ActionMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step57_ActionMinimumDefinition";

    private TestStep57ActionMinimumDefinition testStep57ActionMinimumDefinition;


    @BeforeEach
    public void setUp() {

        super.setUp();
        // https://tools.ietf.org/html/rfc3986#section-1.1.1 for formal alternative of uri ipv6 url urn
        payloadCreator_ActionMinimumDefinition_failure();
        payloadCreator_ActionMinimumDefinition_success();
        payloadCreator_ActionMinimumDefinition_Anomaly_success();
        testStep57ActionMinimumDefinition = new TestStep57ActionMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_57_ACTION_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_ActionMinimumDefinition_validation() {
        TAR report = testStep57ActionMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_ActionMinimumDefinition_Anomaly_validation() {
        TAR report = testStep57ActionMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_Anomaly_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_ActionMinimumDefinition_validation() {
        TAR report = testStep57ActionMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_ActionMinimumDefinition_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Action action = new Action();
        impliedEventRel.setEvent(action);
        // success factor
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        action.getInvolvedObjectRels().add(0, involvedObjectRel);
        Vessel objet = new Vessel();
        involvedObjectRel.setObject(objet);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_ActionMinimumDefinition_Anomaly_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Action action = new Action();
        impliedEventRel.setEvent(action);
        // success factor
        Event.InvolvedWithRel involvedObjectRel = new Event.InvolvedWithRel();
        action.getInvolvedWithRels().add(0, involvedObjectRel);
        Anomaly anomaly = new Anomaly();
        involvedObjectRel.setEvent(anomaly);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_Anomaly_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_ActionMinimumDefinition_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Action action = new Action();
        impliedEventRel.setEvent(action);
        // success factor absent = failure
        //        Event.InvolvedObjectRel involvedObjectRel= new Event.InvolvedObjectRel();
        //        action.getInvolvedObjectRels().add(0,involvedObjectRel);
        //        Vessel objet= new Vessel();
        //        involvedObjectRel.setObject(objet);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

}
