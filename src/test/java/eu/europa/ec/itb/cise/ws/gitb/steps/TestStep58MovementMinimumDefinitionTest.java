package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.movement.Movement;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep58MovementMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep58MovementMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step58_MovementMinimumDefinition";

    private TestStep58MovementMinimumDefinition testStep58MovementMinimumDefinition;


    @BeforeEach
    public void setUp() {

        super.setUp();
        payloadCreator_MovementMinimumDefinition_failure();
        payloadCreator_MovementMinimumDefinition_success();
        testStep58MovementMinimumDefinition = new TestStep58MovementMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_58_MOVEMENT_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_MovementMinimumDefinition_validation() {
        TAR report = testStep58MovementMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_MovementMinimumDefinition_NoObject_validation() {
        TAR report = testStep58MovementMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_NoObject_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_MovementMinimumDefinition_NoLocation_validation() {
        TAR report = testStep58MovementMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_NoLocation_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_MovementMinimumDefinition_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Movement movement = new Movement();
        impliedEventRel.setEvent(movement);
        // success factor
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        movement.getInvolvedObjectRels().add(0, involvedObjectRel);
        Vessel objet = new Vessel();
        involvedObjectRel.setObject(objet);
        Event.LocationRel locationRel = new Event.LocationRel();
        locationRel.setLocation(new Location());
        movement.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_MovementMinimumDefinition_failure() {
        payloadCreator_MovementMinimumDefinition_nolocation_failure();
        payloadCreator_MovementMinimumDefinition_noobject_failure();
    }

    public void payloadCreator_MovementMinimumDefinition_nolocation_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Movement movement = new Movement();
        impliedEventRel.setEvent(movement);
        // success factor
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        movement.getInvolvedObjectRels().add(0, involvedObjectRel);
        Vessel objet = new Vessel();
        involvedObjectRel.setObject(objet);
        // less 1 success factor
//        Event.LocationRel locationRel= new Event.LocationRel();
//        locationRel.setLocation(new Location());
//        movement.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_NoLocation_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_MovementMinimumDefinition_noobject_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Risk risk = new Risk();
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        maritimeSafetyIncident.getImpliedRiskRels().add(impliedRiskRel);
        Risk.ImpliedEventRel impliedEventRel = new Risk.ImpliedEventRel();
        risk.getImpliedEventRels().add(impliedEventRel);
        Movement movement = new Movement();
        impliedEventRel.setEvent(movement);
        // success factor
        //Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        //movement.getInvolvedObjectRels().add(0, involvedObjectRel);
        //Vessel objet = new Vessel();
        //involvedObjectRel.setObject(objet);
        // less 1 success factor
        Event.LocationRel locationRel = new Event.LocationRel();
        locationRel.setLocation(new Location());
        movement.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_NoObject_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

}
