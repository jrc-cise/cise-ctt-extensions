package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep41WKTValidity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep41WKTValidityTest extends CiseTestStepTest {

    private TestStep41WKTValidity testStep41WKTValidity;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep41WKTValidity = new TestStep41WKTValidity(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_41_WKT_VALIDITY.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_for_correctly_defined_Location_with_WKT() {
        String messageXml = utils.readFile("messages/push_Step41_WKTValidity_success.xml");

        TAR report = testStep41WKTValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_for_Location_with_no_WKT() {
        String messageXml = utils.readFile("messages/push_Step41_WKTValidity_no_WKT_success.xml");

        TAR report = testStep41WKTValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_for_bad_defined_Location_with_WKT() {
        String messageXml = utils.readFile("messages/push_Step41_WKTValidity_failure.xml");

        TAR report = testStep41WKTValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_for_bad_defined_WKT_coordinate_values() {
        String messageXml = utils.readFile("messages/push_Step41_WKTValidity_bad_coordinate_values_failure.xml");

        TAR report = testStep41WKTValidity.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

}
