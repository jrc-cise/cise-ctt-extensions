package eu.europa.ec.itb.cise.ws.gitb;

import com.gitb.core.AnyContent;
import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.InformationSecurityLevelType;
import eu.cise.servicemodel.v1.message.InformationSensitivityType;
import eu.cise.servicemodel.v1.message.PriorityType;
import eu.cise.servicemodel.v1.message.PurposeType;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.transport.MessageBuilderUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Date;
import java.util.List;

import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class ReportBuilderTest {

    private final Utils utils = new Utils();
    private Acknowledgement acknowledgement;
    private Push push;

    private XmlMapper xmlMapper;
    private ReportBuilder reportBuilder;

    private SignatureService signatureService;
    private CisePayloadHelper payloadHelper;

    @BeforeEach
    public void setUp() {
        xmlMapper = new DefaultXmlMapper.NotValidating();
        payloadHelper = new CisePayloadHelper(xmlMapper);
        signatureService = mock(SignatureService.class);
        reportBuilder = new ReportBuilder(xmlMapper, signatureService, null);
        acknowledgement = MessageBuilderUtil.createAcknowledgeMessage();
        push = newPush()
                .id("push-message-id")
                .creationDateTime(new Date())
                .priority(PriorityType.LOW)
                .sender(newService().type(ServiceType.VESSEL_SERVICE).id("service-id").operation(ServiceOperationType.PUSH))
                .informationSecurityLevel(InformationSecurityLevelType.EU_CONFIDENTIAL)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .isPersonalData(false)
                .purpose(PurposeType.BORDER_OPERATION)
                .build();
        utils.setVesselPayload(push);
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void createMessageReportForReceivedMessage_creates_a_report_with_xml_message_received() {
        TAR report = reportBuilder.createMessageReportMessageAndAck(acknowledgement, push);

        String messageReport = utils.getReportFor(report, ReportBuilder.REPORT_ELEMENT_MESSAGE);

        assertThat(messageReport).isEqualTo(xmlMapper.toXML(push));
    }

    @Test
    public void createMessageReportForReceivedMessage_creates_a_report_with_xml_ack_produced() {
        TAR report = reportBuilder.createMessageReportMessageAndAck(acknowledgement, push);

        String messageReport = utils.getReportFor(report, ReportBuilder.REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT);

        assertThat(messageReport).isEqualTo(xmlMapper.toXML(acknowledgement));
    }

    @Test
    public void createMessageReportForSentMessage_creates_a_report_with_xml_message_received() {
        TAR report = reportBuilder.createSentMessageReportMessageAndAck(acknowledgement, push);

        String messageReport = utils.getReportFor(report, ReportBuilder.REPORT_ELEMENT_SENT_MESSAGE);

        assertThat(messageReport).isEqualTo(xmlMapper.toXML(push));
    }

    @Test
    public void createMessageReportForSentMessage_creates_a_report_with_xml_ack_produced() {
        TAR report = reportBuilder.createSentMessageReportMessageAndAck(acknowledgement, push);

        String messageReport = utils.getReportFor(report, ReportBuilder.REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT_FOR_SENT_MESSAGE);

        assertThat(messageReport).isEqualTo(xmlMapper.toXML(acknowledgement));
    }

    private String getReportFor(TAR report, String reportSectionName) {
        List<AnyContent> contents = report.getContext().getItem();
        for (AnyContent content : contents) {
            if (reportSectionName.equals(content.getName())) {
                return content.getValue();
            }
        }
        return null;
    }

    private void setVesselPayload(Push push) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = null;
            documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // Vessel element
            Element root = document.createElementNS("", "Vessel");
            document.appendChild(root);

            // IMONumber element
            Element imoNumber = document.createElement("IMONumber");
            Text imoNumberValue = document.createTextNode("10208");
            imoNumber.appendChild(imoNumberValue);

            root.appendChild(imoNumber);

            XmlEntityPayload xmlEntityPayload = (XmlEntityPayload) push.getPayload();
            xmlEntityPayload.getAnies().add(root);

            this.push.setPayload(xmlEntityPayload);
        } catch (ParserConfigurationException e) {

        }
    }


    private void setWrongVesselPayload(Push push) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = null;
            documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // Vessel element
            Element root = document.createElementNS("", "Vessel");
            document.appendChild(root);

            XmlEntityPayload xmlEntityPayload = (XmlEntityPayload) push.getPayload();
            xmlEntityPayload.getAnies().clear();
            xmlEntityPayload.getAnies().add(root);

            this.push.setPayload(xmlEntityPayload);
        } catch (ParserConfigurationException e) {

        }
    }

}
