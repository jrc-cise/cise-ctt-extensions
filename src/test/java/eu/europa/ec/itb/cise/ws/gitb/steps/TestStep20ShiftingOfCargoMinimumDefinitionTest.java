package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep20ShiftingOfCargoMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep20ShiftingOfCargoMinimumDefinitionTest extends CiseTestStepTest {


    private static final String TEST_FILE_PREFIX = "messages/push_Step20_ShiftingOfCargoMinimumDefinition";
    private TestStep20ShiftingOfCargoMinimumDefinition testStep20ShiftingOfCargoMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_ShiftingOfCargoMinimumDefinition_failure();
        payloadCreator_ShiftingOfCargoMinimumDefinition_success();
        testStep20ShiftingOfCargoMinimumDefinition = new TestStep20ShiftingOfCargoMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_20_SHIFTING_OF_CARGO_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_StainOfOilSightedMinimumDefinition_validation() {
        TAR report = testStep20ShiftingOfCargoMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_StainOfOilSightedMinimumDefinition_validation() {
        TAR report = testStep20ShiftingOfCargoMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_ShiftingOfCargoMinimumDefinition_failure() {
        // GIVEN payload include entity Anomaly
        Anomaly anomalyMain = new Anomaly();
        // WHEN condition
        Anomaly anomalySecondary = new Anomaly();
        anomalyMain.setAnomalyType(AnomalyType.SHIFTING_OF_CARGO);
        // THEN condition
        Location location = new Location();
        Event.LocationRel locationRel = new Event.LocationRel();
        locationRel.setLocation(location);
        anomalyMain.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, anomalyMain);
    }

    public void payloadCreator_ShiftingOfCargoMinimumDefinition_success() {
        // GIVEN payload include entity Anomaly
        Anomaly anomalyMain = new Anomaly();
        // WHEN condition
        Anomaly anomalySecondary = new Anomaly();
        anomalySecondary.setAnomalyType(AnomalyType.SHIFTING_OF_CARGO);
        // UNMEET THEN condition
        /* Location location= new Location();
        Event.LocationRel locationRel= new Event.LocationRel();
        locationRel.setLocation(location);
        anomalySecondary.getLocationRels().add(locationRel);*/

        // Technical Condition for a in deep visit of the payload
        // 2d Level Deep
        Vehicle vehicle = (Vehicle) new Vessel();
        Anomaly.InvolvedObjectRel involvedObjectRel = new Anomaly.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        anomalyMain.getInvolvedObjectRels().add(involvedObjectRel);
        // 3d Level Deep
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(anomalySecondary);
        vehicle.getInvolvedEventRels().add(involvedEventRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, anomalyMain);
    }
}
