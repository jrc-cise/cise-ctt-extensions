package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.location.PortFacilityLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep45PortFacilityLocationMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep45PortFacilityLocationMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step45_PortFacilityLocationIdentification";
    private TestStep45PortFacilityLocationMinimumDefinition testStep45PortFacilityLocationMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_PortFacilityLocationMinimumDefinition_failure();
        payloadCreator_PortFacilityLocationMinimumDefinition_success();
        testStep45PortFacilityLocationMinimumDefinition = new TestStep45PortFacilityLocationMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_45_PORT_FACILITY_LOCATION_MIN_DEF.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_PortFacilityName_validation() {
        TAR report = testStep45PortFacilityLocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_PortFacilityName.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_PortFacilityNumber_validation() {
        TAR report = testStep45PortFacilityLocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_PortFacilityNumber.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep45PortFacilityLocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_PortFacilityLocationMinimumDefinition_failure() {
        Cargo cargo = new Cargo();
        // GIVEN a portFacilityLocation entity
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        // THEN a portFacilityLocation entity as one of child node value :  PortFacilityName | PortFacilityNumber
        // portFacilityLocation.setPortFacilityName("John Doe & Co");
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_failure.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PortFacilityLocationMinimumDefinition_success() {
        payloadCreator_PortFacilityLocationMinimumDefinition_PortFacilityName_success();
        payloadCreator_PortFacilityLocationMinimumDefinition_PortFacilityNumber_success();
    }

    public void payloadCreator_PortFacilityLocationMinimumDefinition_PortFacilityName_success() {
        Cargo cargo = new Cargo();
        // GIVEN a portFacilityLocation entity
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        // THEN a portFacilityLocation entity as one of child node value :  PortFacilityName | PortFacilityNumber
        portFacilityLocation.setPortFacilityName("John Doe & Co");
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_PortFacilityName.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PortFacilityLocationMinimumDefinition_PortFacilityNumber_success() {
        Cargo cargo = new Cargo();
        // GIVEN a portFacilityLocation entity
        PortFacilityLocation portFacilityLocation = new PortFacilityLocation();
        // THEN a portFacilityLocation entity as one of child node value :  PortFacilityName | PortFacilityNumber
        portFacilityLocation.setPortFacilityNumber("2344324");
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(portFacilityLocation);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_PortFacilityLocationMinimumDefinition_PortFacilityNumber.xml", xmlMapper, (Cargo) cargo);
    }


}
