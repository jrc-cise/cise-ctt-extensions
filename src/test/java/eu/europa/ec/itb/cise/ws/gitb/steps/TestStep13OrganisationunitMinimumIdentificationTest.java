package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.OrganizationalUnit;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep13OrganisationunitMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep13OrganisationunitMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step13_OrganisationunitMinimumIdentification";

    private TestStep13OrganisationunitMinimumIdentification testStep13OrganisationunitMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_OrganisationunitMinimumIdentification_failure();
        payloadCreator_OrganisationunitMinimumIdentification_success();
        testStep13OrganisationunitMinimumIdentification = new TestStep13OrganisationunitMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_13_ORGANISATIONUNIT_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_OrganizationUnitMinimumDefinition_validation() {
        TAR report = testStep13OrganisationunitMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_OrganizationUnitMinimumDefinition_validation() {
        TAR report = testStep13OrganisationunitMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_OrganisationunitMinimumIdentification_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(organizationalUnit);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_OrganisationunitMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        //organization.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(organizationalUnit);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
