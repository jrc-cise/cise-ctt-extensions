package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;

import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #79 FORMAT Vessel NAME Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `NAME` is defined
 * `*THEN*` attribute `NAME` must follow a predefined format.
 */
public class TestStep79FormatVesselNameTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step79_FormatVesselName";
    private TestStep79FormatVesselName testStep79FormatVesselName;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep79FormatVesselName = new TestStep79FormatVesselName(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_79_FORMAT_VESSEL_NAME.getUiCheckName();
        payloadCreator_FormatVesselCallsign_failure();
        payloadCreator_FormatVesselCallsign_success();
        payloadCreator_FormatVesselCallsign_failure_More35();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep79FormatVesselName.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep79FormatVesselName.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure__More35_report_with_correct_validation() {
        TAR report = testStep79FormatVesselName.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure_More35.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_FormatVesselCallsign_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.getNames().add("LAUST.MAERSK-potter-'odisey'");// success factor no space -.' sign and letters
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselCallsign_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.getNames().add("SEASPAN RIO_DE_JANEIRO");// failure factor  space + extra signs
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselCallsign_failure_More35() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.getNames().add("0123456789012345678901234567890123456789");// failure factor 40>35
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure_More35.xml", xmlMapper, vessel);
    }
}