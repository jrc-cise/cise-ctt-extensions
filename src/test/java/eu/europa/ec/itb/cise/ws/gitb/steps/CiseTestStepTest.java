package eu.europa.ec.itb.cise.ws.gitb.steps;

import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;

import static org.mockito.Mockito.mock;

public abstract class CiseTestStepTest {

    protected CisePayloadHelper payloadHelper;
    protected ReportBuilder reportBuilder;
    protected XmlMapper xmlMapper;
    protected SignatureService signatureService;
    protected String testStepRefUiCheckName;
    protected GitbErrorHelper errorHelper;
    protected final Utils utils = new Utils();

    public void setUp() {
        xmlMapper = new DefaultXmlMapper.PrettyNotValidating();
        signatureService = mock(SignatureService.class);
        payloadHelper = new CisePayloadHelper(xmlMapper);
        errorHelper = new GitbErrorHelper(utils.labels, utils.boundaries, xmlMapper);
        reportBuilder = new ReportBuilder(xmlMapper, signatureService, null);
    }
}
