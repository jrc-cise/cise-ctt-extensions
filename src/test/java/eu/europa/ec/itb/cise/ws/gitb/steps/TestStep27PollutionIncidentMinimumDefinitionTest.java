package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.incident.PollutionIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep27PollutionIncidentMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep27PollutionIncidentMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step27_PollutionIncidentMinimumDefinition";

    private TestStep27PollutionIncidentMinimumDefinition testStep27PollutionIncidentMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_PollutionIncidentMinimumDefinition_failure();
        payloadCreator_PollutionIncidentMinimumDefinition_success();
        testStep27PollutionIncidentMinimumDefinition = new TestStep27PollutionIncidentMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_27_POLLUTION_INCIDENT_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep27PollutionIncidentMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PollutionIncidentMinimumDefinition_validation() {
        TAR report = testStep27PollutionIncidentMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_PollutionIncidentMinimumDefinition_success() {
        PollutionIncident pollutionIncident = new PollutionIncident();
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        Vehicle vehicle = (Vehicle) new Vessel();
        PollutionIncident pollutionIncident2 = new PollutionIncident();
        // here should have a link to an object reward
        Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
        involvedEventRel.setEvent(pollutionIncident2);
        vehicle.getInvolvedEventRels().add(involvedEventRel);
        // finally add vessel to first maritime incident (to link to vessel)
        PollutionIncident.InvolvedObjectRel involvedObjectRel = new PollutionIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        //mount up everything
        pollutionIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(pollutionIncident));
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (MaritimeSafetyIncident) pollutionIncident);
    }

    public void payloadCreator_PollutionIncidentMinimumDefinition_failure() {
        PollutionIncident pollutionIncident = new PollutionIncident();
        System.out.println(xmlMapper.toXML(pollutionIncident));
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (MaritimeSafetyIncident) pollutionIncident);
    }


}
