package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep47CheckVesselIMONumberTest extends CiseTestStepTest {

    private TestStep47CheckVesselIMONumber testStep47CheckVesselIMONumber;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep47CheckVesselIMONumber = new TestStep47CheckVesselIMONumber(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_47_CHECK_VESSEL_IMO_NUMBER.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_report_with_simple_content_for_valid_message() {
        TAR report = testStep47CheckVesselIMONumber.createReport(utils.readFile("messages/pullResponse_step47_checkVesselIMONumber_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep47CheckVesselIMONumber.createReport(utils.readFile("messages/pullResponse_step47_checkVesselIMONumber_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep47CheckVesselIMONumber.createReport(utils.readFile("messages/pullResponse_step47_checkVesselIMONumber_noVessel.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_no_IMONumber() {
        TAR report = testStep47CheckVesselIMONumber.createReport(utils.readFile("messages/pullResponse_step47_checkVesselIMONumber_noIMONumber.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}
