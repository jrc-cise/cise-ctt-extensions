package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep39LocationMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep39LocationMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step39_LocationMinimumDefinition";
    private TestStep39LocationMinimumDefinition testStep39LocationMinimumDefinition;

    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_LocationMinimumDefinition_failure();
        payloadCreator_LocationMinimumDefinition_success();
        testStep39LocationMinimumDefinition = new TestStep39LocationMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_39_LOCATION_MIN_DEF.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep39LocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep39LocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_LocationMinimumDefinition_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Location location = new Location();
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);
        Geometry geometry = new Geometry();
        // success factor
        location.getGeometries().add(geometry);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_LocationMinimumDefinition_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Location location = new Location();
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);
        // unsuccess factor
        // Geometry geometry =new Geometry();
        // location.getGeometries().add(geometry);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
