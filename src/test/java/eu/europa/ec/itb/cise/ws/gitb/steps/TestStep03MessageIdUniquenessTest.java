package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestStep03MessageIdUniquenessTest extends CiseTestStepTest {



    private TestStep03MessageIdUniqueness messageIdUniquenessTest;
    private MessageIdStore messageIdStore;

    @BeforeEach
    public void setUp() {
        super.setUp();
        messageIdStore = mock(MessageIdStore.class);
        reportBuilder = new ReportBuilder(xmlMapper, signatureService, messageIdStore);
        messageIdUniquenessTest = new TestStep03MessageIdUniqueness(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_03_MESSAGEID_UNIQUENESS.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void createReportForMessageIdUniqueness_creates_a_report_with_simple_content_for_successful_messageId_uniqueness() {
        String messageXml = xmlMapper.toXML(utils.getPush());
        TAR report = messageIdUniquenessTest.createReport(messageXml);

        String messageIdReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageIdReport).isEqualTo("VALID");
    }

    @Test
    public void createReportForMessageIdUniqueness_creates_a_report_with_simple_content_for_unsuccessful_messageId_uniqueness() {
        Push push = utils.getPush();
        String messageXml = xmlMapper.toXML(push);
        when(messageIdStore.exists(push.getMessageID())).thenReturn(true);
        TAR report = messageIdUniquenessTest.createReport(messageXml);

        String messageIdReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageIdReport).isNotEqualTo("VALID");
    }
}
