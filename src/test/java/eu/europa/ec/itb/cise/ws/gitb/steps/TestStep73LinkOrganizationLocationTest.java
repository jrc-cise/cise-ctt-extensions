package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInLocationType;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep73LinkOrganizationLocation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #73 LinkMovementLocation
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*SCENARIO*` it must not provide *attribute* `EventArea` to *relation(s)* between `Organization (+ child entities)`  and `Location`
 * `*GIVEN*` relation(s) to `Location` *entity(ies)* from *entity* of type `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *attribute* `AgentRole` _must not be one of _
 * `CountryOfBirth`,
 * `PlaceOfBirth`,
 * `CountryOfDeath`,
 * `EmbarkationPort`,
 * `DisembarkationPort`,
 * `CountryOfResidence`
 * <p>
 * examples:
 */
public class TestStep73LinkOrganizationLocationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step73_LinkOrganizationLocation";
    private TestStep73LinkOrganizationLocation testStep73LinkOrganizationLocation;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep73LinkOrganizationLocation = new TestStep73LinkOrganizationLocation(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_73_LINK_ORGANIZATION_LOCATION.getUiCheckName();
        payloadCreator_LinkOrganization_failure();
        payloadCreator_LinkOrganization_success();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep73LinkOrganizationLocation.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep73LinkOrganizationLocation.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_LinkOrganization_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setPortName("ANTERP");
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portLocation);
        organization.getLocationRels().add(locationRel);
        // success factor
        locationRel.setAgentRole(AgentRoleInLocationType.IS_LOCATED_IN);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Vessel) vessel);
    }

    public void payloadCreator_LinkOrganization_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();

        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);

        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setPortName("ANTERP");
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portLocation);
        organization.getLocationRels().add(locationRel);
        // failure factor
        locationRel.setAgentRole(AgentRoleInLocationType.COUNTRY_OF_BIRTH);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Vessel) vessel);
    }
}