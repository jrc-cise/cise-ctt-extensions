package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep48CheckVesselMMSITest extends CiseTestStepTest {

    private TestStep48CheckVesselMMSI testStep48CheckVesselMMSI;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep48CheckVesselMMSI = new TestStep48CheckVesselMMSI(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_48_CHECK_VESSEL_MMSI.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_message() {
        TAR report = testStep48CheckVesselMMSI.createReport(utils.readFile("messages/pullResponse_step48_checkVesselMMSI_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep48CheckVesselMMSI.createReport(utils.readFile("messages/pullResponse_step48_checkVesselMMSI_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep48CheckVesselMMSI.createReport(utils.readFile("messages/pullResponse_step48_checkVesselMMSI_noVessel.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_no_MMSI() {
        TAR report = testStep48CheckVesselMMSI.createReport(utils.readFile("messages/pullResponse_step48_checkVesselMMSI_noMMSI.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}
