package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.anomaly.Anomaly;
import eu.cise.datamodel.v1.entity.anomaly.AnomalyType;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep18UnexpectedMovementMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep18UnexpectedMovementMinimumDefinitionTest extends CiseTestStepTest {



    private TestStep18UnexpectedMovementMinimumDefinition testStep18UnexpectedMovementMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        setupFiles();
        testStep18UnexpectedMovementMinimumDefinition = new TestStep18UnexpectedMovementMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_18_UNEXPECTED_MOVEMENT_MIN_DEF.getUiCheckName();
    }

    private void setupFiles() {
        TestFileGenerator testFileGenerator = new TestFileGenerator();
        testFileGenerator.generate();

    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_UnexpectedMovementMinimumDefinition() {
        TAR report = testStep18UnexpectedMovementMinimumDefinition.createReport(utils.readFile("messages/push_Step18_UnexpectedMovementMinimumDefinition_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_backward_UnexpectedMovementMinimumDefinition() {
        TAR report = testStep18UnexpectedMovementMinimumDefinition.createReport(utils.readFile("messages/push_Step18_UnexpectedMovementMinimumDefinition_success_backward.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_UnexpectedMovementMinimumDefinition() {
        TAR report = testStep18UnexpectedMovementMinimumDefinition.createReport(utils.readFile("messages/push_Step18_UnexpectedMovementMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_backward_UnexpectedMovementMinimumDefinition() {
        TAR report = testStep18UnexpectedMovementMinimumDefinition.createReport(utils.readFile("messages/push_Step18_UnexpectedMovementMinimumDefinition_failure_backward.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    private class TestFileGenerator {
        void setupSuccess() {
            Anomaly anomaly = new Anomaly();
            anomaly.setAnomalyType(AnomalyType.UNEXPECTED_MOVEMENT);
            Vessel vessel = new Vessel();
            Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
            involvedObjectRel.setObject(vessel);
            anomaly.getInvolvedObjectRels().add(involvedObjectRel);
            String valueFile = "messages/push_Step18_UnexpectedMovementMinimumDefinition_success.xml";
            if (!utils.findFile(valueFile)) utils.writePushMessageFiles(valueFile, xmlMapper, (Entity) anomaly);
        }

        void setupFailure() {
            Anomaly anomaly = new Anomaly();
            anomaly.setAnomalyType(AnomalyType.UNEXPECTED_MOVEMENT);
            String valueFile = "messages/push_Step18_UnexpectedMovementMinimumDefinition_failure.xml";
            if (!utils.findFile(valueFile)) utils.writePushMessageFiles(valueFile, xmlMapper, (Entity) anomaly);
        }

        void setupBackwardSuccess() {
            Anomaly anomaly = new Anomaly();
            Vessel vessel = new Vessel();
            //
            Anomaly anomaly2 = new Anomaly();
            anomaly2.setAnomalyType(AnomalyType.UNEXPECTED_MOVEMENT);
            Objet.InvolvedEventRel involvedEventRel = new Objet.InvolvedEventRel();
            involvedEventRel.setEvent(anomaly2);
            vessel.getInvolvedEventRels().add(involvedEventRel);
            //
            Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
            involvedObjectRel.setObject(vessel);
            anomaly.getInvolvedObjectRels().add(involvedObjectRel);
            String valueFile = "messages/push_Step18_UnexpectedMovementMinimumDefinition_success_backward.xml";
            if (!utils.findFile(valueFile)) utils.writePushMessageFiles(valueFile, xmlMapper, (Entity) anomaly);
        }

        void setupBackwardFailure() {
            Anomaly anomaly = new Anomaly();
            Risk risk = new Risk();
            //
            Anomaly anomaly2 = new Anomaly();
            anomaly2.setAnomalyType(AnomalyType.UNEXPECTED_MOVEMENT);
            Risk.ImpliedEventRel involvedEventRel = new Risk.ImpliedEventRel();
            involvedEventRel.setEvent(anomaly2);
            risk.getImpliedEventRels().add(involvedEventRel);
            //
            Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
            impliedRiskRel.setRisk(risk);
            anomaly.getImpliedRiskRels().add(impliedRiskRel);
            String valueFile = "messages/push_Step18_UnexpectedMovementMinimumDefinition_failure_backward.xml";
            if (!utils.findFile(valueFile)) utils.writePushMessageFiles(valueFile, xmlMapper, (Entity) anomaly);
        }

        public void generate() {
            setupSuccess();
            setupBackwardSuccess();
            setupFailure();
            setupBackwardFailure();
        }
    }

}
