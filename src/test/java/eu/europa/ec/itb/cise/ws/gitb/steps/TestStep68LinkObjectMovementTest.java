package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep68LinkObjectMovement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep68LinkObjectMovementTest extends CiseTestStepTest {

    private TestStep68LinkObjectMovement testStep68LinkObjectMovement;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep68LinkObjectMovement = new TestStep68LinkObjectMovement(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_68_LINK_OBJECT_MOVEMENT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectMovement_CargoUnit_to_Movement_validation() {
        TAR report = testStep68LinkObjectMovement.createReport(utils.readFile("messages/push_Step68_LinkObjectMovement_CargoUnit_to_Movement_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_CargoUnit_to_Movement_with_not_valid_AgentRole() {
        TAR report = testStep68LinkObjectMovement.createReport(utils.readFile("messages/push_Step68_LinkObjectMovement_CargoUnit_to_Movement_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectMovement_Movement_to_CargoUnit_validation() {
        TAR report = testStep68LinkObjectMovement.createReport(utils.readFile("messages/push_Step68_LinkObjectMovement_Movement_to_CargoUnit_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Movement_to_CargoUnit_with_not_valid_AgentRole() {
        TAR report = testStep68LinkObjectMovement.createReport(utils.readFile("messages/push_Step68_LinkObjectMovement_Movement_to_CargoUnit_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }
}