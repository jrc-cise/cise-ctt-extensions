package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep69LinkObjectAnomaly;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep69LinkObjectAnomalyTest extends CiseTestStepTest {

    private TestStep69LinkObjectAnomaly testStep69LinkObjectAnomaly;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep69LinkObjectAnomaly = new TestStep69LinkObjectAnomaly(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_69_LINK_OBJECT_ANOMALY.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_CargoUnit_to_Anomaly_validation() {
        TAR report = testStep69LinkObjectAnomaly.createReport(utils.readFile("messages/push_Step69_LinkObjectAnomaly_CargoUnit_to_Anomaly_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_CargoUnit_to_Anomaly_with_not_valid_AgentRole() {
        TAR report = testStep69LinkObjectAnomaly.createReport(utils.readFile("messages/push_Step69_LinkObjectAnomaly_CargoUnit_to_Anomaly_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_Anomaly_to_CargoUnit_validation() {
        TAR report = testStep69LinkObjectAnomaly.createReport(utils.readFile("messages/push_Step69_LinkObjectAnomaly_Anomaly_to_CargoUnit_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Anomaly_to_CargoUnit_with_not_valid_AgentRole() {
        TAR report = testStep69LinkObjectAnomaly.createReport(utils.readFile("messages/push_Step69_LinkObjectAnomaly_Anomaly_to_CargoUnit_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

}