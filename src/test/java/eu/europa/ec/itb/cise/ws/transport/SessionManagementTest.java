package eu.europa.ec.itb.cise.ws.transport;

import com.gitb.ms.SendRequest;
import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.SessionData;
import eu.europa.ec.itb.cise.ws.gitb.SessionManager;
import eu.europa.ec.itb.cise.ws.util.ProxyInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SessionManagementTest {

    private Push push, push2;
    private IncomingMessagesServiceImpl incomingMessagesService;
    private OutgoingMessagesServiceImpl outgoingMessagesService;
    private SessionManager sessionManager;
    private MessageProcessor messageProcessor;
    private XmlMapper xmlMapper;
    private ReportBuilder reportBuilder;
    private SendRequest sendRequest;
    private Acknowledgement acknowledgement;
    private SignatureService signature;

    private ProxyInfo proxyInfo;
    private MessageIdStore messageIdStore;

    @BeforeEach
    public void setUp() throws Exception {
        xmlMapper = new DefaultXmlMapper.NotValidating();
        messageProcessor = mock(MessageProcessor.class);
        messageIdStore = mock(MessageIdStore.class);

        reportBuilder = mock(ReportBuilder.class);
        when(reportBuilder.createReport(any())).thenReturn(new TAR());
        //when (reportBuilder.extractContent(any())).thenReturn(MessageBuilder.TEST_MESSAGE_XML);

        proxyInfo = mock(ProxyInfo.class);
        when(proxyInfo.isEnabled()).thenReturn(false);

        sendRequest = mock(SendRequest.class);
        when(sendRequest.getSessionId()).thenReturn("90909090909090909090");

        // tests consist in following class behaviour validation
        sessionManager = new SessionManager(proxyInfo, reportBuilder);
        // we create outgoing and incoming to invoke sessionmanager
        outgoingMessagesService = new OutgoingMessagesServiceImpl(sessionManager, messageIdStore, xmlMapper, messageProcessor, reportBuilder);
        incomingMessagesService = new IncomingMessagesServiceImpl(sessionManager, messageIdStore, messageProcessor, xmlMapper, reportBuilder);

        acknowledgement = MessageBuilderUtil.createAcknowledgeMessage();
        push = newPush().id("push-message-id").sender(newService().id("es.test")).build();
        push2 = newPush().id("push-message-id").sender(newService().id("de.test")).build();

        Map<String, Object> sessionItem = new HashMap<>();
        Map<String, Map<String, Object>> sessions = new HashMap<>();
        Map<String, Object> sessionObjects = new HashMap<>();
        sessionObjects.put("session", "session-id");
        sessionObjects.put(SessionData.CALLBACK_URL, "callbackURL");
        sessions.put("sessions", sessionObjects);


    }

    @AfterEach
    public void tearDown() throws Exception {
    }


    @Disabled
    @Test
    public void it_add_2_distinct_session_from_2_distinct_server_serviceId() {

        //SendResponse sendResponse = outgoingMessagesService.send(sendRequest);
    }


}
