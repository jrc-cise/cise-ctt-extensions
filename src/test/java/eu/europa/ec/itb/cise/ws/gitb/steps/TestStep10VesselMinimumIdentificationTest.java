package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep10VesselMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep10VesselMinimumIdentificationTest extends CiseTestStepTest {



    private TestStep10VesselMinimumIdentification testStep10VesselMinimumIdentification;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep10VesselMinimumIdentification = new TestStep10VesselMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_10_VESSEL_MIN_IDENT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_successful_report_with_simple_content_for_valid_Vessel_entity() {
        String messageXml = xmlMapper.toXML(push);
        TAR report = testStep10VesselMinimumIdentification.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_Vessel_entity() {
        utils.setWrongVesselPayload(push);
        String messageXml = xmlMapper.toXML(push);
        TAR report = testStep10VesselMinimumIdentification.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_successful_report_with_simple_content_for_no_Vessel_entity() {
        XmlEntityPayload xmlEntityPayload = (XmlEntityPayload) push.getPayload();
        xmlEntityPayload.getAnies().clear();
        String messageXml = xmlMapper.toXML(push);
        TAR report = testStep10VesselMinimumIdentification.createReport(messageXml);
        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(reportResult).isEqualTo("VALID");
    }
}
