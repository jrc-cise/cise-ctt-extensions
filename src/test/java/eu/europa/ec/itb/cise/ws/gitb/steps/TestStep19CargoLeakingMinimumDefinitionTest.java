package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep19CargoLeakingMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep19CargoLeakingMinimumDefinitionTest extends CiseTestStepTest {



    private TestStep19CargoLeakingMinimumDefinition testStep19CargoLeakingMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep19CargoLeakingMinimumDefinition = new TestStep19CargoLeakingMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_19_CARGO_LEAKING_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_ModelCargoLeaking_validation() {
        TAR report = testStep19CargoLeakingMinimumDefinition.createReport(utils.readFile("messages/push_Step19_ModelCargoLeaking_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_backward_ModelCargoLeaking_validation() {
        TAR report = testStep19CargoLeakingMinimumDefinition.createReport(utils.readFile("messages/push_Step19_ModelCargoLeaking_success_backward.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_ModelCargoLeaking_validation() {
        TAR report = testStep19CargoLeakingMinimumDefinition.createReport(utils.readFile("messages/push_Step19_ModelCargoLeaking_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_empty_ModelCargoLeaking_validation() {
        TAR report = testStep19CargoLeakingMinimumDefinition.createReport(utils.readFile("messages/push_Step19_ModelCargoLeaking_failure_empty.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}
