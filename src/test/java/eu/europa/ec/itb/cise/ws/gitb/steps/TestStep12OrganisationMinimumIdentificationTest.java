package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep12OrganisationMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep12OrganisationMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step12_OrganisationMinimumIdentification";

    private TestStep12OrganisationMinimumIdentification testStep12OrganisationMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_OrganisationMinimumIdentification_failure();
        payloadCreator_OrganisationMinimumIdentification_success();
        testStep12OrganisationMinimumIdentification = new TestStep12OrganisationMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_12_ORGANISATION_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_OrganizationMinimumDefinition_validation() {
        TAR report = testStep12OrganisationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_OrganizationMinimumDefinition_validation() {
        TAR report = testStep12OrganisationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_OrganisationMinimumIdentification_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Organization organization = new Organization();
        organization.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(organization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_OrganisationMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Organization organization = new Organization();
        //organization.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(organization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
