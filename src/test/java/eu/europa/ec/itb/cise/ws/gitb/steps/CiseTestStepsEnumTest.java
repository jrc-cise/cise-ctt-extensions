package eu.europa.ec.itb.cise.ws.gitb.steps;


import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep24AttachedDocumentMinimumDefinition;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep22StainOfOilSightedMinimumDefinition;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep27PollutionIncidentMinimumDefinition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.Constructor;

import static org.assertj.core.api.Assertions.assertThat;


public class CiseTestStepsEnumTest {


    @Test
    public void getClassFromUiCheckNameTest27() {
        Class aclass = CiseTestStepsEnum.getClassFromUiCheckName("ts27PollutionIncidentMinimumDefinition");
        assertThat(aclass).isEqualTo(TestStep27PollutionIncidentMinimumDefinition.class);
    }


    @ParameterizedTest
    @ValueSource(strings = {"ts27PollutionIncidentMinimumDefinition", "ts22StainOfOilSightedMinimumDefinition", "ts24AttachedDocumentMinimumDefinition"})
    public void getClassFromUiCheckNameTestAsConstructor(String arg) {
        Class ClassFromUiCheckName = CiseTestStepsEnum.getClassFromUiCheckName(arg);
        try {
            Constructor constructor = ClassFromUiCheckName.getConstructor(ReportBuilder.class, CisePayloadHelper.class);
            assertThat(constructor).isNotNull();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getClassFromUiCheckNameTest22() {
        Class aclass = CiseTestStepsEnum.getClassFromUiCheckName("ts22StainOfOilSightedMinimumDefinition");
        assertThat(aclass).isEqualTo(TestStep22StainOfOilSightedMinimumDefinition.class);
    }


    @Test
    public void getClassFromUiCheckNameTest24() {
        Class aclass = CiseTestStepsEnum.getClassFromUiCheckName("ts24AttachedDocumentMinimumDefinition");
        assertThat(aclass).isEqualTo(TestStep24AttachedDocumentMinimumDefinition.class);
    }

}
