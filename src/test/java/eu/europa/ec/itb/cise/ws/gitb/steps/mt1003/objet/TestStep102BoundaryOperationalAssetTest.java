package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.operationalasset.OperationalAsset;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep102BoundaryOperationalAssetTest extends CiseTestStepTestChildValueBoundaries {

    private final String testFilePrefix = "messages/push_Step102_BoundaryOperationalAssetTest";
    private final String propertyFile = "src/test/java/eu/europa/ec/itb/cise/ws/gitb/steps/mt1003/objet/Objet.properties";
    private TestStep102BoundaryOperationalAsset testStep102BoundaryOperationalAsset;

    private static final String[] FIELDS = new String[]{
            "maxPassengers",
            "range",
            "maxSpeed"
    };

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep102BoundaryOperationalAsset = new TestStep102BoundaryOperationalAsset(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_102_BOUNDARY_OPERATIONALASSET.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Objet");
        if (errorBoundaries == null)
            if (reinitCondition)
                payload_PropertyBoundary_maker(Catch.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }

    }



    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep102BoundaryOperationalAsset.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep102BoundaryOperationalAsset.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts102BoundaryOperationalAsset");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }

    @NotNull
    public Vessel getTestedEntity(String[] fields, boolean purposeSuccess) {
        Vessel vessel = new Vessel();
        OperationalAsset operationalAsset = (OperationalAsset) PropertyMaker.populateAttributes(errorBoundaries, OperationalAsset.class, fields, purposeSuccess);
        Vehicle.CorrespondentAssetRel correspondentAssetRel= new Vehicle.CorrespondentAssetRel();
        correspondentAssetRel.setOperationalAsset(operationalAsset);
        vessel.setCorrespondentAssetRel(correspondentAssetRel);
        return vessel;
    }
}