package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep02XMLValidationTest extends CiseTestStepTest {



    private TestStep02XMLValidation testStep02XMLValidation;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep02XMLValidation = new TestStep02XMLValidation(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_02_XML_VALIDATION.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void createReportForXMLValidityForDataModel_creates_a_report_with_successful_xsd_validation() {
        TAR report = testStep02XMLValidation.createReport(xmlMapper.toXML(utils.getPush()));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void createReportForXMLValidityForDataModel_creates_a_report_with_unsuccessful_xsd_validation_for_service_model() {
        String messageXml = xmlMapper.toXML(utils.getPush());
        messageXml = messageXml.replace("CreationDateTime", "DateTime");
        TAR report = testStep02XMLValidation.createReport(messageXml);

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }


    @Test
    public void createReportForXMLValidityForDataModel_creates_a_report_with_unsuccessful_xsd_validation_for_data_model() {
        String messageXml = xmlMapper.toXML(utils.getPush());
        messageXml = messageXml.replace("IMONumber", "Heading");
        TAR report = testStep02XMLValidation.createReport(messageXml);

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }
}
