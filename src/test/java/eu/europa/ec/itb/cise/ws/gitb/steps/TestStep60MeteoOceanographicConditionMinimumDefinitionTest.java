package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep60MeteoOceanographicConditionMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep60MeteoOceanographicConditionMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep60MeteoOceanographicConditionMinimumDefinition testStep60MeteoOceanographicConditionMinimumDefinition;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep60MeteoOceanographicConditionMinimumDefinition = new TestStep60MeteoOceanographicConditionMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_60_METEOOCEANOGRAPHICCONDITION_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_link_to_a_Location() {
        TAR report = testStep60MeteoOceanographicConditionMinimumDefinition.createReport(utils.readFile("messages/push_Step60_MeteoOceanographicConditionMinimumDefinition_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_link_to_NamedLocation() {
        TAR report = testStep60MeteoOceanographicConditionMinimumDefinition.createReport(utils.readFile("messages/push_Step60_MeteoOceanographicConditionMinimumDefinition_NamedLocation_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_no_MeteoOceanographicCondition() {
        TAR report = testStep60MeteoOceanographicConditionMinimumDefinition.createReport(utils.readFile("messages/push_Step60_MeteoOceanographicConditionMinimumDefinition_no_MeteoOceanographicCondition_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_no_Location() {
        TAR report = testStep60MeteoOceanographicConditionMinimumDefinition.createReport(utils.readFile("messages/push_Step60_MeteoOceanographicConditionMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}