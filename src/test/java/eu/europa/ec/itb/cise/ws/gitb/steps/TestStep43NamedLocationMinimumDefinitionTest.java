package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.NamedLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep43NamedLocationMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep43NamedLocationMinimumDefinitionTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step43_NamedLocationMinimumDefinition";

    private TestStep43NamedLocationMinimumDefinition testStep43NamedLocationMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_NamedLocationMinimumDefinition_failure();
        payloadCreator_NamedLocationMinimumDefinition_success();
        testStep43NamedLocationMinimumDefinition = new TestStep43NamedLocationMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_43_NAMED_LOCATION_MIN_DEF.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_NamedLocationMinimumDefinition_validation() {
        TAR report = testStep43NamedLocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_NamedLocationMinimumDefinition_validation() {
        TAR report = testStep43NamedLocationMinimumDefinition.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_NamedLocationMinimumDefinition_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        NamedLocation location = new NamedLocation();
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);
        Geometry geometry = new Geometry();
        // success factor
        location.getGeographicNames().add("el boulou");
        location.getGeometries().add(geometry);
        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_NamedLocationMinimumDefinition_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        NamedLocation location = new NamedLocation();
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);

        Geometry geometry = new Geometry();
        // unsuccess factor
        //location.getGeographicNames().add("el boulou");
        location.getGeometries().add(geometry);

        cargo.getLocationRels().add(locationRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
