package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep52CheckEventVesselIMONumberTest extends CiseTestStepTest {


    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step52_CheckEventVesselIMONumber";
    private TestStep52CheckEventVesselIMONumber testStep52CheckEventVesselIMONumber;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep52CheckEventVesselIMONumber = new TestStep52CheckEventVesselIMONumber(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckEventVesselIMONumber_success();
        payloadCreator_CheckEventVesselIMONumber_failure();
        payloadCreator_CheckRiskVesselIMONumber_multirisk_success();
        payloadCreator_CheckRiskVesselIMONumber_multirisk_failure();
        payloadCreator_CheckEventVesselIMONumber_noMMSI_failure();
        payloadCreator_CheckEventVesselIMONumber_noVessel_failure();
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_52_CHECK_EVENT_VESSEL_IMO_NUMBER.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_message() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_multirisk_message() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_52_CHECK_EVENT_VESSEL_IMO_NUMBER.getUiCheckName());
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_multirisk_message() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml"), "7710525");
        // will give only first so if message report have 2 item and an error is second while valid is first does not ring the bell
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_52_CHECK_EVENT_VESSEL_IMO_NUMBER.getUiCheckName());
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_noIMONumber() {
        TAR report = testStep52CheckEventVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noIMONumber_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_CheckEventVesselIMONumber_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_success.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_multirisk_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        MaritimeSafetyIncident maritimeSafetyIncident1 = new MaritimeSafetyIncident();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel1 = new Event.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        maritimeSafetyIncident1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        entities.add(maritimeSafetyIncident1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_multirisk_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        MaritimeSafetyIncident maritimeSafetyIncident1 = new MaritimeSafetyIncident();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710526L);
        Vessel vessel2;
        vessel2 = new Vessel();
        vessel2.setIMONumber(7710527L);
        //!!! we  add 2 vessel to the second
        Event.InvolvedObjectRel involvedObjectRel1 = new Event.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        maritimeSafetyIncident1.getInvolvedObjectRels().add(involvedObjectRel1);
        //!!!
        Event.InvolvedObjectRel involvedObjectRel2 = new Event.InvolvedObjectRel();
        involvedObjectRel2.setObject(vessel2);
        maritimeSafetyIncident1.getInvolvedObjectRels().add(involvedObjectRel2);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        entities.add(maritimeSafetyIncident1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckEventVesselIMONumber_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710526L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);

    }

    public void payloadCreator_CheckEventVesselIMONumber_noVessel_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }

    public void payloadCreator_CheckEventVesselIMONumber_noMMSI_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noIMONumber_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }
}
