package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep35AttachedDocumentCoherentWithCargo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep35AttachedDocumentCoherentWithCargoTest extends CiseTestStepTest {



    private TestStep35AttachedDocumentCoherentWithCargo testStep35AttachedDocumentCoherentWithCargo;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep35AttachedDocumentCoherentWithCargo = new TestStep35AttachedDocumentCoherentWithCargo(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_35_ATTACHED_DOCUMENT_COHERENT_WITH_CARGO.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_for_valid_Document_type() {
        String messageXml = utils.readFile("messages/push_Step35_AttachedDocumentCoherentWithCargo_success.xml");

        TAR report = testStep35AttachedDocumentCoherentWithCargo.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_for_Vessel_with_no_Document() {
        String messageXml = utils.readFile("messages/push_Step35_AttachedDocumentCoherentWithCargo_no_document_success.xml");

        TAR report = testStep35AttachedDocumentCoherentWithCargo.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_for_Document_of_wrong_type() {
        String messageXml = utils.readFile("messages/push_Step35_AttachedDocumentCoherentWithCargo_failure.xml");

        TAR report = testStep35AttachedDocumentCoherentWithCargo.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_for_one_Document_out_of_many_of_wrong_type() {
        String messageXml = utils.readFile("messages/push_Step35_AttachedDocumentCoherentWithCargo_one_Document_out_of_many_failure.xml");

        TAR report = testStep35AttachedDocumentCoherentWithCargo.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

}
