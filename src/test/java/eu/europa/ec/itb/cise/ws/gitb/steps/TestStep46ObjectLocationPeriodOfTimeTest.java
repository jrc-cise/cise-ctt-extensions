package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep46ObjectLocationPeriodOfTimeTest extends CiseTestStepTest {

    private TestStep46ObjectLocationPeriodOfTime testStep46ObjectLocationPeriodOfTime;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep46ObjectLocationPeriodOfTime = new TestStep46ObjectLocationPeriodOfTime(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_46_OBJECTLOCATION_PERIODOFTIME.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_ModelObjectLocationPeriodOfTime_validation() {
        TAR report = testStep46ObjectLocationPeriodOfTime.createReport(utils.readFile("messages/push_Step46_ObjectLocationPeriodOfTime_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_ModelObjectLocationPeriodOfTime_validation() {
        TAR report = testStep46ObjectLocationPeriodOfTime.createReport(utils.readFile("messages/push_Step46_ObjectLocationPeriodOfTime_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }
}
