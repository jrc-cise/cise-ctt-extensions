package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep74LinkPersonPerson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #73 LinkMovementLocation
 * ^^^^^^^^^^^^^^^^^^^^^^^^
 * `*SCENARIO*` it must not provide *attribute* `EventArea` to *relation(s)* between `Organization (+ child entities)`  and `Location`
 * `*GIVEN*` relation(s) to `Location` *entity(ies)* from *entity* of type `Organization` or `OrganizationUnit` or `PortOrganization` or `FormalOrganization` or `OrganizationalCollaboration` is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*THEN*` its *attribute* `AgentRole` _must not be one of _
 * `CountryOfBirth`,
 * `PlaceOfBirth`,
 * `CountryOfDeath`,
 * `EmbarkationPort`,
 * `DisembarkationPort`,
 * `CountryOfResidence`
 * <p>
 * examples:
 */
public class TestStep74LinkPersonPersonTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step74_LinkPersonPerson";
    private TestStep74LinkPersonPerson testStep74LinkPersonPerson;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep74LinkPersonPerson = new TestStep74LinkPersonPerson(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_74_LINK_PERSON_PERSON.getUiCheckName();
        payloadCreator_LinkPersonPerson_failure();
        payloadCreator_LinkPersonPerson_success();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep74LinkPersonPerson.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep74LinkPersonPerson.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_LinkPersonPerson_success() {
        Vessel vessel = new Vessel();
        Person person1 = new Person();
        person1.setFullName("Thomas");
        Objet.InvolvedAgentRel involvedAgentRel= new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person1);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        Person person2 = new Person();
        person1.setFullName("Antoine");
        Agent.InvolvedWithRel involvedWithRel=new Agent.InvolvedWithRel();
        involvedWithRel.setAgent(person2);
        // success factor : person1.getInvolvedWithRels().add(involvedWithRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Vessel) vessel);
    }

    public void payloadCreator_LinkPersonPerson_failure() {
        Vessel vessel = new Vessel();
        Person person1 = new Person();
        person1.setFullName("Thomas");
        Objet.InvolvedAgentRel involvedAgentRel= new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person1);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        Person person2 = new Person();
        person1.setFullName("Antoine");
        Agent.InvolvedWithRel involvedWithRel=new Agent.InvolvedWithRel();
        involvedWithRel.setAgent(person2);
        person1.getInvolvedWithRels().add(involvedWithRel); // failure factor

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Vessel) vessel);
    }
}