package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.VesselDocument;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.uniqueidentifier.UniqueIdentifier;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #94 Format Document Metadata Designation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Document` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` `Metadata` *attribute* _is defined_
 * `*THEN*` key `Designation` of *attribute* `Metadata` must have CISE Data Model entity valid name .
 */
public class TestStep95FormatUniqueIdentifierUuidTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step95_FormatUniqueIdentifierUuid";
    private TestStep95FormatUniqueIdentifierUuid testStep95FormatUniqueIdentifierUuid;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep95FormatUniqueIdentifierUuid = new TestStep95FormatUniqueIdentifierUuid(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_95_FORMAT_UNIQUE_IDENTIFIER_UUID.getUiCheckName();
        payloadCreator_Format_success();
        payloadCreator_hexFormat_success();
        payloadCreator_Format_failure();
        payloadCreator_Format_empty();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep95FormatUniqueIdentifierUuid.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_with_correctHexValue_validation() {
        TAR report = testStep95FormatUniqueIdentifierUuid.createReport(Utils.readFile(TEST_FILE_PREFIX + "_hex_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep95FormatUniqueIdentifierUuid.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_empty_validation() {
        TAR report = testStep95FormatUniqueIdentifierUuid.createReport(Utils.readFile(TEST_FILE_PREFIX + "_empty.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    public void payloadCreator_Format_success() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        UniqueIdentifier uniqueIdentifier=new UniqueIdentifier();
        uniqueIdentifier.setUUID("12345678-1234-1234-1234-123456789012");
        document.setIdentifier(uniqueIdentifier);
        documentRel.setDocument(document);
        vessel.getDocumentRels().add(documentRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }
    public void payloadCreator_hexFormat_success() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        UniqueIdentifier uniqueIdentifier=new UniqueIdentifier();
        uniqueIdentifier.setUUID("f81d4fae-7dec-11d0-a765-00a0c91e6bf6");
        document.setIdentifier(uniqueIdentifier);
        documentRel.setDocument(document);
        vessel.getDocumentRels().add(documentRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_hex_success.xml", xmlMapper, vessel);
    }
    public void payloadCreator_Format_failure() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        UniqueIdentifier uniqueIdentifier=new UniqueIdentifier();
        uniqueIdentifier.setUUID("4bcdefgh-ijkl-mnop-qrst-123456789012");
        document.setIdentifier(uniqueIdentifier);
        documentRel.setDocument(document);
        vessel.getDocumentRels().add(documentRel);
         utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }
    public void payloadCreator_Format_empty() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        //UniqueIdentifier uniqueIdentifier=new UniqueIdentifier();
        //document.setIdentifier(uniqueIdentifier);
        documentRel.setDocument(document);
        vessel.getDocumentRels().add(documentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_empty.xml", xmlMapper, vessel);
    }

}