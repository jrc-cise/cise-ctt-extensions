package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.VesselDocument;
import eu.cise.datamodel.v1.entity.metadata.Metadata;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #93 FORMAT Metadata Language
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Document`  (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE`  *payload*
 * `*AND*` attribute `Document` have a `Metadata` attribute  defined
 * `*THEN*` `Metadata` must contain a key "language"
 * `*AND*` "language" value must be composed of three-letter codes based on ISO 639-3 standard.
 */
public class TestStep93FormatMetadataLanguageTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step93_FormatMetadataLanguage";
    private TestStep93FormatMetadataLanguage testStep93FormatMetadataLanguage;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep93FormatMetadataLanguage = new TestStep93FormatMetadataLanguage(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_93_FORMAT_METADATA_LANGUAGE.getUiCheckName();
        payloadCreator_Format_success();
        payloadCreator_Format_failure();
        payloadCreator_Format_warning();
        payloadCreator_Format_empty();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep93FormatMetadataLanguage.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep93FormatMetadataLanguage.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_warning_report_with_correct_validation() {
        TAR report = testStep93FormatMetadataLanguage.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_empty_correct_validation() {
        TAR report = testStep93FormatMetadataLanguage.createReport(Utils.readFile(TEST_FILE_PREFIX + "_empty.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    public void payloadCreator_Format_success() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        Metadata metadata=new Metadata();
        metadata.setLanguage("FRA");
        document.getMetadatas().add(metadata);
        documentRel.setDocument(document);
        vessel.getDocumentRels().add(documentRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_Format_failure() {
        Vessel vessel = new Vessel();
            Objet.DocumentRel documentRel = new Objet.DocumentRel();
            Document document = new VesselDocument();
            // here should have a link to a location
            Metadata metadata=new Metadata();
            metadata.setLanguage("FfER");
            document.getMetadatas().add(metadata);
            documentRel.setDocument(document);

        vessel.getDocumentRels().add(documentRel);
         utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_Format_warning() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        Metadata metadata=new Metadata();
        metadata.setLanguage("FRX");
        document.getMetadatas().add(metadata);
        documentRel.setDocument(document);

        vessel.getDocumentRels().add(documentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning.xml", xmlMapper, vessel);
    }


    public void payloadCreator_Format_empty() {
        Vessel vessel = new Vessel();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        Document document = new VesselDocument();
        // here should have a link to a location
        Metadata metadata=new Metadata();
        document.getMetadatas().add(metadata);
        documentRel.setDocument(document);

        vessel.getDocumentRels().add(documentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_empty.xml", xmlMapper, vessel);
    }
}