package eu.europa.ec.itb.cise.ws.transport;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.AcknowledgementType;
import eu.cise.servicemodel.v1.message.Push;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.SessionData;
import eu.europa.ec.itb.cise.ws.gitb.SessionManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.Map;

import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("test")
public class IncomingMessagesServiceImplTest {
    private Acknowledgement acknowledgement;
    private Push push;

    private IncomingMessagesServiceImpl incomingMessagesService;
    private CisePayloadHelper payloadHelper;
    private SessionManager sessionManager;
    private MessageProcessor messageProcessor;
    private XmlMapper xmlMapper;
    private ReportBuilder reportBuilder;
    private MessageIdStore messageIdStore;
    public static final String TEST_SESSION_ID = "com.test";

    @BeforeEach
    public void setUp() throws Exception {
        payloadHelper = new CisePayloadHelper(xmlMapper);
        reportBuilder = mock(ReportBuilder.class);
        sessionManager = mock(SessionManager.class);
        messageProcessor = mock(MessageProcessor.class);
        messageIdStore = mock(MessageIdStore.class);
        xmlMapper = new DefaultXmlMapper.NotValidating();
        incomingMessagesService = new IncomingMessagesServiceImpl(sessionManager, messageIdStore, messageProcessor, xmlMapper, reportBuilder);
        acknowledgement = MessageBuilderUtil.createAcknowledgeMessage();
        push = newPush().id("push-message-id").sender(newService().id(TEST_SESSION_ID).build()).build();

        Map<String, Map<String, Object>> sessions = new HashMap<>();
        Map<String, Object> sessionObjects = new HashMap<>();
        sessionObjects.put(SessionData.SERVICE_ID, TEST_SESSION_ID);
        sessionObjects.put(SessionData.CALLBACK_URL, "http://localhost:8080/test");
        sessions.put(TEST_SESSION_ID, sessionObjects);

        when(sessionManager.getAllSessions()).thenReturn(sessions);
        when(messageProcessor.receive(any())).thenReturn(acknowledgement);
        when(sessionManager.obtainIdentifierFromMessage(any())).thenReturn(TEST_SESSION_ID);
        when(sessionManager.isIdentifiedBy(any(), any())).thenReturn(true);

    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void it_calls_the_message_processor_receive_method() {
        incomingMessagesService.send(push);
        verify(messageProcessor).receive(any());
    }

    @Test
    public void it_returns_consistent_value_of_acknowledge_when_invoked_receive() {
        Acknowledgement response = incomingMessagesService.send(push);
        assertThat(response).isEqualTo(acknowledgement);
    }

    @Test
    public void it_notify_the_testbed_for_the_received_message() {
        incomingMessagesService.send(push);
        verify(sessionManager).notifyTestBed(any(), any());
    }

    @Test
    public void it_notify_the_testbed_for_the_received_message_containing_a_report_with_the_xmlmessage() {
        incomingMessagesService.send(push);
        verify(reportBuilder).createMessageReportMessageAndAck(any(), any());
    }

    @Test
    public void it_notifies_the_testbed_for_the_signature_error() {
        Acknowledgement signatureErrorAck = MessageBuilderUtil.createAcknowledgeMessage();
        signatureErrorAck.setAckCode(AcknowledgementType.AUTHENTICATION_ERROR);
        when(messageProcessor.receive(any())).thenReturn(signatureErrorAck);
        TAR reportSignError = createReportWithSignError();
        when(reportBuilder.createMessageReportMessageAndAck(signatureErrorAck, push)).thenReturn(reportSignError);
        incomingMessagesService.send(push);
        verify(sessionManager).notifyTestBed(TEST_SESSION_ID, reportSignError);
    }


    private TAR createReportWithSignError() {
        AnyContent reportContentSignError = new AnyContent();
        reportContentSignError.setName("signature");
        reportContentSignError.setValue("signature error");
        reportContentSignError.setType("string");
        reportContentSignError.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        when(reportBuilder.createAnyContentSimple(any(), any(), any(), any())).thenReturn(reportContentSignError);
        TAR reportSignError = new TAR();
        reportSignError.setContext(reportContentSignError);
        return reportSignError;
    }

    private TAR createReportWithMessage() {
        AnyContent contentWithMessage = new AnyContent();
        contentWithMessage.setName("message");
        contentWithMessage.setValue(xmlMapper.toXML(push));
        contentWithMessage.setType("string");
        contentWithMessage.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        when(reportBuilder.createAnyContentSimple(any(), any(), any(), any())).thenReturn(contentWithMessage);
        TAR reportMessage = new TAR();
        reportMessage.setContext(contentWithMessage);
        return reportMessage;
    }

}
