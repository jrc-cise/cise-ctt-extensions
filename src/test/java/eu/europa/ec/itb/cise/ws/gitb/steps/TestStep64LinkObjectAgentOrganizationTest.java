package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep64LinkObjectAgentOrganization;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep64LinkObjectAgentOrganizationTest extends CiseTestStepTest {

    private TestStep64LinkObjectAgentOrganization testStep64LinkObjectAgentOrganization;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep64LinkObjectAgentOrganization = new TestStep64LinkObjectAgentOrganization(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_64_LINK_OBJECT_AGENT_ORGANIZATION.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectAgentOrganization_validation() {
        TAR report = testStep64LinkObjectAgentOrganization.createReport(utils.readFile("messages/push_Step64_LinkObjectAgentOrganization_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_Organization_to_Object_validation() {
        TAR report = testStep64LinkObjectAgentOrganization.createReport(utils.readFile("messages/push_Step64_LinkObjectAgentOrganization_Agent_to_Object_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_not_valid_AgentRole() {
        TAR report = testStep64LinkObjectAgentOrganization.createReport(utils.readFile("messages/push_Step64_LinkObjectAgentOrganization_wrong_AgentRole_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_TransitPassenger_defined() {
        TAR report = testStep64LinkObjectAgentOrganization.createReport(utils.readFile("messages/push_Step64_LinkObjectAgentOrganization_TransitPassenger_defined_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_Duty_defined() {
        TAR report = testStep64LinkObjectAgentOrganization.createReport(utils.readFile("messages/push_Step64_LinkObjectAgentOrganization_Duty_defined_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


}