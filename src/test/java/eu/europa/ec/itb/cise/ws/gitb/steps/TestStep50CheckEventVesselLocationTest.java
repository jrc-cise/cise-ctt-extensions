package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.LocationQualitativeAccuracyType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep50CheckEventVesselLocationTest extends CiseTestStepTest {

    private TestStep50CheckEventVesselLocation testStep50CheckEventVesselLocation;
    private String pullRequest = "";
    private static final String TEST_FILE_REQUEST_PREFIX = "messages/pullRequest_Step50_CheckEventVesselLocation";
    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step50_CheckEventVesselLocation";
    public static final long SAMPLE_VESSEL_ONE_IMO_NUMBER = 7710525L;
    public static final long SAMPLE_VESSEL_TWO_IMO_NUMBER = 7710526L;
    public static final String POINT_01_IN_AREA_LAT = "2.0";
    public static final String POINT_01_IN_AREA_LON = "2.0";
    public static final String POINT_02_OUT_AREA_LON = "50.0";
    public static final String POINT_02_OUT_AREA_LAT = "55.0";
    public static final String POINT_03_OUT_AREA_LAT = "60.0";
    public static final String POINT_03_OUT_AREA_LON = "66.0";
    public static final String POINT_04_IN_AREA_LON = "0.0";
    public static final String POINT_04_IN_AREA_LAT = "0.0";
    public static final String POINT_05_OUT_AREA_LON = "30.0";
    public static final String POINT_05_OUT_AREA_LAT = "33.0";
    private static final String RECTANGLE_COORDINATE_BOTTOM_LEFT_LON = "-10.0";
    private static final String RECTANGLE_COORDINATE_BOTTOM_LEFT_LAT = "-10.0";
    private static final String RECTANGLE_COORDINATE_BOTTOM_RIGHT_LON = "10.0";
    private static final String RECTANGLE_COORDINATE_BOTTOM_RIGHT_LAT = "-10.0";
    private static final String RECTANGLE_COORDINATE_TOP_RIGHT_LON = "10.0";
    private static final String RECTANGLE_COORDINATE_TOP_RIGHT_LAT = "10.0";
    private static final String RECTANGLE_COORDINATE_TOP_LEFT_LON = "-10.0";
    private static final String RECTANGLE_COORDINATE_TOP_LEFT_LAT = "10.0";
    public static String POLYGON_SEARCH_AREA = "POLYGON (("
            + RECTANGLE_COORDINATE_BOTTOM_LEFT_LON + " " + RECTANGLE_COORDINATE_BOTTOM_LEFT_LAT + ", "
            + RECTANGLE_COORDINATE_BOTTOM_RIGHT_LON + " " + RECTANGLE_COORDINATE_BOTTOM_RIGHT_LAT + ", "
            + RECTANGLE_COORDINATE_TOP_RIGHT_LON + " " + RECTANGLE_COORDINATE_TOP_RIGHT_LAT + ", "
            + RECTANGLE_COORDINATE_TOP_LEFT_LON + " " + RECTANGLE_COORDINATE_TOP_LEFT_LAT + ", "
            + RECTANGLE_COORDINATE_BOTTOM_LEFT_LON + " " + RECTANGLE_COORDINATE_BOTTOM_LEFT_LAT + "))";

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep50CheckEventVesselLocation = new TestStep50CheckEventVesselLocation(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckEventVesselLocation_request();
        pullRequest = utils.readFile(TEST_FILE_REQUEST_PREFIX + ".xml");
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_50_CHECK_EVENT_VESSEL_LOCATION.getUiCheckName();
    }

    @Test
    public void it_creates_a_success_report_with_one_empty_event() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_one_empty_event();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_one_empty_event() {
        MaritimeSafetyIncident emptyEvent = new MaritimeSafetyIncident();
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_one_empty_event.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(emptyEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_one_event_containing_location_outside_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_one_event_containing_location_outside_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_fail_report_with_one_event_containing_location_outside_polygon() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_03_OUT_AREA_LAT, POINT_03_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_one_event_containing_location_outside_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_one_event_that_contains_location_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_one_event_that_contains_location_in_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_one_event_that_contains_location_in_polygon() {
        MaritimeSafetyIncident hitEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_one_event_that_contains_location_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_successful_localisation_of_one_out_of_two_vessels() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_successful_localisation_of_one_out_of_two_vessels();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_successful_localisation_of_one_out_of_two_vessels() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_successful_localisation_of_one_out_of_two_vessels.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<Entity>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon() {
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon();
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon() {
        MaritimeSafetyIncident missEvent = new MaritimeSafetyIncident();
        MaritimeSafetyIncident emptyEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(missEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(missEvent, emptyEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon() {
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon();
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon() {
        MaritimeSafetyIncident hitEvent = new MaritimeSafetyIncident();
        MaritimeSafetyIncident emptyEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(emptyEvent, hitEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon() {
        MaritimeSafetyIncident missEvent1 = new MaritimeSafetyIncident();
        MaritimeSafetyIncident missEvent2 = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(missEvent1, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_03_OUT_AREA_LAT, POINT_03_OUT_AREA_LON);
        createSampleVesselAndAddItToEvent(missEvent2, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(missEvent1, missEvent2)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon() {
        MaritimeSafetyIncident hitEvent = new MaritimeSafetyIncident();
        MaritimeSafetyIncident missEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(missEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitEvent, missEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon() {
        MaritimeSafetyIncident emptyEvent = new MaritimeSafetyIncident();
        MaritimeSafetyIncident hitEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(emptyEvent, hitEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon() {
        MaritimeSafetyIncident hitEvent1 = new MaritimeSafetyIncident();
        MaritimeSafetyIncident hitEvent2 = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent1, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(hitEvent2, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitEvent1, hitEvent2)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }


    @Test
    public void it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon() {
        MaritimeSafetyIncident hitEvent = new MaritimeSafetyIncident();
        MaritimeSafetyIncident hitAndMissEvent = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(hitEvent, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(hitAndMissEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        createSampleVesselAndAddItToEvent(hitAndMissEvent, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitEvent, hitAndMissEvent)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_with_one_event_containing_malformed_location() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_with_one_event_containing_malformed_location();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_with_one_event_containing_malformed_location() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_with_one_event_containing_malformed_location.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon();

        //act
        TAR report = testStep50CheckEventVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
    }

    private String payloadCreator_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_TWO_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
        return utils.readFile(path);
    }

    private void payloadCreator_CheckEventVesselLocation_request() {
        MaritimeSafetyIncident event = new MaritimeSafetyIncident();
        createSampleVesselAndAddItToEvent(event, SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        utils.writePullRequestMessageFiles(TEST_FILE_REQUEST_PREFIX + ".xml", xmlMapper, new ArrayList<>(Arrays.asList(event)), MaritimeSafetyIncident.class);
    }

    public static Vessel getSampleVessel(long someImoNumber, String latitude, String longitude) {
        Vessel vessel = new Vessel();
        vessel.setIMONumber(someImoNumber);
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setLatitude(latitude);
        geometry.setLongitude(longitude);
        location.getGeometries().add(geometry);
        location.setLocationQualitativeAccuracy(LocationQualitativeAccuracyType.HIGH);
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);
        vessel.getLocationRels().add(locationRel);
        return vessel;
    }

    public static Vessel getSampleVessel(long someImoNumber, String polygonSearchArea) {
        Vessel vessel = new Vessel();
        vessel.setIMONumber(someImoNumber);
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setWKT(polygonSearchArea);
        location.getGeometries().add(geometry);
        location.setLocationQualitativeAccuracy(LocationQualitativeAccuracyType.HIGH);
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(location);
        vessel.getLocationRels().add(locationRel);
        return vessel;
    }

    public void createSampleVesselAndAddItToEvent(MaritimeSafetyIncident event, long vesselImoNumber, String latitude, String longitude) {
        Vessel vessel = getSampleVessel(vesselImoNumber, latitude, longitude);
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        event.getInvolvedObjectRels().add(involvedObjectRel);
    }

    public void createSampleVesselAndAddItToEvent(Event event, long vesselImoNumber, String polygonSearchArea) {
        Vessel vessel = getSampleVessel(vesselImoNumber, polygonSearchArea);
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        event.getInvolvedObjectRels().add(involvedObjectRel);
    }

}
