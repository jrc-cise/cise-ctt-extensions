package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep54CheckRiskVesselMMSITest extends CiseTestStepTest {

    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step54_CheckRiskVesselMMSI";

    private TestStep54CheckRiskVesselMMSI testStep54CheckRiskVesselMMSI;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep54CheckRiskVesselMMSI = new TestStep54CheckRiskVesselMMSI(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckRiskVesselMMSI_success();
        payloadCreator_CheckRiskVesselMMSI_failure();
        payloadCreator_CheckRiskVesselMMSI_multirisk_success();
        payloadCreator_CheckRiskVesselMMSI_multiRisk_failure();
        payloadCreator_CheckRiskVesselMMSI_noMMSI_failure();
        payloadCreator_CheckRiskVesselMMSI_noVessel_failure();
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_54_CHECK_RISK_VESSEL_MMSI.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_message() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid__multirisk_message() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_54_CHECK_RISK_VESSEL_MMSI.getUiCheckName());
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid__multirisk_message() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_54_CHECK_RISK_VESSEL_MMSI.getUiCheckName());
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_no_MMSI() {
        TAR report = testStep54CheckRiskVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noMMSI_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_CheckRiskVesselMMSI_success() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);

        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselMMSI_multirisk_success() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        Risk risk1 = new Risk();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel1 = new Risk.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel);
        risk1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        entities.add(risk1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselMMSI_multiRisk_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        Risk risk1 = new Risk();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710234L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel1 = new Risk.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        risk1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        entities.add(risk1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselMMSI_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710526L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);

        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselMMSI_noVessel_failure() {
        Risk risk = new Risk();

        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselMMSI_noMMSI_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noMMSI_failure.xml", xmlMapper, entities, Risk.class);
    }
}
