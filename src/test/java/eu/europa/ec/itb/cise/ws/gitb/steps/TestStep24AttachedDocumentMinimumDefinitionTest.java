package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.VesselDocument;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep24AttachedDocumentMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep24AttachedDocumentMinimumDefinitionTest extends CiseTestStepTest {



    private TestStep24AttachedDocumentMinimumDefinition testStep​24AttachedDocumentMinimumDefinition;


    @BeforeEach
    public void setUp() {
        super.setUp();
        // https://tools.ietf.org/html/rfc3986#section-1.1.1 for formal alternative of uri ipv6 url urn
        payloadCreator_AttachedDocumentMinimumDefinition_content_success();
        payloadCreator_AttachedDocumentMinimumDefinition_referenceUri_success();
        payloadCreator_AttachedDocumentMinimumDefinition_with_no_AttachedDocument_success();
        payloadCreator_AttachedDocumentMinimumDefinition_failure();
        testStep​24AttachedDocumentMinimumDefinition = new TestStep24AttachedDocumentMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_24_ATTACHED_DOCUMENT_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_AttachedDocumentMinimumDefinition_content_validation() {
        TAR report = testStep​24AttachedDocumentMinimumDefinition.createReport(utils.readFile("messages/push_Step24_AttachedDocumentMinimumDefinition_content_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_PortLocationMinimumDefinition_ReferenceURI_validation() {
        TAR report = testStep​24AttachedDocumentMinimumDefinition.createReport(utils.readFile("messages/push_Step24_AttachedDocumentMinimumDefinition_referenceUri_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_validation_with_no_AttachedDocument_message() {
        TAR report = testStep​24AttachedDocumentMinimumDefinition.createReport(utils.readFile("messages/push_Step24_AttachedDocumentMinimumDefinition_no_AttachedDocument_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PortLocationMinimumDefinition_validation() {
        TAR report = testStep​24AttachedDocumentMinimumDefinition.createReport(utils.readFile("messages/push_Step24_AttachedDocumentMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_AttachedDocumentMinimumDefinition_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle = new Vessel();
        // create document of type vesseldocument (to link to vessel)
        Document vesselDocument = new VesselDocument();
        /*here lacking minimal identification*/
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(vesselDocument);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step24_AttachedDocumentMinimumDefinition_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_AttachedDocumentMinimumDefinition_referenceUri_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle = new Vessel();
        // create document of type vesseldocument (to link to vessel)
        Document vesselDocument = new VesselDocument();
        try {
            ((VesselDocument) vesselDocument).setReferenceURI(new URI("ldap://[2001:db8::7]/c=GB?objectClass?one").toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(vesselDocument);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step24_AttachedDocumentMinimumDefinition_referenceUri_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_AttachedDocumentMinimumDefinition_with_no_AttachedDocument_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle = new Vessel();
        // create document of type vesseldocument (to link to vessel)
        Document vesselDocument = new VesselDocument();
        try {
            ((VesselDocument) vesselDocument).setReferenceURI(new URI("no?Cla=ss?one").toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step24_AttachedDocumentMinimumDefinition_no_AttachedDocument_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_AttachedDocumentMinimumDefinition_content_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle = new Vessel();
        // create document of type vesseldocument (to link to vessel)
        Document vesselDocument = new VesselDocument();
        ((VesselDocument) vesselDocument).setContent("content in the format allowed / not specified".getBytes());
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(vesselDocument);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step24_AttachedDocumentMinimumDefinition_content_success.xml", xmlMapper, maritimeSafetyIncident);
    }


}
