package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.person.Person;
import eu.cise.datamodel.v1.entity.person.PersonIdentificationType;
import eu.cise.datamodel.v1.entity.person.PersonIdentifier;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep11PersonMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep11PersonMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step11_PersonMinimumIdentification";

    private TestStep11PersonMinimumIdentification testStep11PersonMinimumIdentificationTest;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_PersonMinimumIdentification_failure();
        payloadCreator_PersonMinimumIdentification_success();
        payloadCreator_PersonMinimumIdentification_personalIdentifier_success();
        payloadCreator_PersonMinimumIdentification_personalIdentifier_failure();
        testStep11PersonMinimumIdentificationTest = new TestStep11PersonMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_11_PERSON_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PersonMinimumDefinition_validation() {
        TAR report = testStep11PersonMinimumIdentificationTest.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PersonMinimumDefinition_validation() {
        TAR report = testStep11PersonMinimumIdentificationTest.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_PersonMinimumIdentification_personalIdentifier_validation() {
        TAR report = testStep11PersonMinimumIdentificationTest.createReport(utils.readFile(TEST_FILE_PREFIX + "_personalIdentifier_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PersonMinimumIdentification_personalIdentifier_validation() {
        TAR report = testStep11PersonMinimumIdentificationTest.createReport(utils.readFile(TEST_FILE_PREFIX + "_personalIdentifier_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    public void payloadCreator_PersonMinimumIdentification_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Person person = new Person();
        person.setFullName("John Doe");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PersonMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Person person = new Person();
        // person.setFullName("John Doe"); // here where the test fail
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PersonMinimumIdentification_personalIdentifier_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Person person = new Person();
        PersonIdentifier personIdentifier = new PersonIdentifier();
        person.getPersonIdentifiers().add(personIdentifier); // here where the test fail as incomplete PersonIdentifier without IdentierType and IdentifierValue defined (not null or empty String)
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_personalIdentifier_failure.xml", xmlMapper, (Cargo) cargo);
    }

    private void payloadCreator_PersonMinimumIdentification_personalIdentifier_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        Person person = new Person();
        PersonIdentifier personIdentifier = new PersonIdentifier();
        personIdentifier.setIdentifierType(PersonIdentificationType.IDENTITY_CARD);
        personIdentifier.setIdentifierValue("x53246723e");
        person.getPersonIdentifiers().add(personIdentifier);
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(person);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_personalIdentifier_success.xml", xmlMapper, (Cargo) cargo);
    }

}
