package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep66LinkObjectAgentCargoUnit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep66LinkObjectAgentCargoUnitTest extends CiseTestStepTest {

    private TestStep66LinkObjectAgentCargoUnit testStep66LinkObjectAgentCargoUnit;

    private String testStepRefUiCheckName;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep66LinkObjectAgentCargoUnit = new TestStep66LinkObjectAgentCargoUnit(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_66_LINK_OBJECT_AGENT_CARGOUNIT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectAgentCargoUnit_validation() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step66_LinkObjectAgentCargoUnit_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_Person_to_Object_validation() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step66_LinkObjectAgentCargoUnit_Agent_to_Object_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_not_valid_AgentRole() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step66_LinkObjectAgentCargoUnit_wrong_AgentRole_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_TransitPassenger_defined() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step66_LinkObjectAgentCargoUnit_TransitPassenger_defined_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_Duty_defined() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step66_LinkObjectAgentCargoUnit_Duty_defined_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

}