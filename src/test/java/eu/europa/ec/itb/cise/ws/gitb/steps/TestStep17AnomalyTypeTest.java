package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep17AnomalyType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep17AnomalyTypeTest extends CiseTestStepTest {



    private TestStep17AnomalyType testStep17AnomalyType;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep17AnomalyType = new TestStep17AnomalyType(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_17_ANOMALY_TYPE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_AnomalyType_validation() {
        TAR report = testStep17AnomalyType.createReport(utils.readFile("messages/push_Step17_AnomalyType_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_Anomaly_validation() {
        TAR report = testStep17AnomalyType.createReport(utils.readFile("messages/push_Step17_AnomalyType_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


}
