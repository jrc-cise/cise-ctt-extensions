package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.cargo.CargoUnit;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep100BoundaryContainmentUnitTest extends CiseTestStepTestChildValueBoundaries {

    private final String testFilePrefix = "messages/push_Step_100_BoundaryContainmentUnitTest";
    private final String propertyFile = "src/main/resources/Proposed_Bnd_Objet.properties";
    private TestStep100BoundaryContainmentUnit testStep100BoundaryContainmentUnit;

    private static final String[] FIELDS = new String[]{
            "flashPoint",
            "grossQuantity",
            "netQuantity"};

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = false, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep100BoundaryContainmentUnit = new TestStep100BoundaryContainmentUnit(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_100_BOUNDARY_CONTAINMENTUNIT.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Objet");
        if (reinitCondition)
            payload_PropertyBoundary_maker(Vessel.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep100BoundaryContainmentUnit.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep100BoundaryContainmentUnit.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts100BoundaryContainmentUnit");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }

    @NotNull
    public Cargo getTestedEntity(String[] fields, boolean purposeSuccess) {
        Cargo cargo = new Cargo();
        CargoUnit cargoUnit1 = (ContainmentUnit) PropertyMaker.populateAttributes(errorBoundaries, ContainmentUnit.class, fields, purposeSuccess);
        Catch catch1 = new Catch();
        Cargo.ContainedCargoUnitRel containedCargoUnitRel = new Cargo.ContainedCargoUnitRel();
        containedCargoUnitRel.setCargoUnit(cargoUnit1);
        cargo.getContainedCargoUnitRels().add(containedCargoUnitRel);
        return cargo;
    }
}