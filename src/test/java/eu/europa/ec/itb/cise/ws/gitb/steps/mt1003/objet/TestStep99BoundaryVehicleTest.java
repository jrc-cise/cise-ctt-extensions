package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.object.Aircraft;
import eu.cise.datamodel.v1.entity.object.LandVehicle;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #98 Boundary Vessel
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `beam`,`breadth`,`containerCapacity`,`deadweight`,`depth`,`draught`,`grossTonnage`,`length`,`lengthenedYear`,`loa`,`netTonnage`,`segregatedBallastVolume`,`yearBuilt` _is  defined_
 * `*THEN*` *attribute*  _must be into accepted range _ .
 */

public class TestStep99BoundaryVehicleTest extends CiseTestStepTestChildValueBoundaries {

    private final String testFilePrefix = "messages/push_Step99_BoundaryVehicle";
    private final String propertyFile = "src/main/resources/Proposed_Bnd_Objet.properties";
    private TestStep99BoundaryVehicle testStep99BoundaryVehicule;
    private static final String[] FIELDS = new String[]{
            "totalPersonsOnBoard",
            "maximumSpeed"
    };

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep99BoundaryVehicule = new TestStep99BoundaryVehicle(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_99_BOUNDARY_VEHICLE.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Objet");
        if (reinitCondition)
            payload_PropertyBoundary_maker(Vessel.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep99BoundaryVehicule.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep99BoundaryVehicule.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts99BoundaryVehicle");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }


    @NotNull
    public Vehicle getTestedEntity(String[] fields, boolean purposeSuccess) {
        Vehicle vehicle1 = (Vehicle) PropertyMaker.populateAttributes(errorBoundaries, Aircraft.class, fields, purposeSuccess);
        Vehicle vehicle2 = (Vehicle) PropertyMaker.populateAttributes(errorBoundaries, LandVehicle.class, fields, purposeSuccess);
        Vehicle vehicle3 = (Vehicle) PropertyMaker.populateAttributes(errorBoundaries, Vessel.class, fields, purposeSuccess);
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle2);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        Agent.InvolvedObjectRel involvedObjectRel2 = new Agent.InvolvedObjectRel();
        involvedObjectRel2.setObject(vehicle3);
        organization.getInvolvedObjectRels().add(involvedObjectRel2);
        involvedAgentRel.setAgent(organization);
        vehicle1.getInvolvedAgentRels().add(involvedAgentRel);
        return vehicle1;
    }

}