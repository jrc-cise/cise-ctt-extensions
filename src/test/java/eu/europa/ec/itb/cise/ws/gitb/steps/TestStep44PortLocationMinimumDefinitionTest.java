package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep44PortLocationMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep44PortLocationMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep44PortLocationMinimumDefinition testStep44PortLocationMinimumDefinition;

    @BeforeEach
    public void setUp() {
        super.setUp();
        // https://tools.ietf.org/html/rfc3986#section-1.1.1 for formal alternative of uri ipv6 url urn
        payloadCreator_PortLocationMinimumDefinition_success();
        payloadCreator_PortLocationMinimumDefinition_failure();
        testStep44PortLocationMinimumDefinition = new TestStep44PortLocationMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_44_PORT_LOCATION_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortLocationMinimumDefinition_validation() {
        TAR report = testStep44PortLocationMinimumDefinition.createReport(utils.readFile("messages/push_Step44_PortLocationMinimumDefinition_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_PortLocationMinimumDefinition_validation() {
        TAR report = testStep44PortLocationMinimumDefinition.createReport(utils.readFile("messages/push_Step44_PortLocationMinimumDefinition_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_PortLocationMinimumDefinition_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create Location of type portLocation (to link to vessel)
        PortLocation portLocation = new PortLocation();
        portLocation.setPortName("goodPort");
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(portLocation);
        vehicle.getLocationRels().add(locationRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step44_PortLocationMinimumDefinition_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_PortLocationMinimumDefinition_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // don't create Location of type portLocation (to link to vessel)
        PortLocation portLocation = new PortLocation();
        //portLocation.setPortName("goodPort");
        Objet.LocationRel locationRel = new Objet.LocationRel();
        locationRel.setLocation(portLocation);
        vehicle.getLocationRels().add(locationRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step44_PortLocationMinimumDefinition_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

}
