package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #96 Format Agent ContactInformation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Agent`  (+ allchildren) is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `ContactInformation` is  defined
 * `*THEN*` *attribute* `ContactInformation` must be composed according to VCARD standard format.
 */
public class TestStep96FormatAgentContactinformationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step96_FormatAgentContactinformation";
    private TestStep96FormatAgentContactinformation testStep96FormatAgentContactinformation;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep96FormatAgentContactinformation = new TestStep96FormatAgentContactinformation(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_96_FORMAT_AGENT_CONTACTINFORMATION.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_warning();
        payloadCreator_FormatCatchSpeciesTest_failure();
        payloadCreator_FormatCatchSpeciesTest_organization_success();
        payloadCreator_FormatCatchSpeciesTest_person_success();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_organizationvcard_report_with_correct_validation() {
        TAR report = testStep96FormatAgentContactinformation.createReport(Utils.readFile(TEST_FILE_PREFIX + "_organization_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_personvcard_report_with_correct_validation() {
        TAR report = testStep96FormatAgentContactinformation.createReport(Utils.readFile(TEST_FILE_PREFIX + "_person_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep96FormatAgentContactinformation.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_warning_report_with_correct_validation() {
        TAR report = testStep96FormatAgentContactinformation.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_organization_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : here should have a contact well setted
        organization.setContactInformation("BEGIN:VCARD\r\n" +
                "VERSION:4.0\r\n" +
                "ORG:ABC\\, Inc.;North American Division;Marketing\r\n" +
                "END:VCARD\r\n");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_organization_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_person_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : here should have a contact well setted
        organization.setContactInformation("BEGIN:VCARD\r\n" +
                "VERSION:4.0\r\n" +
                "N:Doe;Jonathan;;Mr;\r\n" +
                "FN:John Doe\r\n" +
                "END:VCARD\r\n");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_person_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : should have a contact badly setted
        organization.setContactInformation(
                "BEGIN:VCARD\r\n" +
                        "N:;;;;;;;Jonathan;;Mr;\r\n" +
                        "SE:John Doe\r\n" +
                        "\r\n");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_warning() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : should have a contact badly setted
        organization.setContactInformation(
                "BEGIN:VCARD\r\n" +
                        "END:VCARD\r\n");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning.xml", xmlMapper, vessel);
    }

}