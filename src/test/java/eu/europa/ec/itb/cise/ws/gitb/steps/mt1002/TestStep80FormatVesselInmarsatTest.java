package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;

import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #80 FORMAT Vessel InMarsat Validation
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `InMarsat` is defined
 * `*THEN*` attribute `InMarsat` must follow a predefined format.
 */
public class TestStep80FormatVesselInmarsatTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step80_FormatVesselInmarsa";
    private TestStep80FormatVesselInmarsat testStep80FormatVesselInmarsa;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep80FormatVesselInmarsa = new TestStep80FormatVesselInmarsat(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_80_FORMAT_VESSEL_INMARSAT.getUiCheckName();
        payloadCreator_FormatVesselInmarsa_failure();
        payloadCreator_FormatVesselInmarsa_success();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep80FormatVesselInmarsa.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep80FormatVesselInmarsa.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatVesselInmarsa_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setINMARSATNumber("0087945454584");// success factor no space -.' sign and letters
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatVesselInmarsa_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Vessel vessel1 = new Vessel();
        vessel1.setINMARSATNumber("087945454584");// failure factor  space + extra signs
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

}