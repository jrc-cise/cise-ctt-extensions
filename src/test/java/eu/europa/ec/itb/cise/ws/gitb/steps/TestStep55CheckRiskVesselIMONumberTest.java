package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep55CheckRiskVesselIMONumberTest extends CiseTestStepTest {

    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step55_CheckRiskVesselIMONumber";

    private TestStep55CheckRiskVesselIMONumber testStep55CheckRiskVesselIMONumber;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep55CheckRiskVesselIMONumber = new TestStep55CheckRiskVesselIMONumber(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckRiskVesselIMONumber_success();
        payloadCreator_CheckRiskVesselIMONumber_failure();
        payloadCreator_CheckRiskVesselIMONumber_multirisk_success();
        payloadCreator_CheckRiskVesselIMONumber_multiRisk_failure();
        payloadCreator_CheckRiskVesselIMONumber_noMMSI_failure();
        payloadCreator_CheckRiskVesselIMONumber_noVessel_failure();
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_55_CHECK_RISK_VESSEL_IMO_NUMBER.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_message() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid__multirisk_message() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_55_CHECK_RISK_VESSEL_IMO_NUMBER.getUiCheckName());
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid__multirisk_message() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml"), "7710525");
        // will give only first so if message report have 2 item and an error is second while valid is first does not ring the bell
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_55_CHECK_RISK_VESSEL_IMO_NUMBER.getUiCheckName());
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_noIMONumber() {
        TAR report = testStep55CheckRiskVesselIMONumber.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noIMONumber_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_CheckRiskVesselIMONumber_success() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_multirisk_success() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        Risk risk1 = new Risk();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel1 = new Risk.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        risk1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        entities.add(risk1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_multiRisk_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        Risk risk1 = new Risk();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710526L);
        Vessel vessel2;
        vessel2 = new Vessel();
        vessel2.setIMONumber(7710527L);
        //!!! we  add 2 vessel to the second
        Risk.InvolvedObjectRel involvedObjectRel1 = new Risk.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        risk1.getInvolvedObjectRels().add(involvedObjectRel1);
        //!!!
        Risk.InvolvedObjectRel involvedObjectRel2 = new Risk.InvolvedObjectRel();
        involvedObjectRel2.setObject(vessel2);
        risk1.getInvolvedObjectRels().add(involvedObjectRel2);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        entities.add(risk1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710526L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_noVessel_failure() {
        Risk risk = new Risk();

        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckRiskVesselIMONumber_noMMSI_failure() {
        Risk risk = new Risk();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(risk);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noIMONumber_failure.xml", xmlMapper, entities, Risk.class);
    }
}
