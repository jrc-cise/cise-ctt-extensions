package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.FormalOrganization;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep15FormalOrganizationMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep15FormalOrganizationMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step15_FormalOrganizationMinimumIdentification";

    private TestStep15FormalOrganizationMinimumIdentification testStep15FormalOrganizationMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_FormalOrganizationMinimumIdentification_failure();
        payloadCreator_FormalOrganizationMinimumIdentification_success();
        testStep15FormalOrganizationMinimumIdentification = new TestStep15FormalOrganizationMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_15_FORMAL_ORGANIZATION_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep15FormalOrganizationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep15FormalOrganizationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormalOrganizationMinimumIdentification_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        FormalOrganization formalOrganization = new FormalOrganization();
        formalOrganization.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(formalOrganization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_FormalOrganizationMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        FormalOrganization formalOrganization = new FormalOrganization();
        //no set child node value
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(formalOrganization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
