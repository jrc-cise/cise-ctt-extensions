package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
* #87 FORMAT PortLocation LocationCode
* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* `*GIVEN*` *entity(ies)* of type `PortLocation` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
* `*AND*` attribute `LocationCode` is defined
* `*THEN*` attribute `LocationCode` must be composed by first 2 Letters in accordance with iso-3160-1 and then following 3 Character identifying location in this country.
*/
public class TestStep87FormatPortlocationLocationcodeTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step87_FormatPortlocationLocationcode";
    private TestStep87FormatPortlocationLocationcode testStep87FormatPortlocationLocationcode;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep87FormatPortlocationLocationcode = new TestStep87FormatPortlocationLocationcode(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_87_FORMAT__PORTLOCATION_LOCATIONCODE.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_success();
        payloadCreator_FormatCatchSpeciesTest_failure();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep87FormatPortlocationLocationcode.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep87FormatPortlocationLocationcode.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setLocationCode("FRPFR");// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portLocation);
        organization.getLocationRels().add(locationRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        PortLocation portLocation = new PortLocation();
        portLocation.setLocationCode("FXSRL");// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(portLocation);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

}