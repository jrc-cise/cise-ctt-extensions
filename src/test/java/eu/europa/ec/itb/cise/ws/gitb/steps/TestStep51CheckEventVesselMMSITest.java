package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep51CheckEventVesselMMSITest extends CiseTestStepTest {

    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step51_CheckEventVesselMMSI";

    private TestStep51CheckEventVesselMMSI testStep51CheckEventVesselMMSI;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep51CheckEventVesselMMSI = new TestStep51CheckEventVesselMMSI(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckEventVesselMMSI_success();
        payloadCreator_CheckEventVesselMMSI_failure();
        payloadCreator_CheckEventVesselMMSI_multirisk_success();
        payloadCreator_CheckEventVesselMMSI_multiRisk_failure();
        payloadCreator_CheckEventVesselMMSI_noMMSI_failure();
        payloadCreator_CheckEventVesselMMSI_noVessel_failure();
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_51_CHECK_EVENT_VESSEL_MMSI.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid_message() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_simple_content_for_valid__multirisk_message() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_51_CHECK_EVENT_VESSEL_MMSI.getUiCheckName());
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid__multirisk_message() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_51_CHECK_EVENT_VESSEL_MMSI.getUiCheckName());
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_not_valid_message() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_with_simple_content_for_no_vessel_inside() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_simple_content_for_vessel_with_no_MMSI() {
        TAR report = testStep51CheckEventVesselMMSI.createReport(utils.readFile(TEST_FILE_RESPONSE_PREFIX + "_noMMSI_failure.xml"), "7710525");
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_CheckEventVesselMMSI_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);

        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_success.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }

    public void payloadCreator_CheckEventVesselMMSI_multirisk_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        MaritimeSafetyIncident maritimeSafetyIncident1 = new MaritimeSafetyIncident();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel1 = new Event.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel);
        maritimeSafetyIncident1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        entities.add(maritimeSafetyIncident1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_success.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckEventVesselMMSI_multiRisk_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 risk to entities arrayList
        MaritimeSafetyIncident maritimeSafetyIncident1 = new MaritimeSafetyIncident();
        Vessel vessel1;
        vessel1 = new Vessel();
        vessel1.setIMONumber(7710234L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel1 = new Event.InvolvedObjectRel();
        involvedObjectRel1.setObject(vessel1);
        maritimeSafetyIncident1.getInvolvedObjectRels().add(involvedObjectRel1);
        // finally add 1 risk to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        entities.add(maritimeSafetyIncident1);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_multirisk_failure.xml", xmlMapper, entities, Risk.class);
    }

    public void payloadCreator_CheckEventVesselMMSI_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setMMSI(7710526L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);

        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }

    public void payloadCreator_CheckEventVesselMMSI_noVessel_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();

        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noVessel_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }

    public void payloadCreator_CheckEventVesselMMSI_noMMSI_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vessel vessel;
        vessel = new Vessel();
        vessel.setIMONumber(7710525L);
        // finally add vessel to first maritime incident (to link to vessel)
        Event.InvolvedObjectRel involvedObjectRel = new Event.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        // finally add 1 maritimeSafetyIncident to entities arrayList
        ArrayList<Entity> entities = new ArrayList<Entity>();
        entities.add(maritimeSafetyIncident);
        utils.writePullResponseMessageFiles(TEST_FILE_RESPONSE_PREFIX + "_noMMSI_failure.xml", xmlMapper, entities, MaritimeSafetyIncident.class);
    }
}
