package eu.europa.ec.itb.cise.ws.gitb.steps;

import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.config.CTTCiseExtensionAppContext;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.GitbErrorHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import org.apache.commons.lang3.tuple.Triple;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static eu.europa.ec.itb.cise.ws.Utils.getMessagePushBytes;
import static org.mockito.Mockito.mock;

public abstract class CiseTestStepTestChildValueBoundaries {

    protected CisePayloadHelper payloadHelper;
    protected ReportBuilder reportBuilder;
    protected XmlMapper xmlMapper;
    protected SignatureService signatureService;
    protected String testStepRefUiCheckName;
    protected GitbErrorHelper errorHelper;
    protected Properties errorBoundaries = null;
    protected final Utils utils = new Utils();

    public void setUp() {
        xmlMapper = new DefaultXmlMapper.PrettyNotValidating();
        signatureService = mock(SignatureService.class);
        payloadHelper = new CisePayloadHelper(xmlMapper);
        errorHelper = new GitbErrorHelper(utils.labels,
                CTTCiseExtensionAppContext.getboundariesResourceBundles(CTTCiseExtensionAppContext.class.getClassLoader()),
                xmlMapper);
        reportBuilder = new ReportBuilder(xmlMapper, signatureService, null);
    }

    protected abstract Entity getTestedEntity(String[] fields, boolean expectedDefault);

    public String payloadGetter_BoundaryTest_success(String[] fields) {
        Entity vessel = getTestedEntity(fields, true);
        return new String(getMessagePushBytes(utils, xmlMapper, vessel));
    }

    public String payloadGetter_BoundaryTest_failure(String[] fields) {
        Entity vessel = getTestedEntity(fields, false);
        return new String(getMessagePushBytes(utils, xmlMapper, vessel));
    }

    public void payloadCreator_BoundaryTest_success(String testFilePrefix, String[] fields) {
        Entity entity = getTestedEntity(fields, true);
        utils.writePushMessageFiles(testFilePrefix + "_success.xml", xmlMapper, entity);
    }

    public void payloadCreator_BoundaryTest_failure(String testFilePrefix, String[] fields) {
        Entity entity = getTestedEntity(fields, false);
        utils.writePushMessageFiles(testFilePrefix + "_failure.xml", xmlMapper, entity);
    }


    public void payload_PropertyBoundary_maker(Class entityClass, String propertyField) {
        HashMap<String, Triple<Class, String, String>> fieldsWithBoundaries = null;
        try {
            fieldsWithBoundaries = generateListOfBoundableFields(entityClass);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
        if (fieldsWithBoundaries != null)
            PropertyMaker.write(entityClass.toString(), propertyField, fieldsWithBoundaries);
    }

    public static HashMap<String, Triple<Class, String, String>> generateListOfBoundableFields(Class entityClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Entity entity;
        entity = (Entity) entityClass.getConstructor().newInstance();
        ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
        Class entityParentClass = entityClass.getSuperclass();
        while (!(entityParentClass.equals(Entity.class))) {
            Field[] entityParentFields = entityParentClass.getDeclaredFields();
            entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityParentFields)));
            entityParentClass = entityParentClass.getSuperclass();
        }
        HashMap<String, Triple<Class, String, String>> isBoundable = new HashMap<>();
        for (Field field : entityFields) {
            field.setAccessible(true);
            Object content = null;
            if (("serialVersionUID").equals(field.getName()))
                continue;
            try {
                content = field.get(entity);
            } catch (IllegalAccessException eIAE) {
                content = null;
            }
            if (content == null) {
                switch (field.getType().toString()) {
                    case ("double"):
                    case ("class java.lang.Double"):
                        Triple<Class, String, String> valueT1 = new TypeBoundaryDefinition(
                                java.lang.Double.class,
                                "0.0",
                                "1.0");
                        isBoundable.put((field.getName()), valueT1);
                        break;
                    case ("long"):
                    case ("class java.lang.Long"):
                        Triple<Class, String, String> valueT2 = new TypeBoundaryDefinition(
                                java.lang.Long.class,
                                "0L",
                                "1L");
                        isBoundable.put((field.getName()), valueT2);
                        break;
                    case ("int"):
                    case ("class java.lang.Integer"):
                        Triple<Class, String, String> valueT3 = new TypeBoundaryDefinition(
                                java.lang.Integer.class,
                                "0",
                                "1");
                        isBoundable.put((field.getName()), valueT3);
                        break;
                    default:
                        break;
                }
            }
        }
        return isBoundable;
    }

    public static class PropertyMaker {


        public static void write(String signature, String pathName, HashMap<String, Triple<Class, String, String>> mapTypeMinMax) {
            try {
                Properties prop = new Properties();
                Set<String> keySet = mapTypeMinMax.keySet();
                keySet.forEach(s -> {
                    writebBlock(prop, s, ((TypeBoundaryDefinition) mapTypeMinMax.get(s)));
                });
                Properties tmp = new Properties() {
                    @Override
                    public synchronized Enumeration<Object> keys() {
                        Comparator<Object> byCaseInsensitiveString = Comparator.comparing(Object::toString,
                                String.CASE_INSENSITIVE_ORDER);
                        Supplier<TreeSet<Object>> supplier = () -> new TreeSet<>(byCaseInsensitiveString);
                        TreeSet<Object> sortedSet = super.keySet().stream()
                                .collect(Collectors.toCollection(supplier));
                        return Collections.enumeration(sortedSet);
                    }
                };
                tmp.putAll(prop);
                try (FileOutputStream fileOutputStream = new FileOutputStream(new File(pathName), true)) {
                    tmp.store(fileOutputStream, "" + signature);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static void writebBlock(Properties prop, String key, TypeBoundaryDefinition typeBoundaryDefinition) {
            prop.setProperty(key + ".type", typeBoundaryDefinition.getLeft().toString().split(" ")[1]);
            prop.setProperty(key + ".unit", "unit(s)");
            prop.setProperty(key + ".min", typeBoundaryDefinition.getMiddle());
            prop.setProperty(key + ".max", typeBoundaryDefinition.getRight());
        }

        public static Entity populateAttributes(Properties prop, Class entityClass, String[] fields, boolean purposeSuccess) {
            List<String> fieldsList = Arrays.asList(fields);
            Entity entity = null;
            try {
                entity = (Entity) entityClass.getConstructor().newInstance();
                ArrayList<Field> entityFields = new ArrayList<Field>(Arrays.asList(entityClass.getDeclaredFields()));
                entityFields.addAll(new ArrayList<Field>(Arrays.asList(entityClass.getSuperclass().getDeclaredFields())));
                for (Field field : entityFields) {
                    field.setAccessible(true);
                    Object content = null;
                    try {
                        content = field.get(entity);
                    } catch (IllegalAccessException eIAE) {
                        content = null;
                    }
                    if (fieldsList.contains(field.getName())) {

                        String max = prop.getProperty(field.getName() + ".max"),
                                min = prop.getProperty(field.getName() + ".min");
                        Integer MAXI = Math.toIntExact(Math.round(Double.parseDouble(max)) - Math.round(Double.parseDouble(min)));
                        Integer randI = Math.toIntExact(Math.round(Math.abs(Math.random() * MAXI)));
                        Double randD = Math.abs(Math.random() * MAXI);
                        if (purposeSuccess) {
                            switch (prop.getProperty(field.getName() + ".type")) {
                                case ("java.lang.Long"):
                                case ("java.lang.Integer"):
                                    Integer correctI = Integer.parseInt(min) + 1 + randI;
                                    int maxAsInt = Math.toIntExact(Math.round(Double.parseDouble(max)));
                                    if (correctI >= maxAsInt) {
                                        correctI = maxAsInt - 1;
                                    }
                                    field.set(entity, correctI);
                                    break;
                                case ("java.lang.Double"):
                                    Double correctD = Double.parseDouble(min) + randD - 0.000001;
                                    field.set(entity, correctD);
                                    break;
                            }
                        } else {
                            switch (prop.getProperty(field.getName() + ".type")) {
                                case ("java.lang.Integer"):
                                case ("java.lang.Long"):
                                    Integer downI = Integer.parseInt(min) - randI;
                                    Integer upI = Integer.parseInt(max) + randI;
                                    if (Math.random() >= 0.5) field.set(entity, upI);
                                    else field.set(entity, downI);
                                    break;
                                case ("java.lang.Double"):

                                    Double downD = Double.parseDouble(min) - randD;
                                    Double upD = Double.parseDouble(max) + randD;
                                    if (Math.random() >= 0.5) field.set(entity, upD);
                                    else field.set(entity, downD);
                                    break;
                            }
                        }
                    }
                }
            } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                System.out.println(e.getMessage());
            }

            return entity;
        }
    }

    static class TypeBoundaryDefinition extends Triple<Class, String, String> {
        private final Class left;
        private final String middle;
        private final String right;

        TypeBoundaryDefinition(Class classType, String min, String max) {
            this.left = classType;
            this.middle = min;
            this.right = max;
        }


        @Override
        public Class getLeft() {
            return left;
        }

        @Override
        public String getMiddle() {
            return middle;
        }

        @Override
        public String getRight() {
            return right;
        }

    }
}
