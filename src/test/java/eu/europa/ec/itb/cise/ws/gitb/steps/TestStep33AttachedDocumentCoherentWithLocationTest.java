package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep33AttachedDocumentCoherentWithLocation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class TestStep33AttachedDocumentCoherentWithLocationTest extends CiseTestStepTest {


    private TestStep33AttachedDocumentCoherentWithLocation testStep33AttachedDocumentCoherentWithLocation;

    private Push push;

    @BeforeEach
    public void setUp() {
        super.setUp();
        push = utils.getPush();
        testStep33AttachedDocumentCoherentWithLocation = new TestStep33AttachedDocumentCoherentWithLocation(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_33_ATTACHED_DOCUMENT_COHERENT_WITH_LOCATION.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_for_valid_Document_type() {
        String messageXml = utils.readFile("messages/push_Step33_AttachedDocumentCoherentWithLocation_success.xml");

        TAR report = testStep33AttachedDocumentCoherentWithLocation.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_success_report_for_Document_with_no_Location() {
        String messageXml = utils.readFile("messages/push_Step33_AttachedDocumentCoherentWithLocation_Document_with_no_Location_success.xml");

        TAR report = testStep33AttachedDocumentCoherentWithLocation.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_for_Document_of_wrong_type() {
        String messageXml = utils.readFile("messages/push_Step33_AttachedDocumentCoherentWithLocation_failure.xml");

        TAR report = testStep33AttachedDocumentCoherentWithLocation.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_for_one_Document_out_of_many_of_wrong_type() {
        String messageXml = utils.readFile("messages/push_Step33_AttachedDocumentCoherentWithLocation_one_Document_out_of_many_failure.xml");

        TAR report = testStep33AttachedDocumentCoherentWithLocation.createReport(messageXml);

        String reportResult = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(reportResult).isNotEqualTo("VALID");
    }


}
