package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_01_IN_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_01_IN_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_03_OUT_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_03_OUT_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_04_IN_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_04_IN_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_05_OUT_AREA_LAT;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POINT_05_OUT_AREA_LON;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.POLYGON_SEARCH_AREA;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.SAMPLE_VESSEL_ONE_IMO_NUMBER;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.SAMPLE_VESSEL_TWO_IMO_NUMBER;
import static eu.europa.ec.itb.cise.ws.gitb.steps.TestStep50CheckEventVesselLocationTest.getSampleVessel;
import static org.assertj.core.api.Assertions.assertThat;

public class TestStep53CheckRiskVesselLocationTest extends CiseTestStepTest {

    private static final String TEST_FILE_REQUEST_PREFIX = "messages/pullRequest_Step53_CheckRiskVesselLocation";
    private static final String TEST_FILE_RESPONSE_PREFIX = "messages/pullResponse_Step53_CheckRiskVesselLocation";

    private TestStep53CheckRiskVesselLocation testStep53CheckRiskVesselLocation;

    private String pullRequest = "";

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep53CheckRiskVesselLocation = new TestStep53CheckRiskVesselLocation(reportBuilder, payloadHelper, errorHelper);
        payloadCreator_CheckRiskVesselLocation_request();
        pullRequest = utils.readFile(TEST_FILE_REQUEST_PREFIX + ".xml");
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_53_CHECK_RISK_VESSEL_LOCATION.getUiCheckName();
    }

    @Test
    public void it_creates_a_success_report_with_one_empty_event() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_one_empty_event();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_one_empty_event() {
        Risk risk = new Risk();
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_one_empty_event.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon() {
        Risk risk = new Risk();
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_one_event_containing_two_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon() {
        Risk missRisk = new Risk();
        Risk emptyRisk = new Risk();
        createSampleVesselAndAddItToRisk(missRisk, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_but_only_one_of_them_have_one_location_outside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(missRisk, emptyRisk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon() {
        Risk hitRisk = new Risk();
        Risk emptyRisk = new Risk();
        createSampleVesselAndAddItToRisk(hitRisk, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_where_one_of_them_have_one_location_inside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(emptyRisk, hitRisk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon() {
        Risk missRisk1 = new Risk();
        Risk missRisk2 = new Risk();
        createSampleVesselAndAddItToRisk(missRisk1, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_03_OUT_AREA_LAT, POINT_03_OUT_AREA_LON);
        createSampleVesselAndAddItToRisk(missRisk2, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_and_two_locations_outside_of_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(missRisk1, missRisk2)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon() {
        Risk hitRisk = new Risk();
        Risk missRisk = new Risk();
        createSampleVesselAndAddItToRisk(hitRisk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(missRisk, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_two_events_where_only_one_location_hits_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitRisk, missRisk)), Risk.class);
        return utils.readFile(path);
    }


    @Test
    public void it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon() {
        Risk emptyRisk = new Risk();
        Risk hitRisk = new Risk();
        createSampleVesselAndAddItToRisk(hitRisk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(hitRisk, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_where_first_event_is_empty_and_second_have_two_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(emptyRisk, hitRisk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon() {
        Risk hitRisk1 = new Risk();
        Risk hitRisk2 = new Risk();
        createSampleVesselAndAddItToRisk(hitRisk1, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(hitRisk2, SAMPLE_VESSEL_TWO_IMO_NUMBER, POINT_04_IN_AREA_LAT, POINT_04_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_each_containing_a_vessel_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitRisk1, hitRisk2)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.SUCCESS);
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    private String payloadCreator_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon() {
        Risk hitRisk = new Risk();
        Risk hitAndMissRisk = new Risk();
        createSampleVesselAndAddItToRisk(hitRisk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(hitAndMissRisk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        createSampleVesselAndAddItToRisk(hitAndMissRisk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_01_IN_AREA_LAT, POINT_01_IN_AREA_LON);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_success_report_with_two_events_that_contain_locations_in_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(hitRisk, hitAndMissRisk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format() {
        Risk risk = new Risk();
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_with_one_event_containing_location_in_unexpected_format.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_warning_report_with_one_event_containing_malformed_location() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_warning_report_with_one_event_containing_malformed_location();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.WARNING);
    }

    private String payloadCreator_it_creates_a_warning_report_with_one_event_containing_malformed_location() {
        Risk risk = new Risk();
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_ONE_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_warning_report_with_one_event_containing_malformed_location.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
        return utils.readFile(path);
    }

    @Test
    public void it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon() {
        //arrange
        String pullResponse = payloadCreator_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon();

        //act
        TAR report = testStep53CheckRiskVesselLocation.createReport(pullResponse, pullRequest);

        //asses
        assertThat(report.getResult()).isEqualTo(TestResultType.FAILURE);
    }

    private String payloadCreator_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon() {
        Risk risk = new Risk();
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POINT_05_OUT_AREA_LAT, POINT_05_OUT_AREA_LON);
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_TWO_IMO_NUMBER, "<POLYGON>");
        String path = TEST_FILE_RESPONSE_PREFIX + "_it_creates_a_fail_report_with_one_event_containing_malformed_location_and_location_outside_polygon.xml";
        utils.writePullResponseMessageFiles(path, xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
        return utils.readFile(path);
    }

    private void payloadCreator_CheckRiskVesselLocation_request() {
        Risk risk = new Risk();
        createSampleVesselAndAddItToRisk(risk, SAMPLE_VESSEL_ONE_IMO_NUMBER, POLYGON_SEARCH_AREA);
        utils.writePullRequestMessageFiles(TEST_FILE_REQUEST_PREFIX + ".xml", xmlMapper, new ArrayList<>(Arrays.asList(risk)), Risk.class);
    }

    public void createSampleVesselAndAddItToRisk(Risk risk, long vesselImoNumber, String polygonSearchArea) {
        Vessel vessel = getSampleVessel(vesselImoNumber, polygonSearchArea);
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
    }

    public void createSampleVesselAndAddItToRisk(Risk risk, long vesselImoNumber, String latitude, String longitude) {
        Vessel vessel = getSampleVessel(vesselImoNumber, latitude, longitude);
        Risk.InvolvedObjectRel involvedObjectRel = new Risk.InvolvedObjectRel();
        involvedObjectRel.setObject(vessel);
        risk.getInvolvedObjectRels().add(involvedObjectRel);
    }

}
