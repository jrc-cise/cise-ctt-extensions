package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep65LinkObjectAgentVehicle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep65LinkObjectAgentVehicleTest extends CiseTestStepTest {

    private TestStep65LinkObjectAgentVehicle testStep65LinkObjectAgentVehicle;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep65LinkObjectAgentVehicle = new TestStep65LinkObjectAgentVehicle(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_65_LINK_OBJECT_AGENT_VEHICLE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectAgentVehicle_validation() {
        TAR report = testStep65LinkObjectAgentVehicle.createReport(utils.readFile("messages/push_Step65_LinkObjectAgentVehicle_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_Person_to_Object_validation() {
        TAR report = testStep65LinkObjectAgentVehicle.createReport(utils.readFile("messages/push_Step65_LinkObjectAgentVehicle_Agent_to_Object_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_not_valid_AgentRole() {
        TAR report = testStep65LinkObjectAgentVehicle.createReport(utils.readFile("messages/push_Step65_LinkObjectAgentVehicle_wrong_AgentRole_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_no_Person_defined() {
        TAR report = testStep65LinkObjectAgentVehicle.createReport(utils.readFile("messages/push_Step65_LinkObjectAgentVehicle_no_person_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

}