package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.metadata.FileMediaType;
import eu.cise.datamodel.v1.entity.metadata.Metadata;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep25IdentificationOfDocumentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep25IdentificationOfDocumentTypeTest extends CiseTestStepTest {

    private TestStep25IdentificationOfDocumentType testStep25IdentificationOfDocumentType;

    private String TEST_FILE_PREFIX;

    @BeforeEach
    public void setUp() {
        TEST_FILE_PREFIX = "messages/push_Step25_IdentificationOfDocumentType";
        super.setUp();
        // https://tools.ietf.org/html/rfc3986#section-1.1.1 for formal alternative of uri ipv6 url urn
        payloadCreator_IdentificationOfDocumentType_failure();
        payloadCreator_IdentificationOfDocumentType_success();
        testStep25IdentificationOfDocumentType = new TestStep25IdentificationOfDocumentType(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_25_IDENTIFICATION_OF_DOCUMENT_TYPE.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_IdentificationOfDocumentType_validation() {
        TAR report = testStep25IdentificationOfDocumentType.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_IdentificationOfDocumentType_validation() {
        TAR report = testStep25IdentificationOfDocumentType.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_IdentificationOfDocumentType_success() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        Document document = new Document();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        // success factor
        Metadata metadata = new Metadata();
        metadata.setFileMediaType(FileMediaType.fromValue("video/avi"));
        document.getMetadatas().add(metadata);
        // linkage
        documentRel.setDocument(document);
        cargo.getDocumentRels().add(documentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_IdentificationOfDocumentType_failure() {
        Cargo cargo = new Cargo();
        // here should have event to link to a objet
        Document document = new Document();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        // un/success factor
//        Metadata metadata=new Metadata();
//        metadata.setFileMediaType(FileMediaType.fromValue("zip"));
//        document.getMetadatas().add(metadata);
        // linkage
        documentRel.setDocument(document);
        cargo.getDocumentRels().add(documentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
