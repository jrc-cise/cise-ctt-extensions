package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #97 Format Agent Nationality
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Agent` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `Nationality` _is  defined_
 * `*THEN*` *attribute* `Nationality` _must be composed_ with 2 upper characters format
 * `*AND*` *attribute* `Nationality` _should be present_ in ISO-3166 reference list.
 */
public class TestStep97FormatAgentNationalityTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step97_FormatAgentNationality";
    private TestStep97FormatAgentNationality testStep97FormatAgentNationality;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep97FormatAgentNationality = new TestStep97FormatAgentNationality(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_97_FORMAT_AGENT_NATIONALITY.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_warning();
        payloadCreator_FormatCatchSpeciesTest_failure();
        payloadCreator_FormatCatchSpeciesTest_success();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep97FormatAgentNationality.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep97FormatAgentNationality.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_warning_report_with_correct_validation() {
        TAR report = testStep97FormatAgentNationality.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : here should have a Nationality well setted
        organization.getNationalities().add("FRA");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }


    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : should have a Nationality badly setted
        organization.getNationalities().add("ER3");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_warning() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        // success factor : should have a Nationality badly setted
        organization.getNationalities().add("XSR");
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning.xml", xmlMapper, vessel);
    }

}