package eu.europa.ec.itb.cise.ws.transport;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.ms.SendRequest;
import com.gitb.ms.SendResponse;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.MessageIdStore;
import eu.europa.ec.itb.cise.ws.domain.MessageProcessor;
import eu.europa.ec.itb.cise.ws.gitb.CisePayloadHelper;
import eu.europa.ec.itb.cise.ws.gitb.ReportBuilder;
import eu.europa.ec.itb.cise.ws.gitb.SessionManager;
import eu.europa.ec.itb.cise.ws.util.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static eu.eucise.helpers.AckBuilder.newAck;
import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OutgoingMessagesServiceImplTest {

    private SendRequest sendRequest;
    private Acknowledgement acknowledgement;
    private Push push;

    private OutgoingMessagesServiceImpl messagingService;
    private CisePayloadHelper payloadHelper;
    private SessionManager sessionManager;
    private XmlMapper xmlMapper;
    private MessageProcessor messageProcessor;
    private ReportBuilder reportBuilder;
    private SignatureService signature;
    private MessageIdStore messageIdStore;

    @BeforeEach
    public void setUp() throws Exception {
        sendRequest = mock(SendRequest.class);
        payloadHelper = new CisePayloadHelper(xmlMapper);
        sessionManager = mock(SessionManager.class);
        xmlMapper = new DefaultXmlMapper.NotValidating();
        messageProcessor = mock(MessageProcessor.class);
        messageIdStore = mock(MessageIdStore.class);
        signature = mock(SignatureService.class);
        reportBuilder = new ReportBuilder(xmlMapper, signature, messageIdStore);
        messagingService = new OutgoingMessagesServiceImpl(sessionManager, messageIdStore, xmlMapper, messageProcessor, reportBuilder);
        acknowledgement = newAck().id("ack-message-id").sender(newService()).build();
        push = newPush().id("push-message-id").sender(newService()).build();
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void it_return_a_sendResponse_with_the_acknowledge_received_on_successful_send() {
        Pair<Acknowledgement, Message> messageProcessorSendResult = new Pair(acknowledgement, push);
        when(sendRequest.getInput()).thenReturn(buildParameters());
        when(messageIdStore.getAndRecord()).thenReturn("message-id");
        when(sessionManager.getSessionInfo(any(), any())).thenReturn("htt://localhost");
        when(messageProcessor.send(any(), any())).thenReturn(messageProcessorSendResult);

        SendResponse sendResponse = messagingService.send(sendRequest);

        String ackString = null;
        for (AnyContent anyContent : sendResponse.getReport().getContext().getItem()) {
            if (ReportBuilder.REPORT_ELEMENT_SYNC_ACKNOWLEDGEMENT_FOR_SENT_MESSAGE.equals(anyContent.getName())) {
                ackString = anyContent.getValue();
            }
        }
        if (ackString != null) {
            Acknowledgement ack = xmlMapper.fromXML(ackString);
            assertThat((ack)).isEqualTo(acknowledgement);
        } else {
            fail("No acknowledgement found.");
        }
    }

    @Test
    public void it_return_a_sendResponse_with_the_message_sent_on_successful_send() {
        Pair<Acknowledgement, Message> messageProcessorSendResult = new Pair(acknowledgement, push);
        when(sendRequest.getInput()).thenReturn(buildParameters());
        when(messageIdStore.getAndRecord()).thenReturn("message-id");
        when(sessionManager.getSessionInfo(any(), any())).thenReturn("htt://localhost");
        when(messageProcessor.send(any(), any())).thenReturn(messageProcessorSendResult);

        SendResponse sendResponse = messagingService.send(sendRequest);

        String messageString = null;
        for (AnyContent anyContent : sendResponse.getReport().getContext().getItem()) {
            if (ReportBuilder.REPORT_ELEMENT_SENT_MESSAGE.equals(anyContent.getName())) {
                messageString = anyContent.getValue();
            }
        }

        assertThat((messageString)).isNotBlank();
    }

    private List<AnyContent> buildParameters() {

        AnyContent anyContent = new AnyContent();
        anyContent.setType("object");
        anyContent.setName("messageToSend");
        anyContent.setValue("PG5zNDpQdWxsUmVxdWVzdCB4bWxuczpuczQ9Imh0dHA6Ly93d3cuY2lzZS5ldS9zZXJ2aWNlbW9kZWwvdjEvbWVzc2FnZS8iCiAgICAgICAgICAgICAgICAgeG1sbnM6bnMyPSJodHRwOi8vd3d3LmNpc2UuZXUvc2VydmljZW1vZGVsL3YxL2F1dGhvcml0eS8iCiAgICAgICAgICAgICAgICAgeG1sbnM6bnMzPSJodHRwOi8vd3d3LmNpc2UuZXUvc2VydmljZW1vZGVsL3YxL3NlcnZpY2UvIj4KICAgIDxDcmVhdGlvbkRhdGVUaW1lPjIwMTktMDgtMDFUMTU6NDE6MjRaPC9DcmVhdGlvbkRhdGVUaW1lPgogICAgPE1lc3NhZ2VJRD40MzA4YmU0Mi1kZmRiLTRmOWYtOTY1Ny1kYWE3ZDEwNjkwZGE8L01lc3NhZ2VJRD4KICAgIDxQcmlvcml0eT5IaWdoPC9Qcmlvcml0eT4KICAgIDxSZXF1aXJlc0Fjaz50cnVlPC9SZXF1aXJlc0Fjaz4KICAgIDxTZW5kZXI+CiAgICAgICAgPFNlcnZpY2VJRD5jeC5jaXNlc2ltMi1ub2RlY3gudmVzc2VsLnB1bGwuY29uc3VtZXI8L1NlcnZpY2VJRD4KICAgICAgICA8U2VydmljZU9wZXJhdGlvbj5QdWxsPC9TZXJ2aWNlT3BlcmF0aW9uPgogICAgICAgIDxTZXJ2aWNlVHlwZT5WZXNzZWxTZXJ2aWNlPC9TZXJ2aWNlVHlwZT4KICAgIDwvU2VuZGVyPgogICAgPFJlY2lwaWVudD4KICAgICAgICA8U2VydmljZUlEPmN4LmNpc2VzaW0xLW5vZGVjeC52ZXNzZWwucHVsbC5wcm92aWRlcjwvU2VydmljZUlEPgogICAgICAgIDxTZXJ2aWNlT3BlcmF0aW9uPlB1bGw8L1NlcnZpY2VPcGVyYXRpb24+CiAgICAgICAgPFNlcnZpY2VUeXBlPlZlc3NlbFNlcnZpY2U8L1NlcnZpY2VUeXBlPgogICAgPC9SZWNpcGllbnQ+CiAgICA8UGF5bG9hZCB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIgogICAgICAgICAgICB4c2k6dHlwZT0ibnM0OlhtbEVudGl0eVBheWxvYWQiPgogICAgICAgIDxJc1BlcnNvbmFsRGF0YT50cnVlPC9Jc1BlcnNvbmFsRGF0YT4KICAgICAgICA8UHVycG9zZT5Ob25TcGVjaWZpZWQ8L1B1cnBvc2U+CiAgICAgICAgPEluZm9ybWF0aW9uU2VjdXJpdHlMZXZlbD5Ob25DbGFzc2lmaWVkPC9JbmZvcm1hdGlvblNlY3VyaXR5TGV2ZWw+CiAgICAgICAgPEluZm9ybWF0aW9uU2Vuc2l0aXZpdHk+R3JlZW48L0luZm9ybWF0aW9uU2Vuc2l0aXZpdHk+CiAgICAgICAgPHZlc3NlbDpWZXNzZWwgeG1sbnM6dmVzc2VsPSJodHRwOi8vd3d3LmNpc2UuZXUvZGF0YW1vZGVsL3YxL2VudGl0eS92ZXNzZWwvIj4KICAgICAgICAgICAgPElNT051bWJlcj43NzEwNTI1PC9JTU9OdW1iZXI+CiAgICAgICAgICAgIDxNTVNJPjIyODAwMjAwMDwvTU1TST4KICAgICAgICAgICAgPFNoaXBUeXBlPkZpc2hpbmdWZXNzZWw8L1NoaXBUeXBlPgogICAgICAgIDwvdmVzc2VsOlZlc3NlbD4KICAgIDwvUGF5bG9hZD4KICAgIDxQdWxsVHlwZT5SZXF1ZXN0PC9QdWxsVHlwZT4KICAgIDxSZXNwb25zZVRpbWVPdXQ+MTAwMDwvUmVzcG9uc2VUaW1lT3V0Pgo8L25zNDpQdWxsUmVxdWVzdD4K");
        anyContent.setEncoding("Encoding");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> parameterItems = new ArrayList<>();
        parameterItems.add(anyContent);

        return parameterItems;
    }

}
