package eu.europa.ec.itb.cise.ws.domain;


import eu.cise.dispatcher.DispatchResult;
import eu.cise.dispatcher.Dispatcher;
import eu.cise.dispatcher.DispatcherException;
import eu.cise.servicemodel.v1.message.Acknowledgement;
import eu.cise.servicemodel.v1.message.Message;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.signature.SignatureService;
import eu.eucise.xml.DefaultXmlMapper;
import eu.eucise.xml.XmlMapper;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.exceptions.EmptyMessageIdEx;
import eu.europa.ec.itb.cise.ws.exceptions.EndpointNotFoundEx;
import eu.europa.ec.itb.cise.ws.exceptions.NullSenderEx;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static eu.cise.servicemodel.v1.message.AcknowledgementType.SUCCESS;
import static eu.cise.servicemodel.v1.service.ServiceType.VESSEL_SERVICE;
import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CTTSimEngineTest {

    private String destinationUrl;
    private SimEngine engine;
    private Dispatcher dispatcher;
    private Push message;
    private final Utils utils = new Utils();

    private static final String TEST_FILE_DOMAIN_SCENARIOS_PREFIX = "messages/acknowledgement_domainScenario_";

    @BeforeEach
    public void before() {
        dispatcher = mock(Dispatcher.class);
        engine = new CTTSimEngine(mock(SignatureService.class), dispatcher);
        message = newPush()
                .id("aMessageId")
                .sender(newService().id("aServiceId").type(VESSEL_SERVICE))
                .recipient(newService().id("recipient-id"))
                .build();

        destinationUrl = "endpointUrl";
    }

    @AfterEach
    public void after() {

    }

    @Test
    public void it_sends_the_message_using_the_dispatcher() {
        when(dispatcher.send(message, destinationUrl)).thenReturn(
                new DispatchResult(true, getSyncAckMsgSuccess()));

        CTTSendParam param = new CTTSendParam(true,"aMessageId", null, destinationUrl );
        engine.prepare(message, param);
        engine.send(message);

        verify(dispatcher).send(message, destinationUrl);
    }

    @Test
    public void it_sends_a_message_failing_the_dispatch_for_end_point_not_found() {
        when(dispatcher.send(any(), any())).thenThrow(DispatcherException.class);

        assertThatExceptionOfType(EndpointNotFoundEx.class)
                .isThrownBy(() -> engine.send(message))
                .withMessageContaining("endpoint not found");
    }

    @Test
    public void it_sends_a_message_getting_a_successful_response_and_returns_the_acknowledge() {
        when(dispatcher.send(any(), any())).thenReturn(
                new DispatchResult(true, getSyncAckMsgSuccess()));

        assertThat(engine.send(message).getAckCode()).isEqualTo(SUCCESS);
    }



    @Test
    public void it_receives_a_valid_message() {
        try {
            engine.receive(message);
        } catch (Exception e) {
            fail("Receive raised an exception");
        }
    }


    @Test
    public void it_checks_the_messageId_exists() {
        Message message = newPush()
                .sender(newService().id("aSender"))
                .build();

        assertThatExceptionOfType(EmptyMessageIdEx.class)
                .isThrownBy(() -> engine.receive(message))
                .withMessageContaining("empty");
    }


    @Test
    public void it_receives_a_valid_message_without_sender() {
        Push message = newPush().id("aMessageId").build();

        assertThatExceptionOfType(NullSenderEx.class)
                .isThrownBy(() -> engine.receive(message))
                .withMessageContaining("The sender of the message passed can't be null.");
    }

    @Test
    public void it_receives_a_valid_message_and_returns_the_acknowledge() {
        assertThat(engine.receive(message).getAckCode()).isEqualTo(SUCCESS);
    }

    // --------------- correlation id related test -------------//
    @Test
    public void it_receives_a_valid_message_and_returns_an_ack_with_correlation_id() {
        Acknowledgement ack = engine.receive(message);

        assertThat(ack.getCorrelationID()).isNotEmpty();
    }


    @Test
    public void it_receives_message_it_create_acknowledge_with_new_message_id() {
        Acknowledgement ack = engine.receive(message);
        assertThat(ack.getMessageID()).isNotEqualTo(message.getMessageID());
    }

    @Test
    public void it_receives_message_without_correlation_id_then_overwrite_it_with_previous_message_id() {

        message.setCorrelationID(null);
        String previousMessageId = message.getMessageID();
        Acknowledgement ack = engine.receive(message);

        assertThat(ack.getCorrelationID()).isEqualTo(previousMessageId);
    }


    @Test
    public void it_answer_with_a_the_same_correlation_id_of_the_received_message() {
        message.setCorrelationID("aCorrelationId");
        Acknowledgement ack = engine.receive(message);

        assertThat(ack.getCorrelationID()).isEqualTo("aCorrelationId");
    }

    private Acknowledgement getSyncAckMsgSuccess() {
        XmlMapper xmlMapper = new DefaultXmlMapper();
        return xmlMapper.fromXML(utils.readFile(TEST_FILE_DOMAIN_SCENARIOS_PREFIX + "syncAck_success.xml"));
    }

}