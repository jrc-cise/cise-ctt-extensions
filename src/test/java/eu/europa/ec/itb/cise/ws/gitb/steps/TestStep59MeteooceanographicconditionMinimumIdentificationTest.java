package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep59MeteoOceanographicConditionMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep59MeteooceanographicconditionMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step59_MeteoOceanographicConditionMinimumIdentification";

    private TestStep59MeteoOceanographicConditionMinimumIdentification step59MeteoOceanographicConditionMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_MeteoOceanographicCondition_failure();
        payloadCreator_MeteoOceanographicCondition_success();
        step59MeteoOceanographicConditionMinimumIdentification = new TestStep59MeteoOceanographicConditionMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_59_METEOOCEANOGRAPHICCONDITION_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_validation() {
        TAR report = step59MeteoOceanographicConditionMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_PortOrganizationMinimumDefinition_validation() {
        TAR report = step59MeteoOceanographicConditionMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_MeteoOceanographicCondition_success() {
        // successFactor
        MeteoOceanographicCondition meteoOceanographicCondition = new MeteoOceanographicCondition();
        meteoOceanographicCondition.setAirTemperature(18.0);
        meteoOceanographicCondition.setPrecipitation(3);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (MeteoOceanographicCondition) meteoOceanographicCondition);
    }

    public void payloadCreator_MeteoOceanographicCondition_failure() {
        // UNsuccessFactor
        MeteoOceanographicCondition meteoOceanographicCondition = new MeteoOceanographicCondition();
//        meteoOceanographicCondition.setAirTemperature(18);
//        meteoOceanographicCondition.setPrecipitation(3);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (MeteoOceanographicCondition) meteoOceanographicCondition);
    }


}
