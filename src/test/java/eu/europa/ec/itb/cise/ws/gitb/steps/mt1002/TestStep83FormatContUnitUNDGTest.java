package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.cargo.ContainmentUnit;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #83 FORMAT ContainmentUnit UNDG
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `ContainmentUnit` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE` *payload*
 * `*AND*` attribute `UNDG` is defined
 * `*THEN*` attribute `UNDG` must be composed by 4 Digit characters in accordance with UNDG code format.
 */
public class TestStep83FormatContUnitUNDGTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step83_FormatContUnitUNDG";
    private TestStep83FormatContUnitUNDG testStep83FormatContUnitUNDG;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep83FormatContUnitUNDG = new TestStep83FormatContUnitUNDG(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_83_FORMAT_CONT_UNIT_UNDG.getUiCheckName();
        payloadCreator_FormatContUnitUNDGTest_toolong_failure();
        payloadCreator_FormatContUnitUNDGTest_incorrect_failure();
        payloadCreator_FormatContUnitUNDGTest_success();

    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep83FormatContUnitUNDG.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation_toolong() {
        TAR report = testStep83FormatContUnitUNDG.createReport(Utils.readFile(TEST_FILE_PREFIX + "_toolong_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation_incorrect() {
        TAR report = testStep83FormatContUnitUNDG.createReport(Utils.readFile(TEST_FILE_PREFIX + "_incorrect_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatContUnitUNDGTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        ContainmentUnit containmentUnit = new ContainmentUnit();
        containmentUnit.setUNDG("0446");// success factor known reference
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(containmentUnit);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatContUnitUNDGTest_toolong_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        ContainmentUnit containmentUnit = new ContainmentUnit();
        containmentUnit.setUNDG("00874");// failure factor more then 4 characters
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(containmentUnit);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_toolong_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatContUnitUNDGTest_incorrect_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        ContainmentUnit containmentUnit = new ContainmentUnit();
        containmentUnit.setUNDG("3535");// failure factor above max 3534
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        involvedObjectRel.setObject(containmentUnit);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_incorrect_failure.xml", xmlMapper, vessel);
    }

}