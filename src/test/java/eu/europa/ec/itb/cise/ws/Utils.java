package eu.europa.ec.itb.cise.ws;

import com.gitb.core.AnyContent;
import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.Entity;
import eu.cise.servicemodel.v1.message.InformationSecurityLevelType;
import eu.cise.servicemodel.v1.message.InformationSensitivityType;
import eu.cise.servicemodel.v1.message.PriorityType;
import eu.cise.servicemodel.v1.message.PullRequest;
import eu.cise.servicemodel.v1.message.PullResponse;
import eu.cise.servicemodel.v1.message.PurposeType;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.servicemodel.v1.message.XmlEntityPayload;
import eu.cise.servicemodel.v1.service.ServiceOperationType;
import eu.cise.servicemodel.v1.service.ServiceType;
import eu.eucise.helpers.ServiceTypeMainClassMap;
import eu.eucise.xml.XmlMapper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import static eu.eucise.helpers.PullRequestBuilder.newPullRequest;
import static eu.eucise.helpers.PullResponseBuilder.newPullResponse;
import static eu.eucise.helpers.PushBuilder.newPush;
import static eu.eucise.helpers.ServiceBuilder.newService;

public class Utils {

    private Push push;
    private final HashMap<ServiceType, Class<? extends Entity>> serviceTypeMainClassMap;

    public Utils() {
        serviceTypeMainClassMap = new ServiceTypeMainClassMap();
        push = newPush()
                .id("push-message-id")
                .creationDateTime(new Date())
                .priority(PriorityType.LOW)
                .sender(newService().type(ServiceType.VESSEL_SERVICE).id("service-id").operation(ServiceOperationType.PUSH))
                .informationSecurityLevel(InformationSecurityLevelType.EU_CONFIDENTIAL)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .isPersonalData(false)
                .purpose(PurposeType.BORDER_OPERATION)
                .build();
        setVesselPayload(push);
    }

    public ResourceBundle labels = new ResourceBundle() {
        @Override
        protected Object handleGetObject(String key) {
            return "fake_translated_value";
        }

        @Override
        public Enumeration<String> getKeys() {
            return null;
        }
    };

    public HashMap<String, Properties> boundaries = new HashMap<String, Properties>() ;

    public static String readFile(String path) {
        String s = null;
        try {
            ClassLoader classLoader = new Utils().getClass().getClassLoader();
            File file = new File(classLoader.getResource(path).getFile());
            s = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return s;
    }

    public Push getPush() {
        return push;
    }

    public String getReportFor(TAR report, String reportSectionName) {
        List<AnyContent> contents = report.getContext().getItem();
        for (AnyContent content : contents) {
            if (reportSectionName.equals(content.getName())) {
                return content.getValue();
            }
        }
        return null;
    }

    public void setVesselPayload(Push push) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = null;
            documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // Vessel element
            Element root = document.createElementNS("", "Vessel");
            document.appendChild(root);

            // IMONumber element
            Element imoNumber = document.createElement("IMONumber");
            Text imoNumberValue = document.createTextNode("10208");
            imoNumber.appendChild(imoNumberValue);

            root.appendChild(imoNumber);

            XmlEntityPayload xmlEntityPayload = (XmlEntityPayload) push.getPayload();
            xmlEntityPayload.getAnies().add(root);

            this.push.setPayload(xmlEntityPayload);
        } catch (ParserConfigurationException e) {

        }
    }


    public void setWrongVesselPayload(Push push) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = null;
            documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // Vessel element
            Element root = document.createElementNS("", "Vessel");
            document.appendChild(root);

            XmlEntityPayload xmlEntityPayload = (XmlEntityPayload) push.getPayload();
            xmlEntityPayload.getAnies().clear();
            xmlEntityPayload.getAnies().add(root);

            this.push.setPayload(xmlEntityPayload);
        } catch (ParserConfigurationException e) {

        }
    }

    public boolean findFile(String path) {
        ClassLoader classLoader = new Utils().getClass().getClassLoader();
        Path directory = new File(classLoader.getResource(path.split("/")[0]).getFile()).toPath();
        Path file = directory.resolve(path.split("/")[1]);
        return (Files.exists(file));
    }

    public boolean writePushMessageFiles(String path, XmlMapper xmlMapper, Entity entity) {
        ClassLoader classLoader = new Utils().getClass().getClassLoader();
        Path directory = new File(classLoader.getResource(path.split("/")[0]).getFile()).toPath();
        Path file = directory.resolve(path.split("/")[1]);


        try (OutputStream outputStream = Files.newOutputStream(file)) {
            outputStream.write(getMessagePushBytes(this, xmlMapper, entity));
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static byte[] getMessagePushBytes(Utils utils, XmlMapper xmlMapper, Entity entity) {
        List<Entity> entities = new ArrayList<Entity>();
        entities.add(entity);
        String shortServiceType = "incident";
        ServiceType serviceType = ServiceType.INCIDENT_SERVICE;
        for (ServiceType serviceTypeVariante : utils.serviceTypeMainClassMap.keySet()) {
            if (utils.serviceTypeMainClassMap.get(serviceTypeVariante) == entity.getClass()) {
                serviceType = serviceTypeVariante;
                shortServiceType = serviceTypeVariante.value().replace("Service", "").toLowerCase();
            }
        }

        Push pushMessage = utils.createPushMessage(shortServiceType, serviceType, entities);

        return xmlMapper.toXML(pushMessage).getBytes();
    }

    public boolean writePullResponseMessageFiles(String path, XmlMapper xmlMapper, ArrayList<Entity> entities, Class elementClass) {
        ClassLoader classLoader = new Utils().getClass().getClassLoader();
        Path directory = new File(classLoader.getResource(path.split("/")[0]).getFile()).toPath();
        Path file = directory.resolve(path.split("/")[1]);
        // all push tests
        String shortServiceType = "incident";
        ServiceType serviceType = ServiceType.INCIDENT_SERVICE;
        for (ServiceType serviceTypeVariante : serviceTypeMainClassMap.keySet()) {
            if (serviceTypeMainClassMap.get(serviceTypeVariante) == elementClass) {
                serviceType = serviceTypeVariante;
                shortServiceType = serviceTypeVariante.value().replace("Service", "").toLowerCase();
            }
        }

        PullResponse pullResponse = createPullResponseMessage(shortServiceType, serviceType, entities);

        try (OutputStream outputStream = Files.newOutputStream(file)) {
            outputStream.write(xmlMapper.toXML(pullResponse).getBytes());
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public boolean writePullRequestMessageFiles(String path, XmlMapper xmlMapper, ArrayList<Entity> entities, Class entityClass) {
        ClassLoader classLoader = new Utils().getClass().getClassLoader();
        Path directory = new File(classLoader.getResource(path.split("/")[0]).getFile()).toPath();
        Path file = directory.resolve(path.split("/")[1]);
        // all push tests
        String shortServiceType = "incident";
        ServiceType serviceType = ServiceType.INCIDENT_SERVICE;
        for (ServiceType serviceTypeVariante : serviceTypeMainClassMap.keySet()) {
            if (serviceTypeMainClassMap.get(serviceTypeVariante) == entityClass) {
                serviceType = serviceTypeVariante;
                shortServiceType = serviceTypeVariante.value().replace("Service", "").toLowerCase();
            }
        }

        PullRequest pullRequest = createPullRequestMessage(shortServiceType, serviceType, entities);


        try (OutputStream outputStream = Files.newOutputStream(file)) {
            outputStream.write(xmlMapper.toXML(pullRequest).getBytes());
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public Push createPushMessage(String shortServiceType, ServiceType serviceType, List<Entity> entities) {
        Push pushMessage = newPush().id("messageId")
                .correlationId("correlation-id")
                .creationDateTime(new Date())
                .priority(PriorityType.HIGH)
                .isRequiresAck(true)
                .informationSecurityLevel(InformationSecurityLevelType.NON_CLASSIFIED)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .setEncryptedPayload("false")
                .isPersonalData(false)
                .purpose(PurposeType.NON_SPECIFIED)
                .sender(newService().id("eu.cisesim1-nodeeu." + shortServiceType + ".push.provider")
                        .operation(ServiceOperationType.PUSH).type(serviceType).build())
                .recipient(newService().id("eu.ctt-nodeeu." + shortServiceType + ".push.consumer")
                        .operation(ServiceOperationType.PUSH).type(serviceType).build())
                .addEntities(entities)
                .build();
        return pushMessage;
    }

    private PullResponse createPullResponseMessage(String shortServiceType, ServiceType serviceType, List<Entity> entities) {
        PullResponse pullResponse = newPullResponse().id("messageId")
                .correlationId("correlation-id")
                .creationDateTime(new Date())
                .priority(PriorityType.HIGH)
                .isRequiresAck(true)
                .informationSecurityLevel(InformationSecurityLevelType.NON_CLASSIFIED)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .setEncryptedPayload("false")
                .isPersonalData(false)
                .purpose(PurposeType.NON_SPECIFIED)
                .sender(newService().id("eu.cisesim1-nodeeu." + shortServiceType + ".pull.provider")
                        .operation(ServiceOperationType.PULL).type(serviceType).build())
                .recipient(newService().id("eu.ctt-nodeeu." + shortServiceType + ".pull.consumer")
                        .operation(ServiceOperationType.PULL).type(serviceType).build())
                .addEntities(entities)
                .build();
        return pullResponse;
    }

    private PullRequest createPullRequestMessage(String shortServiceType, ServiceType serviceType, List<Entity> entities) {
        PullRequest pullRequest = newPullRequest().id("messageId")
                .correlationId("correlation-id")
                .creationDateTime(new Date())
                .priority(PriorityType.HIGH)
                .isRequiresAck(true)
                .informationSecurityLevel(InformationSecurityLevelType.NON_CLASSIFIED)
                .informationSensitivity(InformationSensitivityType.GREEN)
                .setEncryptedPayload("false")
                .isPersonalData(false)
                .purpose(PurposeType.NON_SPECIFIED)
                .sender(newService().id("eu.cisesim1-nodeeu." + shortServiceType + ".pull.provider")
                        .operation(ServiceOperationType.PULL).type(serviceType).build())
                .recipient(newService().id("eu.ctt-nodeeu." + shortServiceType + ".pull.consumer")
                        .operation(ServiceOperationType.PULL).type(serviceType).build())
                .addEntities(entities)
                .build();
        return pullRequest;
    }


}
