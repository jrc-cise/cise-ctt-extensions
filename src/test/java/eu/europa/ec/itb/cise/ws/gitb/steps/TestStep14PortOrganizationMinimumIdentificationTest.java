package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Cargo;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.PortOrganization;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep14PortOrganizationMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep14PortOrganizationMinimumIdentificationTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step14_PortOrganizationMinimumIdentification";

    private TestStep14PortOrganizationMinimumIdentification testStep14PortOrganizationMinimumIdentification;


    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_PortOrganizationMinimumIdentification_failure();
        payloadCreator_PortOrganizationMinimumIdentification_success();
        payloadCreator_PortOrganizationMinimumIdentification_ImoNumber_success();
        testStep14PortOrganizationMinimumIdentification = new TestStep14PortOrganizationMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_14_PORTORGANIZATION_MIN_IDENT.getUiCheckName();
    }


    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep14PortOrganizationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_PortOrganizationMinimumDefinition_ImoNumber_validation() {
        TAR report = testStep14PortOrganizationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_ImoNumber_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_PortOrganizationMinimumDefinition_validation() {
        TAR report = testStep14PortOrganizationMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_PortOrganizationMinimumIdentification_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        PortOrganization portOrganization = new PortOrganization();
        portOrganization.setLegalName("John Doe & Co");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(portOrganization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PortOrganizationMinimumIdentification_ImoNumber_success() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        PortOrganization portOrganization = new PortOrganization();
        portOrganization.setIMOCompanyIdentificationNumber("test");
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(portOrganization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_ImoNumber_success.xml", xmlMapper, (Cargo) cargo);
    }

    public void payloadCreator_PortOrganizationMinimumIdentification_failure() {
        Cargo cargo = new Cargo();
        // here should have a link to a person
        PortOrganization portOrganization = new PortOrganization();
        //no set child node value
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        involvedAgentRel.setAgent(portOrganization);
        cargo.getInvolvedAgentRels().add(involvedAgentRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (Cargo) cargo);
    }


}
