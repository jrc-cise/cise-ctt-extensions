package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep67LinkObjectEventCargoUnit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep67LinkObjectEventCargoUnitTest extends CiseTestStepTest {

    private TestStep67LinkObjectEventCargoUnit testStep66LinkObjectAgentCargoUnit;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep66LinkObjectAgentCargoUnit = new TestStep67LinkObjectEventCargoUnit(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_67_LINK_OBJECT_EVENT_CARGOUNIT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectEventCargoUnit_CargoUnit_to_Event_validation() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step67_LinkObjectEventCargoUnit_CargoUnit_to_Event_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_CargoUnit_to_Event_with_not_valid_AgentRole() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step67_LinkObjectEventCargoUnit_CargoUnit_to_Event_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_with_correct_LinkObjectEventCargoUnit_Event_to_CargoUnit_validation() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step67_LinkObjectEventCargoUnit_Event_to_CargoUnit_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Event_to_CargoUnit_with_not_valid_AgentRole() {
        TAR report = testStep66LinkObjectAgentCargoUnit.createReport(utils.readFile("messages/push_Step67_LinkObjectEventCargoUnit_Event_to_CargoUnit_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }
}