package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.TestStep70LinkMovementAgent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep70LinkMovementAgentTest extends CiseTestStepTest {

    private TestStep70LinkMovementAgent testStep70LinkMovementAgent;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep70LinkMovementAgent = new TestStep70LinkMovementAgent(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_70_LINK_MOVEMENT_AGENT.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report_Agent_to_Movement() {
        TAR report = testStep70LinkMovementAgent.createReport(utils.readFile("messages/push_Step70_LinkAgentMovement_Agent_to_Movement_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Agent_to_Movement() {
        TAR report = testStep70LinkMovementAgent.createReport(utils.readFile("messages/push_Step70_LinkAgentMovement_Agent_to_Movement_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_success_report_Movement_to_Agent() {
        TAR report = testStep70LinkMovementAgent.createReport(utils.readFile("messages/push_Step70_LinkAgentMovement_Movement_to_Agent_success.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_Movement_to_Agent() {
        TAR report = testStep70LinkMovementAgent.createReport(utils.readFile("messages/push_Step70_LinkAgentMovement_Movement_to_Agent_failure.xml"));

        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(messageReport).isNotEqualTo("VALID");
    }

}