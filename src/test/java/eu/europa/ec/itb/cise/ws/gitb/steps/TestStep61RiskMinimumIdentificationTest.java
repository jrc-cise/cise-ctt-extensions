package eu.europa.ec.itb.cise.ws.gitb.steps;


import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.RiskDocument;
import eu.cise.datamodel.v1.entity.event.Event;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.risk.Risk;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1004.TestStep61RiskMinimumIdentification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep61RiskMinimumIdentificationTest extends CiseTestStepTest {


    private static final String TEST_FILE_PREFIX = "messages/push_Step61_RiskMinimumIdentification";
    private TestStep61RiskMinimumIdentification testStep61RiskMinimumIdentification;

    @BeforeEach
    public void setUp() {
        super.setUp();
        payloadCreator_RiskMinimumIdentification_success();
        payloadCreator_RiskMinimumIdentification_emptyRel_failure();
        payloadCreator_RiskMinimumIdentification_failure();
        testStep61RiskMinimumIdentification = new TestStep61RiskMinimumIdentification(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_61_RISK_MINIMUM_IDENTIFICATION.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_RiskMinimumIdentification_validation() {
        TAR report = testStep61RiskMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    //@Ignore // if xml is validating that relation should not be empty
    @Test
    public void it_creates_a_report_with_successful_empty_RiskMinimumIdentification_validation() {
        TAR report = testStep61RiskMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_empty_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_RiskMinimumIdentification_validation() {
        TAR report = testStep61RiskMinimumIdentification.createReport(utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_RiskMinimumIdentification_success() {
        MaritimeSafetyIncident incident = new MaritimeSafetyIncident();
        // here should have a link to a person
        Risk risk = new Risk();
        Document document = new RiskDocument();
        Risk.DocumentRel documentRel = new Risk.DocumentRel();
        documentRel.setDocument(document);
        risk.getDocumentRels().add(documentRel);
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        incident.getImpliedRiskRels().add(impliedRiskRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, (MaritimeSafetyIncident) incident);
    }

    public void payloadCreator_RiskMinimumIdentification_emptyRel_failure() {
        MaritimeSafetyIncident incident = new MaritimeSafetyIncident();
        // here should have a link to a person
        Risk risk = new Risk();
        Document document = new RiskDocument();
        Risk.DocumentRel documentRel = new Risk.DocumentRel();
        //documentRel.setDocument(document);
        risk.getDocumentRels().add(documentRel);
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        incident.getImpliedRiskRels().add(impliedRiskRel);

        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_empty_failure.xml", xmlMapper, (MaritimeSafetyIncident) incident);
    }

    public void payloadCreator_RiskMinimumIdentification_failure() {
        MaritimeSafetyIncident incident = new MaritimeSafetyIncident();
        // here should have a link to a person
        Risk risk = new Risk();
        Document document = new RiskDocument();
        //Risk.DocumentRel documentRel = new Risk.DocumentRel();
        //documentRel.setDocument(document);
        //risk.getDocumentRels().add(documentRel);
        Event.ImpliedRiskRel impliedRiskRel = new Event.ImpliedRiskRel();
        impliedRiskRel.setRisk(risk);
        incident.getImpliedRiskRels().add(impliedRiskRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, (MaritimeSafetyIncident) incident);
    }
}