package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.objet;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * #98 Boundary Vessel
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Vessel` is defined in *payload*
 * `*AND*` *MessageType* is one of `PUSH` or `PULL_RESPONSE/REQUEST` or `PUSH/PULL_SUBSCRIBE`
 * `*AND*` *attribute* `beam`,`breadth`,`containerCapacity`,`deadweight`,`depth`,`draught`,`grossTonnage`,`length`,`lengthenedYear`,`loa`,`netTonnage`,`segregatedBallastVolume`,`yearBuilt` _is  defined_
 * `*THEN*` *attribute*  _must be into accepted range _ .
 */

public class TestStep98BoundaryVesselTest extends CiseTestStepTestChildValueBoundaries {

    private final String testFilePrefix = "messages/push_Step98_BoundaryVessel";
    private final String propertyFile = "src/main/resources/Proposed_Bnd_Objet.properties";
    private TestStep98BoundaryVessel testStep98BoundaryVessel;
    private static final String[] FIELDS = new String[]{
            "beam",
            "breadth",
            "containerCapacity",
            "deadweight",
            "depth",
            "draught",
            "grossTonnage",
            "length",
            "lengthenedYear",
            "loa",
            "netTonnage",
            "segregatedBallastVolume",
            "yearBuilt"
    };

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TOFO: replace with objective context of exec == IDE devel
        testStep98BoundaryVessel = new TestStep98BoundaryVessel(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_98_BOUNDARY_VESSEL.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Objet");
        if (reinitCondition)
            payload_PropertyBoundary_maker(Vessel.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, FIELDS);
            payloadCreator_BoundaryTest_success(testFilePrefix, FIELDS);
        }
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep98BoundaryVessel.createReport(payloadGetter_BoundaryTest_success(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep98BoundaryVessel.createReport(payloadGetter_BoundaryTest_failure(FIELDS));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts98BoundaryVessel");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(FIELDS);
    }


    @NotNull
    public Vessel getTestedEntity(String[] fields, boolean purposeSuccess) {
        Vessel vessel = (Vessel) PropertyMaker.populateAttributes(errorBoundaries, Vessel.class, fields, purposeSuccess);
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        Agent.InvolvedObjectRel involvedObjectRel = new Agent.InvolvedObjectRel();
        Vessel vessel1 = (Vessel) PropertyMaker.populateAttributes(errorBoundaries, Vessel.class, fields, purposeSuccess);
        involvedObjectRel.setObject(vessel1);
        organization.getInvolvedObjectRels().add(involvedObjectRel);
        involvedAgentRel.setAgent(organization);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        return vessel;
    }

}