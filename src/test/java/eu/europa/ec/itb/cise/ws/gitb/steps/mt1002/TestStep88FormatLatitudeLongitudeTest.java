package eu.europa.ec.itb.cise.ws.gitb.steps.mt1002;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.agent.Agent;
import eu.cise.datamodel.v1.entity.agent.AgentRoleInObjectType;
import eu.cise.datamodel.v1.entity.location.Geometry;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.organization.Organization;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTest;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * #88 FORMAT (LOCATION - GEOMETRY implicit) Latitude Longitude
 * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 * `*GIVEN*` *entity(ies)* of type `Location` (+ its children entities) is defined in `PUSH` or `PULL_RESPONSE` or `PUSH_SUBSCRIBE`  *payload*
 * `*AND*` attribute `Latitude` and `Longitude` of its `Geometry` is defined
 * `*THEN*` attribute `Latitude` and `Longitude` must contain 1-2 digits followed by "." and 1 to 4 digits
 * `*AND*` attribute `Latitude` and `Longitude` must represent a decimal number between accepted range (Latitud -+90 Longitud -+180)
 * `*AND*` attribute `Latitude` and `Longitude` should represent a decimal number with sufficient precision >=2 (2500 meter at the equator)
 */
public class TestStep88FormatLatitudeLongitudeTest extends CiseTestStepTest {

    private static final String TEST_FILE_PREFIX = "messages/push_Step88_FormatLatitudeLongitude";
    private TestStep88FormatLatitudeLongitude testStep88FormatLatitudeLongitude;

    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep88FormatLatitudeLongitude = new TestStep88FormatLatitudeLongitude(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_88_FORMAT_LATITUDE_LONGITUDE.getUiCheckName();
        payloadCreator_FormatCatchSpeciesTest_success();
        payloadCreator_FormatCatchSpeciesTest_failure();
        payloadCreator_FormatCatchSpeciesTest_warning();
        payloadCreator_FormatCatchSpeciesTest_warning_overPrecision();
    }

    @AfterEach
    public void tearDown() {
    }


    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep88FormatLatitudeLongitude.createReport(Utils.readFile(TEST_FILE_PREFIX + "_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep88FormatLatitudeLongitude.createReport(Utils.readFile(TEST_FILE_PREFIX + "_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_warning_report_with_correct_validation() {
        TAR report = testStep88FormatLatitudeLongitude.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }
    @Test
    public void it_creates_a_warning_overPrecision_report_with_correct_validation() {
        TAR report = testStep88FormatLatitudeLongitude.createReport(Utils.readFile(TEST_FILE_PREFIX + "_warning_overPrecision.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_FormatCatchSpeciesTest_success() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setLatitude("-63.123");
        geometry.setLongitude("-174.123");
        location.getGeometries().add(geometry);// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(location);
        organization.getLocationRels().add(locationRel);
        // success factor
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_success.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_failure() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setLatitude("-123.123");
        geometry.setLongitude("190.123");
        location.getGeometries().add(geometry);// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(location);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_failure.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_warning() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setLatitude("123.1");
        geometry.setLongitude("94.1");
        location.getGeometries().add(geometry);// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(location);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning.xml", xmlMapper, vessel);
    }

    public void payloadCreator_FormatCatchSpeciesTest_warning_overPrecision() {
        Vessel vessel = new Vessel();
        Objet.InvolvedAgentRel involvedAgentRel = new Objet.InvolvedAgentRel();
        Organization organization = new Organization();
        involvedAgentRel.setAgent(organization);
        involvedAgentRel.setAgentRole(AgentRoleInObjectType.SHIP_OPERATING_COMPANY);
        vessel.getInvolvedAgentRels().add(involvedAgentRel);
        // here should have a link to a location
        Location location = new Location();
        Geometry geometry = new Geometry();
        geometry.setLatitude("123.0123456789");
        geometry.setLongitude("94.0123456789");
        location.getGeometries().add(geometry);// success factor no space -.' sign and letters
        Agent.LocationRel locationRel = new Agent.LocationRel();
        locationRel.setLocation(location);
        organization.getLocationRels().add(locationRel);
        utils.writePushMessageFiles(TEST_FILE_PREFIX + "_warning_overPrecision.xml", xmlMapper, vessel);
    }
}