package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.Utils;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1001.MetaTestStep1001CoherenceEntitiesAttributes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MetaTestStep1001CoherenceEntitiesAttributesTest extends CiseTestStepTest {

    private final Utils utils = new Utils();

    private MetaTestStep1001CoherenceEntitiesAttributes metaTestStep1001CoherenceEntitiesAttributes;


    @BeforeEach
    public void setUp() {
        super.setUp();
        metaTestStep1001CoherenceEntitiesAttributes = new MetaTestStep1001CoherenceEntitiesAttributes(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1001_COHERENCE_ENTITIES_ATTRIBUTES.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report() {
        TAR report = metaTestStep1001CoherenceEntitiesAttributes.createReport(utils.readFile("messages/push_MetaTestStep1001_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report() {
        TAR report = metaTestStep1001CoherenceEntitiesAttributes.createReport(utils.readFile("messages/push_MetaTestStep1001_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}