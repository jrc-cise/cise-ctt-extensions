package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.document.Document;
import eu.cise.datamodel.v1.entity.document.Stream;
import eu.cise.datamodel.v1.entity.incident.MaritimeSafetyIncident;
import eu.cise.datamodel.v1.entity.object.Objet;
import eu.cise.datamodel.v1.entity.object.Vehicle;
import eu.cise.datamodel.v1.entity.vessel.Vessel;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.TestStep26StreamMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep26StreamMinimumDefinitionTest extends CiseTestStepTest {

    private TestStep26StreamMinimumDefinition testStep26StreamMinimumDefinitionTest;


    @BeforeEach
    public void setUp() {
        super.setUp();
        //https://tools.ietf.org/html/rfc3986#section-1.1.1 for formal alternative of uri ipv6 url urn
        payloadCreator_StreamMinimumDefinition_uriurn_success();
        payloadCreator_StreamMinimumDefinition_urildap_success();
        payloadCreator_StreamMinimumDefinition_nouri_failure();
        payloadCreator_StreamMinimumDefinition_invalidUri_failure();
        testStep26StreamMinimumDefinitionTest = new TestStep26StreamMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_26_STREAM_MIN_DEFINITION.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_successful_StreamMinimumDefinition_ldap_validation() {
        TAR report = testStep26StreamMinimumDefinitionTest.createReport(utils.readFile("messages/push_Step26_StreamMinimumDefinition_ldap_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_successful_StreamMinimumDefinition_urn_validation() {
        TAR report = testStep26StreamMinimumDefinitionTest.createReport(utils.readFile("messages/push_Step26_StreamMinimumDefinition_urn_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }


    @Test
    public void it_creates_a_report_with_unsuccessful_StreamMinimumDefinition_nouri_validation() {
        TAR report = testStep26StreamMinimumDefinitionTest.createReport(utils.readFile("messages/push_Step26_StreamMinimumDefinition_nouri_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_unsuccessful_StreamMinimumDefinition_invaliduri_validation() {
        TAR report = testStep26StreamMinimumDefinitionTest.createReport(utils.readFile("messages/push_Step26_StreamMinimumDefinition_invaliduri_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }


    public void payloadCreator_StreamMinimumDefinition_urildap_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create DOCUMENT of type stream (to link to vessel)
        Document stream = new Stream();
        try {
            ((Stream) stream).setStreamURI(new URI("ldap://[2001:db8::7]/c=GB?objectClass?one").toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(stream);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step26_StreamMinimumDefinition_ldap_success.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_StreamMinimumDefinition_uriurn_success() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create DOCUMENT of type stream (to link to vessel)
        Document stream = new Stream();
        try {
            ((Stream) stream).setStreamURI(new URI("urn:oasis:names:specification:docbook:dtd:xml:4.1.2").toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(stream);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step26_StreamMinimumDefinition_urn_success.xml", xmlMapper, maritimeSafetyIncident);
    }


    public void payloadCreator_StreamMinimumDefinition_nouri_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create DOCUMENT of type stream (to link to vessel)
        Document stream = new Stream();
        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(stream);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step26_StreamMinimumDefinition_nouri_failure.xml", xmlMapper, maritimeSafetyIncident);
    }

    public void payloadCreator_StreamMinimumDefinition_invalidUri_failure() {
        MaritimeSafetyIncident maritimeSafetyIncident = new MaritimeSafetyIncident();
        Vehicle vehicle;
        //vehicle = (Vehicle) new Cargo(); //OCNET-225 : We assume cargo is not child of vehicle
        vehicle = (Vehicle) new Vessel();
        // create DOCUMENT of type stream (to link to vessel)
        Document stream = new Stream();

        ((Stream) stream).setStreamURI("invalidUIR_\"^`{|}");

        Objet.DocumentRel documentRel = new Objet.DocumentRel();
        documentRel.setDocument(stream);
        vehicle.getDocumentRels().add(documentRel);
        // finally add vessel to first maritime incident (to link to vessel)
        MaritimeSafetyIncident.InvolvedObjectRel involvedObjectRel = new MaritimeSafetyIncident.InvolvedObjectRel();
        involvedObjectRel.setObject(vehicle);
        maritimeSafetyIncident.getInvolvedObjectRels().add(involvedObjectRel);
        System.out.println(xmlMapper.toXML(maritimeSafetyIncident));
        utils.writePushMessageFiles("messages/push_Step26_StreamMinimumDefinition_invaliduri_failure.xml", xmlMapper, maritimeSafetyIncident);
    }


}
