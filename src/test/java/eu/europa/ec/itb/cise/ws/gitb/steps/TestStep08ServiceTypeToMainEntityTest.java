package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.cise.servicemodel.v1.message.Push;
import eu.cise.servicemodel.v1.service.ServiceType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep08ServiceTypeToMainEntityTest extends CiseTestStepTest {



    private TestStep08ServiceTypeToMainEntity testStep08ServiceTypeToMainEntity;


    @BeforeEach
    public void setUp() {
        super.setUp();
        testStep08ServiceTypeToMainEntity = new TestStep08ServiceTypeToMainEntity(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_SERVICE_TS_08_SERVICE_TYPE_TO_MAIN_ENTITY.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_valid_ServiceType_MainEntity_check() {
        String messageXml = xmlMapper.toXML(utils.getPush());
        TAR report = testStep08ServiceTypeToMainEntity.createReport(messageXml);

        String serviceTypeToMainEntityReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(serviceTypeToMainEntityReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_report_with_simple_content_for_not_valid_ServiceType_MainEntity_check() {
        Push wrongMessagePush = utils.getPush();
        wrongMessagePush.getSender().setServiceType(ServiceType.CARGO_SERVICE);
        String messageXml = xmlMapper.toXML(wrongMessagePush);
        TAR report = testStep08ServiceTypeToMainEntity.createReport(messageXml);

        String serviceTypeToMainEntityReport = utils.getReportFor(report, testStepRefUiCheckName);

        assertThat(serviceTypeToMainEntityReport).isNotEqualTo("VALID");
    }

}
