package eu.europa.ec.itb.cise.ws.gitb.steps;

import com.gitb.tr.TAR;
import eu.europa.ec.itb.cise.ws.gitb.steps.mt1000.MetaTestStep1000EntityMinimumDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MetaTestStep1000AEntityMinimumDefinitionTest extends CiseTestStepTest {

    private MetaTestStep1000EntityMinimumDefinition metaTestStep1000EntityMinimumDefinition;

    @BeforeEach
    public void setUp() {
        super.setUp();
        metaTestStep1000EntityMinimumDefinition = new MetaTestStep1000EntityMinimumDefinition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_META_MTS_1000_ENTITY_MIN_DEF.getUiCheckName();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void it_creates_a_success_report() {
        TAR report = metaTestStep1000EntityMinimumDefinition.createReport(utils.readFile("messages/push_MetaTestStep1000_success.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report() {
        TAR report = metaTestStep1000EntityMinimumDefinition.createReport(utils.readFile("messages/push_MetaTestStep1000_failure.xml"));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
    }

}