package eu.europa.ec.itb.cise.ws.gitb.steps.mt1003.meteo;

import com.gitb.tr.TAR;
import eu.cise.datamodel.v1.entity.cargo.Catch;
import eu.cise.datamodel.v1.entity.location.Location;
import eu.cise.datamodel.v1.entity.location.MeteoOceanographicCondition;
import eu.cise.datamodel.v1.entity.location.PortLocation;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepTestChildValueBoundaries;
import eu.europa.ec.itb.cise.ws.gitb.steps.CiseTestStepsEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestStep103BoundaryMeteoOceanConditionTest extends CiseTestStepTestChildValueBoundaries {
    private final String testFilePrefix = "messages/push_Step103BoundaryMeteoOceanConditionTest";
    private final String propertyFile = "src/test/java/eu/europa/ec/itb/cise/ws/gitb/steps/mt1003/meteo/Meteo.properties";
    private TestStep103BoundaryMeteoOceanCondition testStep103BoundaryMeteoOceanCondition;
    private final String[] fields = new String[]{
            "airTemperature",
            "cloudCeiling",
            "precipitation",
            "salinity",
            "seaLevelPressure",
            "tidalCurrentDirection",
            "tidalCurrentSpeed",
            "visibility",
            "waterTemperature",
            "waveDirection",
            "waveHeight",
            "windCurrentDirection",
            "windCurrentSpeed"
    };

    @BeforeEach
    public void setUp() {
        super.setUp();
        boolean devCondition = true, reinitCondition = false; //TODO: replace with objective context of exec == IDE devel
        testStep103BoundaryMeteoOceanCondition = new TestStep103BoundaryMeteoOceanCondition(reportBuilder, payloadHelper, errorHelper);
        testStepRefUiCheckName = CiseTestStepsEnum.REPORT_ELEMENT_MODEL_TS_103_BOUNDARY_METEOOCEANCONDITION.getUiCheckName();
        errorBoundaries = errorHelper.getErrorBoundaries("Meteo");
        if (errorBoundaries == null)
            if (reinitCondition)
                payload_PropertyBoundary_maker(Catch.class, propertyFile); //used to create / update properties list

        if (devCondition) {
            payloadCreator_BoundaryTest_failure(testFilePrefix, fields);
            payloadCreator_BoundaryTest_success(testFilePrefix, fields);
        }
    }



    @Test
    public void it_creates_a_success_report_with_correct_validation() {
        TAR report = testStep103BoundaryMeteoOceanCondition.createReport(payloadGetter_BoundaryTest_success(fields));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isEqualTo("VALID");
    }

    @Test
    public void it_creates_a_failure_report_with_correct_validation() {
        TAR report = testStep103BoundaryMeteoOceanCondition.createReport(payloadGetter_BoundaryTest_failure(fields));
        String messageReport = utils.getReportFor(report, testStepRefUiCheckName);
        assertThat(messageReport).isNotEqualTo("VALID");
        assertThat(report.getContext().getItem().get(0).getName()).isEqualTo("ts103BoundaryMeteoOceanCondition");
        assertThat(report.getContext().getItem().get(0).getValue()).contains(fields);
    }



    @Override
    protected MeteoOceanographicCondition getTestedEntity(String[] fields, boolean expectedDefault) {
        MeteoOceanographicCondition meteoOceanographicCondition= (MeteoOceanographicCondition) PropertyMaker.populateAttributes(errorBoundaries, MeteoOceanographicCondition.class, fields, expectedDefault);
        Location mylocation= new PortLocation();
        final MeteoOceanographicCondition.LocationRel locationRel = new MeteoOceanographicCondition.LocationRel();
        locationRel.setLocation(mylocation);
        meteoOceanographicCondition.setLocationRel(locationRel);
        return meteoOceanographicCondition;
    }

}