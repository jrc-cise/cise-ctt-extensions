= CISE Conformity Test Tool (CTT) V3.2
:page-layout: docs
:homepage:
:numbered:
:source-highlighter: highlightjs
ifndef::env-site[]
:toc: right
:toclevels: 4
:idprefix:
:idseparator: -
//:icons: font
endif::[]
ifdef::env-github[]
:tip-caption: :bulb:
:note-caption: :information_source:
:important-caption: :heavy_exclamation_mark:
:caution-caption: :fire:
:warning-caption: :warning:
endif::[]
:source-language: bash

== Introduction
The CISE Conformity Test Tool (CTT) is the conformance testing platform to assess the interoperability between adaptors and CISE nodes.
The CTT has been developed using the ISA2 framework implementation of the GITB standard.

The purpose of the CTT is to test automatically the adapter integration with the CISE node and the conformance with the existing interoperability standards (CISE Data Model and Service Model). 

== Software Architecture
Hereafter we describe the architecture of the CTT, how to check
out the source code for our custom GITB extension, and how to prepare
the software bundle for the deployment in production.

=== General Architecture
As stated earlier, CTT is based on the GITB platform, which provides
components readily available as Docker images that can be deployed using
the *_docker-compose_* utility. What is must to be developed and
maintained is the specific extension integrating the platform with our
own system, which in our case is the *_cise-ctt-extension_* module.

The components provided by GITB platform as Docker images are:

* *gitb-srv*, the test bed’s backend test engine that executes tests.
* *gitb-ui*, the test bed’s frontend.
* *gitb-redis*, a cache server used for user session management.
* *gitb-mysql*, the test bed’s database.
* The *cise-ctt-extension* module enables the communication between the
test bed and the CISE Network.

In the following figure we depict the overall architecture of CTT:

.Conformity Test Tool Architecture
image::media/CTT-Architecture.png[Conformity Test Tool Architecture]


The *cise-ctt-extensions* module has been created using the service APIs
provided by GITB platform as stated in following excerpt:

____
A key element of the GITB specifications is the set of GITB test
service APIs. These are SOAP web service interfaces (defined using WSDLs
and XSDs) that allow extension of the core GITB test bed software by
means of decoupled components that offer domain-specific capabilities.
Such capabilities may be specific to a given project’s testing needs and
would not be offered by the test bed out of the box, such as the
handling of a custom communication protocol or the validation of
arbitrary content. The purpose of the GITB test service APIs is to
define a common interface between such components and the test bed so
that they can interact in a consistent manner as part of test cases.
Currently three types of services are defined: Validation services,
Messaging services, and Processing services.
____

=== Cise-ctt-extensions Project

The *cise-ctt-extensions* project has been built
starting from project templates provided by GITB platform to be able to
integrate the messaging, and validation workflows. The procedure on how
these integrations have been carried out is described in the following:

* *Validation services* are used to validate input and produce a
validation report. https://www.itb.ec.europa.eu/docs/services/latest/validation/index.html

* *Messaging services* are used to extend test bed communication
capabilities. https://www.itb.ec.europa.eu/docs/services/latest/messaging/index.html

To be able to develop and maintain the software it is needed to follow
the guide to install the test bed for development:
____
GITB Guide: Installing the test bed for development use

https://www.itb.ec.europa.eu/docs/guides/latest/installingTheTestBed/
____

Whereas the following guide walks you through the concepts and details
on how the system works:
____
GITB Guide: Getting started (for developers)

https://www.itb.ec.europa.eu/docs/guides/latest/onboardingDevelopers/index.html
____

==== Endpoints

The *cise-ctt-extensions* project is essentially a Web-Application
exposing endpoints described below:

.Exposed Endpoints
[cols=",,,",options="header",]
|===
|*Name* |*URL* |*Description* |*Scope*
|ciseValidationService
|http://cise-ctt-extensions:7171/services/cise-validation?wsdl |Address
for the web-service endpoint of the GITB validation service. |Internal

|messagingService
|http://cise-ctt-extensions:7171/services/gitb-messaging?wsdl |Address
for the web-service endpoint of the GITB messaging service. |Internal

|CISEMessageService
|http://cise-ctt-extensions:7171/services/cise-messaging?wsdl |Address
for the web-service endpoint of CISE messaging |External
|===

Note that all the endpoints exposed are SOAP Web Services.

==== CISE Validation Service
The Validation Service has been extended to implement the specific
validation rules for CISE. Each rule is implemented as a validation
step, validating a portion of the message. These rules are described in
each test (found under *package eu.europa.ec.itb.cise.ws.gitb.steps* package)
using th Gerkin notation (https://cucumber.io/docs/gherkin/).
To properly identify all the rules in code implementation they have been
numbered from 1 to 999. Some of these rules have been grouped together
to provide checks on specific parameters and to ease the implementation
of test cases (for example “minimum definition of entities”). These
rules are numbered starting at 1000 and are named “Meta Test Steps”.
Validation steps are called from GITB platform when a test step in a
test case is executed.

In the following excerpt we show an example of how to refer to a
specific validation rule exposed by the *ciseValidationService*:

[source,xml]
----
<!-- Rule 17 : AnomalyType Minimum Identification -->
<verify id="*ts17AnomalyType*"
handler="$DOMAIN\{*ciseValidationService*}" desc="AnomalyType defined
(test #17)">
  <input name="type">**'ts17AnomalyType'**</input>
  <input name="content" source="$pushMessage\{message}"/>
</verify>
<etxn txnId="t1"/>
----

In this example *ts17AnomalyType* is the identifier of the specific test
step number “17”.

==== CISE Messaging Services
Two Messaging Services have been implemented to bridge the communication
between GITB platform and the CISE Network. There is the
*CISEMessagingService* implementing the interface to the CISE Network
and the *messagingService* implementing the interface to the GITB
platform.

When a message is received from the CISE Network, the
*CISEMesssagingService* endpoint is called, and it then executes the
following:

. Validates the message before unmarshalling into a CISE Message object
(through an Interceptor) to produce a GITB report that will be shown on
the UI even if the message does not pass the WSDL validation.
. Store the MessageID for uniqueness verification.
. Validates the signature and creates the acknowledgement.
. Creates a report with the message, the acknowledgement, and the
validation result and sends it to the GITB platform.
. Returns back the Acknowledgement.

Instead, the *messagingService* is called when the GITB platform needs
to send a message to the CISE Network and implements the following
tasks:

. It extracts the message to be forwarded to the CISE Network
. Sends the message and wait for the synchronous acknowledgement
. Creates a response for the GITB platform and sends it back.

==== Project Structure
The project structure is derived from Maven basic archetype where we
have the source and test directories holding the code along with tests
and a `pom.xml` file defining the project itself. Besides this structure
we have a series of other directories holding scripts needed for the
build and the deployment of the software. Hereafter we describe the most
important:

* *data*: contains files to produce the data bundle (ref. *Table 6.*
Files provided by JRC - Test Configuration) that is distributed for the
deployment. We have keystore with properties file, basic configuration export
and structures needed to produce the documentation.
* *scripts*: here we have all the script files needed to create the
Docker image, to launch all the application as docker containers, and to
clean up the installation.
* Whereas in the *src/main* directory we want to highlight the following
directories:
** *assemblies*: contains files to produce the packaging done by the
maven assembly plug-in
** *resources/test-suites:* contains the test suites described in the
next chapter *Test Suites Definition*.

.Project structure
[cols=",,",options="header",]
|===
|*Project Overall* |*Java Packages* |*Test Suites Structure*
|image:media/ProjectOverall.png[Graphical user interface, text Description
automatically generated with medium confidence,width=144,height=282]
|image:media/JavaPackages.png[Graphical user interface, text, application
Description automatically generated,width=154,height=282]
|image:media/TestSuitesStructure.png[A picture containing text, plaque Description
automatically generated,width=217,height=282]
|===

* The project is a _Spring Boot_ application
(https://spring.io/projects/spring-boot) where the main class is
*Application.java*, and the main component that exposes the endpoints is
*WebServiceConfig.java* in *ws*.*config* package. Here we have the
injection of a custom context class where we set up all the objects
needed for the execution of the program:
*CTTCiseExtensionAppContext.java*. Also, in this package we have the
Interceptor class that captures the received message before
unmarshalling and the classes to read the configuration properties.
* All the packages are created under the package
*eu.europa.ec.itb.cise.ws*, the only class outside this package is the
main *Application.java* class. The *MessageIdStore.java* class is a
utility class needed to implement the rule of uniqueness of message IDs.
* The other packages of the project are:

** *gitb*: contains the implementation of the GITB Validation Service
(*CiseValidationService.java*) that is invoked whenever a Test Step is
executed from the UI. This class is linked to the endpoint
ciseValidationService (ref. Table 1. Exposed Endpoints). Under this
package we have the *steps* sub-package that contains the
implementations of the rules of CISE Conformance
Tests. We have also other sub-packages for the implementation of Meta
Test Steps.
** *transport*: contains implementations of GITB Message Services:
*** *IncomingMessagesServiceImpl.java* is linked to the CISEMessageService
endpoint.
*** *OutgoingMessagesServiceImpl.java* is linked to the messagingService
endpoint.

** *domain*: contains implementation of classes needed for the
communication with the CISE Network.
** *search*: contains classes needed to navigate through the payload of
CISE messages.
** *util*: contains various utility classes.

The class structure implementation of the rules makes use of
object-oriented inheritance and abstract classes. In package
*gitb.steps* the main abstract class to implement a test step is
*CiseTestStep*. We have other abstract classes that inherit from
*CiseTestStep* but are specialised in specific type of rules: For
instance, the class *CiseTestStepLinkedEvent* is specialised for the
rules that need to deal with Linked Events entities. The steps are
identified by means of Java Reflection using the *CiseTestStepsEnum*
enumeration which contains the association between the classes
implementing the rules and the string identifier used in the Test Cases
definition files.

==== Test Suites Definition

CISE CTT Test suites are defined with the language of GITB TDL (Test
Description Language (https://www.itb.ec.europa.eu/docs/tdl/latest/).
The structure of a test suite is hierarchical, i.e., each test suite
contains one or more test cases, and each test case contains one or more
test steps.

.CTT Test Suite structure
image::media/InternalTestSuiteStructure.png[CTT Test Suite structure]


As stated before, most of the test steps implemented in the project
contain one single rule, and for simplicity the related test case
contains a single test step. It is worth mentioning that Test Suites and
Test Cases are defined in TDL language that is an XML format, and a Test
Suite can be uploaded to a running instance of the CTT without the need
to rebuild or redeploy the software. This means that new Test Suites can
be created using the Test Steps already available in the CTT.

The structure of the Test Suites in the project is shown in the above
figure. At the main level we have the test suite definition, the
directory containing the *testcases* and a directory named *sutexamples*
(examples of messages specific for the _system under test_). The latter
is actually not needed in the test suite definition but provided for the
ease of the CTT administrator to understand and create new test cases
out of the provided examples.

In a Test Suite definition, we must declare the Actors involved in the
tests and the list of test cases that are provided in the test cases
directory. Then, these files are packaged together in a compressed file
and when the Test Suite is uploaded in the CTT by the CTT administrator
it is made available to the test user based on the conformance statement
in execution.

=== Building and Testing
The CTT Project is build performing two steps:

. Maven build
. Tests Suites packaging

Whereas the testing of the tool is performed by the unit tests present
in the project and by manual execution of system configuration, test
suites uploading, and Test Case execution.

==== Maven Build
To build the project it is enough to issue the following Maven command
from the main directory of the project:

 #> mvn clean install

Upon a successful build in the *target* directory the following files
are produced:

* *cise-ctt-bin-3.2.tar.gz*: packaging of the *required scripts and
data* directory produced by the Maven assembly plug-in.
* *cise-ctt-extensions.jar*: Java executable of *cise-ctt-extensions*
module.

Also, in this phase the project unit tests are executed.

==== Tests Suites packaging
To package all Test Suites created for the CTT we use *Gradle* build
tool. To issue the packaging execute the following command from the main
directory of the project:

 #> ./gradlew runAllPackage

This produces a new *bin* directory in the *target* directory where all
the Test Suites present in *src/main/resources/test-suites* directory
are compressed in *zip* files and ready to be uploaded in the CTT user
interface as described in section Upload of a Test Suite.

== Installation
The following files are required to install the CTT:

* `cise-ctt-bin-*.tar.gz` (CTT Software including Test Steps and initial configuration)
* Signature key and properties files provided by the node installation renamed this way:
** `keystore.jks`
** `signature.properties`

=== Server Requirements
The hosting system for the CTT (and optionally the CISE-simulator) the requirements are:

* A dual-core CPU (&gt;1.8 GHz) Linux machine with 8 GB of RAM, and 60 GB disk
* Installed software and library:
** Docker v.1.14 <or superior>
** Docker-compose v.1.25 <or superior>
* Free TCP ports:
** 9000 - for CTT web interface
** 7171 - for SOAP service exposed to the CISE node

=== System Preparation
In order to run the CTT tool, docker and docker-compose need to be installed (we provide hints to install the software on Ubuntu OS):

To install docker:

 #> sudo apt install docker

To install docker-compose:

 #> sudo apt install docker-compose

If you face an issue with docker-compose during the execution of the `build_ctt.sh`, please use the following commands:

 #> sudo curl -L 'https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)' -o /usr/local/bin/docker-compose
 #> chmod +x /usr/local/bin/docker-compose

=== Installation execution
After having extracted the `cise-ctt-bin-*.tar.gz` file in a directory, replace the signature files (JKS and properties files)
inside the `data/config/` directory: these two files must be renamed as written above because the installation
script looks exactly for those filenames.

NOTE: The user that executes the following script must belong to the docker group (test that the user can execute the docker ps command)

Execute the following script:

 #> ./build_ctt.sh

=== First Access
CTT Web interface is accessible through http://localhost:9000.
Default user are:

* Administrator: admin@ctt.cx (with password `Pa$$word1!`)
* Test Manager: manager@ctt.cx (with password `Pa$$word1!`)

NOTE: Remember to change the password at first login.
